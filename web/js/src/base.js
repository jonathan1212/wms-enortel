$(document).ready(function(){
    $('body').on('click','.table-dropdown button',function(e){
        e.stopPropagation();
        if($(this).next().is(":hidden")){
            $('.table-dropdown ul').fadeOut(200);
            $(this).next().fadeIn(200);
        }
        else{
            $('.table-dropdown ul').fadeOut(200);
        }
    });

    $('.filter').on('click',function(e){
        e.preventDefault();

        var formheight = $('.filter-main form').height();
        $('.filter-main form').css({
            'height':formheight,
            'position':'absolute',
        });

        $('main, body').css({
            height:'100%',
            overflow:'hidden'
        });
        $('.shader').fadeIn(300);
        $('.filter-slide').width(450).animate({
            right:'-20px'
        },175).animate({
                right:'-50px'
            },120).animate({
                    right:'-40px'
                },75);
    });

    $('.shader,.filter-cancel,.filter-close').on('click',function(e){
        e.preventDefault();        
        $('.filter-slide').animate({
            right:'-20px'
        },300).animate({
                right:'-450px'
            },200,function(){
                    $('main, body').css({
                        height:'100%',
                        'overflowX':'hidden',
                        'overflowY':'scroll'
                    });         
                });
        $('.shader').fadeOut(600);
    });

    $('.account > a').on('click',function(e){
        e.preventDefault();
        e.stopPropagation();
        if($(this).next().is(":hidden")){
            $(this).next().fadeIn(200);
        }
        else{
            $(this).next().fadeOut(200);
        }
    });

    $('body').on('click',function(){
        $('.table-dropdown ul').fadeOut(200);
        $('.account ul').fadeOut(200);
    });

    $('.datepicker').pikaday({
            firstDay: 1,
            format: 'YYYY-MM-DD',
        });

});

function addLoader()
{
    // improve this one
    var loader = '<div class="loader"><img src="https://lmbmlogistics.com/images/loading.gif"></div>';

    $('section.heading').append(loader);
}

function removeLoader()
{
    $('div.loader').remove();
}