$(function(){
    $('#submit-button').on('click', function(e){
        e.preventDefault();
        var details = {};
        $("tbody[id^='prd-']").each(function(index, elem){
            var parent = $(elem);
            var sku = $(elem).find('.sku');
            details[sku.text()] = {};
            parent.find('.detail').each(function(index, elem){
                var elem = $(elem);

                // special checkbox case
                if(elem.attr('type') === 'checkbox'){
                    details[sku.text()][elem.data('idx')] = elem.is(":checked");
                }
                else{
                    details[sku.text()][elem.data('idx')] = elem.text().trim() || elem.val().trim();
                }
            });
        });

        if(isSubmissionInvalid(details)){
            alert('Invalid Received Quantity. Please check your inputs.');
            return;
        }
        var submissionUrl = $('#submission-url').val();
        $('#submit-button').text('Submitting...').attr('disabled', true);
        $.post(submissionUrl, $.param(details), function(res){
            if(res.isSuccessful){
                window.location.href = $('#inbounding-print-url').val();
                return;
            }
            alert(res.errorMessage);
            $('#submit-button').text('Submit').attr('disabled', false);
        });
    });

    function isSubmissionInvalid(details){
        var productSkus = Object.keys(details);
        return productSkus.some(function(sku){
            var recQty = Math.floor(details[sku]['receivedQuantity']);
            var targetQty = details[sku]['expectedQuantity'] - details[sku]['partialQuantity'];
            if(!$.isNumeric(recQty) || recQty < 0 || recQty > targetQty){
                return true;
            }
            return false;
        });
    }
});
