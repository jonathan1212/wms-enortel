$(function(){
    // globals
    var check = 'font-blue fa fa-check';
    var cross = 'fa-times';

    /**
     * Handles reprinting
     */
    $('#reprint-pod').on('click', function(event){
        event.preventDefault();
        $('#reprint-form').submit();
    });

    $('#checkAll').on('click',function(event){
        event.preventDefault();

        $('.tagsName').each(function(el){

            $(this).prop('checked', true);
            
            var params = {
                tag: $(this).val().trim(),
                picklistNumber: $('#picklist-number').val()
            }

            console.log(params);

            $.post($('#pack-inventory-path').val(), $.param(params), function(result){
                $('#pack-notif').hide();
                $('#inventory-scan').prop('disabled', false);
                if(result.isSuccessful){
                    $('#'+result.tag).attr('class', check);
                    return;
                }

                alert(result.errorMessage);
                return;
            });
        });

    });

    /**
     * Handles action on tag input
     */
    $('#inventory-scan').on('keypress', function(e){
        if(e.which != 13){
            return;
        }

        e.preventDefault();
        var input = $(this);
        var tag = input.val().trim();
        if(!tag){
            return;
        }
        
        var params = {
            tag: tag,
            picklistNumber: $('#picklist-number').val()
        }
        $('#pack-notif').show();
        input.prop('disabled', true);
        $.post($('#pack-inventory-path').val(), $.param(params), function(result){
            $('#pack-notif').hide();
            $('#inventory-scan').prop('disabled', false);
            input.val('');
            input.focus();
            if(result.isSuccessful){
                $('#'+result.tag).attr('class', check);
                return;   
            }

            alert(result.errorMessage);
        });
    });

    /**
     * Handles for dispatch submission
     */
    $('#submit-for-dispatch').on('click', function(){
        event.preventDefault();
        var notYetPicked = $('.'+cross);
        if(notYetPicked.length === 0){
            $('#dispatch-form').submit();
            return;
        }
        $('.LMBM-WMS').css({
            height:'100%',
            overflow:'hidden'
        });
        var root = $('#incomplete-picklist-ul');
        notYetPicked.each(function(){
            root.append(
                $('<li>').attr('class', 'clearfix').append(
                    $('<span>').attr('class', 'float-left').text($(this).attr('id'))
                ).append(
                    $('<span>').attr('class', 'float-right').text($(this).parent().attr('data-desc'))
                )
            );
        });
        $('.modal').fadeIn(300);
    });

    /**
     * Handles OK button at modal
     */
    $('#modal-ok-button').on('click', function(event){
        event.preventDefault();
        $('.LMBM-WMS').css({
            height:'auto',
            'overflowY':'scroll'
        });
        $('#incomplete-picklist-ul').empty();
        $('.modal').fadeOut(300);
    });
});
