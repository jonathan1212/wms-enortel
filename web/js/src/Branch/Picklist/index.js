$(function(){
    var modal = $('.modal');
    if(modal.length > 0){
        modal.show();
    }

    $('#modal-ok-button').on('click', function(e){
        e.preventDefault();
        modal.hide();
    });

    $('#submit-print').on('click', function(e){
        e.preventDefault();
        $('#picklist-print-form').submit();
    });
});
