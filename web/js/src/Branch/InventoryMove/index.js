$(function(){
    // Listeners
    $('.target-input').on('keypress', function(e){
        if(e.which != 13){
            return;
        }

        e.preventDefault();
        var input = $(this);
        var target = input.val().trim();
        if(!target){
            $('#single-item-details').hide();
            $('#multiple-item-details').hide();
            $('#storage-location-entry').hide();
            $('#mobile-location-entry').hide();
            return;
        }

        $('.searching').show();
        $.post($('#search-url').val(), $.param({q: target}), function(result){
            $('.searching').hide();
            $('#submit-button').attr('disabled', false);
            $('.storage-location-input').val('');
            $('.mobile-location-input').val('');
            if(result.isSuccessful){
                buildDisplay(result);
                return;
            }
            input.val('');
            alert(target + ' is either an empty container or a nonexistent tag');
            return;
        });
    });

    function buildDisplay(result){
        if(result.type == 'multiple'){
            $('#single-item-details').hide();
            buildMultipleDisplay(result.data);
            return;
        }
        $('#multiple-item-details').hide();
        buildSingleDisplay(result.data);
    }

    function buildSingleDisplay(data)
    {
        data = data[0];
        var anchor = $('#single-details');
        anchor.empty();
        anchor.append(
            $('<li>').text(data.product.productDescription).prepend($('<h3>').text('PRODUCT'))
        ).append(
            $('<li>').text(data.product.sku).prepend($('<h3>').text('SKU'))
        ).append(
            $('<li>').text(data.product.clientSku).prepend($('<h3>').text('CLIENT SKU'))
        ).append(
            $('<li>').text(data.product.supplierSku).prepend($('<h3>').text('SUPPLIER SKU'))
        );

        var mobileLocation = data.mobileLocation ? 
            $('<span>').text(data.mobileLocation['name']).attr('class', 'mobile-location') : null;

        anchor.append(
            $('<li>').append($('<h3>').text('CURRENT LOCATION')).append(
                $('<span>').text(data.storageLocation.name).attr('class', 'storage-location')
            ).append(mobileLocation)
        );
        $('#single-item-details').show();
        $('#storage-location-entry').show();
        $('#mobile-location-entry').show();
    }

    function buildMultipleDisplay(data)
    {
        var anchor = $('#multiple-inventory-table');
        anchor.find("tbody:gt(0)").remove();
        data.forEach(function(inventory){
            anchor.append(
                $('<tbody>').append(
                    $('<tr>').append(
                        $('<td>').attr('class', 'space').attr('colspan', 5)
                    )
                ).append(
                    $('<tr>').append(
                        $('<td>').text(inventory.product.sku)
                    ).append(
                        $('<td>').text(inventory.product.clientSku)
                    ).append(
                        $('<td>').text(inventory.product.supplierSku)
                    ).append(
                        $('<td>').text(inventory.tag)
                    ).append(
                        $('<td>').text(inventory.storageLocation.name)
                    )
                )
            );
        });
        $('#multiple-item-details').show();
        $('#storage-location-entry').show();
        $('#mobile-location-entry').hide();
    }

    // Enable live reloading on form error
    if($('.target-input').val().trim()){
        var e = jQuery.Event("keypress");
        e.which = 13;
        $(".target-input").trigger(e);
    }
});
