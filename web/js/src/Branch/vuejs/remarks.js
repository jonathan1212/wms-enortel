new Vue({
    delimiters: ['<%=', '%>'],
  el: '#app',
  data: function() {
     return {
        billingId: 0
     }
  },
  methods: {
    
    showModal: function(billingId) {
        this.billingId = billingId;
        $('#ex1').modal();
        console.log(this.billingId);
    }, 


    save:function(el) {
      addLoader();
      event.stopPropagation();
      event.preventDefault();

      var form = new FormData();
      form.append('id',this.billingId);
      form.append('remarks',$('#remarks').val());

      

      this.$http.post('/billing/pay', form)
        .then(response => {
            return response.json();
        }, error => {
            

        })
        .then(result => {

          if (result.isSuccessful) {
              $('input[name=remarks]').val('');
              $('.isPaid_'+this.billingId).text('TRUE');
          }
          
          
          removeLoader();
      });

      $('.jquery-modal').hide();
      $('#ex1').hide();
      
    }
  }

});  

//$('.class').modal();