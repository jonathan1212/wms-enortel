new Vue({
    delimiters: ['<%=', '%>'],
  el: '#app',
  //render: h => h(App),
  data: function() {
     return {
        storageLocation: '',
        mobileLocation: '',
        data: [],
        listSave: [],
        clientId: 0,
        results: [],
        correct: 0,
        wrong: 0,
        startTime: '',
        endTime: '',
        diffInMin: '',
        diffInSec: '',
        close: false
     }
  },
  methods: {
    scanTag: function(event) {
      if (this.startTime == '') {
        var today = new Date();
        this.startTime = today.toISOString().substring(0, 10) + ' ' + today.getHours() +':' + today.getMinutes() + ':' + today.getSeconds() ;
      }

      var storageloc = $('#storageloc').find(":selected").text();
      var mobileloc = $('#mobileloc').find(":selected").text();

      var data = {
        tag: event.target.value,
        mobileloc: mobileloc,
        storageloc: storageloc,        
        storagelocId: $('#storageloc').val(),
        mobilelocId: $('#mobileloc').val()
      };

      
      this.storageLocation = $('#storageloc').val();
      this.mobileLocation = $('#mobileloc').val();
      this.clientId = $('#clientId').val();

      this.data.push(data);

      event.target.value = '';

    },

    hiddednSave: function(event){

      event.stopPropagation();
      event.preventDefault();
    },
    saveCycle: function(event) {
      addLoader();
      var today = new Date();
      this.endTime = today.toISOString().substring(0, 10) + ' ' + today.getHours() +':' + today.getMinutes() + ':' + today.getSeconds() ;

      event.stopPropagation();
      event.preventDefault();
      
      var form = new FormData();
      form.append('storage',this.storageLocation);
      form.append('mobile',this.mobileLocation);
      form.append('clientId',this.clientId);

      form.append('params',JSON.stringify(this.data));
      form.append('startDate',this.startTime);
      form.append('endDate',this.endTime);
    
      this.$http.post('/cycle-count/save', form)
        .then(response => {
            return response.json();
        }, error => {
            console.log(error);
        })
        .then(result => {

            this.results = result.data;
            this.listSave = result.list;
            this.correct = result.cyclecountOk;
            this.wrong  = result.cyclecountNot;
            this.diffInMin = result.diffInMin;
            this.diffInSec = result.diffInSec;

            if (!result.isSuccessful) {
              self.errormessage = result.message;
            } else {
              self.errormessage = '';

            }

            removeLoader();
      });

    },

    viewDetails: function(el) {
      addLoader();
      event.stopPropagation();
      event.preventDefault();
      this.close = true;

      var path = $(el.target).data('link');

      this.$http.get(path)
        .then(response => {
            return response.json();
        }, error => {
            console.log(error);
        })
        .then(result => {

            this.results = JSON.parse(result.data);
            this.list = JSON.parse(result.list);
            this.correct = result.cyclecountOk;
            this.wrong  = result.cyclecountNot;
            this.diffInMin = result.diffInMin;
            this.diffInSec = result.diffInSec;

            if (!result.isSuccessful) {
              self.errormessage = result.message;
            } else {
              self.errormessage = '';

            }

            removeLoader();
      });

    },

    resetCycle: function(event) {
        
        event.stopPropagation();
        event.preventDefault();

        this.storageLocation = '';
        this.mobileLocation = '';
        this.data = [];
        this.results = [];
        this.correct = 0;
        this.wrong = 0;
        this.startTime = '';
        this.endTime = '';
        this.diffInMin = '';
        this.diffInSec = '';
    },

    closeCycle: function(event){
      this.close = false;
    }

  },
    created(){
      
    },
    mounted: function () {
      console.log('sdf');
    }

});  