$('body').on('click',function(e){
    if(!$(e.target).is('span')){
      $('.statusfilter ul').slideUp(200);
    }
});

new Vue({
    delimiters: ['<%=', '%>'],
  el: '#app',
  data: function() {
     return {
        status: 0,
        picklistId: 0,
        statusName: '',
        statusId: 0,
        data: {}
     }
  },
  methods: {
    showStatus: function(el) {
      $('.statusfilter ul').hide();
      $(el.target).parent('.statusfilter').find('ul').show(500);
    },

    showform(el,statusId) {

      el.preventDefault();
      this.statusId = statusId;
      $('#ex2').modal();

    },

    changeStatus(el) {    
      el.preventDefault();


      var picklistIds = $('#picklist-print-form').serializeArray();

      if (picklistIds.length == 0) {
        alert ('select picklist');
        return false;
      }      

      addLoader();

      var formData = new FormData($('form')[3]);
      formData.append('statusId',this.statusId);

      for (var x in picklistIds) {        
        formData.append(picklistIds[x].name, picklistIds[x].value);
      }

      this.$http.post('/status/update-multiple', formData)
        .then(response => {
            return response.json();
        }, error => {
        })
        .then(result => {

          window.location.reload(true);
      });
    },

  

    processStatus: function(el,status) {
      
      el.preventDefault();

      var picklist = $(el.target).parents('.status-collection').data('picklist');
      this.status = status;
      this.picklistId = picklist;
      if (status == 1) {
        this.statusName = 'INTRANSIT';
      } else if (status == 2) {
        this.statusName = 'DELIVERED';
      } else if (status == 3) {
        this.statusName = 'RELEASED';
      }

      addLoader();

      var params = {
        picklistId: picklist
      };

      this.$http.get('/status/status',{params: params})
        .then(response => {
            return response.json();
        }, error => {
            console.log(error);
        })
        .then(result => {

            this.data = {
              waybillNumber : result.data.waybill,
              deliveredTo: result.data.deliveredTo,
              remarks: result.data.remarks
            };

            removeLoader();
      });

      $('#ex1').modal();
    },



    save:function(el) {
      addLoader();
      el.stopPropagation();
      el.preventDefault();

      var formData = new FormData($('form')[3]);
      formData.append('picklistId', this.picklistId);
      formData.append('status',this.status);

      this.$http.post('/status/update', formData)
        .then(response => {
            return response.json();
        }, error => {
            

        })
        .then(result => {

          $('input[name=waybill]').val('');
          $('input[name=pickupBy]').val('');
          $('input[name=remarks]').val('');
          $('input[name=attachment]').val('');

          $('.id_'+ this.picklistId).text(result['data'].statusname);

          removeLoader();
      });

      $('.jquery-modal').hide();
      $('#ex1').hide();
      $('.statusfilter ul').slideUp(200);
    }
  }

});  

//$('.class').modal();