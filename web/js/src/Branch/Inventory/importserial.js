new Vue({
    delimiters: ['<%=', '%>'],
  el: '#app',
  
  data: function() {
     return {
        show: false,
        results: []
     }
  },
  methods: {

    showImport: function() {
      this.show = true;
    },

    importFile: function(el) {
      console.log('hi');
        var self = this;
        var file = el.target.files[0];
        var params = { file: file };

        var form = new FormData();
        form.append('file', file);
      
        this.$http.post('/inventory/import-serial', form)
          .then(response => {
              return response.json();
          }, error => {
              console.log(error);
          })
          .then(result => {

              if (result.isSuccessful) {
                    alert ('Successfully imported, Refresh page to reflect');

                    if (result.data.length > 0) {
                        this.results = result.data;
                    } else {
                        this.results = [];
                    }

              } else {
                    alert ('Invalid File');                                    
              }
        });

        el.target.value = '';

    },

  },
    created(){
      
    },
    mounted: function () {
    }

});  

