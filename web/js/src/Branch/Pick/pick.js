$(function(){
    // globals
    var check = 'font-blue fa fa-check';
    var cross = 'fa-times';

    /**
     * Handles action on tag input
     */
    $('#inventory-scan').on('keypress', function(e){
        if(e.which != 13){
            return;
        }

        e.preventDefault();
        var input = $(this);
        var tag = input.val().trim();
        if(!tag){
            return;
        }
        
        var params = {
            tag: tag,
            picklistNumber: $('#picklist-number').val()
        }

        $('#pick-notif').show();
        input.prop('disabled', true);
        $.post($('#pick-inventory-path').val(), $.param(params), function(result){
            $('#pick-notif').hide();
            $('#inventory-scan').prop('disabled', false);
            input.val('');
            input.focus();
            if(result.isSuccessful){
                $('#'+result.tag).attr('class', check);
                return;   
            }

            alert(result.errorMessage);
        });
    });

    /**
     * Handles Ready for Packing button on page
     */
    $('#submit-for-packing').on('click', function(event){
        event.preventDefault();
        var notYetPicked = $('.'+cross);
        if(notYetPicked.length === 0){
            submitForPacking();
            return;
        }
        $('.LMBM-WMS').css({
            height:'100%',
            overflow:'hidden'
        });
        var root = $('#incomplete-picklist-ul');
        notYetPicked.each(function(){
            root.append(
                $('<li>').attr('class', 'clearfix').append(
                    $('<span>').attr('class', 'float-left').text($(this).attr('id'))
                ).append(
                    $('<span>').attr('class', 'float-right').text($(this).parent().attr('data-desc'))
                )
            );
        });
        $('.modal').fadeIn(300);
    });

    /**
     * Handles Cancel button at modal
     */
    $('#modal-cancel-button').on('click', function(event){
        event.preventDefault();
        $('.LMBM-WMS').css({
            height:'auto',
            'overflowY':'scroll'
        });
        $('#incomplete-picklist-ul').empty();
        $('.modal').fadeOut(300);
    });

    /**
     * Handles continue button at modal
     */
    $('#modal-continue-button').on('click', function(event){
        submitForPacking();
    });

    /**
     * Submit picklist for packing
     */
    function submitForPacking(){
        $.post(
            $('#for-packing-path').val(), 
            $.param({picklistNumber: $('#picklist-number').val()}), 
            function(result){
                if(result.isSuccessful){
                    window.location = $('#pick-index-path').val();
                    return;
                }
                alert(result.errorMessage);
            }
        );
    }
});
