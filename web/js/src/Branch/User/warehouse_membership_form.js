$(function(){
    // global variables
    var collectionHolder;
    var newLinkLi = $('<li></li>');

    // initial stuffs
    (function(){
        collectionHolder = $('ul.membership-v2');
        collectionHolder.data('index', collectionHolder.find(':input').length);
        collectionHolder.find('li').each(function() {
            addMembershipFormDeleteLink($(this));
        });
    })();

    // event listeners
    $('a.add_membership_link').on("click", function(e){
        e.preventDefault();
        addMembershipForm(collectionHolder, newLinkLi);
    });

    // helper functions
    function addMembershipForm(collectionHolder, newLinkLi){
        var prototype = collectionHolder.data('prototype');
        
        var index = collectionHolder.data('index');
        
        var newForm = prototype.replace(/__name__/g, index);
        
        collectionHolder.data('index', index + 1);
        
        var newFormLi = $('<li></li>').append(newForm);
        addMembershipFormDeleteLink(newFormLi);
        collectionHolder.append(newFormLi);
    }

    function addMembershipFormDeleteLink(tagFormLi)
    {
        var removeFormA = $('<a href="#" class="delete-membership"><i class="fa fa-times" aria-hidden="true"></i></a>');
        tagFormLi.append(removeFormA);

        removeFormA.on('click', function(e) {
            // prevent the link from creating a "#" on the URL
            e.preventDefault();

            // remove the li for the tag form
            tagFormLi.remove();
        });
    }
});
