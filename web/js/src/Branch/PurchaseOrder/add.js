$(function(){
    // global vars
    var index = 0;

    // setups
    (function(){
        $('.product-search').select2({
            ajax: {
                url: $('#product-search-url').val(),
                dataType: 'json',
                delay: 250,
                method: 'POST',
                data: function (params){
                    return {
                        query: params.term,
                        clientId: $('.client-select').val(),
                        supplierId: $('#purchase_order_supplier').val()
                    }
                },
                processResults: function(data, params){
                    return {
                        results: data.items
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 3,
        });

        index = $('#product-table tr').length;
    })();

    /**
     * Listeners
     */
    // dynamically adds product to table
    $('#add-product').on('click', function(e){
        e.preventDefault();
        var selectedProduct = $(".product-search").select2('data');
        if(selectedProduct.length <= 0){
            alert('Please select a product');
            return;
        }

        selectedProduct = selectedProduct[0];
        var quantity = Math.floor($('#product-quantity').val());
        if(!$.isNumeric(quantity) || quantity <= 0){
            alert('Invalid quantity');
            return;
        }

        if($('#row-'+selectedProduct.id).length > 0){
            alert('Product is on the list already');
            return;
        }

        var productDetails = selectedProduct.text.split('<>');
        var productDescription = productDetails[0];
        var costOfGoodsPurchased = productDetails[productDetails.length-1].trim();
        var proto = $($('#product-proto')
            .data('prototype')
            .replace(/__name__/g, ++index)
            .replace(/__id__/g, selectedProduct.id)
            .replace(/__desc__/g, productDescription)
            .replace(/__qty__/g, quantity)
            .replace(/__cogp__/g, costOfGoodsPurchased));

        proto.find('option').attr('value', selectedProduct.id);
        proto.find('td.qty > input').attr('value', quantity);
        proto.find('td.cogp > input').attr('value', costOfGoodsPurchased);

        $('#product-table').append(proto);
    });

    // dynamically removes product on table
    $('.form-table').on('click', '.product-remove', function(e){
        e.preventDefault();
        $('#'+$(this).data('row')).remove();
    });

    // remove all products on supplier change
    $('.supplier-select').on('change', function(e){
        $('.product-search').val('').trigger('change');
        $('#product-quantity').text('0').val('0');   
        $(".form-table").find("tr:gt(0)").remove();
    });
});
