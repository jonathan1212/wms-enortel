<?php

namespace BranchBundle\Security\User;

use Symfony\Component\HttpFoundation\RequestStack;
use CoreBundle\Repository\WarehouseMemberRepository;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class WarehouseUserProvider implements UserProviderInterface
{
    private $warehouseMemberRepository;
    private $requestStack;

    public function __construct(WarehouseMemberRepository $warehouseMemberRepository, RequestStack $requestStack)
    {
        $this->warehouseMemberRepository = $warehouseMemberRepository;
        $this->requestStack = $requestStack;
    }

    public function loadUserByUsername($emailAddress, $warehouse = null)
    {
        if($warehouse === null){
            $warehouse = $this->requestStack
                ->getCurrentRequest()
                ->get('_warehouse');
        }

        $warehouseUser = $this->warehouseMemberRepository
            ->createQueryBuilder('wm')
            ->leftJoin('wm.user', 'u')
            ->leftJoin('wm.warehouse', 'w')
            ->where('u.emailAddress = :emailAddress')
            ->andWhere('w = :warehouse')
            ->setParameter('emailAddress', $emailAddress)
            ->setParameter('warehouse', $warehouse)
            ->getQuery()
            ->getOneOrNullResult();

        if ($warehouseUser) {
            $user = $warehouseUser->getUser();
            $user->setWarehouse($warehouseUser->getWarehouse())
                ->setWarehouseId($warehouseUser->getWarehouse()->getId())
                ->setRoles($warehouseUser->getRoles());

            return $user;
        }

        throw new UsernameNotFoundException(
            sprintf('User with email "%s" does not exist.', $emailAddress)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof \CoreBundle\Entity\User || $user->getWarehouseId() === 0) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername(), $user->getWarehouseId());
    }

    public function supportsClass($class)
    {
        return $class === 'CoreBundle\Entity\User';
    }
}
