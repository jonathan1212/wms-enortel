<?php

namespace BranchBundle\Security\RequestMatcher;

use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class BranchFirewallMatcher implements RequestMatcherInterface
{
    private $branchHost;

    public function __construct($branchHost)
    {
        $this->branchHost = $branchHost;
    }

    public function matches(Request $request)
    {
        return $request->getHost() === $this->branchHost;
    }
}
