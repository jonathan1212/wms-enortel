<?php

namespace BranchBundle\Service\Product;

class SkuGenerator
{
    /**
     * @link http://stackoverflow.com/questions/4356289/php-random-string-generator
     * @param  integer $length
     * @return string
     */
    public function generate($length = 5)
    {
        return 'PD-'.strtoupper(substr(str_shuffle(MD5(microtime())), 0, $length));
    }
}
