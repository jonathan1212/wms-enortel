<?php

namespace BranchBundle\Service\PurchaseOrder;

class PurchaseOrderNumberGenerator
{
    /**
     * @link http://stackoverflow.com/questions/4356289/php-random-string-generator
     * @param  integer $length
     * @return string
     */
    public function generate($length = 10)
    {
        return 'PO-'.strtoupper(substr(str_shuffle(MD5(microtime())), 0, $length));
    }
}
