<?php

namespace BranchBundle\Service\Picklist;

class InvoiceNumberGenerator
{
    /**
     * @link http://stackoverflow.com/questions/4356289/php-random-string-generator
     * @param  integer $length
     * @return string
     */
    public function generate($length = 15)
    {
        return 'INV-'.strtoupper(substr(str_shuffle(MD5(microtime())), 0, $length));
    }
}
