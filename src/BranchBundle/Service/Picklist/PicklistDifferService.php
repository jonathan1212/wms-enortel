<?php

namespace BranchBundle\Service\Picklist;

use CoreBundle\Entity\Picklist;
use CoreBundle\Repository\ProductRepository;
use CoreBundle\Repository\PicklistRepository;
use CoreBundle\Repository\PicklistDetailRepository;

class PicklistDifferService
{
    private $picklistRepository;
    private $picklistDetailRepository;
    private $productRepository;

    public function __construct(
        PicklistRepository $picklistRepository, 
        PicklistDetailRepository $picklistDetailRepository,
        ProductRepository $productRepository
    )
    {
        $this->picklistRepository = $picklistRepository;
        $this->picklistDetailRepository = $picklistDetailRepository;
        $this->productRepository = $productRepository;
    }

    public function getDiff(Picklist $picklist)
    {
        $currentPicklistStatus = $picklist->getOrderDetails()['products'];
        $currentPicklistInventoryCount = $this->picklistDetailRepository
            ->getCurrentInventoryCountOfPicklistAsArray($picklist);
        
        $productDescriptions = $this->productRepository->getProductDescriptionsByClientSkuAsArray(
            array_keys($currentPicklistStatus)
        );

        foreach($currentPicklistInventoryCount as $clientSku => $allocatedQuantity){
            $currentPicklistStatus[$clientSku] -= $allocatedQuantity;
        }

        $diff = [];
        foreach($currentPicklistStatus as $clientSku => $requiredQuantity){
            $diff[$clientSku] = [
                'requiredQuantity' => $requiredQuantity,
                'description' => $productDescriptions[$clientSku]
            ];
        }

        return $diff;
    }
}
