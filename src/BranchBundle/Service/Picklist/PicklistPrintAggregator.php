<?php

namespace BranchBundle\Service\Picklist;

use CoreBundle\Repository\PicklistParticularRepository;

class PicklistPrintAggregator
{
    private $picklistParticularRepository;

    public function __construct(PicklistParticularRepository $picklistParticularRepository)
    {
        $this->picklistParticularRepository = $picklistParticularRepository;
    }

    public function aggregatePicklists($picklistParticulars)
    {
        return $this->formatForPrinting(
            $this->picklistParticularRepository->getPicklistParticularsForPrinting($picklistParticulars)
        );
    }

    private function formatForPrinting($picklistParticulars)
    {
        $result = [];
        $reprintStatuses = \CoreBundle\Entity\PicklistStatus::getPicklistForReprint();
        foreach($picklistParticulars as $particular){
            $result[] = [
                'picklistNumber' => $particular['picklistParticularNumber'],
                'orderNumber' => $particular['picklistAggregate']['orderNumber'],
                'orderDetails' => $particular['picklistAggregate']['orderDetails'],
                'client' => $particular['picklistAggregate']['client'],
                'inventoryListing' => $particular['items'],
                'reprint' => in_array($particular['picklistStatus']['value'], $reprintStatuses)
            ];
        }

        return $result;
    }
}
