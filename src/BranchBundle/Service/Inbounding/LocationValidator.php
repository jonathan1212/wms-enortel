<?php

namespace BranchBundle\Service\Inbounding;

use CoreBundle\Repository\InventoryRepository;
use CoreBundle\Repository\MobileLocationRepository;
use CoreBundle\Repository\StorageLocationRepository;

class LocationValidator
{
    private $storageLocationRepository;
    private $mobileLocationRepository;
    private $inventoryRepository;

    public function __construct(
        StorageLocationRepository $storageLocationRepository, 
        MobileLocationRepository $mobileLocationRepository, 
        InventoryRepository $inventoryRepository)
    {
        $this->storageLocationRepository = $storageLocationRepository;
        $this->mobileLocationRepository = $mobileLocationRepository;
        $this->inventoryRepository = $inventoryRepository;
    }

    public function validate($inboundingDetails)
    {
        $returnData = [ 
            'isSuccessful' => true
        ];

        $storageLocations = $this->storageLocationRepository
            ->getAllStorageLocationNamesAsArray();

        $mobileLocations = $this->mobileLocationRepository
            ->getAllMobileLocationNamesAsArray();

        $occupiedMobileLocations = null; /*$this->inventoryRepository
            ->getOccupiedMobileLocations();*/

        foreach ($inboundingDetails as $productSku => $details) {
            if(!$details['receivedQuantity']){
                continue;
            }

            if(!$details['storageLocation']){
                $returnData['isSuccessful'] = false;
                $returnData['errorMessage'] = 'Storage Location is required';
                break;
            }

            if(!in_array($details['storageLocation'], $storageLocations)){
                $returnData['isSuccessful'] = false;
                $returnData['errorMessage'] = sprintf('%s is not a valid Storage Location', $details['storageLocation']);
                break;
            }

            if($details['mobileLocation']){
                if(!in_array($details['mobileLocation'], $mobileLocations)){
                    $returnData['isSuccessful'] = false;
                    $returnData['errorMessage'] = sprintf('%s is not a valid Mobile Location', $details['mobileLocation']);
                    break;
                }

                /*if(in_array($details['mobileLocation'], $occupiedMobileLocations)){
                    $returnData['isSuccessful'] = false;
                    $returnData['errorMessage'] = sprintf('%s currently has contents. Please empty it first.', $details['mobileLocation']);
                    break;
                }*/
            }
        }

        return $returnData;
    }
}
