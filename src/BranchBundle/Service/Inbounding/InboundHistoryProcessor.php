<?php

namespace BranchBundle\Service\Inbounding;

use CoreBundle\Entity\PurchaseOrder;
use CoreBundle\Repository\PurchaseOrderRepository;
use CoreBundle\Repository\InboundHistoryRepository;

class InboundHistoryProcessor
{
    private $inboundHistoryRepository;
    private $purchaseOrderRepository;

    public function __construct(
        InboundHistoryRepository $inboundHistoryRepository, 
        PurchaseOrderRepository $purchaseOrderRepository = null
    )
    {
        $this->inboundHistoryRepository = $inboundHistoryRepository;
        $this->purchaseOrderRepository = $purchaseOrderRepository;
    }

    public function computeCurrentPurchaseOrderInboundDetails(PurchaseOrder $purchaseOrder)
    {
        $inboundHistory = $this->inboundHistoryRepository
            ->retrievePurchaseOrderInboundHistoryAsArray($purchaseOrder);

        if(count($inboundHistory) <= 0){
            return [];
        }

        $productSkus = array_keys($inboundHistory[0]['details']);
        $inboundDetails = $this->formatBaseInboundHistory($inboundHistory[0], $productSkus);

        // Replay each history on top of the other to determine the current state
        foreach($inboundHistory as $history){
            foreach($productSkus as $sku){
                $inboundDetails[$sku]['partialQuantity'] += $history['details'][$sku]['receivedQuantity'];
            }
        }

        return $inboundDetails;
    }

    public function isPOalreadyInbounded(PurchaseOrder $purchaseOrder)
    {
        $co = $this->computeCurrentPurchaseOrderInboundDetails($purchaseOrder);
        
        $c = array_shift($co);
        
        if (isset($c['expectedQuantity']) && isset($c['partialQuantity'])) {

            if ($c['expectedQuantity'] == $c['partialQuantity']) {
                return 'greenflag';
            }
        }

        return 'redflag';
    }


    public function retrieveListOfCorrectCostOfGoodsPurchased(PurchaseOrder $purchaseOrder)
    {
        $purchaseOrderProducts = $this->purchaseOrderRepository
            ->getPurchaseOrderProductsAsArray($purchaseOrder)['products'];

        foreach($purchaseOrderProducts as $purchaseOrderProduct){
            $costOfGoodsPurchasedList[$purchaseOrderProduct['product']['sku']] = $purchaseOrderProduct['costOfGoodsPurchased'];
        }

        return $costOfGoodsPurchasedList;
    }

    private function formatBaseInboundHistory($initialHistory, $productSkus)
    {
        $baseHistory = $initialHistory['details'];
        return array_reduce($productSkus, function($result, $sku) use ($baseHistory) {
            $result[$sku] = [
                'supplierSku' => $baseHistory[$sku]['supplierSku'],
                'clientSku' => $baseHistory[$sku]['clientSku'],
                'expectedQuantity' => intval($baseHistory[$sku]['expectedQuantity']),
                'partialQuantity' => 0,
                'productDescription' => $baseHistory[$sku]['productDescription'],
                'costOfGoodsPurchased' => $baseHistory[$sku]['costOfGoodsPurchased']
            ];

            return $result;
        }, []);
    } 
}
