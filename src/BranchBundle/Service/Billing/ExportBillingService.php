<?php

namespace BranchBundle\Service\Billing;

use Symfony\Component\HttpFoundation\Response;
use CoreBundle\Entity\Billing;
use CoreBundle\Entity\BillingDetailInbound;
use CoreBundle\Entity\BillingDetailPick;
use CoreBundle\Entity\BillingDetailPack;
use CoreBundle\Entity\BillingDetailStorage;
use CoreBundle\Entity\BillingDetailInsurance;

class ExportBillingService
{
    private $container;
    
    private $argument; 

    private $phpExcel;

    public function __construct($container,$phpExcel)
    {
        $this->container = $container;
        $this->phpExcel = $phpExcel;
        $this->em = $container->get('doctrine')->getManager();
        $this->conn = $this->em->getConnection();
    }


    public function setParams($param, $value)
    {
        $this->argument[$param] = $value;

        return $this;
    }
    
    public function saveBilling($params)
    {
        $authenticatedUser = $params['authenticatedUser'];
        $warehouseId = $params['warehouseId'];
        $client = $params['client'];
        $params['clientId'] = $client->getId();        
        $billingtype = $this->getBillingType($params);
        $billingTable = Billing::getBillingDetailTables($billingtype);

        if (count($params['results']) == 0) {
            return ['isSuccessfull' => false, 'message' => 'No Record Found'];
        }

        try {
            // billing 
            $billingSQL = "INSERT INTO billing (date_added, date_to,modified_by,is_paid,billing_number, warehouse_id, client_id, type, remarks)
             values (:date_added,:date_to, :modified_by, :is_paid, :billing_number, :warehouse_id, :client_id, :type, :remarks) ";

            $insert = $this->conn->prepare($billingSQL);
            $insert->execute(
                ['date_added'   => $params['dateFrom'],
                 'date_to'      => $params['dateTo'],
                 'modified_by'  => $authenticatedUser,
                 'is_paid'      => 0,
                 'billing_number'   => 'BL-0-',
                 'warehouse_id' => $warehouseId,
                 'client_id'    => $client->getId(),
                 'type'         => $billingtype,
                 'remarks'      => ''
                 ]
            );

            $id = $this->conn->lastInsertId();

            $update = $this->conn->prepare("UPDATE billing set billing_number = :billing_number where id = :id");
            $update->execute(
                ['billing_number' => 'BL-0-' . $id, 'id' => $id]
            );

            //billing details
            

            $billingDetailSQL = "INSERT INTO {$billingTable} 
                (inventory_id, billing_id, date_process) VALUES
                ";

            $sql = "";

            foreach ($params['results'] as $inventory) {

                if (is_null($inventory->getProduct())) {
                    continue;
                }

                if (!in_array($billingtype, [Billing::BILLING_STORAGE, Billing::BILLING_INSURANCE])) {

                    $prepare = "SELECT id FROM {$billingTable} WHERE inventory_id = :inventoryId";
                    $stmt = $this->conn->prepare($prepare); 
                    $stmt->execute(['inventoryId' => $inventory->getId()]); 
                    $row = $stmt->fetch();

                    if ($row) {
                        continue;
                    }
                }

                if ($params['column'] == 'dateInbound') {             
                    $dateProcess = $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('Y-m-d H:i:s') : null;
                } else if ($params['column'] == 'datePick') {
                    $dateProcess = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('Y-m-d H:i:s') : null;
                } else if ($params['column'] == 'datePack') {
                    $dateProcess = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('Y-m-d H:i:s') : null;
                }

                if (in_array($billingtype, [Billing::BILLING_STORAGE, Billing::BILLING_INSURANCE])) {
                    $dateProcess = $params['dateFrom'];
                }
                
                $inventoryId = $inventory->getId();
                $sql .=  <<<EOF
( $inventoryId , $id, '$dateProcess' ) ,
EOF;
            }

            $bsql = $billingDetailSQL . $sql;

            $this->computePrice($params,$billingtype,$id);

            if ($sql == "") {
                $delete = $this->conn->prepare("DELETE FROM billing where id = {$id}");
                $delete->execute();
                return ['isSuccessfull' => false, 'message' => 'No Record Found. Already Billed.'];
            }

            $rtrimSql = rtrim($bsql,',');

            $insert = $this->conn->prepare($rtrimSql);
            $insert->execute();  


            return ['isSuccessfull' => true, 'message' => 'Successfully Saved', 'id' => $id];

        } catch (\Exception $e) {
            return ['isSuccessfull' => false, 'message' => $e->getMessage()];
        }        
    }


    public function insertBillingSummary($params,$authenticatedUser, $warehouseId,$client,$billingIds)
    {
        $sqlAmount =  ' select sum(amount) as total from billing where id IN ("' . implode('", "', $billingIds) . '") ';
        $fetchAmount = $this->conn->fetchAll($sqlAmount);
        $total = 0;
        if ($fetchAmount) {
            $total = $fetchAmount[0]['total'];
        }

         $billingSQL = "INSERT INTO billing_summary (amount, date_added, date_to,modified_by,is_paid,billing_number, warehouse_id, client_id, remarks)
             values (:amount, :date_added,:date_to, :modified_by, :is_paid, :billing_number, :warehouse_id, :client_id, :remarks) ";

            $insert = $this->conn->prepare($billingSQL);
            $insert->execute(
                [
                 'amount'       => $total,
                 'date_added'   => $params['dateFrom'],
                 'date_to'      => $params['dateTo'],
                 'modified_by'  => $authenticatedUser,
                 'is_paid'      => 0,
                 'billing_number'   => 'BL-0-'. date('md-s'),
                 'warehouse_id' => $warehouseId,
                 'client_id'    => $client->getId(),
                 'remarks'      => json_encode($billingIds)
                 ]
            );

        return $this->conn->lastInsertId();
    }

    public function getBillingType($params)
    {
        if ($params['template'] == 'insurance') {
            $type = Billing::BILLING_INSURANCE;
        }

        else if ($params['template'] == 'storage') {
            $type = Billing::BILLING_STORAGE;
        }
        
        else if ($params['template'] == 'all') {
            
        }
        else {
            if ($params['column'] == 'dateInbound') {
                $type = Billing::BILLING_INBOUND;
            } else if ($params['column'] == 'datePick') {
                $type = Billing::BILLING_PICKED;
            } else if ($params['column'] == 'datePack') {
                $type = Billing::BILLING_PACKED;
            }                    
        }


        return $type;

    }


    public function exportBilling($billing, $billingDetails)
    {
        ob_start();
        $fp = fopen('php://output', 'w');
        fputcsv($fp, array('BILLING FOR ',$billing->getType(true)));
        
        $params = ['results' => $billingDetails, 'billingType' => $billing->getType()];

        if ($billing->getType() == BILLING::BILLING_INBOUND) {

            $params['column'] = 'dateInbound';
            $params['price'] = 11.20;

            $fp = $this->pickpackinbound($params,$fp);
        }

        else if ($billing->getType() == BILLING::BILLING_PICKED) {

            $params['column'] = 'datePick';
            $params['price'] = 4.48;

            $fp = $this->pickpackinbound($params,$fp);
        }

        else if ($billing->getType() == BILLING::BILLING_PACKED) {

            $params['column'] = 'datePack';
            $params['price'] = 11.20;

            $fp = $this->pickpackinbound($params,$fp);
        }

        else if ($billing->getType() == BILLING::BILLING_INSURANCE) {
            $fp = $this->insurance($params,$fp);
        }

        else if ($billing->getType() == BILLING::BILLING_STORAGE) {
            $params['resultsStorage'] = $billingDetails;

            $datediff = $this->datediff($billing->getDateAdded()->format('Y-m-d') ,$billing->getDateTo()->format('Y-m-d'));
            $dayleft = (int)$datediff + 1;

            $params['dayleft'] = $dayleft;

            $fp = $this->locationStorage($params,$fp);
        }
        
        fclose($fp);
        $exportDate = $billing->getBillingNumber() . '.csv';

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', "attachment; filename={$exportDate}");
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
        
        return $response;
        ob_end_flush();

    }



    public function export($params)
    {
        ob_start();
        $fp = fopen('php://output', 'w');

        if ($params['template'] == 'print') {
            $column = $params['column'];
        } else {
            $column = $params['template'];
        }

        fputcsv($fp, array('BILLING FOR ',$column));

        if ($params['template'] == 'insurance') {
            $fp = $this->insurance($params,$fp);
        }
        else if ($params['template'] == 'storage') {
            $fp = $this->locationStorage($params,$fp);
        }
        else if ($params['template'] == 'all') {
            $fp = $this->all($params,$fp);   
        }
        else {
            $fp = $this->pickpackinbound($params,$fp);
        }

        fclose($fp);
        $exportDate = date('mdYHis') . '.csv';

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', "attachment; filename={$exportDate}");
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
        
        return $response;
        ob_end_flush();
    }

    protected function all($params,$fp)
    {
        fputcsv($fp, array('WAREHOUSE','TAGNO','SERIALNO', 'INVENTORY-STATUS', 'PICKLISTNO' , 'PONO', 'STORAGELOCATION','MOBILELOCATION',
            'PRODUCT DESCRIPTION', 'CLIENT SKU','ORDER NUMBER', 'COST Of GOODS',
            'INBOUNDED', 'INBOUNDED RATE', 'PICKED' , 'PICKED RATE', 'PACKED', 'PACKED RATE'));

        $inboundPrice = 0;
        $picked = 0;
        $packed = 0;
        $total = 0;

        foreach ($params['results'] as  $inventory) {
            $picklistNumber = '';
            $inboundPrice = (float)$inboundPrice + 11.20;
            
            if ($inventory->getDatePicked())
                $picked = (float)$picked + 4.48;
            
            if ($inventory->getDatePacked())
                $packed = ($packed) + 11.20;
    
            $cogp = $inventory->getProduct()->getCostOfGoodsPurchased();
            
            $total = $total + $cogp;
            $percentage = $total * 0.05;

            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklistNumber = $picklistDetail[0]->getPicklist()->getPicklistNumber();
            }

            fputcsv($fp, array( 
                $inventory->getWarehouse()->getName(),
                $inventory->getTag(),
                $inventory->getSerialNo(),
                $inventory->getInventoryStatus()->getName(),
                $picklistNumber,
                $inventory->getPurchaseOrder()->getPurchaseOrderNumber(),
                $inventory->getStorageLocation() ? $inventory->getStorageLocation()->getName() : '',
                $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '',
                $inventory->getProduct()->getProductDescription(),
                $inventory->getProduct()->getClientSku(),
                $inventory->getOrderNumber(),
                $cogp,
                $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '',
                $inventory->getDateInbounded() ? 11.20 : '',
                $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '',
                $inventory->getDatePicked() ? 4.48 : '',
                $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '',
                $inventory->getDatePacked() ? 11.20 : ''                
                )               
            );
        }

        fputcsv($fp, array('','', '','','', '','','', '','', 'TOTAL', $total,'',$inboundPrice, '',$picked,'',$packed));
        fputcsv($fp, array('','', '','','', '','','', '','', '5%', $percentage));

        fputcsv($fp, array(''));
        fputcsv($fp, array('************ STORAGE *********'));

        fputcsv($fp, array('STORAGE LOCATION','DAYS','PRICE','TOTAL'));

        $storageTotal = 0;
        $grandStorageTotal = 0;
        $dayleft = $params['dayleft'];

        $storage = [];
        foreach ($params['results'] as $inventory) {
            $location = $inventory->getStorageLocation()->getName();

            $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
            $storage[$location] = array_unique($storage[$location]);
        }

        $results = $storage;    

        foreach ($results as $key => $value) {
            $storageTotal = 11.20 * $dayleft;
            $grandStorageTotal = $grandStorageTotal + $storageTotal;

            fputcsv($fp, array($key,$dayleft,11.20,$storageTotal));
        }

        fputcsv($fp, array('','','GRAND TOTAL (PHP)',$grandStorageTotal));


        fputcsv($fp, array(''));
        fputcsv($fp, array(''));
        fputcsv($fp, array('******************'));
        fputcsv($fp, array('INBOUNDED',$inboundPrice));
        fputcsv($fp, array('PICKED' , $picked));
        fputcsv($fp, array('PACKED' , $packed));
        fputcsv($fp, array('INSURANCE', $percentage));
        fputcsv($fp, array('STORAGED',$grandStorageTotal));
        fputcsv($fp, array('TOTAL',$inboundPrice + $picked + $packed + $percentage + $grandStorageTotal));

        return $fp;
    }


    protected function pickpackinbound($params,$fp)
    {
        $column = $params['column'];
        
        fputcsv($fp, array('WAREHOUSE','CLIENT',
            'TAGNO','SERIALNO', 'INVENTORY-STATUS', 'PICKLISTNO' , 'PONO' ,'CREATED BY' ,'REMARKS' ,'PICKLISTED BY', 'DATE PICKLISTED', 'CONSIGNEE', 'ADDRESS',
            'MOBILELOCATION','PRODUCT DESCRIPTION', 'CLIENT SKU','ORDER NUMBER', 'INBOUNDED', 'PICKED', 'PACKED', 'BILL'));

        $total = 0;
        foreach ($params['results'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $inboundedDate = '';
            $pickedDate = '';
            $packedDate = '';


            if (is_null($inventory->getProduct())) {
                continue;
            }

            if ($column == 'dateInbound') {
                $inboundedDate = $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '';
            } else if ($column == 'datePick') {
                $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            } else if ($column == 'datePack') {
                $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';
            }

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            $total = $total + $params['price'];
            
            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklist = $picklistDetail[0]->getPicklist();                
                $picklistNumber = $picklist->getPicklistNumber();
                $uploadedBy = $this->uploadedBy($picklist);

                $uploadedDate = $picklist->getDateCreated()->format('Y-m-d'); 
                $details = $picklist->getOrderDetails();
                $consignee = $details['consignee'];
                $address = $details['consignee_address'];
            }
            $po = $inventory->getPurchaseOrder();

            $poUser = $this->poUser($po);

            fputcsv($fp, array( 
                $inventory->getWarehouse()->getName(),
                $inventory->getClient()->getName(),
                $inventory->getTag(),
                $inventory->getSerialNo(),
                $inventory->getInventoryStatus()->getName(),
                $picklistNumber,
                $po->getPurchaseOrderNumber(),
                $poUser,
                $po->getRemarks(),
                $uploadedBy,
                $uploadedDate,
                $consignee,
                $address,
                $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '',
                $inventory->getProduct()->getProductDescription(),
                $inventory->getProduct()->getClientSku(),
                $inventory->getOrderNumber(),
                $inboundedDate,
                $pickedDate,
                $packedDate,
                $params['price']
                )               
            );
        }

        return $fp;
    }

    protected function insurance($params,$fp)
    {
        fputcsv($fp, array('WAREHOUSE','CLIENT','TAGNO','SERIALNO', 'INVENTORY-STATUS', 'PICKLISTNO' , 
            'PONO' ,'CREATED BY' ,'REMARKS' ,'PICKLISTED BY', 'DATE PICKLISTED', 'CONSIGNEE', 'ADDRESS',
             'MOBILELOCATION','PRODUCT DESCRIPTION', 'CLIENT SKU','ORDER NUMBER', 'INBOUNDED','PICKED', 'PACKED', 'COGP'));

        $total = 0;
        $percentage = 0;
        foreach ($params['results'] as  $inventory) {
            
            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            if (is_null($inventory->getProduct())) {
                continue;
            }

            $cogp = $inventory->getProduct()->getCostOfGoodsPurchased();

            $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';

            $total = $total + $cogp;
            $percentage = $total * 0.05;
            
            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklist = $picklistDetail[0]->getPicklist();                
                $picklistNumber = $picklist->getPicklistNumber();
                $uploadedBy = $this->uploadedBy($picklist);
                $uploadedDate = $picklist->getDateCreated()->format('Y-m-d'); 
                $details = $picklist->getOrderDetails();
                $consignee = $details['consignee'];
                $address = $details['consignee_address'];
            }
            $po = $inventory->getPurchaseOrder();
            $poUser = $this->poUser($po);
            
            fputcsv($fp, array(
                $inventory->getWarehouse()->getName(),
                $inventory->getClient()->getName(),
                $inventory->getTag(),
                $inventory->getSerialNo(),
                $inventory->getInventoryStatus()->getName(),
                $picklistNumber,
                $po->getPurchaseOrderNumber(),
                $poUser,
                $po->getRemarks(),
                $uploadedBy,
                $uploadedDate,
                $consignee,
                $address,
                $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '', 
                $inventory->getProduct()->getProductDescription(),
                $inventory->getProduct()->getClientSku(),
                $inventory->getOrderNumber(),
                $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '',
                $pickedDate,
                $packedDate,
                $cogp
                )               
            );
        }

        fputcsv($fp, array('', '','','', '','','', '','','', '','','', '','','','', 'TOTAL', $total));
        fputcsv($fp, array('', '','','', '','','', '','','', '','','', '','','','', '5%', $percentage));

        return $fp;
    }


    protected function locationStorage($params,$fp)
    {
        fputcsv($fp, array('WAREHOUSE','TAGNO','SERIALNO', 'INVENTORY-STATUS', 'PICKLISTNO' , 'PONO' ,'MOBILELOCATION','PRODUCT DESCRIPTION', 'CLIENT SKU','ORDER NUMBER', 'INBOUNDED','DATE PICKED','DATE PACKED', 'STORAGE LOCATION'));

        foreach ($params['resultsStorage'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';

            $picklistNumber = '';

            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklistNumber = $picklistDetail[0]->getPicklist()->getPicklistNumber();
            }

            if (is_null($inventory->getProduct())) {
                continue;
            }

            fputcsv($fp, array( 
                $inventory->getWarehouse()->getName(),
                $inventory->getTag(),
                $inventory->getSerialNo(),
                $inventory->getInventoryStatus()->getName(),
                $picklistNumber,
                $inventory->getPurchaseOrder()->getPurchaseOrderNumber(),
                $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '',                 
                $inventory->getProduct()->getProductDescription(),
                $inventory->getProduct()->getClientSku(),
                $inventory->getOrderNumber(),
                $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '',
                $pickedDate,
                $packedDate,
                $inventory->getStorageLocation()->getName()
                )               
            );
        }


        fputcsv($fp, array(''));
        fputcsv($fp, array('************ STORAGE *********'));

        fputcsv($fp, array('STORAGE LOCATION','DAYS','PRICE','TOTAL'));

        $storageTotal = 0;
        $grandStorageTotal = 0;
        $dayleft = $params['dayleft'];

        $storage = [];
        foreach ($params['resultsStorage'] as $inventory) {
            
            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            if (is_null($inventory->getProduct())) {
                continue;
            }

            $location = $inventory->getStorageLocation()->getName();

            $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
            $storage[$location] = array_unique($storage[$location]);
        }

        $results = $storage;    

        foreach ($results as $key => $value) {
            $storageTotal = 11.20 * $dayleft;
            $grandStorageTotal = $grandStorageTotal + $storageTotal;

            fputcsv($fp, array($key,$dayleft,11.20,$storageTotal));
        }

        fputcsv($fp, array('','','GRAND TOTAL (PHP)',$grandStorageTotal));

        return $fp;
    }

    protected function computePrice($params,$billingtype,$billingId)
    {
        $mount = 0;
        
        if (!in_array($billingtype, [Billing::BILLING_STORAGE, Billing::BILLING_INSURANCE])) {
            $total = 0;
            foreach ($params['results'] as  $inventory) {

                if (method_exists($inventory, 'getInventory')) {
                    $inventory = $inventory->getInventory();
                }

                if (is_null($inventory->getProduct())) {
                    continue;
                }

                $total = $total + $params['price'];                
            }

            $amount = $total;
                

        } else if ($billingtype == Billing::BILLING_INSURANCE) {
    
            $total = 0;
            $percentage = 0;
    
            foreach ($params['results'] as  $inventory) {
                
                if (method_exists($inventory, 'getInventory')) {
                    $inventory = $inventory->getInventory();
                }

                if (is_null($inventory->getProduct())) {
                    continue;
                }

                $cogp = $inventory->getProduct()->getCostOfGoodsPurchased();

                $total = $total + $cogp;
                $percentage = $total * $params['price'];

            }

            $amount = $percentage;

        } else if ($billingtype == Billing::BILLING_STORAGE) {

            if ($params['isCbm'] === true ) {
                $params['billingId'] = $billingId;
                $amount = $this->computeCbm($params);
                    
                return true;
                //continue;
            }

            $storageTotal = 0;
            $grandStorageTotal = 0;
            $dayleft = $params['dayleft'];

            $storage = [];


            foreach ($params['resultsStorage'] as $inventory) {
                
                if (method_exists($inventory, 'getInventory')) {
                    $inventory = $inventory->getInventory();
                }

                if (is_null($inventory->getProduct())) {
                    continue;
                }
                
                if ($inventory->getStorageLocation()) {
                    $location = $inventory->getStorageLocation()->getName();    
                } else {
                    $location = 'location-1';
                }

                $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
                $storage[$location] = array_unique($storage[$location]);
            }

            $results = $storage;    


            if ($params['warehouseId'] != 1  && $params['clientId'] == 10) {
                
                $grandStorageTotal = 10500;
            
            } else if (in_array($params['clientId'], [19,20] )) {
                $grandStorageTotal = 1355;
            }
            else {

                foreach ($results as $key => $value) {
                    sort($value);
                    
                    if ($params['clientId'] == 16) { //asea                    
                        $dayleft = $this->datediff($value[0], $params['dateTo']);    
                        $left = abs($dayleft);
                        $dayleft = $left >= 31 ? 31 : $left;

                    } else {
                        $dayleft = $params['dayleft'];
                    }                

                    $storageTotal = $params['price'] * abs($dayleft);
                    $grandStorageTotal = $grandStorageTotal + $storageTotal;

                    //dr corp & orion
                    if (in_array($params['clientId'], [21,22] )) {
                        $storageTotal = 350 * 5;
                        $grandStorageTotal = $storageTotal;
                    }
                }
            }

            $amount = $grandStorageTotal;

        }

        $update = $this->conn->prepare("UPDATE billing set amount = :amount where id = :id");
        $update->execute(
            ['amount' => $amount, 
            'id'    => $billingId
            ]
        );

    }


    protected function computeCbm($params)
    {
        $total = 0;
        foreach ($params['results'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            if (is_null($inventory->getProduct())) {
                continue;
            }
            
            $product = $inventory->getProduct();

            $w = $product->getWidth();
            $l = $product->getLength();
            $h = $product->getHeight();

            $cbm = ($w * $l * $h) / 1000000;

            if ($inventory->getDatePicked()) {

                $datediff = $this->datediff($inventory->getDateInbounded()->format('Y-m-d') ,$inventory->getDatePicked()->format('Y-m-d'));
                $cbmTotal = ($cbm * 50) * $datediff;
            } else {

                $datediff = $this->datediff($inventory->getDateInbounded()->format('Y-m-d'), $params['dateTo']);
                $cbmTotal = ($cbm * 50) * $datediff;

            }
            
            if ((int)$datediff > 30) {
                $cbmTotal = ($cbm * 50) * date('d',strtotime($params['dateTo']));
            }

            $total = $total + $cbmTotal;
        }

        $update = $this->conn->prepare("UPDATE billing set amount = :amount where id = :id");
        $update->execute(
            ['amount' => $total, 
            'id'    => $params['billingId']
            ]
        );

    }

    private function uploadedBy($picklist)
    {
        try {
            $uploadedBy = !is_null($picklist->getCreatedBy()) ? $picklist->getCreatedBy()->getFullName() : '';     
        } catch (\Exception $e) {
            $uploadedBy = '';
        }

        return $uploadedBy;
    }

    private function poUser($po)
    {
        try{
            $poUser = !is_null($po->getCreatedBy()) ? $po->getCreatedBy()->getFullName() : '';    
        } catch (\Exception $e) {
            $poUser = '';
        }

        return $poUser;
        
    }

    private function datediff($start,$end)
    {

        $now = strtotime($end);
        $your_date = strtotime($start);
        $datediff = $now - $your_date;

        return floor($datediff / (60 * 60 * 24));
    }




}
