<?php

namespace BranchBundle\Service\Billing;

use Symfony\Component\HttpFoundation\Response;
use CoreBundle\Entity\Billing;
use CoreBundle\Entity\BillingSummary;
use CoreBundle\Entity\BillingDetailInbound;
use CoreBundle\Entity\BillingDetailPick;
use CoreBundle\Entity\BillingDetailPack;
use CoreBundle\Entity\BillingDetailStorage;
use CoreBundle\Entity\BillingDetailInsurance;
use CoreBundle\Entity\BillingKitting;
use CoreBundle\Entity\BillingStorage;
use CoreBundle\Entity\BillingKittingDetail;
use CoreBundle\Entity\Inventory;

use Liuggio\ExcelBundle\Factory;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class ExportBillingExcelService
{
    private $container;
    
    private $argument; 

    private $phpExcel;

    protected $totalInbound = 0;
    protected $totalPicked = 0;
    protected $totalPacked = 0;
    protected $totalInsuranced = 0;
    protected $totalStorage = 0;
    protected $totalKitting = 0;
    protected $billingIds = [];

    public function __construct($container,$phpExcel)
    {
        $this->container = $container;
        $this->phpExcel = $phpExcel;
        $this->em = $container->get('doctrine')->getManager();
        $this->conn = $this->em->getConnection();
    }

    public function save($clientId,$dateFrom, $dateTo, $authenticatedUser, $warehouseId) 
    {
        $em = $this->em;

        $client = $em->getRepository('CoreBundle:Client')->find($clientId);
        $response = $this->container->get('branch_billing_export_service')
            ->insertBillingSummary([
                'dateFrom'=> $dateFrom , 
                'dateTo' => $dateTo 
            ], $authenticatedUser, $warehouseId, $client, $this->billingIds);


        return $response;

    }

    public function saveKitting($params)
    {
        $order_number = trim($params['order_number']);

        $orderArr = array_map('trim', explode(PHP_EOL, $order_number));
        
        $em = $this->em;

        $dateAdded = \DateTime::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $params['start'], date('H:i:s', time())));
        $dateTo = \DateTime::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $params['end'], date('H:i:s', time())));

        $amount = ((int)$params['no_of_person'] * $params['rate']) + $params['over_charge'];
        $client = $em->getRepository('CoreBundle:Client')->find($params['client']);
        $rate = $params['rate'];

        $billingKiting = new BillingKitting;
        $billingKiting->setDateAdded($dateAdded);
        $billingKiting->setDateTo($dateTo);
        $billingKiting->setRemarks($params['remarks']);
        $billingKiting->setAmount($amount);
        $billingKiting->setWarehouse($params['user']->getWarehouse());
        $billingKiting->setClient($client);
        $billingKiting->setModifiedBy($params['user']->getFullName());
        $billingKiting->setBillingNumber('KIT-'. date('Ymd-His'));
        $billingKiting->setRate($rate);
        $billingKiting->setNoOfPerson($params['no_of_person']);
        $billingKiting->setOvercharge($params['over_charge']);
            
        $billingKiting->setIsPaid(0);

        $em->persist($billingKiting);

        $p = $em->getRepository('CoreBundle:Picklist')->getPicklistByOrderNumber($orderArr);

        foreach ($p as $k => $each) {
            
            $picklist = $em->getRepository('CoreBundle:Picklist')->findOneByOrderNumber($k);

            $billingKitingDetail = new BillingKittingDetail;
            $billingKitingDetail->setPicklist($picklist);
            $billingKitingDetail->setBillingKitting($billingKiting);

            $em->persist($billingKitingDetail);

        }

        $em->flush();


    }

    public function addStorageBilling($params)
    {
        $tagno = trim($params['tagno']);

        $tagArr = array_map('trim', explode(PHP_EOL, $tagno));
        
        $em = $this->em;

        $dateAdded = \DateTime::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $params['start'], date('H:i:s', time())));
        $dateTo = \DateTime::createFromFormat('Y-m-d H:i:s', sprintf('%s %s', $params['end'], date('H:i:s', time())));
        $client = $em->getRepository('CoreBundle:Client')->find($params['client']);

        $sqmAmount = $params['sqm'] * 200;

        $billingStorage = new BillingStorage;
        $billingStorage->setDateAdded($dateAdded);
        $billingStorage->setDateTo($dateTo);
        $billingStorage->setStorageAmount($sqmAmount);
        $billingStorage->setWarehouse($params['user']->getWarehouse());
        $billingStorage->setClient($client);
        $billingStorage->setBillingNumber('STO-'. date('Ymd-His'));
        $billingStorage->setStorageInsurance(0);
        $billingStorage->setRemarks('');
        $billingStorage->setModifiedBy($params['user']->getFullName());
            
        $billingStorage->setIsPaid(0);

        

        $p = $em->getRepository('CoreBundle:Inventory')->criteria([
            'tag'   => $tagArr
        ])->result();
        
        $inventoryIds = [];
        $totalCOGP = 0;


        foreach ($p as $k => $each) {
            
            if ($each->getProduct()) {

                $cogp = $each->getProduct()->getCostOfGoodsPurchased();
                $totalCOGP = $totalCOGP + (int)$cogp;

                array_push($inventoryIds, $each->getId());    
            }

        }

        $billingStorage->setStorageInsurance($totalCOGP * 0.005);
        $billingStorage->setRemarks(json_encode($inventoryIds,true));

        $em->persist($billingStorage);
        
        $em->flush();

    }

    public function exportKittingToExcel($kitting)
    {
        $em = $this->em;
        $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-template-kitting.xls';
    
        $phpExcelObject = $this->phpExcel->createPHPExcelObject($exportfile);
        
        $writer = $this->phpExcel->createWriter($phpExcelObject, 'Excel5');
        $response = $this->phpExcel->createStreamedResponse($writer);

        $dateFromTo = $kitting->getDateAdded()->format('M d Y') . ' to ' . $kitting->getDateTo()->format('M d Y');
        //$to = $billing->getDateCreated()->format('M d Y');
        
        $phpExcelObject->setActiveSheetIndex(1)
            /*->setCellValue('B6','Bill to: '. $billingInfo['BILLTO'])
            ->setCellValue('B7','Address: '. $billingInfo['ADDRESS'])
            ->setCellValue('B8','Tin #: ' . $billingInfo['TIN'])
            ->setCellValue('B9','Attention: '. $billingInfo['ATTENTION'])*/

            ->setCellValue('N3', date('M d Y'))
            ->setCellValue('N4', $kitting->getBillingNumber())
            ->setCellValue('B16', $kitting->getWarehouse()->getName() . ' - ' . $dateFromTo )
            ->setCellValue('E16', $kitting->getRate())
            ->setCellValue('G16', $kitting->getNoOfPerson())
            ->setCellValue('I16', $kitting->getOvercharge())
            ->setCellValue('K16', $kitting->getOtherCharge())
            ;

        $billingKiting = $em->getRepository('CoreBundle:BillingKittingDetail')->findByBillingKitting($kitting);
        $row = 2;
        foreach ($billingKiting as $e) {
            
            $each = $e->getPicklist();

            $phpExcelObject->setActiveSheetIndex(0)
            
                ->setCellValue('A'. ($row),  $each->getWarehouse()->getName())
                ->setCellValue('B'. ($row),  $each->getClient()->getName())
                ->setCellValue('C'. ($row),  $each->getPicklistStatus()->getName())
                ->setCellValue('D'. ($row),  $each->getPicklistNumber())
                ->setCellValue('E'. ($row),  $each->getOrderNumber())
                ->setCellValue('F'. ($row),  $each->getDateCreated()->format('m/d/Y'))
                ;

            $row++;
        }
        
        $phpExcelObject->setActiveSheetIndex(1);

        $filename = $kitting->getBillingNumber() . '.xls';


        $dispositionHeader = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');

        return $response;

    }

    public function process($request, $column, $template, $clientId, $price,$dateFrom,$dateTo,$authenticatedUser,$warehouseId, $isCbm=false)
    {
        $em = $this->em;
        $results = $this->getResults($request,$column,$template,$clientId);  

            $resultsStorage = $results;
            $datediff = $this->datediff($request->get('start'),$request->get('end'));
            
            if ($template == 'storage') {

                if ($isCbm) {

                    $results = $results;

                } else {


                    $storage = [];
                    foreach ($results as $inventory) {
                        
                        if ($inventory->getStorageLocation()) {
                            $location = $inventory->getStorageLocation()->getName();    
                        } else {
                            $location = 'location-1';
                        }

                        if (is_null($inventory->getProduct())) {
                            continue;
                        }
                
                        $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
                        $storage[$location] = array_unique($storage[$location]);
                    }

                    $results = $storage;   

                } 
            }

            $client = $em->getRepository('CoreBundle:Client')->find($clientId);
            $dayleft = (int)$datediff + 1;

           
                if ($template == 'storage') {
                    $results = $resultsStorage;    
                }
                
                $response = $this->container->get('branch_billing_export_service')->saveBilling(
                    compact("results", "price", "client","template","column","dayleft" ,"resultsStorage", "authenticatedUser", "warehouseId", "dateFrom", "dateTo","isCbm")
                );

                //$this->addFlash('success', $response['message']);

                if ($response['isSuccessfull']) {
                    array_push($this->billingIds, $response['id']);
                    //return $this->redirectToRoute('branch_billing_print',['id' => $response['id']]);
                } else {
                    //return $this->redirectToRoute('branch_billing_list');    
                }
                
    }

    protected function getResults($request,$column,$template,$clientId,$someQuery=null)
    {
        $em = $this->em;
        $type = Billing::getTypeByField($column);
        $billingTable = Billing::getBillingDetailObject($type);

        if ($template == 'storage' || $template == 'insurance') {
            // from the beginning date
            $resultsV1 = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "{$column}Start" => $request->get('start'),
                    "{$column}End" => $request->get('end'),
                    'client'    => $clientId
                ))
                ->leftJoin(
                    "CoreBundle:{$billingTable}",
                    'billingDetail',
                    'WITH',
                    "this = billingDetail.inventory"
                )
                ->andWhere('billingDetail.inventory IS NULL')

                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();

            $resultsV2 = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "{$column}Start" => '2016-01-01',
                    "{$column}End" => $request->get('start'),
                    'client'    => $clientId
                ))

                ->andWhere('this.inventoryStatus != :dispatch')
                ->setParameter('dispatch', Inventory::DISPATCHED_STATUS)

                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();


            $resultsPicked = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "datePickStart" => $request->get('start'),
                    "datePickEnd" => $request->get('end'),
                    'client'    => $clientId
                ))
                ->query()
                ->getResult();

            $resultDiff = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    /*"{$column}Start" => '2016-01-01',
                    "{$column}End" => $request->get('start'),*/
                    'client'    => $clientId
                ))
                //->andWhere('this.inventoryStatus != :dispatch')
                //->setParameter('dispatch', Inventory::DISPATCHED_STATUS)
                ->leftJoin(
                    "CoreBundle:{$billingTable}",
                    'billingDetail',
                    'WITH',
                    "this = billingDetail.inventory"
                )
                ->andWhere('billingDetail.dateProcess >= :start and billingDetail.dateProcess <= :end')
                ->setParameter('start', $request->get('start'))
                ->setParameter('end', $request->get('end'))


                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();


            $v2 = array_map(function ($inventory) {
                return $inventory->getId();
            },$resultsV2);

            $v3 = array_map(function ($inventory) {
                return $inventory->getId();
            },$resultDiff);

            
            $diff = array_diff($v2, $v3);


            $newResult = $em->getRepository('CoreBundle:Inventory')
                ->criteria()
                ->andWhere('this.id IN (:inventoryId)')
                ->setParameter('inventoryId', $diff)
                ->query()
                ->getResult();

            $results = array_merge($resultsV1,$newResult,$resultsPicked);


        } else {

            $results = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "{$column}Start" => $request->get('start'),
                    "{$column}End" => $request->get('end'),
                    'client'    => $clientId,
                ))
                ->leftJoin(
                    "CoreBundle:{$billingTable}",
                    'billingDetail',
                    'WITH',
                    "this = billingDetail.inventory"
                )
                ->andWhere('billingDetail.inventory IS NULL')

                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();
            
        }

        return $results;
    }

    public function exportBillingReport($data)
    {
        $sql = "select bs.*, c.name as client, w.name as warehouse from billing_summary bs 
            left join client c on bs.client_id = c.id
            left join warehouse w on bs.warehouse_id = w.id
            where date_added >= :start and date_added <= :end";

        $res = $this->conn->fetchAll($sql,array('start'=> $data['start'], 'end' => $data['end'] ));


        foreach ($res as $k => $e) {

            $remarks = json_decode($e['remarks'],true);
            
            $amounts = [];
            foreach ($remarks as $r) {
                
                $sql2 = "select amount,type from billing where id = :id";
                $a = $this->conn->fetchAll($sql2, ['id' => $r]);
                
                $amounts[$a[0]['type']] = $a[0]['amount'];

            }
            
            $res[$k]['charges'] = $amounts;
            
        }
        
        $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-report.xls';

        $phpExcelObject = $this->phpExcel->createPHPExcelObject($exportfile);
        $phpExcelObject->setActiveSheetIndex(0);
        $writer = $this->phpExcel->createWriter($phpExcelObject, 'Excel5');
        $response = $this->phpExcel->createStreamedResponse($writer);

        $row = 9;
        foreach ($res as $each) {

            $phpExcelObject->setActiveSheetIndex(0)
                ->setCellValue('A'. ($row),  $each['date_added'])
                ->setCellValue('B'. ($row),  $each['date_to'])
                ->setCellValue('C'. ($row),  $each['warehouse'])
                ->setCellValue('D'. ($row),  $each['client'])
                ->setCellValue('E'. ($row),  isset($each['charges'][1]) ? $each['charges'][1] : 0 ) 
                ->setCellValue('F'. ($row),  isset($each['charges'][2]) ? $each['charges'][2] : 0 )
                ->setCellValue('G'. ($row),  isset($each['charges'][3]) ? $each['charges'][3] : 0)
                ->setCellValue('H'. ($row),  isset($each['charges'][4]) ? $each['charges'][4] : 0)
                ->setCellValue('I'. ($row),  isset($each['charges'][5]) ? $each['charges'][5] : 0)
            ; 
            
            $row++;
        }

        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            'billing-report.xls'
            );

        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');

        return $response;

    }

    public function exportToExcel($billingSummary,$saveToDir = false)
    {
        $index = 5;
        $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-template.xls';

        $billingParams = $this->container->getParameter('billing');

        $cID = $billingSummary->getClient()->getId();
        if ($cID == 13) {
            $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-template-asea.xls';
        } else if ($cID == 12) {
            $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-template-truemoney.xls';   
            $index = 6;        
        }

        $billingInfo = $billingParams['default'];

        if (isset($billingParams['client_' . $cID])) {
             $billingInfo = $billingParams['client_'.$cID]; 
             $index = $billingInfo['SIMPLIFY'] ? 3: 5;
        }
        if ($index == 3) {
            $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-template-simplify.xls';
        }

        if ($cID == 12) {
            $index = 6;
        }
        
        $phpExcelObject = $this->phpExcel->createPHPExcelObject($exportfile);    
        $phpExcelObject->setActiveSheetIndex($index);
        $writer = $this->phpExcel->createWriter($phpExcelObject, 'Excel5');
        $response = $this->phpExcel->createStreamedResponse($writer);

        if ($cID == 12) {
            $this->billingKiting($phpExcelObject,$billingSummary);
        }

        $billingIds = json_decode($billingSummary->getRemarks());

        foreach ($billingIds as  $billingId) {
          
            $billing = $this->em->getRepository('CoreBundle:Billing')->find($billingId);

            $billingTable = Billing::getBillingDetailObject($billing->getType());

            $billingDetail = $this->em->getRepository("CoreBundle:{$billingTable}")->criteria([
                'billing'   => $billing
            ])
            ->query()
            ->getResult();

            $this->exportBilling($billing, $billingDetail,$phpExcelObject);
        }

        //$from = date('M d', strtotime($billingSummary->getDateAdded()->format('Y-m-d') .' +7 days'));
        $dateFromTo = $billingSummary->getDateAdded()->format('M d Y') . ' to ' . $billingSummary->getDateTo()->format('M d Y');
        //$to = $billing->getDateCreated()->format('M d Y');
        
        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('B6','Bill to: '. $billingInfo['BILLTO'])
            ->setCellValue('B7','Address: '. $billingInfo['ADDRESS'])
            ->setCellValue('B8','Tin #: ' . $billingInfo['TIN'])
            ->setCellValue('B9','Attention: '. $billingInfo['ATTENTION'])

            ->setCellValue('N3', date('M d Y'))
            ->setCellValue('N4', $billingSummary->getBillingNumber(). '(' . $dateFromTo . ')' )
            ->setCellValue('B16', $billingSummary->getWarehouse()->getName() . ' - ' . $dateFromTo )
            ->setCellValue('C16', $this->totalInbound)
            ->setCellValue('E16', $this->totalPicked)
            ->setCellValue('G16', $this->totalPacked)
            ->setCellValue('I16', $this->totalInsuranced)
            ->setCellValue('K16', $this->totalStorage)
            //->setCellValue('N16', $this->totalKitting)
            ;

        if ($cID == 12) {
            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('N16', $this->totalKitting);
        }
        
        $filename = $billingSummary->getBillingNumber() . '.xls';

        if ($saveToDir) {

            $docsPath = $this->container->get('kernel')->getRootDir() . '/../web/docs';
            $filename = $docsPath . '/'. $filename;

            $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');

            
            $writer = $this->phpExcel->createWriter($phpExcelObject, 'Excel5');
            $writer->save($filename);
            

        } else {

            $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $filename
            );
            $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
            $response->headers->set('Pragma', 'public');
            $response->headers->set('Cache-Control', 'maxage=1');
            $response->headers->set('Content-Disposition', $dispositionHeader);
            $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');

            return $response;

        }

    }

    public function exportStorageToExcel($billingSummary,$saveToDir = false)
    {
        $index = 2;
        $billingParams = $this->container->getParameter('billing');

        $billingInfo = $billingParams['storage'];

        $cID = $billingSummary->getClient()->getId();
        
        $exportfile = $this->container->get('kernel')->getRootDir() . '/../web/templates/billing-template-storage.xls';

        $phpExcelObject = $this->phpExcel->createPHPExcelObject($exportfile);    
        $phpExcelObject->setActiveSheetIndex($index);
        $writer = $this->phpExcel->createWriter($phpExcelObject, 'Excel5');
        $response = $this->phpExcel->createStreamedResponse($writer);

        
        $inventoryIds = json_decode($billingSummary->getRemarks());

        $inventories = $this->em->getRepository('CoreBundle:Inventory')->criteria([
            'ids'   => $inventoryIds
        ])->result();

        $phpExcelObject = $this->storageBilling($billingSummary,$inventories, $phpExcelObject);

        $dateFromTo = $billingSummary->getDateAdded()->format('M d Y') . ' to ' . $billingSummary->getDateTo()->format('M d Y');
        
        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('B6','Bill to: '. $billingInfo['BILLTO'])
            ->setCellValue('B7','Address: '. $billingInfo['ADDRESS'])
            ->setCellValue('B8','Tin #: ' . $billingInfo['TIN'])
            ->setCellValue('B9','Attention: '. $billingInfo['ATTENTION'])

            ->setCellValue('N3', date('M d Y'))
            ->setCellValue('N4', $billingSummary->getBillingNumber(). '(' . $dateFromTo . ')' )
            ->setCellValue('B16', $billingSummary->getWarehouse()->getName() . ' - ' . $dateFromTo )
            ->setCellValue('C16', $this->totalInbound)
            ->setCellValue('E16', $this->totalPicked)
            ->setCellValue('G16', $this->totalPacked)
            ->setCellValue('I16', $this->totalInsuranced)
            ->setCellValue('K16', $this->totalStorage)
            //->setCellValue('N16', $this->totalKitting)
            ;

        
        $filename = $billingSummary->getBillingNumber() . '.xls';

        

        $dispositionHeader = $response->headers->makeDisposition(
        ResponseHeaderBag::DISPOSITION_ATTACHMENT,
        $filename
        );
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');

        return $response;

        

    }

    protected function storageBilling($billing,$inventories,$phpExcelObject)
    {
        
        $total = 0;
        $row = 5;
        foreach ($inventories as  $inventory) {


            $inboundedDate = '';
            $pickedDate = '';
            $packedDate = '';


            if (is_null($inventory->getProduct())) {
                continue;
            }

            $inboundedDate = $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '';

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            $cogp = $inventory->getProduct()->getCostOfGoodsPurchased();
            $total = 0;
            $cogpSum = $cogp * 0.005;
            
            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklist = $picklistDetail[0]->getPicklist();                
                $picklistNumber = $picklist->getPicklistNumber();
                
                $details = $picklist->getOrderDetails();
                $consignee = $details['consignee'];
                $address = $details['consignee_address'];

            }
            $po = $inventory->getPurchaseOrder();

            $poUser = $this->poUser($po);

            $phpExcelObject->setActiveSheetIndex(1)
                ->setCellValue('A'. ($row),  $inboundedDate)
                ->setCellValue('B'. ($row),  $inventory->getClient()->getName())
                ->setCellValue('C'. ($row),  '')
                ->setCellValue('D'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('E'. ($row),  $inventory->getProduct()->getProductDescription())                
                ->setCellValue('F'. ($row),  $cogp)

                ->setCellValue('G'. ($row),  $inventory->getTag())
                ->setCellValue('H'. ($row),  $cogp)
                ->setCellValue('I'. ($row),  $cogpSum)
                ;
            
            $total = $total + $cogpSum;
            $row++;

        }

        $phpExcelObject->setActiveSheetIndex(1)
            ->setCellValue('H'. ($row+1),  'TOTAL')
            ->setCellValue('I'. ($row+1),  $total)
        ;

        $this->totalInsuranced = $total;
        $this->totalInbound = 0;
        $this->totalPicked = 0;
        $this->totalPacked = 0;
        $this->totalStorage = $billing->getStorageAmount();

        return $phpExcelObject;
    }

    public function billingKiting($phpExcelObject,$billingSummary)
    {
        $dateFrom = $billingSummary->getDateAdded()->format('Y-m-d');
        $dateTo = $billingSummary->getDateTo()->format('Y-m-d');

        $kitting = $this->em->getRepository('CoreBundle:BillingKitting')->findByDateFromAndDateTo($dateFrom,$dateTo);

        if ($kitting) {

            $phpExcelObject->setActiveSheetIndex(5)
     
                ->setCellValue('A5', $dateFrom . '- '. $dateTo)
                ->setCellValue('B2', $kitting->getBillingNumber())
                ->setCellValue('D5', $kitting->getRate())
                ->setCellValue('F5', $kitting->getNoOfPerson())
                ->setCellValue('H5', $kitting->getOvercharge())
                ->setCellValue('J5', $kitting->getOtherCharge())
                ;

            $row = 9;

            $billingKiting = $this->em->getRepository('CoreBundle:BillingKittingDetail')->findByBillingKitting($kitting);
            foreach ($billingKiting as $e) {
                
                $each = $e->getPicklist();

                $phpExcelObject->setActiveSheetIndex(5)
                
                    ->setCellValue('A'. ($row),  $each->getWarehouse()->getName())
                    ->setCellValue('B'. ($row),  $each->getClient()->getName())
                    ->setCellValue('C'. ($row),  $each->getPicklistStatus()->getName())
                    ->setCellValue('D'. ($row),  $each->getPicklistNumber())
                    ->setCellValue('E'. ($row),  $each->getOrderNumber())
                    ->setCellValue('F'. ($row),  $each->getDateCreated()->format('m/d/Y'))
                    ;

                $row++;
            }
            
            $this->totalKitting = $kitting->getAmount();
            return $phpExcelObject;     
        }
        
         
    }


    public function exportBilling($billing, $billingDetails, $phpExcelObject)
    {

        $billingParams = $this->container->getParameter('billing');

        $clientId = $billing->getClient()->getId();

        $billingPrice = $billingParams['default'];
        $isSimplify = false;

        if (isset($billingParams['client_' . $clientId])) {
            $billingPrice = $billingParams['client_'.$clientId]; 
            $isSimplify = $billingPrice['SIMPLIFY'];
        }

        $params = ['results' => $billingDetails, 'billingType' => $billing->getType()];
        $params['dateTo'] = $billing->getDateTo()->format('Y-m-d');
        $params['clientId'] = $clientId;
        $params['dateFrom'] = $billing->getDateAdded()->format('Y-m-d');
        $params['warehouseId'] = $billing->getWarehouse()->getId();
        $params['isCbm']    = $billingPrice['IS_CBM'];

        if ($billing->getType() == BILLING::BILLING_INBOUND) {

            $params['column'] = 'dateInbound';
            $params['price'] = $billingPrice['DATEINBOUNDED']['value'];

            if ($isSimplify == true) {
                $total = 0; // need improvement
            } else {
                $total = $this->pickpackinbound($params,$phpExcelObject, 4);
            }
            
            $this->totalInbound = $total;
        }

        else if ($billing->getType() == BILLING::BILLING_PICKED) {

            $params['column'] = 'datePick';
            $params['price'] = $billingPrice['DATEPICKED']['value'];

            if ($isSimplify == true) {
                $total = $this->pickpackinboundSimplify($params,$phpExcelObject,2);
            } else {
                $total = $this->pickpackinbound($params,$phpExcelObject,3);    
            }

            
            $this->totalPicked = $total;
        }

        else if ($billing->getType() == BILLING::BILLING_PACKED) {

            $params['column'] = 'datePack';
            $params['price'] = $billingPrice['DATEPACKED']['value'];

            if ($isSimplify == true) {
                $total = 0;
            } else {
                $total = $this->pickpackinbound($params,$phpExcelObject, 2);
            }
            
            $this->totalPacked = $total;
        }

        else if ($billing->getType() == BILLING::BILLING_INSURANCE) {
            $params['price'] = $billingPrice['INSURANCEED']['value'];
            
            if ($isSimplify == true) {
                $total = $this->insuranceSimplify($params,$phpExcelObject, 1);
            } else {
                $total = $this->insurance($params,$phpExcelObject, 1);    
            }

            $this->totalInsuranced = $total;
        }

        else if ($billing->getType() == BILLING::BILLING_STORAGE) {
            $params['resultsStorage'] = $billingDetails;
            
            if ($isSimplify == true) {
                $total = 1355;

            } else {
                $datediff = $this->datediff($billing->getDateAdded()->format('Y-m-d') ,$billing->getDateTo()->format('Y-m-d'));
                $dayleft = (int)$datediff + 1;

                $params['dayleft'] = $dayleft;
                $params['price'] = $billingPrice['STORAGEED']['value'];

                $total = $this->locationStorage($params,$phpExcelObject, 0);
            }

            $this->totalStorage = $total;
        }
        
        
    }



    protected function pickpackinbound($params,$phpExcelObject, $index)
    {
        $column = $params['column'];
        
        $total = 0;
        $row = 5;
        foreach ($params['results'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $inboundedDate = '';
            $pickedDate = '';
            $packedDate = '';


            if (is_null($inventory->getProduct())) {
                continue;
            }

            if ($column == 'dateInbound') {
                $inboundedDate = $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '';
            } else if ($column == 'datePick') {
                $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            } else if ($column == 'datePack') {
                $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';
            }

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            $total = $total + $params['price'];
            
            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklist = $picklistDetail[0]->getPicklist();                
                $picklistNumber = $picklist->getPicklistNumber();
                $uploadedBy = $this->uploadedBy($picklist);

                $uploadedDate = $picklist->getDateCreated()->format('Y-m-d'); 
                $details = $picklist->getOrderDetails();
                $consignee = $details['consignee'];
                $address = $details['consignee_address'];
            }
            $po = $inventory->getPurchaseOrder();

            $poUser = $this->poUser($po);

            $p = '';
            if (method_exists($po, 'getPurchaseOrderNumber')) {
                $p = $po->getPurchaseOrderNumber();
            }

            $r = '';
            if (method_exists($po, 'getRemarks')) {
                $r = $po->getRemarks();
            }

            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('A'. ($row),  $inventory->getWarehouse()->getName())
                ->setCellValue('B'. ($row),  $inventory->getClient()->getName())
                ->setCellValue('C'. ($row),  $inventory->getTag())
                ->setCellValue('D'. ($row),  $inventory->getSerialNo())
                ->setCellValue('E'. ($row),  $inventory->getInventoryStatus()->getName())
                ->setCellValue('F'. ($row),  $picklistNumber)
                ->setCellValue('G'. ($row),  $p)
                ->setCellValue('H'. ($row),  $poUser)
                ->setCellValue('I'. ($row),  $r)
                ->setCellValue('J'. ($row),  $uploadedBy)
                ->setCellValue('K'. ($row),  $uploadedDate)
                ->setCellValue('L'. ($row),  $consignee)
                ->setCellValue('M'. ($row),  $address)
                ->setCellValue('N'. ($row),  $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '')
                ->setCellValue('O'. ($row),  $inventory->getProduct()->getProductDescription())
                ->setCellValue('P'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('Q'. ($row),  $inventory->getOrderNumber())
                ->setCellValue('R'. ($row),  $inboundedDate)
                ->setCellValue('S'. ($row),  $pickedDate)
                ->setCellValue('T'. ($row),  $packedDate)
                ->setCellValue('W'. ($row),  $inventory->getProduct()->getCostOfGoodsPurchased())
                ->setCellValue('U'. ($row),  $params['price']);
            
            $row++;
        }
        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('T'. ($row+1),  'TOTAL')
            ->setCellValue('U'. ($row+1),  $total)
        ;


        return $total;
    }

    protected function pickpackinboundSimplify($params,$phpExcelObject, $index)
    {
        $column = $params['column'];
        
        $total = 0;
        $row = 5;
        foreach ($params['results'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $inboundedDate = '';
            $pickedDate = '';
            $packedDate = '';


            if (is_null($inventory->getProduct())) {
                continue;
            }

            if ($column == 'dateInbound') {
                $inboundedDate = $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '';
            } else if ($column == 'datePick') {
                $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            } else if ($column == 'datePack') {
                $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';
            }

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            $total = $total + ($params['price'] * 2);
            
            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklist = $picklistDetail[0]->getPicklist();                
                $picklistNumber = $picklist->getPicklistNumber();
                
                $details = $picklist->getOrderDetails();
                $consignee = $details['consignee'];
                $address = $details['consignee_address'];
            }
            $po = $inventory->getPurchaseOrder();

            $poUser = $this->poUser($po);

            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('A'. ($row),  $pickedDate)
                ->setCellValue('B'. ($row),  $inventory->getClient()->getName())
                ->setCellValue('C'. ($row),  $consignee)
                ->setCellValue('D'. ($row),  $inventory->getTag())
                ->setCellValue('E'. ($row),  $picklistNumber)
                ->setCellValue('F'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('G'. ($row),  $inventory->getProduct()->getProductDescription())
                ->setCellValue('H'. ($row),  $params['price'])
                ->setCellValue('I'. ($row),  $params['price'])
                ->setCellValue('J'. ($row),  (int)$params['price'] * 2)
                ;
            
            $row++;


        }
        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('I'. ($row+1),  'TOTAL')
            ->setCellValue('J'. ($row+1),  $total)
        ;


        return $total;
    }


    protected function insurance($params,$phpExcelObject, $index)
    {
        $row = 5;
        $total = 0;
        $percentage = 0;
        foreach ($params['results'] as  $inventory) {
            
            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            if (is_null($inventory)) {
                continue;
            }

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            if (is_null($inventory->getProduct())) {
                continue;
            }

            $cogp = $inventory->getProduct()->getCostOfGoodsPurchased();

            $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';

            $total = $total + (int)$cogp;
            $percentage = $total * $params['price'];
            
            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklist = $picklistDetail[0]->getPicklist();                
                $picklistNumber = $picklist->getPicklistNumber();
                $uploadedBy = $this->uploadedBy($picklist);
                $uploadedDate = $picklist->getDateCreated()->format('Y-m-d'); 
                $details = $picklist->getOrderDetails();
                $consignee = $details['consignee'];
                $address = $details['consignee_address'];
            }
            $po = $inventory->getPurchaseOrder();
            $poUser = $this->poUser($po);
            
            $ponumber = '';
            if (method_exists($po, 'getPurchaseOrderNumber')) {
                $ponumber = $po->getPurchaseOrderNumber();
            }
            $r = '';
            if (method_exists($po, 'getRemarks')) {
                $r = $po->getRemarks();
            }

            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('A'. ($row),  $inventory->getWarehouse()->getName())
                ->setCellValue('B'. ($row),  $inventory->getClient()->getName())
                ->setCellValue('C'. ($row),  $inventory->getTag())
                ->setCellValue('D'. ($row),  $inventory->getSerialNo())
                ->setCellValue('E'. ($row),  $inventory->getInventoryStatus()->getName())
                ->setCellValue('F'. ($row),  $picklistNumber)
                ->setCellValue('G'. ($row),  $ponumber)
                ->setCellValue('H'. ($row),  $poUser)
                ->setCellValue('I'. ($row),  $r)
                ->setCellValue('J'. ($row),  $uploadedBy)
                ->setCellValue('K'. ($row),  $uploadedDate)
                ->setCellValue('L'. ($row),  $consignee)
                ->setCellValue('M'. ($row),  $address)
                ->setCellValue('N'. ($row),  $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '')
                ->setCellValue('O'. ($row),  $inventory->getProduct()->getProductDescription())
                ->setCellValue('P'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('Q'. ($row),  $inventory->getOrderNumber())
                ->setCellValue('R'. ($row),  $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '')
                ->setCellValue('S'. ($row),  $pickedDate)
                ->setCellValue('T'. ($row),  $packedDate)
                ->setCellValue('U'. ($row),  $cogp)
            ;

            $row++;
        }
        $total_row = $row + 1;
        $percent_row = $row + 2;

        $phpExcelObject->setActiveSheetIndex($index)
        ->setCellValue('T'. ($total_row),  'TOTAL')
        ->setCellValue('U'. ($total_row),  $total);

        $phpExcelObject->setActiveSheetIndex($index)
        ->setCellValue('T'. ($percent_row),  '5%')
        ->setCellValue('U'. ($percent_row),  $percentage);

        return $percentage;
    }

    protected function insuranceSimplify($params,$phpExcelObject, $index)
    {
        $row = 5;
        $total = 0;
        $percentage = 0;
        foreach ($params['results'] as  $inventory) {
            
            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $picklistNumber = '';
            $uploadedBy = '';
            $uploadedDate = '';
            $consignee = '';
            $address = '';

            if (is_null($inventory->getProduct())) {
                continue;
            }

            $cogp = $inventory->getProduct()->getCostOfGoodsPurchased();

            $s = $cogp * 0.005;
            $total = $total + $s;

            $po = $inventory->getPurchaseOrder();
            
            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('A'. ($row),  $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '')
                ->setCellValue('B'. ($row),  $inventory->getClient()->getName())
                ->setCellValue('C'. ($row),  $po->getPurchaseOrderNumber())
                ->setCellValue('D'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('E'. ($row),  $inventory->getProduct()->getProductDescription())
                ->setCellValue('F'. ($row),  $cogp)
                ->setCellValue('G'. ($row),  $inventory->getTag())
                ->setCellValue('H'. ($row),  $cogp)
                ->setCellValue('I'. ($row),  $s)
                
            ;
            $row++;
        }
        $total_row = $row + 1;
        $percent_row = $row + 2;

        $phpExcelObject->setActiveSheetIndex($index)
        ->setCellValue('H'. ($total_row),  'TOTAL')
        ->setCellValue('I'. ($total_row),  $total);

        

        return $total;
    }

    protected function locationStorage($params,$phpExcelObject, $index)
    {
        if ($params['isCbm']) {
            return $this->exportLocationStorageCbm($params,$phpExcelObject, $index);
            
        }

        $row = 5;
        foreach ($params['resultsStorage'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            if (is_null($inventory)) {
                continue;
            }
            
            $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';

            $picklistNumber = '';

            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklistNumber = $picklistDetail[0]->getPicklist()->getPicklistNumber();
            }

            if (is_null($inventory->getProduct())) {
                continue;
            }

            $p = '';
            if (method_exists($inventory->getPurchaseOrder(), 'getPurchaseOrderNumber')) {
                $p = $inventory->getPurchaseOrder()->getPurchaseOrderNumber();
            }

            $loc = '';
            if (method_exists($inventory->getStorageLocation(), 'getName')) {
                $loc = $inventory->getStorageLocation()->getName();
            }

            

            $phpExcelObject->setActiveSheetIndex($index)
            
                ->setCellValue('A'. ($row),  $inventory->getWarehouse()->getName())
                ->setCellValue('B'. ($row),  $inventory->getTag())
                ->setCellValue('C'. ($row),  $inventory->getSerialNo())
                ->setCellValue('D'. ($row),  $inventory->getInventoryStatus()->getName())
                ->setCellValue('E'. ($row),  $picklistNumber)
                ->setCellValue('F'. ($row),  $p)
                ->setCellValue('G'. ($row),  $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '')
                ->setCellValue('H'. ($row),  $inventory->getProduct()->getProductDescription())
                ->setCellValue('I'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('J'. ($row),  $inventory->getOrderNumber())
                ->setCellValue('K'. ($row),  $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '')
                ->setCellValue('L'. ($row),  $pickedDate)
                ->setCellValue('M'. ($row),  $packedDate)
                ->setCellValue('N'. ($row),  $loc)
                ;

            $row++;
        }


        $total_row = $row + 3;
        $storage_row = $total_row + 2;
        $storage_final_row = $storage_row + 1;

        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('A'. ($total_row),  '************ STORAGE *********');
        

        $storageTotal = 0;
        $grandStorageTotal = 0;
        $dayleft = $params['dayleft'];

        $storage = [];
        foreach ($params['resultsStorage'] as $inventory) {
            
            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            if (is_null($inventory)) {
                continue;
            }
            
            if (is_null($inventory->getProduct())) {
                continue;
            }


            $location = 'location-x';
            if (method_exists($inventory->getStorageLocation(), 'getName')) {
                $location = $inventory->getStorageLocation()->getName();
            }

            
            $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
            $storage[$location] = array_unique($storage[$location]);
        }

        $results = $storage; 

        if ($params['warehouseId'] != 1  && $params['clientId'] == 10) {
           return $this->forMuntinlupaWarehouse($results,$phpExcelObject, $storage_row,$index,$params);
        }


        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('A'. ($storage_row),  'STORAGE LOCATION')
            ->setCellValue('B'. ($storage_row),  'DAYS')
            ->setCellValue('C'. ($storage_row),  'PRICE')
            ->setCellValue('D'. ($storage_row),  'TOTAL')
        ;


        foreach ($results as $key => $value) {

            sort($value);

            if ($params['clientId'] == 16) { //asea
                $dayleft = $this->datediff($value[0], $params['dateTo']);
                $left = abs($dayleft);
                $dayleft = $left >= 31 ? 31 : $left;
            } else {
                $dayleft = $params['dayleft'];
            }
            

            $storageTotal = $params['price'] * abs($dayleft);
            $grandStorageTotal = $grandStorageTotal + $storageTotal;

            //dr corp & orion
            if (in_array($params['clientId'], [21,22] )) {

                $params['price'] = 350;
                $dayleft = 5;
                $storageTotal = $params['price'] * $dayleft;
                $grandStorageTotal = $storageTotal;
            }

            
            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('A'. ($storage_final_row),  $key)            
                ->setCellValue('B'. ($storage_final_row),  $dayleft)
                ->setCellValue('C'. ($storage_final_row),  $params['price'])
                ->setCellValue('D'. ($storage_final_row),  $storageTotal)
                ;

            $storage_final_row ++;

        }

        $last_row = $storage_final_row + 2;
        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('C'. ($last_row),  'GRAND TOTAL (PHP)')
            ->setCellValue('D'. ($last_row),  $grandStorageTotal);

        
        return $grandStorageTotal;
    }

    protected function exportLocationStorageCbm($params,$phpExcelObject, $index)
    {
        $row = 5;
        $total = 0;
        foreach ($params['resultsStorage'] as  $inventory) {

            if (method_exists($inventory, 'getInventory')) {
                $inventory = $inventory->getInventory();
            }

            $pickedDate = $inventory->getDatePicked() ? $inventory->getDatePicked()->format('m/d/Y') : '';
            $packedDate = $inventory->getDatePacked() ? $inventory->getDatePacked()->format('m/d/Y') : '';

            $picklistNumber = '';

            if ($inventory->getPicklistDetail()->count() > 0) {
                $picklistDetail = $inventory->getPicklistDetail();
                $picklistNumber = $picklistDetail[0]->getPicklist()->getPicklistNumber();
            }

            if (is_null($inventory->getProduct())) {
                continue;
            }

            $p = $inventory->getProduct();

            $l = $p->getLength();
            $w = $p->getWidth();
            $h = $p->getHeight();
            $t = ($l * $w * $h) / 1000000;
            $cbm = $t * 50;

            if ($inventory->getDatePicked()) {

                $datediff = $this->datediff($inventory->getDateInbounded()->format('Y-m-d') ,$inventory->getDatePicked()->format('Y-m-d'));
                $cbmTotal = $cbm * $datediff;
            } else {

                $datediff = $this->datediff($inventory->getDateInbounded()->format('Y-m-d'), $params['dateTo']);
                $cbmTotal = $cbm * $datediff;

            }

            if ((int)$datediff > 30) {
                $cbmTotal = $cbm * date('d',strtotime($params['dateTo']));

                $datediff = date('d',strtotime($params['dateTo']));
            }

            $phpExcelObject->setActiveSheetIndex($index)
            
                ->setCellValue('A'. ($row),  $inventory->getWarehouse()->getName())
                ->setCellValue('B'. ($row),  $inventory->getTag())
                ->setCellValue('C'. ($row),  $inventory->getSerialNo())
                ->setCellValue('D'. ($row),  $inventory->getInventoryStatus()->getName())
                ->setCellValue('E'. ($row),  $picklistNumber)
                ->setCellValue('F'. ($row),  $inventory->getPurchaseOrder()->getPurchaseOrderNumber())
                ->setCellValue('G'. ($row),  $inventory->getMobileLocation() ? $inventory->getMobileLocation()->getName() : '')
                ->setCellValue('H'. ($row),  $inventory->getProduct()->getProductDescription())
                ->setCellValue('I'. ($row),  $inventory->getProduct()->getClientSku())
                ->setCellValue('J'. ($row),  $inventory->getOrderNumber())
                ->setCellValue('K'. ($row),  $inventory->getDateInbounded() ? $inventory->getDateInbounded()->format('m/d/Y') : '')
                ->setCellValue('L'. ($row),  $pickedDate)
                ->setCellValue('M'. ($row),  $packedDate)
                ->setCellValue('N'. ($row),  $inventory->getStorageLocation()->getName())
                ->setCellValue('O'. ($row),  $l)
                ->setCellValue('P'. ($row),  $w)
                ->setCellValue('Q'. ($row),  $h)
                ->setCellValue('R'. ($row),  $t)
                ->setCellValue('S'. ($row),  $cbm)
                ->setCellValue('T'. ($row),  $datediff)
                ->setCellValue('U'. ($row),  $cbmTotal)
                ;

                $total = $total + $cbmTotal;

            $row++;
        }

        $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('T'. ($row+1),  'TOTAL')
            ->setCellValue('U'. ($row+1),  $total)
        ;

        return $total;
    }

    protected function forMuntinlupaWarehouse($results, $phpExcelObject,$storage_row, $index,$params)
    {
        $storage_final_row = $storage_row + 1;

         $phpExcelObject->setActiveSheetIndex($index)
            ->setCellValue('B'. ($storage_row),  'STORAGE LOCATION')
            ->setCellValue('C'. ($storage_row),  'Per CBM')
            ->setCellValue('D'. ($storage_row),  'Days')
            ->setCellValue('E'. ($storage_row),  'Total');

            $day = date('d',strtotime($params['dateTo']));

            $grandTotal = 0;
            foreach ($this->hubStorages() as $key => $value) {
                $total = $day * 50;
                $grandTotal = $grandTotal + $total;
                $phpExcelObject->setActiveSheetIndex($index)
                    ->setCellValue('B'. ($storage_final_row),  $value)            
                    ->setCellValue('C'. ($storage_final_row),  50)
                    ->setCellValue('D'. ($storage_final_row),  $day)
                    ->setCellValue('E'. ($storage_final_row),  $total)
                    ;

                
                $storage_final_row ++;
            }

            $storage_final_row_nd = $storage_final_row++;

            $phpExcelObject->setActiveSheetIndex($index)
                ->setCellValue('C'. ($storage_final_row_nd),  'GRAND TOTAL (PHP)')
                ->setCellValue('D'. ($storage_final_row_nd),  $grandTotal);

        
        return $grandTotal;
            
    }

    protected function hubStorages()
    {
        return ['STORAGE-1', 'STORAGE-2', 'STORAGE-3', 'STORAGE-4', 'STORAGE-5','STORAGE-6', 'STORAGE-7'];
    }


    private function uploadedBy($picklist)
    {
        try {
            $uploadedBy = !is_null($picklist->getCreatedBy()) ? $picklist->getCreatedBy()->getFullName() : '';     
        } catch (\Exception $e) {
            $uploadedBy = '';
        }

        return $uploadedBy;
    }

    private function poUser($po)
    {
        $poUser = '';

        try{
            if (method_exists($po, 'getCreatedBy')) {
                $poUser = !is_null($po->getCreatedBy()) ? $po->getCreatedBy()->getFullName() : '';    
            }
        } catch (\Exception $e) {
            $poUser = '';
        }

        return $poUser;
        
    }

    private function datediff($start,$end)
    {

        $now = strtotime($end);
        $your_date = strtotime($start);
        $datediff = $now - $your_date;

        return floor($datediff / (60 * 60 * 24));
    }
}


