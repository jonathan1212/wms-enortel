<?php

namespace BranchBundle\Service;
use CoreBundle\Entity\Client;
use CoreBundle\Entity\User;

class BlameableUser
{
    private $tokenStorage;
    private $em;
    
    public function __construct($em, $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }


    public function getBlameable()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if ($user instanceof Client) {

            $account = $this->em->getRepository('CoreBundle:User')->findOneBy(['clientAccount' => $user , 'emailAddress' => $user->getEmailAddress()]);
            
            if (is_null($account)) {

                $account = new User();
                $account->setFirstName('admin');
                $account->setLastName($user->getContactPerson());
                $account->setEmailAddress($user->getEmailAddress());
                $account->setContactNumber($user->getContactNumber());
                $account->setClientAccount($user);    
                $account->setPassword('admin');
                
                $this->em->persist($account);
                $this->em->flush();

            }

            return $account;
        }

        return $user;
    }
}
