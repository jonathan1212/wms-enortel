<?php

namespace BranchBundle\Service\Inventory;

class InventoryTagGenerator
{
    /**
     * @link http://stackoverflow.com/questions/4356289/php-random-string-generator
     * @param  integer $length
     * @return string
     */
    public function generate($length = 8)
    {
        return 'IT-'.strtoupper(substr(str_shuffle(MD5(microtime())), 0, $length));
    }
}
