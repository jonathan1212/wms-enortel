<?php

namespace BranchBundle\Service\Inventory;

class InventoryService
{
    protected $service;
    protected $em;

    public function __construct($service)
    {
        $this->service = $service;
        $this->em = $service->get('doctrine')->getManager();
        $this->conn = $this->em->getConnection();
    }


    /**
     * saving of serialno to inventory
     * @param [type] $data  [0 => tagId, 1=> serialno ]
     */
    public function addSerial($data)
    {
        $notFoundIds = array();

        foreach ($data as $tag) {
            
            $inventory = $this->em->getRepository('CoreBundle:Inventory')->findOneBy(['tag' => $tag[0]]);
            
            if ($inventory) {
                $inventory->setSerialNo($tag[1]);
            } else {
                array_push($notFoundIds, $tag[0]);
            }
        }

        $this->em->flush();
        
        return $notFoundIds;    
    }

    public function count($each,$warehouseId,$clientId)
    {
        $conn = $this->em->getConnection();

        $rs = $conn->fetchAll(" 
             select count(i.product_id) as productCount,
                 count(i.date_picked) as countDatePick, 
                 count(date_packed) as countDatePack,
                 i.inventory_status_id as status 
             from inventory i 
             where i.product_id = :productId
              and i.client_id = :clientId
              and i.warehouse_id = :warehouseId
             group by i.product_id ,i.inventory_status_id", ['productId' => $each['id'],'clientId' => $clientId, 'warehouseId' => $warehouseId]
            );

        $count = [
            'onHand'        => 0,
            'allocated'     => 0,
            'quaranteed'    => 0,
            'dispatch'      => 0,
            'picked'        => 0,
            'packed'        => 0,
        ];

        foreach ($rs as $each) {
                
            // inbounded
            if ($each['status'] == 1) {
                $count['onHand'] = $each['productCount']; 
            }
            // allocated
            else if ($each['status'] == 2) {
                $count['allocated'] = $each['productCount']; 
            }
            // quaranteen
            else if ($each['status'] == 4) {
                $count['quaranteed'] = $each['productCount']; 
            }

            // dispatch
            else if ($each['status'] == 7) {
                $count['dispatch'] = $each['productCount']; 
                $count['picked'] = $each['countDatePick'];
                $count['packed'] = $each['countDatePack'];
            }
            // lost 8 
            else if ($each['status'] == 8) {
                $count['lost'] = $each['productCount']; 
            }

        }

        return $count;
    }

    public function countByOrderNumber($each)
    {
        $conn = $this->em->getConnection();
        $rs = $conn->fetchAll(" 
             select order_number ,
              count(order_number) as orderNumberCount,
              count(date_picked) as picked,
              count(date_packed) as packed
            from inventory 
            where 
            product_id = :productId 
            group by order_number", ['productId' => $each['id']]
            
            );


        return $rs;
    }

    public function getPicklist($picklistDetail)
    {
        $data = [];

        foreach ($picklistDetail as $detail) {

            $sql = "
                SELECT pd.*, p.*, u.*, pp.*,
                pdelivered.waybill_number ,pdelivered.remarks as deliveredRemakrs, pdelivered.deliveredto as deliveredTo,
                pdelivered.date_created as deliveredCreated, pdelivered.status
                FROM picklist_detail pd 
                LEFT JOIN picklist_particular pp ON pp.id = pd.picklist_particular_id 
                LEFT JOIN picklist p ON pp.picklist_aggregate_id = p.id
                LEFT JOIN user u ON p.created_by = u.id
                LEFT JOIN picklist_delivered pdelivered ON pp.id = pdelivered.picklist_particular_id 
                WHERE pd.id = :pdId LIMIT 1
            ";

            $picklist = $this->conn->fetchAll($sql,['pdId' => $detail['id']]);

            if ($picklist) {
                
                $details = json_decode($picklist[0]['order_details'],true);
                $status = '';
                if ($picklist[0]['status'] == 1) {
                    $status = 'INTRANSIT';
                } else if ($picklist[0]['status'] == 2) {
                    $status = 'DELIVERED';
                } else if ($picklist[0]['status'] == 3) {
                    $status = 'RELEASED';
                }

                $attachment = '';
                if ($picklist[0]['file']) {
                    $attachment = 'https://wms.lmbmlogistics.com/documents/'.$picklist[0]['file'];
                }

                $data = [
                    'picklistNumber'    => $picklist[0]['picklist_number'],
                    'createdBy'         => $picklist[0]['last_name'] . ' ' . $picklist[0]['first_name'],
                    'datePicklisted'    => $picklist[0]['date_created'],
                    'consignee'         => $details['consignee'],
                    'address'           => $details['consignee_address'],
                    'waybill_number'    => $picklist[0]['waybill_number'],
                    'remarks'           => $picklist[0]['deliveredRemakrs'],
                    'deliveredto'       => $picklist[0]['deliveredTo'],
                    'deliveredCreated'  => $picklist[0]['deliveredCreated'],
                    'status'            => $status,
                    'picklistParticularNumber' => $picklist[0]['picklist_particular_number'],
                    'attachment'        => $attachment
                ];

                return $data;
            }
        }

        return [
            'picklistNumber' => '',
            'createdBy' =>      '',
            'datePicklisted' => '',
            'details' =>        '',
            'consignee' =>      '',
            'address' =>        '',
            'waybill_number'    => '',
            'remarks'           => '',
            'deliveredto'       => '',
            'deliveredCreated'  => '',
            'status'            => '',
            'picklistParticularNumber'  => '',
            'attachment'        => ''
        ];


    }
}
