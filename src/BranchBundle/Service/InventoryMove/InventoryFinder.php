<?php

namespace BranchBundle\Service\InventoryMove;

use CoreBundle\Repository\InventoryRepository;
use CoreBundle\Repository\MobileLocationRepository;

class InventoryFinder
{
    private $inventoryRepository;
    const TAG_SINGLE = 'single';
    const TAG_MULTIPLE = 'multiple';

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function getInventoryDetailsAsArrayByQueryTag($query)
    {
        $returnData = [
            'isSuccessful' => false
        ];

        $inventory = $this->inventoryRepository
            ->findOneByTagForMovingAsArray($query);

        if(count($inventory) === 1){
            $returnData['isSuccessful'] = true;
            $returnData['data'] = $inventory;
            $returnData['type'] = self::TAG_SINGLE;
            return $returnData;
        }

        $inventories = $this->inventoryRepository
            ->findByMobileLocationNameForMovingAsArray($query);

        if(count($inventories) >= 1){
            $returnData['isSuccessful'] = true;
            $returnData['data'] = $inventories;
            $returnData['type'] = self::TAG_MULTIPLE;
            return $returnData;
        }

        return $returnData;
    }

    public function getInventoryTagsAsArrayByQueryTag($query)
    {
        $returnData = $this->getInventoryDetailsAsArrayByQueryTag($query);
        if(!$returnData['isSuccessful']){
            return $returnData;
        }

        $returnData['data'] = array_map(function($data){
            return $data['tag'];
        }, $returnData['data']);

        return $returnData;
    }
}
