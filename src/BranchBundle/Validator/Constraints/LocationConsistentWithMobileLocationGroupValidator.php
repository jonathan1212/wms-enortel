<?php

namespace BranchBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use CoreBundle\Repository\InventoryRepository;
use Symfony\Component\Validator\ConstraintValidator;

class LocationConsistentWithMobileLocationGroupValidator extends ConstraintValidator
{
    private $inventoryRepository;

    public function __construct(InventoryRepository $inventoryRepository)
    {
        $this->inventoryRepository = $inventoryRepository;
    }

    public function validate($inventoryMove, Constraint $constraint)
    {
        if(!$inventoryMove->getMobileLocation()){
            return;
        }

        $inventoryMember = $this->inventoryRepository->getOneMemberOfMobileLocationAsArray(
            $inventoryMove->getMobileLocation()->getName()
        );

        if(count($inventoryMember) === 0){
            return;
        }

        $inventoryMember = $inventoryMember[0];
        if($inventoryMove->getStorageLocation()->getName() !== $inventoryMember['storageLocation']['name']){
            $this->context->buildViolation($constraint->message)
                ->setParameter('%mobile_location%', $inventoryMember['mobileLocation']['name'])
                ->setParameter('%storage_location%', $inventoryMember['storageLocation']['name'])
                ->addViolation();
        }
    }
}
