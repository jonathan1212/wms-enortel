<?php

namespace BranchBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class LocationConsistentWithMobileLocationGroup extends Constraint
{
    public $message = 'Current storage location of "%mobile_location%" is "%storage_location%".';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
