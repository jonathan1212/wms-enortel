<?php

namespace BranchBundle\Validator\Constraints;

use CoreBundle\Repository\UserRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueClientUserEmailAddressValidator extends ConstraintValidator
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function validate($user, Constraint $constraint)
    {
        $parentClient = $user->getClientAccount();
        $clientUser = $this->userRepository->findOneBy([
            'clientAccount' => $user->getClientAccount(),
            'emailAddress' => $user->getEmailAddress()
        ]);

        // If no user is found
        if($clientUser === null){
            return;
        }

        // If same user is retrieved
        if($user->getId() === $clientUser->getId()){
            return;
        }

        // Notify error, has duplicate
        $this->context->buildViolation($constraint->message)
            ->atPath('emailAddress')
            ->addViolation();
    }
}
