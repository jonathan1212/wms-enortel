<?php

namespace BranchBundle\Eventing\Inbounding;

class PurchaseOrderInboundedEvent
{
    private $purchaseOrderNumber;
    private $inboundDetails;
    private $warehouseId;

    public function __construct($purchaseOrderNumber, $inboundDetails, $warehouseId)
    {
        $this->purchaseOrderNumber = $purchaseOrderNumber;
        $this->inboundDetails = $inboundDetails;
        $this->warehouseId = $warehouseId;
    }

    /**
     * Gets the value of purchaseOrderNumber.
     *
     * @return mixed
     */
    public function getPurchaseOrderNumber()
    {
        return $this->purchaseOrderNumber;
    }

    /**
     * Gets the value of inboundDetails.
     *
     * @return mixed
     */
    public function getInboundDetails()
    {
        return $this->inboundDetails;
    }

    /**
     * Gets the value of warehouseId.
     *
     * @return mixed
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }
}
