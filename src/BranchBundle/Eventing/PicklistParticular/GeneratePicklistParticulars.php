<?php

namespace BranchBundle\Eventing\PicklistParticular;

use Doctrine\ORM\EntityManager;
use CoreBundle\Entity\PicklistParticular;
use BranchBundle\Eventing\Picklist\PicklistGeneratedEvent;

class GeneratePicklistParticulars
{
    private $em;
    private $picklistParticularRepository;
    private $picklistRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->picklistParticularRepository = $em->getRepository('CoreBundle:PicklistParticular');
        $this->picklistRepository = $em->getRepository('CoreBundle:Picklist');
    }

    public function notify(PicklistGeneratedEvent $event)
    {
        $this->persistPicklistParticulars(
            $event->getCompletePicklistNumbers()
        );
    }

    private function persistPicklistParticulars($picklistNumbers)
    {
        $iterable = $this->picklistRepository
            ->getPicklistsAsIterable(
                $picklistNumbers
            );

        foreach($iterable as $row){
            $picklist = $row[0];
            $picklistNumber = $picklist->getPicklistNumber();
            $picklistStatus = $picklist->getPicklistStatus();
            $warehouse = $picklist->getWarehouse();
            $client = $picklist->getClient();
            $picklistDetailsChunk = array_chunk(
                $picklist->getInventoryListing()->toArray(), 
                \CoreBundle\Entity\Picklist::PRINT_AGGREGATE_COUNT
            );

            $serial = 1;
            foreach($picklistDetailsChunk as $picklistDetails){
                $picklistParticular = new PicklistParticular();
                $picklistParticular->setPicklistStatus($picklistStatus)
                    ->setWarehouse($warehouse)
                    ->setClient($client)
                    ->setPicklistParticularNumber(
                        sprintf('%s-%s', $picklistNumber, $serial)
                    );

                $this->em->persist($picklistParticular);
                foreach($picklistDetails as $picklistDetail){
                    $picklistParticular->addItem($picklistDetail);
                }
                $picklist->addParticular($picklistParticular);
                ++$serial;
            }
        }
        $this->em->flush();
        $this->em->clear();
    }
}
