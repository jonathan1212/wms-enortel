<?php

namespace BranchBundle\Eventing\Picklist;

class PicklistsPrintedEvent
{
    private $picklistNumbers;

    public function __construct($picklistNumbers)
    {
        $this->picklistNumbers = $picklistNumbers;
    }

    /**
     * Gets the value of picklistNumbers.
     *
     * @return mixed
     */
    public function getPicklistNumbers()
    {
        return $this->picklistNumbers;
    }
}
