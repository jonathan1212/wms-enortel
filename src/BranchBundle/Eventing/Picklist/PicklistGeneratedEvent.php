<?php

namespace BranchBundle\Eventing\Picklist;

class PicklistGeneratedEvent
{
    private $completePicklistNumbers;
    private $incompletePicklistNumbers;

    public function __construct($completePicklistNumbers, $incompletePicklistNumbers)
    {
        $this->completePicklistNumbers = $completePicklistNumbers;
        $this->incompletePicklistNumbers = $incompletePicklistNumbers;
    }

    /**
     * Gets the value of completePicklistNumbers.
     *
     * @return mixed
     */
    public function getCompletePicklistNumbers()
    {
        return $this->completePicklistNumbers;
    }

    /**
     * Gets the value of incompletePicklistNumbers.
     *
     * @return mixed
     */
    public function getIncompletePicklistNumbers()
    {
        return $this->incompletePicklistNumbers;
    }
}
