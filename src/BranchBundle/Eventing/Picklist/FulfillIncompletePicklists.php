<?php

namespace BranchBundle\Eventing\Picklist;

use Doctrine\ORM\EntityManager;
use CoreBundle\Entity\PicklistDetail;
use BranchBundle\Eventing\Inventory\InventoryGeneratedEvent;
use SimpleBus\Message\Bus\Middleware\MessageBusSupportingMiddleware;

class FulfillIncompletePicklists
{
    private $em;
    private $picklistRepository;
    private $inventoryRepository;
    private $inventoryStatusRepository;

    /**
     * @link https://github.com/SimpleBus/MessageBus/issues/26
     */
    private $eventBus;

    public function __construct(MessageBusSupportingMiddleware $eventBus, EntityManager $em)
    {
        $this->eventBus = $eventBus;
        $this->em = $em;
        $this->picklistRepository = $em->getRepository('CoreBundle:Picklist');
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
    }

    public function notify(InventoryGeneratedEvent $event)
    {
        $picklistsCompleted = [];
        $newInventorySummary = $event->getGeneratedInventoryForAllocation();
        $incompletePicklists = $this->picklistRepository
            ->getIncompletePicklistsWithRequiredInventoryLeftAsArray();

        $picklistMapping = $this->mapNewInventoryToIncompletePicklists($newInventorySummary, $incompletePicklists);
        foreach($picklistMapping as $picklistNumber => $newPicklistDetail){
            $picklist = $this->picklistRepository->findOneByPicklistNumber($picklistNumber);
            $picklist->setIsComplete($newPicklistDetail['isComplete']);
            $allocatedStatus = $this->inventoryStatusRepository->findOneByValue(\CoreBundle\Entity\InventoryStatus::ALLOCATED);
            foreach($newPicklistDetail['newInventory'] as $clientSku => $newInventoryTags){
                foreach($newInventoryTags as $tag){
                    $inventory = $this->inventoryRepository->findOneByTag($tag);
                    $picklistDetail = new PicklistDetail();
                    $picklistDetail->setInventory($inventory);
                    $inventory->setInventoryStatus($allocatedStatus);
                    $this->em->persist($picklistDetail);

                    $picklist->addInventoryListing($picklistDetail);
                }
                $this->em->flush();
            }
            $this->em->flush();
            $this->em->clear();

            if($newPicklistDetail['isComplete']){
                $picklistsCompleted[] = $picklist->getPicklistNumber();
            }
        }

        if(count($picklistsCompleted) > 0){
            $this->eventBus->handle(
                new \BranchBundle\Eventing\Picklist\PicklistGeneratedEvent(
                    $picklistsCompleted,
                    []
                )
            );
        }
    }

    private function mapNewInventoryToIncompletePicklists($newInventory, $incompletePicklists)
    {
        $inventoryToPicklistMapping = [];
        foreach($incompletePicklists as $picklistNumber => $picklistOrderProducts){
            if(!array_key_exists($picklistNumber, $inventoryToPicklistMapping)){
                $inventoryToPicklistMapping[$picklistNumber] = [
                    'isComplete' => false,
                    'newInventory' => []
                ];
            }

            foreach($newInventory as $clientSku => $inventoryTags){
                if(!array_key_exists($clientSku, $incompletePicklists[$picklistNumber])){
                    continue;
                }

                $quantityNeeded = $incompletePicklists[$picklistNumber][$clientSku];
                if($quantityNeeded === 0){
                    continue;
                }

                $inventoryCount = count($newInventory[$clientSku]);
                while($inventoryCount > 0 && $quantityNeeded > 0){
                    $inventoryToPicklistMapping[$picklistNumber]['newInventory'][$clientSku][] = $newInventory[$clientSku][0];

                    // Invariance
                    $inventoryCount--;
                    $quantityNeeded--;

                    // Reset keys
                    unset($newInventory[$clientSku][0]);
                    $newInventory[$clientSku] = array_values($newInventory[$clientSku]);

                    // Incomplete Picklist Copy
                    $incompletePicklists[$picklistNumber][$clientSku]--;
                }
            }
        }

        // Check if any picklist is now complete
        foreach($incompletePicklists as $picklistNumber => $picklistOrderProducts){
            $isComplete = true;
            foreach($picklistOrderProducts as $clientSku => $quantityNeeded){
                if($quantityNeeded > 0){
                    $isComplete = false;
                    break;
                }
            }
            $inventoryToPicklistMapping[$picklistNumber]['isComplete'] = $isComplete;
        }

        return $inventoryToPicklistMapping;
    }
}
