<?php

namespace BranchBundle\Eventing\Picklist;

use CoreBundle\Entity\PicklistImportHistory;

class RequestForPicklistGenerationEvent
{
    private $picklistImportHistory;

    public function __construct(PicklistImportHistory $picklistImportHistory)
    {
        $this->picklistImportHistory = $picklistImportHistory;
    }

    /**
     * Gets the value of picklistImportHistory.
     *
     * @return mixed
     */
    public function getPicklistImportHistory()
    {
        return $this->picklistImportHistory;
    }
}
