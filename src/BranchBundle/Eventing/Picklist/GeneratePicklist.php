<?php

namespace BranchBundle\Eventing\Picklist;

use CoreBundle\Entity\Picklist;
use Doctrine\ORM\EntityManager;
use CoreBundle\Entity\PicklistDetail;
use CoreBundle\Entity\PicklistImportHistory;
use Symfony\Component\HttpFoundation\Session\Session;
use BranchBundle\Service\Picklist\PicklistNumberGenerator;
use SimpleBus\Message\Bus\Middleware\MessageBusSupportingMiddleware;
use BranchBundle\Eventing\Picklist\RequestForPicklistGenerationEvent;

class GeneratePicklist
{
    private $em;
    private $inventoryRepository;
    private $warehouseRepository;
    private $clientRepository;
    private $generator;
    private $inventoryStatusRepository;
    private $picklistStatusRepository;
    private $session;
    private $blameableUser;

    /**
     * @link https://github.com/SimpleBus/MessageBus/issues/26
     */
    private $eventBus;

    public function __construct(
        MessageBusSupportingMiddleware $eventBus,
        EntityManager $em, 
        PicklistNumberGenerator $generator, 
        Session $session,
        $blameableUser
    )
    {
        $this->eventBus = $eventBus;
        $this->em = $em;
        $this->generator = $generator;
        $this->session = $session;
        $this->clientRepository = $em->getRepository('CoreBundle:Client');
        $this->picklistStatusRepository = $em->getRepository('CoreBundle:PicklistStatus');
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
        $this->warehouseRepository = $em->getRepository('CoreBundle:Warehouse');
        $this->blameableUser = $blameableUser;
    }

    public function notify(RequestForPicklistGenerationEvent $event)
    {
        // Disable warehouse aware filter since this event can span multiple warehouses
        $reEnableFilter = false;
        try{
            $this->em->getFilters()->disable('warehouseFilter');
            $reEnableFilter = true;
        } catch (\InvalidArgumentException $e){
            // nothing to see here
        }

        $picklistHistory = $event->getPicklistImportHistory();
        $picklistSummary = $this->persistNewPicklists(
            $picklistHistory->getId(),
            $picklistHistory->getData()['orders'],
            $picklistHistory->getData()['client_id']
        );

        $this->session->getFlashBag()->set('unservedOrders', $picklistSummary['incomplete']);
        $this->eventBus->handle(
            new \BranchBundle\Eventing\Picklist\PicklistGeneratedEvent(
                $picklistSummary['complete'],
                $picklistSummary['incomplete']
            )
        );

        // Re-enable warehouse filter for other events that may use it
        if($reEnableFilter){
            $this->em->getFilters()->enable('warehouseFilter');
        }


    }

    private function persistNewPicklists($picklistHistoryId, $importOrders, $clientId)
    {
        $picklistSummary = [
            'complete' => [],
            'incomplete' => []
        ];

        foreach($importOrders as $orderNumber => $details){
            $isQuarantine = $details['isQuarantine'];

            $picklist = new Picklist();
            $picklistNumber = $this->generator->generate();
            $deps = $this->getEntityDependencies($details['warehouse'], $clientId);
            $isComplete = $this->canOrderBeFulfilled($details['products'], $deps['warehouse'], $isQuarantine);
            $orderDetails = [
                'picklistImportHistoryId' => $picklistHistoryId,
                'products' => $details['products'],
                'srps' => $details['srps'],
                'consignee' => $details['consignee'],
                'consignee_address' => $details['consignee_address'],
                'contact_number' => $details['contact_number'],
                'payment_method' => $details['payment_method'],
                'serial_no' => $details['serial_no']
            ];
            $serialNo = $details['serial_no'];
            

            $picklist->setPicklistNumber($picklistNumber)
                ->setOrderNumber($orderNumber)
                ->setClient($deps['client'])
                ->setWarehouse($deps['warehouse'])
                ->setPicklistStatus($deps['picklistCreatedStatus'])
                ->setOrderDetails($orderDetails)
                ->setCreatedBy($this->blameableUser->getBlameable())
                ->setIsComplete($isComplete);

            $this->em->persist($picklist);

            // Assemble picklist detail here
            // 1. Retrieve iterable for all available products
            foreach($details['products'] as $clientSku => $requiredQuantity){
                $iterableInventory = $this->inventoryRepository
                    ->getAvailableInventoryOfClientSkuAsIterable($clientSku, $deps['warehouse'], $serialNo,$isQuarantine);
                // 2. Apply each product iterable to create picklist detail
                $i = 0;
                foreach($iterableInventory as $row){
                    $inventory = $row[0];
                    $picklistDetail = new PicklistDetail();
                    $picklistDetail->setInventory($inventory);
                    $inventory->setInventoryStatus($deps['allocatedStatus']);
                    $this->em->persist($picklistDetail);

                    $picklist->addInventoryListing($picklistDetail);

                    ++$i;
                    if($i >= $requiredQuantity){
                        break;
                    }
                }

                $this->em->flush();
            }

            $this->em->flush();
            //$this->em->clear();

            $picklistSummary[($isComplete ? 'complete' : 'incomplete')][] = $isComplete ? $picklistNumber : 
                sprintf('%s - %s', $picklistNumber, $deps['warehouse']->getName());
        }

        return $picklistSummary;
    }

    private function getEntityDependencies($warehouseId, $clientId)
    {
        return [
            'warehouse' => $this->warehouseRepository->findOneByName($warehouseId), // depends on the agreed upon identifier on import (name as of now)
            'client' => $this->clientRepository->find($clientId),
            'allocatedStatus' => $this->inventoryStatusRepository->findOneByValue(\CoreBundle\Entity\InventoryStatus::ALLOCATED),
            'picklistCreatedStatus' => $this->picklistStatusRepository->findOneByValue(\CoreBundle\Entity\PicklistStatus::CREATED)
        ];
    }

    /**
     * Determines if current state of inventory can fulfill a particular order
     * @param  array $orderProducts
     * @return boolean
     */
    private function canOrderBeFulfilled($orderProducts, $warehouse, $isQuarantine=false)
    {
        $inventory = $this->inventoryRepository
            ->getAvailableInventoryCountOfClientSkusAsArray(array_keys($orderProducts), $warehouse, $isQuarantine);

        foreach($orderProducts as $clientSku => $requiredQuantity){
            if(!array_key_exists($clientSku, $inventory)){
                return false;
            }

            if($inventory[$clientSku] < $requiredQuantity){
                return false;
            }
        }

        return true;
    }
}
