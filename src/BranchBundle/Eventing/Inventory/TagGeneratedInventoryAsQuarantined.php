<?php

namespace BranchBundle\Eventing\Inventory;

use Doctrine\ORM\EntityManager;
use BranchBundle\Eventing\Inventory\InventoryGeneratedEvent;

class TagGeneratedInventoryAsQuarantined
{
    private $batchSize = 30;
    private $inventoryRepository;
    private $inventoryStatusRepository;
    private $em;

    public function __construct(
        EntityManager $em
    )
    {
        $this->em = $em;
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
    }

    public function notify(InventoryGeneratedEvent $event)
    {
        $forQuarantine = $event->getGeneratedInventoryForQuarantine();
        $tags = [];

        foreach($forQuarantine as $clientSku => $tags){
            $tags[] = $tags;
        }

        $inventoryIterable = $this->inventoryRepository
            ->getInventoryByTagsAsIterable($tags);

        $quarantinedStatus = $this->inventoryStatusRepository
            ->findOneByValue(\CoreBundle\Entity\InventoryStatus::QUARANTINED);

        $i = 0;
        foreach($inventoryIterable as $row){
            $inventory = $row[0];
            $inventory->setInventoryStatus($quarantinedStatus);
            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $quarantinedStatus = $this->inventoryStatusRepository
                    ->findOneByValue(\CoreBundle\Entity\InventoryStatus::QUARANTINED);
            }
            ++$i;
        }

        $this->em->flush();
        $this->em->clear();
    }
}
