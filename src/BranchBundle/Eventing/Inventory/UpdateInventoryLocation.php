<?php

namespace BranchBundle\Eventing\Inventory;

use Doctrine\ORM\EntityManager;
use BranchBundle\Service\InventoryMove\InventoryFinder;
use BranchBundle\Eventing\InventoryMove\InventoryMovedEvent;

class UpdateInventoryLocation
{
    private $batchSize = 30;

    private $inventoryRepository;
    private $storageLocationRepository;
    private $mobileLocationRepository;
    private $em;
    private $finder;

    public function __construct(EntityManager $em, InventoryFinder $finder)
    {
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->storageLocationRepository = $em->getRepository('CoreBundle:StorageLocation');
        $this->mobileLocationRepository = $em->getRepository('CoreBundle:MobileLocation');
        $this->em = $em;
        $this->finder = $finder;
    }

    /**
     * @todo Can be moved to a DQL update if this becomes a bottleneck.
     * You might need to investigate the loggable trigger when you do this though
     */
    public function notify(InventoryMovedEvent $event)
    {
        $returnData = $this->finder
            ->getInventoryTagsAsArrayByQueryTag($event->getTarget());

        if(!$returnData['isSuccessful']){
            throw new \UnexpectedValueException("Invalid inventory tag or empty container");
        }

        $mobileLocation = $this->getCorrectMobileLocation($event, $returnData);
        $deps = $this->getEntityDependencies(
            $event->getStorageLocation(),
            $mobileLocation
        );

        $i = 0;
        $iterableInventory = $this->inventoryRepository
            ->getInventoriesForLocationUpdateByTagsAsIterable($returnData['data']);

        foreach($iterableInventory as $row){
            $inventory = $row[0];
            $inventory->setStorageLocation($deps['storageLocation'])
                ->setMobileLocation($deps['mobileLocation']);

            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $deps = $this->getEntityDependencies(
                    $event->getStorageLocation(),
                    $mobileLocation
                );
            }
            ++$i;
        }
        $this->em->flush();
    }

    private function getCorrectMobileLocation(InventoryMovedEvent $event, $inventoryTags)
    {
        return $inventoryTags['type'] === InventoryFinder::TAG_MULTIPLE ? $event->getTarget() : $event->getMobileLocation();
    }

    private function getEntityDependencies($storageLocation, $mobileLocation)
    {
        return [
            'storageLocation' => $this->storageLocationRepository
                ->findOneByName($storageLocation),
            'mobileLocation' => $this->mobileLocationRepository
                ->findOneByName($mobileLocation)
        ];
    }
}
