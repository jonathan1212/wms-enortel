<?php

namespace BranchBundle\Eventing\Inventory;

class InventoryGeneratedEvent
{
    private $generatedInventoryForAllocation;
    private $generatedInventoryForQuarantine;

    public function __construct($generatedInventoryForAllocation, $generatedInventoryForQuarantine)
    {
        $this->generatedInventoryForAllocation = $generatedInventoryForAllocation;
        $this->generatedInventoryForQuarantine = $generatedInventoryForQuarantine;
    }

    /**
     * Gets the value of generatedInventoryForAllocation.
     *
     * @return mixed
     */
    public function getGeneratedInventoryForAllocation()
    {
        return $this->generatedInventoryForAllocation;
    }

    /**
     * Gets the value of generatedInventoryForQuarantine.
     *
     * @return mixed
     */
    public function getGeneratedInventoryForQuarantine()
    {
        return $this->generatedInventoryForQuarantine;
    }
}
