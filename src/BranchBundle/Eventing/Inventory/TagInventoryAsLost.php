<?php

namespace BranchBundle\Eventing\Inventory;

use Doctrine\ORM\EntityManager;
use BranchBundle\Eventing\Pick\PicklistTaggedForPackingEvent;

class TagInventoryAsLost 
{
    private $batchSize = 30;
    private $em;
    private $picklistParticularRepository;
    private $inventoryRepository;
    private $inventoryStatusRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->picklistParticularRepository = $em->getRepository('CoreBundle:PicklistParticular');
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
    }

    public function notify(PicklistTaggedForPackingEvent $event)
    {
        $tags = $this->picklistParticularRepository
            ->getInventoryTagsOfPicklistParticularsAsArray([$event->getPicklistNumber()]);

        $inventoryIterable = $this->inventoryRepository
            ->getInventoryToBeTaggedAsLostByTagsAsIterables($tags);

        $inventoryLostStatus = $this->inventoryStatusRepository
            ->findOneByValue(\CoreBundle\Entity\InventoryStatus::LOST);

        $i = 0;
        foreach($inventoryIterable as $row){
            $inventory = $row[0];
            $inventory->setInventoryStatus($inventoryLostStatus);

            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $inventoryLostStatus = $this->inventoryStatusRepository
                    ->findOneByValue(\CoreBundle\Entity\InventoryStatus::LOST);
            }
            ++$i;
        }

        $this->em->flush();
        $this->em->clear();
    }
}
