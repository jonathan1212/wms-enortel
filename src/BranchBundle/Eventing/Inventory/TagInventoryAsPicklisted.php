<?php

namespace BranchBundle\Eventing\Inventory;

use Doctrine\ORM\EntityManager;
use BranchBundle\Eventing\Picklist\PicklistsPrintedEvent;

class TagInventoryAsPicklisted
{
    private $batchSize = 30;
    private $em;
    private $inventoryRepository;
    private $picklistParticularRepository;
    private $inventoryStatusRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->picklistParticularRepository = $em->getRepository('CoreBundle:PicklistParticular');
        $this->inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
    }

    public function notify(PicklistsPrintedEvent $event)
    {
        $tags = $this->picklistParticularRepository
            ->getInventoryTagsOfPicklistParticularsAsArray($event->getPicklistNumbers());

        $inventoryIterable = $this->inventoryRepository
            ->getInventoryByTagsAsIterable($tags);

        $picklistedStatus = $this->inventoryStatusRepository->findOneByValue(
            \CoreBundle\Entity\InventoryStatus::PICKLISTED
        );

        $i = 0;
        foreach($inventoryIterable as $row){
            $inventory = $row[0];
            $inventory->setInventoryStatus($picklistedStatus);

            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $picklistedStatus = $this->inventoryStatusRepository->findOneByValue(
                    \CoreBundle\Entity\InventoryStatus::PICKLISTED
                );
            }

            ++$i;
        }

        $this->em->flush();
        $this->em->clear();
    }
}
