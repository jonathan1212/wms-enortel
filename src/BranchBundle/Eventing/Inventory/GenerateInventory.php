<?php

namespace BranchBundle\Eventing\Inventory;

use Doctrine\ORM\EntityManager;
use CoreBundle\Entity\Inventory;
use Symfony\Component\HttpFoundation\Session\Session;
use BranchBundle\Service\Inventory\InventoryTagGenerator;
use BranchBundle\Eventing\Inbounding\PurchaseOrderInboundedEvent;
use SimpleBus\Message\Bus\Middleware\MessageBusSupportingMiddleware;

/**
 * @todo May need to be offloaded in a queue if this becomes a bottleneck in the future
 */
class GenerateInventory
{
    private $batchSize = 30;

    private $inventoryRepository;
    private $purchaseOrderRepository;
    private $warehouseRepository;
    private $productRepository;
    private $storageLocationRepository;
    private $mobileLocationRepository;
    private $em;
    private $generator;
    private $session;

    /**
     * @link https://github.com/SimpleBus/MessageBus/issues/26
     */
    private $eventBus;

    public function __construct(
        MessageBusSupportingMiddleware $eventBus,
        EntityManager $em,
        InventoryTagGenerator $generator,
        Session $session
    )
    {
        $this->em = $em;
        $this->generator = $generator;
        $this->eventBus = $eventBus;
        $this->session = $session;
        $this->productRepository = $em->getRepository('CoreBundle:Product');
        $this->warehouseRepository = $em->getRepository('CoreBundle:Warehouse');
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->purchaseOrderRepository = $em->getRepository('CoreBundle:PurchaseOrder');
        $this->mobileLocationRepository = $em->getRepository('CoreBundle:MobileLocation');
        $this->storageLocationRepository = $em->getRepository('CoreBundle:StorageLocation');
    }

    public function notify(PurchaseOrderInboundedEvent $event)
    {
        $inventorySummary = $this->persistNewInventory(
            $event->getInboundDetails(),
            $event->getWarehouseId(),
            $event->getPurchaseOrderNumber()
        );

        $this->session->getFlashBag()->set('tagsGenerated', $inventorySummary['tagsGenerated']);
        $this->eventBus->handle(
            new \BranchBundle\Eventing\Inventory\InventoryGeneratedEvent(
                $inventorySummary['forAllocation'],
                $inventorySummary['forQuarantine']
            )
        );
    }

    private function persistNewInventory($inboundDetails, $warehouseId, $purchaseOrderNumber)
    {
        $inventorySummary = [
            'forQuarantine' => [],
            'forAllocation' => [],
            'tagsGenerated' => []
        ];

        foreach($inboundDetails as $productSku => $details){
            if(!$details['receivedQuantity']){
                continue;
            }

            $category = filter_var($details['forQuarantine'], FILTER_VALIDATE_BOOLEAN) ? 'forQuarantine' : 'forAllocation';
            $dependencies = $this->getEntityDependencies(
                $warehouseId,
                $productSku,
                $purchaseOrderNumber,
                $details['storageLocation'],
                $details['mobileLocation']
            );

            $inventorySummary[$category][$details['clientSku']] = [];
            for ($i=1; $i <= $details['receivedQuantity']; ++$i) {
                $inventoryTag = $this->generator->generate();
                $inventory = new Inventory();
                $inventory->setTag($inventoryTag)
                    ->setCostOfGoodsPurchased($details['costOfGoodsPurchased'])
                    ->setStorageLocation($dependencies['storageLocation'])
                    ->setMobileLocation($dependencies['mobileLocation'])
                    ->setProduct($dependencies['product'])
                    ->setPurchaseOrder($dependencies['purchaseOrder'])
                    ->setClient($dependencies['purchaseOrder']->getClient())
                    ->setWarehouse($dependencies['warehouse']);

                $this->em->persist($inventory);
                if(($i % $this->batchSize) === 0){
                    // Flush and Clear everything
                    $this->em->flush();
                    $this->em->clear();

                    // Regenerate entity dependencies
                    $dependencies = $this->getEntityDependencies(
                        $warehouseId,
                        $productSku,
                        $purchaseOrderNumber,
                        $details['storageLocation'],
                        $details['mobileLocation']
                    );
                }

                $inventorySummary[$category][$details['clientSku']][] = $inventoryTag;
                $inventorySummary['tagsGenerated'][] = $inventoryTag;
            }

            $this->em->flush();
            $this->em->clear();
        }

        return $inventorySummary;
    }

    private function getEntityDependencies
    (
        $warehouseId, 
        $productSku, 
        $purchaseOrderNumber, 
        $storageLocation, 
        $mobileLocation
    )
    {
        return [
            'warehouse' => $this->warehouseRepository->find($warehouseId),
            'product' => $this->productRepository->findOneBySku($productSku),
            'storageLocation' => $this->storageLocationRepository->findOneByName($storageLocation),
            'mobileLocation' => $this->mobileLocationRepository->findOneByName($mobileLocation),
            'purchaseOrder' => $this->purchaseOrderRepository
                ->findOneByPurchaseOrderNumber($purchaseOrderNumber)
        ];
    }    
}
