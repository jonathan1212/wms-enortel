<?php

namespace BranchBundle\Eventing\Inventory;

use Doctrine\ORM\EntityManager;
use BranchBundle\Eventing\Pack\PicklistTaggedForDispatchingEvent;

class TagInventoryAsDispatched
{
    private $batchSize = 30;
    private $em;
    private $inventoryStatusRepository;
    private $picklistDetailRepository;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
        $this->picklistDetailRepository = $em->getRepository('CoreBundle:PicklistDetail');
    }

    public function notify(PicklistTaggedForDispatchingEvent $event)
    {   
        $detailIterable = $this->picklistDetailRepository
            ->getPicklistDetailsByPicklistNumberAndInventoryStatusAsIterable(
                $event->getPicklistNumber(),
                \CoreBundle\Entity\InventoryStatus::PACKED
            );

        $dispatchedStatus = $this->inventoryStatusRepository
            ->findOneByValue(\CoreBundle\Entity\InventoryStatus::DISPATCHED);

        $i = 0;
        foreach($detailIterable as $row){
            $inventory = $row[0]->getInventory();
            $inventory->setInventoryStatus($dispatchedStatus);
            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $dispatchedStatus = $this->inventoryStatusRepository
                    ->findOneByValue(\CoreBundle\Entity\InventoryStatus::DISPATCHED);
            }
            ++$i;
        }

        $this->em->flush();
        $this->em->clear();
    }
}
