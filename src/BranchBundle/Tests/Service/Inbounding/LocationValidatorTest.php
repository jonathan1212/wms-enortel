<?php

namespace BranchBundle\Tests\Service\Inbounding;

use BranchBundle\Service\Inbounding\LocationValidator;

class LocationValidatorTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function should_fail_if_there_is_an_empty_storage_location_field()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "ML-123",
                "receivedQuantity" => "1"
            ],
            'PD-M9ECZB' => [
                "storageLocation" => "",
                "mobileLocation" => "ML-456",
                "receivedQuantity" => "1"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(false, $resultData['isSuccessful']);
        $this->assertEquals('Storage Location is required', $resultData['errorMessage']);
    }

    /** @test */
    public function should_succeed_even_if_there_is_an_empty_mobile_location_field()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "ML-123",
                "receivedQuantity" => "1"
            ],
            'PD-M9ECZB' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "",
                "receivedQuantity" => "1"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(true, $resultData['isSuccessful']);
    }

    /** @test */
    public function should_fail_if_the_submitted_data_has_a_non_existent_storage_location_name()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "ML-123",
                "receivedQuantity" => "1"
            ],
            'PD-M9ECZB' => [
                "storageLocation" => "SHIZAAAAAAAA",
                "mobileLocation" => "ML-456",
                "receivedQuantity" => "1"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(false, $resultData['isSuccessful']);
        $this->assertEquals('SHIZAAAAAAAA is not a valid Storage Location', $resultData['errorMessage']);
    }

    /** @test */
    public function should_ignore_entries_whose_received_quantity_is_zero()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SHIZAAAAA",
                "mobileLocation" => "NANIIIIIIIIII",
                "receivedQuantity" => ""
            ],
            'PD-M9ECZB' => [
                "storageLocation" => "ORYAAAAAAAAA",
                "mobileLocation" => "ORAORAORAORAORAORAORAORA",
                "receivedQuantity" => "0"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(true, $resultData['isSuccessful']);
    }

    /** @test */
    public function should_fail_if_the_submitted_data_has_a_non_existent_mobile_location_name()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "ML-123",
                "receivedQuantity" => "1"

            ],
            'PD-M9ECZB' => [
                "storageLocation" => "SL-789",
                "mobileLocation" => "ORAORAORAORAORAORAORAORA",
                "receivedQuantity" => "1"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(false, $resultData['isSuccessful']);
        $this->assertEquals('ORAORAORAORAORAORAORAORA is not a valid Mobile Location', $resultData['errorMessage']);
    }

    /** @test */
    public function should_fail_if_submitted_mobile_location_name_is_already_occupied()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "",
                "receivedQuantity" => "1"
            ],
            'PD-M9ECZB' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "ML-789",
                "receivedQuantity" => "1"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(false, $resultData['isSuccessful']);
        $this->assertEquals('ML-789 currently has contents. Please empty it first.', $resultData['errorMessage']);
    }

    /** @test */
    public function should_succeed_if_everything_goes_to_plan()
    {
        $storageLocationRepository = $this->getStorageLocationRepositoryMock();
        $mobileLocationRepository = $this->getMobileLocationRepositoryMock();
        $inventoryRepository = $this->getInventoryRepositoryMock();

        $testDetails = [
            'PD-DGWVT3' => [
                "storageLocation" => "SL-123",
                "mobileLocation" => "ML-456",
                "receivedQuantity" => "1"
            ],
            'PD-M9ECZB' => [
                "storageLocation" => "SL-789",
                "mobileLocation" => "ML-123",
                "receivedQuantity" => "1"
            ]
        ];

        $validator = new LocationValidator(
            $storageLocationRepository,
            $mobileLocationRepository,
            $inventoryRepository
        );

        $resultData = $validator->validate($testDetails);
        $this->assertEquals(true, $resultData['isSuccessful']);
    }

    private function getStorageLocationRepositoryMock()
    {
        $storageLocationRepository = $this
            ->getMockBuilder('\CoreBundle\Repository\StorageLocationRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $storageLocationRepository->expects($this->once())
            ->method('getAllStorageLocationNamesAsArray')
            ->will($this->returnValue(
                $this->getSampleStorageLocationNames()
            ));

        return $storageLocationRepository;
    }

    private function getMobileLocationRepositoryMock()
    {
        $mobileLocationRepository = $this
            ->getMockBuilder('\CoreBundle\Repository\MobileLocationRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $mobileLocationRepository->expects($this->once())
            ->method('getAllMobileLocationNamesAsArray')
            ->will($this->returnValue(
                $this->getSampleMobileLocationNames()
            ));

        return $mobileLocationRepository;
    }

    private function getInventoryRepositoryMock()
    {
        $mobileLocationRepository = $this
            ->getMockBuilder('\CoreBundle\Repository\InventoryRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $mobileLocationRepository->expects($this->once())
            ->method('getOccupiedMobileLocations')
            ->will($this->returnValue(
                $this->getSampleOccupiedMobileLocationNames()
            ));

        return $mobileLocationRepository;
    }

    private function getSampleStorageLocationNames()
    {
        return [
            'SL-123',
            'SL-456',
            'SL-789'
        ];
    }

    private function getSampleMobileLocationNames()
    {
        return [
            'ML-123',
            'ML-456',
            'ML-789'
        ];
    }

    private function getSampleOccupiedMobileLocationNames()
    {
        return [
            'ML-789'
        ];
    }
}
