<?php

namespace BranchBundle\Tests\Service\Inbounding;

use CoreBundle\Entity\PurchaseOrder;
use BranchBundle\Service\Inbounding\InboundHistoryProcessor;

class InboundHistoryProcessorTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function should_correctly_determine_current_inbound_state()
    {
        $inboundRepository = $this->getInboundRepositoryMock(
            'retrievePurchaseOrderInboundHistoryAsArray',
            'getSampleInboundHistory'
        );
        $inboundHistoryProcessor = new InboundHistoryProcessor(
            $inboundRepository
        );

        $currentInboundState = $inboundHistoryProcessor->computeCurrentPurchaseOrderInboundDetails(new PurchaseOrder());

        $this->assertEquals(45, $currentInboundState['PD-DGWVT3']['partialQuantity']);
        $this->assertEquals(60, $currentInboundState['PD-DGWVT3']['expectedQuantity']);
        $this->assertEquals(236, $currentInboundState['PD-DGWVT3']['costOfGoodsPurchased']);
        $this->assertEquals('SK-IW055K-WX1X-JGH', $currentInboundState['PD-DGWVT3']['supplierSku']);
        $this->assertEquals('CK-8AXRNCE-2BCQMO-BK', $currentInboundState['PD-DGWVT3']['clientSku']);

        $this->assertEquals(13, $currentInboundState['PD-M9ECZB']['partialQuantity']);
        $this->assertEquals(13, $currentInboundState['PD-M9ECZB']['expectedQuantity']);
        $this->assertEquals(712, $currentInboundState['PD-M9ECZB']['costOfGoodsPurchased']);
        $this->assertEquals('SK-04-04-UB', $currentInboundState['PD-M9ECZB']['supplierSku']);
        $this->assertEquals('CK-J-1ZRHN9-VCET', $currentInboundState['PD-M9ECZB']['clientSku']);
    }

    /** @test */
    public function should_be_able_handle_no_pre_existing_inbound_history()
    {
        $inboundRepository = $this->getInboundRepositoryMock('retrievePurchaseOrderInboundHistoryAsArray');
        $inboundHistoryProcessor = new InboundHistoryProcessor(
            $inboundRepository
        );

        $currentInboundState = $inboundHistoryProcessor->computeCurrentPurchaseOrderInboundDetails(new PurchaseOrder());
        $this->assertEquals([], $currentInboundState);
    }

    /** @test */
    public function should_retrieve_cost_of_goods_purchased_from_purchase_order_and_not_from_product()
    {
        $inboundRepository = $this->getInboundRepositoryMock();
        $purchaseOrderRepository = $this->getPurchaseOrderRepositoryMock();
        $inboundHistoryProcessor = new InboundHistoryProcessor(
            $inboundRepository,
            $purchaseOrderRepository
        );

        $listOfCostOfGoodsPurchased = $inboundHistoryProcessor->retrieveListOfCorrectCostOfGoodsPurchased(new PurchaseOrder());
        $this->assertEquals('111', $listOfCostOfGoodsPurchased['PD-DGWVT3']);
        $this->assertEquals('333', $listOfCostOfGoodsPurchased['PD-M9ECZB']);
    }

    private function getPurchaseOrderRepositoryMock()
    {
        $purchaseOrderRepository = $this
            ->getMockBuilder('\CoreBundle\Repository\PurchaseOrderRepository')
            ->disableOriginalConstructor()
            ->getMock();

        $purchaseOrderRepository->expects($this->once())
            ->method('getPurchaseOrderProductsAsArray')
            ->will($this->returnValue(
                $this->getSamplePurchaseOrderProducts()
            ));

        return $purchaseOrderRepository;
    }

    private function getInboundRepositoryMock($methodName = null, $dataSource = null)
    {
        $inboundRepository = $this
            ->getMockBuilder('\CoreBundle\Repository\InboundHistoryRepository')
            ->disableOriginalConstructor()
            ->getMock();

        if($methodName === null){
            return $inboundRepository;
        }

        $inboundRepository->expects($this->once())
            ->method($methodName)
            ->will($this->returnValue(
                $dataSource === null ? [] : $this->$dataSource()
            ));

        return $inboundRepository;
    }

    private function getSampleBaseInboundHistory()
    {
        return [
            [
                "details" => [
                    "PD-DGWVT3" => [
                        "costOfGoodsPurchased" => "987"
                    ],
                    "PD-M9ECZB" => [
                        "costOfGoodsPurchased" => "654"
                    ]
                ]
            ]
        ];
    }

    private function getSamplePurchaseOrderProducts()
    {
        return [
            "products" => [
                [
                    "product" => [
                        "sku" => "PD-DGWVT3",
                        "costOfGoodsPurchased" => "123"
                    ],
                    "costOfGoodsPurchased" => "111"
                ],
                [
                    "product" => [
                        "sku" => "PD-M9ECZB",
                        "costOfGoodsPurchased" => "456"
                    ],
                    "costOfGoodsPurchased" => "333"
                ]
            ]
        ];
    }

    private function getSampleInboundHistory()
    {
        return [
            [
                "details" => [
                    "PD-DGWVT3" => [
                        "supplierSku" => "SK-IW055K-WX1X-JGH",
                        "clientSku" => "CK-8AXRNCE-2BCQMO-BK",
                        "expectedQuantity" => "60",
                        "receivedQuantity" => "3",
                        "storageLocation" => "asdasd",
                        "productDescription" => "Autem inventore aut officia aut aut blanditiis.",
                        "costOfGoodsPurchased" => 236
                    ],
                    "PD-M9ECZB" => [
                        "supplierSku" => "SK-04-04-UB",
                        "clientSku" => "CK-J-1ZRHN9-VCET",
                        "expectedQuantity" => "13",
                        "receivedQuantity" => "2",
                        "storageLocation" => "qwdqwdqw",
                        "productDescription" => "Sequi quia quasi quae sint saepe numquam tempora et.",
                        "costOfGoodsPurchased" => 712
                    ],
                ]
            ],
            [
                "details" => [
                    "PD-DGWVT3" => [
                        "supplierSku" => "SK-IW055K-WX1X-JGH",
                        "clientSku" => "CK-8AXRNCE-2BCQMO-BK",
                        "expectedQuantity" => "60",
                        "receivedQuantity" => "5",
                        "storageLocation" => "asdasd",
                        "productDescription" => "Autem inventore aut officia aut aut blanditiis.",
                        "costOfGoodsPurchased" => 236
                    ],
                    "PD-M9ECZB" => [
                        "supplierSku" => "SK-04-04-UB",
                        "clientSku" => "CK-J-1ZRHN9-VCET",
                        "expectedQuantity" => "13",
                        "receivedQuantity" => "0",
                        "storageLocation" => "qwdqwdqw",
                        "productDescription" => "Sequi quia quasi quae sint saepe numquam tempora et.",
                        "costOfGoodsPurchased" => 712
                    ],
                ]
            ],
            [
                "details" => [
                    "PD-DGWVT3" => [
                        "supplierSku" => "SK-IW055K-WX1X-JGH",
                        "clientSku" => "CK-8AXRNCE-2BCQMO-BK",
                        "expectedQuantity" => "60",
                        "receivedQuantity" => "37",
                        "storageLocation" => "asdasd",
                        "productDescription" => "Autem inventore aut officia aut aut blanditiis.",
                        "costOfGoodsPurchased" => 236
                    ],
                    "PD-M9ECZB" => [
                        "supplierSku" => "SK-04-04-UB",
                        "clientSku" => "CK-J-1ZRHN9-VCET",
                        "expectedQuantity" => "13",
                        "receivedQuantity" => "5",
                        "storageLocation" => "qwdqwdqw",
                        "productDescription" => "Sequi quia quasi quae sint saepe numquam tempora et.",
                        "costOfGoodsPurchased" => 712
                    ],
                ]
            ],
            [
                "details" => [
                    "PD-DGWVT3" => [
                        "supplierSku" => "SK-IW055K-WX1X-JGH",
                        "clientSku" => "CK-8AXRNCE-2BCQMO-BK",
                        "expectedQuantity" => "60",
                        "receivedQuantity" => "0",
                        "storageLocation" => "asdasd",
                        "productDescription" => "Autem inventore aut officia aut aut blanditiis.",
                        "costOfGoodsPurchased" => 236
                    ],
                    "PD-M9ECZB" => [
                        "supplierSku" => "SK-04-04-UB",
                        "clientSku" => "CK-J-1ZRHN9-VCET",
                        "expectedQuantity" => "13",
                        "receivedQuantity" => "6",
                        "storageLocation" => "qwdqwdqw",
                        "productDescription" => "Sequi quia quasi quae sint saepe numquam tempora et.",
                        "costOfGoodsPurchased" => 712
                    ],
                ]
            ]
        ];
    }
}
