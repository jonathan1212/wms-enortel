<?php

namespace BranchBundle\Tests\Commands\PurchaseOrder;

use CoreBundle\Entity\PurchaseOrderProduct;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\PurchaseOrder\CreatePurchaseOrderCommand;

class CreatePurchaseOrderCommandHandlerTest extends WebTestCase
{
    private $container;
    private $dummyPurchaseOrderNumber = 'PO-1234567890';

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));
    }

    /** @test */
    public function should_create_purchase_order()
    {
        $purchaseOrderRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:PurchaseOrder');

        $purchaseOrderProductRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:PurchaseOrderProduct');

        // Sanity Check
        // There should not be any purchase orders and PO Products yet
        $purchaseOrders = $purchaseOrderRepository->findAll();
        $purchaseOrderProducts = $purchaseOrderProductRepository->findAll();
        $this->assertEmpty($purchaseOrders);
        $this->assertEmpty($purchaseOrderProducts);

        // Run command
        $this->runCreatePurchaseOrderCommand();

        // Assert record exists
        $purchaseOrders = $purchaseOrderRepository->findAll();
        $this->assertCount(1, $purchaseOrders);

        // Assert PO product exists
        $purchaseOrderProducts = $purchaseOrderProductRepository->findAll();
        $this->assertCount(2, $purchaseOrderProducts);
    }

    /**
     * Creates a Purchase Order with 2 products:
     *     id: 1,
     *     quantity: 5,
     *     COGP: 50
     *
     *     id: 2,
     *     quantity: 10,
     *     COGP: 60
     *
     * Can be extended by passing an array of details
     * so that PO products can be determined on the fly
     *
     * If supplied with a separate container, will dummify the created PO
     * since it is assumed to be used by other test cases
     * 
     * @return null
     */
    public function runCreatePurchaseOrderCommand($container = null)
    {
        $dummify = $container !== null;
        $container = $container ?: $this->container;

        // Setup
        $em = $container->get('doctrine.orm.entity_manager');
        $supplier = $em->getRepository('CoreBundle:Supplier')->find(1);
        $warehouse = $em->getRepository('CoreBundle:Warehouse')->find(1);
        $client = $em->getRepository('CoreBundle:Client')->find(1);
        $paymentType = $em->getRepository('CoreBundle:PurchaseOrderPaymentType')->find(1);
        

        $product1 = new PurchaseOrderProduct();
        $product1->setQuantity(5)
            ->setProduct(
                $em->getRepository('CoreBundle:Product')->find(1)
            )
            ->setCostOfGoodsPurchased('50');

        $product2 = new PurchaseOrderProduct();
        $product2->setQuantity(10)
            ->setProduct(
                $em->getRepository('CoreBundle:Product')->find(2)
            )
            ->setCostOfGoodsPurchased('60');

        $products = new \Doctrine\Common\Collections\ArrayCollection(
            [$product1, $product2]
        );

        $command = new CreatePurchaseOrderCommand(
            $supplier,
            $products,
            $warehouse,
            $paymentType,
            $client,
            'test_remarks'
        );

        $container->get('command_bus')->handle($command);

        if(!$dummify){
            return;
        }

        $purchaseOrder = $em->getRepository('CoreBundle:PurchaseOrder')->find(1);
        $purchaseOrder->setPurchaseOrderNumber(
            $this->dummyPurchaseOrderNumber
        );

        $em->flush();
    }
}
