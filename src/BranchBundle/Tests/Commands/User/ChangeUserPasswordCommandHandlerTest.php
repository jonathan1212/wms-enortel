<?php

namespace BranchBundle\Tests\Commands\User;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\User\ChangeUserPasswordCommand;

class ChangeUserPasswordCommandHandlerTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));
    }

    /** @test */
    public function should_properly_change_password()
    {
        // Setup
        $passwordEncoder = $this->container->get('security.password_encoder');
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:User');

        // Sanity Check
        $user = $userRepository->find(1);
        $this->assertNotNull($user);
        $this->assertTrue(
            $passwordEncoder->isPasswordValid(
                $user,
                '12345678'
            )
        );

        $command = new ChangeUserPasswordCommand(
            1,
            'password'
        );

        $this->getContainer()->get('command_bus')->handle($command);

        // Assert password was changed
        $this->assertTrue(
            $passwordEncoder->isPasswordValid(
                $user,
                'password'
            )
        );
    }
}
