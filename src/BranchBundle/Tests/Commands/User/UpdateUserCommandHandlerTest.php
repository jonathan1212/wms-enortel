<?php

namespace BranchBundle\Tests\Commands\User;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\User\UpdateUserCommand;

class UpdateUserCommandHandlerTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));
    }

    /** @test */
    public function should_edit_user_details_except_password()
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:User');

        // Sanity Check
        $user = $userRepository->find(1);
        $this->assertNotNull($user);

        $fn = $user->getFirstName();
        $ln = $user->getLastName();
        $ea = $user->getEmailAddress();
        $cn = $user->getContactNumber();

        $command = new UpdateUserCommand(
            1,
            'update_fn',
            'update_ln',
            'update_ea',
            'update_cn'
        );

        // Execute
        $this->getContainer()->get('command_bus')->handle($command);

        // Assert new details
        $this->assertNotEquals($fn, $user->getFirstName()); 
        $this->assertEquals('update_fn', $user->getFirstName()); 

        $this->assertNotEquals($ln, $user->getLastName()); 
        $this->assertEquals('update_ln', $user->getLastName()); 

        $this->assertNotEquals($ea, $user->getEmailAddress()); 
        $this->assertEquals('update_ea', $user->getEmailAddress()); 

        $this->assertNotEquals($cn, $user->getContactNumber()); 
        $this->assertEquals('update_cn', $user->getContactNumber()); 

        // Assert password is unchanged
        $passwordEncoder = $this->container->get('security.password_encoder');
        $this->assertTrue(
            $passwordEncoder->isPasswordValid(
                $user,
                '12345678'
            )
        );
    }
}
