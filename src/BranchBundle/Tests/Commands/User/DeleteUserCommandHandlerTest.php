<?php

namespace BranchBundle\Tests\Commands\User;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\User\DeleteUserCommand;

class DeleteUserCommandHandlerTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));
    }

    /** @test */
    public function should_delete_user()
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:User');

        // Sanity Check
        $user = $userRepository->find(1);
        $this->assertNotNull($user);
        $this->assertNull($user->getDeletedAt());

        $command = new DeleteUserCommand(1);
        $this->getContainer()->get('command_bus')->handle($command);

        // Soft Deleted
        $user = $userRepository->find(1);
        $this->assertNotNull($user->getDeletedAt());
    }
}
