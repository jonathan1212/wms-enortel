<?php

namespace BranchBundle\Tests\Commands\User;

use CoreBundle\Entity\WarehouseMember;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\User\UpdateUserWarehouseMembershipCommand;

class UpdateUserWarehouseMembershipCommandHandlerTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml'
        ));
    }

    /** @test */
    public function should_add_new_warehouse_membership()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $userRepository = $em->getRepository('CoreBundle:User');
        $warehouseMemberRepository = $em->getRepository('CoreBundle:WarehouseMember');
        $warehouseRepository = $em->getRepository('CoreBundle:Warehouse');

        // Sanity Check
        $user = $userRepository->find(1);
        $this->assertEquals(1, $user->getWarehouseMemberships()->count());

        // Save original list of memberships
        $originalMemberships = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($user->getWarehouseMemberships() as $membership) {
            $originalMemberships->add($membership);
        }

        // Add new warehouse membership
        $warehouse2 = $warehouseRepository->find(2);
        $membership = new WarehouseMember();
        $membership->setUser($user)
            ->setWarehouse($warehouse2)
            ->setRoles(['ROLE_TEST']);

        $user->addWarehouseMembership($membership);
        $command = new UpdateUserWarehouseMembershipCommand(
            $user->getId(),
            $originalMemberships
        );

        $this->getContainer()->get('command_bus')->handle($command);

        // Assert Expectations
        $newMembership = $warehouseMemberRepository->find(3);
        $this->assertNotNull($newMembership);
        $this->assertSame($user, $newMembership->getUser());
        $this->assertSame($warehouse2, $newMembership->getWarehouse());
        $this->assertEquals(['ROLE_TEST'], $newMembership->getRoles());
    }

    /** @test */
    public function should_remove_warehouse_membership()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $userRepository = $em->getRepository('CoreBundle:User');
        $warehouseMemberRepository = $em->getRepository('CoreBundle:WarehouseMember');

        // Sanity Check
        $user = $userRepository->find(1);
        $this->assertEquals(1, $user->getWarehouseMemberships()->count());

        // Save original list of memberships
        $originalMemberships = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($user->getWarehouseMemberships() as $membership) {
            $originalMemberships->add($membership);
        }

        // Remove membership
        $targetMembership = $warehouseMemberRepository->find(1);
        $user->removeWarehouseMembership($targetMembership);

        $command = new UpdateUserWarehouseMembershipCommand(
            $user->getId(),
            $originalMemberships
        );

        $this->getContainer()->get('command_bus')->handle($command);

        // Assert Expectations
        $membership = $warehouseMemberRepository->find(1);
        $this->assertNull($membership);
    }
}
