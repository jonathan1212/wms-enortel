<?php

namespace BranchBundle\Tests\Commands\User;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\User\CreateUserCommand;

class CreateUserCommandHandlerTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));
    }

    /** @test */
    public function should_create_new_user_correctly()
    {
        $userRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:User');

        $command = new CreateUserCommand(
            'Kylo',
            'Vader',
            'kylo.vader@gmail.com',
            'r2d2',
            '123456789'
        );

        // Sanity Check
        $user = $userRepository->findOneByEmailAddress('kylo.vader@gmail.com');
        $this->assertNull($user);

        // Execute
        $this->getContainer()->get('command_bus')->handle($command);

        // Assert user existence
        $user = $userRepository->findOneByEmailAddress('kylo.vader@gmail.com');
        $this->assertNotNull($user);

        // Verify User Details
        $this->assertEquals('Kylo', $user->getFirstName());
        $this->assertEquals('Vader', $user->getLastName());
        $this->assertEquals('kylo.vader@gmail.com', $user->getEmailAddress());
        $this->assertEquals('123456789', $user->getContactNumber());

        $passwordEncoder = $this->container->get('security.password_encoder');

        $this->assertTrue(
            $passwordEncoder->isPasswordValid(
                $user,
                'r2d2'
            )
        );
    }
}
