<?php

namespace BranchBundle\Tests\Commands\Inbounding;

use CoreBundle\Entity\InventoryStatus;
use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\Inbounding\CreateInboundHistoryCommand;
use BranchBundle\Tests\Commands\PurchaseOrder\CreatePurchaseOrderCommandHandlerTest;

class CreateInboundHistoryCommandHandlerTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));

        $purchaseOrderTest = new CreatePurchaseOrderCommandHandlerTest();
        $purchaseOrderTest->runCreatePurchaseOrderCommand(
            $this->container
        );
    }

    /** @test */
    public function should_generate_inbound_history_record()
    {
        $inboundHistoryRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:InboundHistory');

        // Sanity Check
        // There should be no history records yet
        $history = $inboundHistoryRepository->findAll();
        $this->assertEmpty($history);

        // Run Command
        $this->runCreateInboundHistoryCommand();

        // Assert record exists
        $history = $inboundHistoryRepository->findAll();
        $this->assertCount(1, $history);
    }

    /** @test */
    public function should_generate_correct_number_of_inventory_records()
    {
        $inventoryRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:Inventory');

        $inventoryStatusRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:InventoryStatus');

        $inboundedStatus = $inventoryStatusRepository->findOneByValue(
            InventoryStatus::INBOUNDED
        );

        $quarantinedStatus = $inventoryStatusRepository->findOneByValue(
            InventoryStatus::QUARANTINED
        );

        // Sanity Check
        // There should be no inventory records yet
        $inventories = $inventoryRepository->findAll();
        $this->assertEmpty($inventories);

        // Run Command
        $this->runCreateInboundHistoryCommand();

        // Assert correct amount of records exist
        $inventories = $inventoryRepository->findAll();
        $this->assertCount(8, $inventories);

        // Assert correct amount of inbounded inventory
        $inventories = $inventoryRepository->findByInventoryStatus($inboundedStatus);
        $this->assertCount(3, $inventories);

        // Assert correct amount of quarantined inventory
        $inventories = $inventoryRepository->findByInventoryStatus($quarantinedStatus);
        $this->assertCount(5, $inventories);

        // Assert tagsGenerated flash bag has the correct amount of tags
        $generatedTags = $this->container->get('session')
            ->getFlashBag()->get('tagsGenerated');
        $this->assertCount(8, $generatedTags);
    }

    /**
     * Target Quantity should be in the form of:
     * [
     *     'PD-12345' => 3,
     *     'PD-67890' => 5
     * ]
     * 
     * @return null
     */
    public function runCreateInboundHistoryCommand($container = null, $targetQuantity = [])
    {
        $container = $container ?: $this->container;

        // Setup 
        $warehouseRepository = $container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:Warehouse');

        $inboundingDetails = [
            'PD-12345' => [
                "clientSku" => "CS-12345",
                "supplierSku" => "SS-12345",
                "expectedQuantity" => "5",
                "partialQuantity" => "0",
                "receivedQuantity" => array_key_exists('PD-12345', $targetQuantity) ? strval($targetQuantity['PD-12345']) : "3",
                "storageLocation" => "storage_location_1",
                "mobileLocation" => "",
                "forQuarantine" => "false",
                "productDescription" => "Voluptas optio quos sed autem."
            ],
            'PD-67890' => [
                "clientSku" => "CS-67890",
                "supplierSku" => "SS-67890",
                "expectedQuantity" => "10",
                "partialQuantity" => "0",
                "receivedQuantity" => array_key_exists('PD-67890', $targetQuantity) ? strval($targetQuantity['PD-67890']) : "5",
                "storageLocation" => "storage_location_1",
                "mobileLocation" => "",
                "forQuarantine" => array_key_exists('PD-12345', $targetQuantity) ? "false" : "true",
                "productDescription" => "Voluptas optio quos sed autem."
            ]
        ];

        $command = new CreateInboundHistoryCommand(
            'PO-1234567890',
            $inboundingDetails,
            $warehouseRepository->find(1)
        );

        $container->get('command_bus')->handle($command);
    }
}
