<?php

namespace BranchBundle\Tests\Commands\Picklist;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use BranchBundle\Commands\Picklist\PrintPicklistsCommand;
use BranchBundle\Tests\Commands\Picklist\CreatePicklistImportHistoryCommandHandlerTest;

class PrintPicklistsCommandHandlerTest extends WebTestCase
{
    private $container;
    private $em;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));

        $createPicklistTest = new CreatePicklistImportHistoryCommandHandlerTest();
        $createPicklistTest->runPicklistImportCommand(
            $this->container
        );
    }

    /** @test */
    public function picklist_particulars_should_be_for_picking_upon_being_printed()
    {
        $picklistParticularRepository = $this->em->getRepository('CoreBundle:PicklistParticular');
        $inventoryRepository = $this->em->getRepository('CoreBundle:Inventory');
        $picklistStatusRepository = $this->em->getRepository('CoreBundle:PicklistStatus');
        $inventoryStatusRepository = $this->em->getRepository('CoreBundle:InventoryStatus');

        $createdStatus = $picklistStatusRepository->findOneByValue(
            \CoreBundle\Entity\PicklistStatus::CREATED
        );

        $pickingStatus = $picklistStatusRepository->findOneByValue(
            \CoreBundle\Entity\PicklistStatus::FOR_PICKING
        );

        $picklistedStatus = $inventoryStatusRepository->findOneByValue(
            \CoreBundle\Entity\InventoryStatus::PICKLISTED
        );

        // There should be particulars that are created
        $particulars = $picklistParticularRepository->findByPicklistStatus($createdStatus);
        $this->assertCount(3, $particulars);

        // Run Command
        $this->runPicklistPrintCommand();

        // Assert that there should be particulars that are for picking
        $particulars = $picklistParticularRepository->findByPicklistStatus($pickingStatus);
        $this->assertCount(3, $particulars);

        // Assert that all inventories are now picklisted
        $inventories = $inventoryRepository->findByInventoryStatus($picklistedStatus);
        $this->assertCount(9, $inventories);
    }

    /**
     * Default inbounding/inventory is:
     * [
     *     'PD-12345' => 3,
     *     'PD-67890' => 6
     * ]
     *
     * Default import is:
     * 1) Test-order-1  Kel Thuzad          Tokyo, Japan    63912345678 warehouse 1 CS-12345    Test    10,000.12   3   COD
     * 2) Test-order-1  Kel Thuzad          Tokyo, Japan    63912345678 warehouse 1 CS-67890    Test    5,000.00    5   Non-COD
     * 3) Test-order-2  Tirion Fordring     Malate, Manila  63923456789 warehouse 1 CS-67890    Test    3,000       1   COD
     *
     * If called with container, should set 3 picklist particulars below to ready for picking
     * 
     */
    public function runPicklistPrintCommand($container = null)
    {
        // If called with a container, generate picklists before printing
        // since we are being called outside the context of this class
        if($container){
            $createPicklistTest = new CreatePicklistImportHistoryCommandHandlerTest();
            $createPicklistTest->runPicklistImportCommand(
                $container
            );
        }

        $container = $container ?: $this->container;
        $command = new PrintPicklistsCommand(
            [
                'PL-12345-1',
                'PL-12345-2',
                'PL-12345-3'
            ]
        );

        $container->get('command_bus')->handle($command);
    }
}
