<?php

namespace BranchBundle\Tests\Commands\Picklist;

use Liip\FunctionalTestBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use BranchBundle\Commands\Picklist\CreatePicklistImportHistoryCommand;
use BranchBundle\Tests\Commands\Inbounding\CreateInboundHistoryCommandHandlerTest;
use BranchBundle\Tests\Commands\PurchaseOrder\CreatePurchaseOrderCommandHandlerTest;

class CreatePicklistImportHistoryCommandHandlerTest extends WebTestCase
{
    private $container;
    private $em;
    private $dummyPicklistParticularBase = 'PL-12345';

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->em = $this->container->get('doctrine.orm.entity_manager');
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/ORM/core.yml',
        ));
    }

    /** @test */
    public function should_create_picklist_history()
    {
        // Setup
        $picklistImportHistoryRepository = $this->em->getRepository('CoreBundle:PicklistImportHistory');

        // Sanity Check
        // There should be no history records yet
        $records = $picklistImportHistoryRepository->findAll();
        $this->assertCount(0, $records);

        // Run Command
        $this->runPicklistImportCommand();

        // Assert history records now exist
        $records = $picklistImportHistoryRepository->findAll();
        $this->assertCount(1, $records);
    }

    /** @test */
    public function no_inventory_should_generate_incomplete_picklists()
    {
        // Setup
        $picklistRepository = $this->em->getRepository('CoreBundle:Picklist');

        // Sanity Check, there should be no picklists yet
        $picklists = $picklistRepository->findAll();
        $this->assertCount(0, $picklists);

        // Run Command
        $this->runPicklistImportCommand();

        // Assert 2 picklists are generated
        $picklists = $picklistRepository->findAll();
        $this->assertCount(2, $picklists);

        // Assert both are incomplete
        $this->assertFalse(
            $picklists[0]->getIsComplete()
        );
        $this->assertFalse(
            $picklists[1]->getIsComplete()
        );
    }

    /** @test */
    public function sufficient_inventory_should_complete_all_picklists()
    {
        // Setup
        $picklistRepository = $this->em->getRepository('CoreBundle:Picklist');
        $inventoryRepository = $this->em->getRepository('CoreBundle:Inventory');
        $inventoryStatusRepository = $this->em->getRepository('CoreBundle:InventoryStatus');
        $allocatedStatus = $inventoryStatusRepository->findOneByValue(
            \CoreBundle\Entity\InventoryStatus::ALLOCATED
        );

        // Inbound Items
        $this->inboundItems([
            'PD-12345' => 3,
            'PD-67890' => 6
        ]);

        // Generate Picklists
        $this->runPicklistImportCommand();

        // Assert 2 picklists are complete
        $picklists = $picklistRepository->findByIsComplete(true);
        $this->assertCount(2, $picklists);

        // Assert all inventories are now allocated
        $inventories = $inventoryRepository->findByInventoryStatus($allocatedStatus);
        $this->assertCount(9, $inventories);
    }

    /** @test */
    public function completed_picklist_should_generate_appropriate_picklist_particulars()
    {
        // Setup
        $picklistRepository = $this->em->getRepository('CoreBundle:Picklist');
        $picklistParticularRepository = $this->em->getRepository('CoreBundle:PicklistParticular');
        $picklistStatusRepository = $this->em->getRepository('CoreBundle:PicklistStatus');
        $createdStatus = $picklistStatusRepository->findOneByValue(
            \CoreBundle\Entity\PicklistStatus::CREATED
        );

        // Inbound Items
        $this->inboundItems([
            'PD-12345' => 3,
            'PD-67890' => 6
        ]);

        // Generate Picklists
        $this->runPicklistImportCommand();

        // Assert 3 picklist particulars generated and are created status
        $particulars = $picklistParticularRepository->findByPicklistStatus($createdStatus);
        $this->assertCount(3, $particulars);
    }

    /** @test */
    public function follow_up_item_inbounding_should_complete_previously_unfulfilled_picklists()
    {
        // Setup
        $picklistRepository = $this->em->getRepository('CoreBundle:Picklist');

        // Generate Picklists
        $this->runPicklistImportCommand();

        // Assert 2 picklists are generated and incomplete
        $picklists = $picklistRepository->findByIsComplete(false);
        $this->assertCount(2, $picklists);

        // Inbound Items
        $this->inboundItems([
            'PD-12345' => 3,
            'PD-67890' => 6
        ]);

        // Get complete picklists
        $picklists = $picklistRepository->findByIsComplete(true);
        $this->assertCount(2, $picklists);
    }

    private function inboundItems($targetQuantity = [], $container = null)
    {
        $container = $container ?: $this->container;

        // Generate PO
        $purchaseOrderTest = new CreatePurchaseOrderCommandHandlerTest();
        $purchaseOrderTest->runCreatePurchaseOrderCommand(
            $container
        );

        // Inbound items
        $inboundTest = new CreateInboundHistoryCommandHandlerTest();
        $inboundTest->runCreateInboundHistoryCommand(
            $container,
            $targetQuantity
        );
    }

    /**
     * Import consists of the following details:
     *
     * 1) Test-order-1  Kel Thuzad          Tokyo, Japan    63912345678 warehouse 1 CS-12345    Test    10,000.12   3   COD
     * 2) Test-order-1  Kel Thuzad          Tokyo, Japan    63912345678 warehouse 1 CS-67890    Test    5,000.00    5   Non-COD
     * 3) Test-order-2  Tirion Fordring     Malate, Manila  63923456789 warehouse 1 CS-67890    Test    3,000       1   COD
     *
     * If called with container, should generate 1 picklist and 3 picklist particulars that are ready for printing
     * 
     */
    public function runPicklistImportCommand($container = null)
    {
        $dummify = $container !== null;
        // If supplied with container, do auto inbound since
        // this is being utilized by an outside test case
        if($container){
            $this->inboundItems(
                [
                    'PD-12345' => 3,
                    'PD-67890' => 6
                ],
                $container
            );
        }

        $container = $container ?: $this->container;

        // Setup
        $em = $container->get('doctrine.orm.entity_manager');
        $client = $em->getRepository('CoreBundle:Client')->find(1);
        $file = new UploadedFile(
            dirname(__FILE__).'/../../Files/Picklist/picklist_import_test.ods',
            'picklist_import_test.ods',
            'application/vnd.oasis.opendocument.spreadsheet',
            30489
        );

        $command = new CreatePicklistImportHistoryCommand(
            $client->getId(),
            $file
        );

        $container->get('command_bus')->handle($command);

        if(!$dummify){
            return;
        }

        $picklistParticularRepository = $em->getRepository('CoreBundle:PicklistParticular');
        $particulars = $picklistParticularRepository->findAll(); // Assuming all belongs to 1 order

        $i = 1;
        foreach($particulars as $particular){
            $particular->setPicklistParticularNumber(
                sprintf('%s-%s', $this->dummyPicklistParticularBase, $i)
            );
            ++$i;
        }
        $em->flush();
    }
}
