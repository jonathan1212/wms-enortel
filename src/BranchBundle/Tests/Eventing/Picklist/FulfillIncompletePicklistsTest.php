<?php

namespace BranchBundle\Tests\Eventing\Picklist;

use BranchBundle\Eventing\Picklist\FulfillIncompletePicklists;

class FulfillIncompletePicklistsTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function mapping_should_ignore_irrelevant_inventories()
    {
        $newInventory = [
            'ignored_sku' => [
                'tag_1',
                'tag_2'
            ]
        ];

        $picklistDeliquency = [
            'picklist-1' => [
                'sku_1' => 3,
                'sku_2' => 2
            ],
            'picklist-2' => [
                'sku_1' => 3,
                'sku_3' => 2
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => false,
                'newInventory' => []
            ],
            'picklist-2' => [
                'isComplete' => false,
                'newInventory' => []
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);
    
        $newInventory = [
            'sku_3' => [
                'tag_5',
                'tag_6'
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => false,
                'newInventory' => []
            ],
            'picklist-2' => [
                'isComplete' => false,
                'newInventory' => [
                    'sku_3' => ['tag_5', 'tag_6']
                ]
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);
    }

    /** @test */
    public function mapping_should_respect_array_order_of_incomplete_picklists()
    {
        $newInventory = [
            'sku_1' => [
                'tag_1',
                'tag_2'
            ]
        ];

        $picklistDeliquency = [
            'picklist-1' => [
                'sku_1' => 3,
                'sku_2' => 2
            ],
            'picklist-2' => [
                'sku_1' => 3,
                'sku_3' => 2
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => false,
                'newInventory' => [
                    'sku_1' => ['tag_1', 'tag_2']
                ]
            ],
            'picklist-2' => [
                'isComplete' => false,
                'newInventory' => []
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);
    
        $newInventory = [
            'sku_1' => [
                'tag_1',
                'tag_2',
                'tag_3',
                'tag_4'
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => false,
                'newInventory' => [
                    'sku_1' => ['tag_1', 'tag_2', 'tag_3']
                ]
            ],
            'picklist-2' => [
                'isComplete' => false,
                'newInventory' => [
                    'sku_1' => ['tag_4']
                ]
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);
    }

    /** @test */
    public function mapping_should_correctly_skip_already_fulfilled_products()
    {
        $newInventory = [
            'sku_1' => [
                'tag_1',
                'tag_2'
            ],
            'sku_3' => [
                'tag_4',
                'tag_5'
            ]
        ];

        $picklistDeliquency = [
            'picklist-1' => [
                'sku_1' => 0,
                'sku_2' => 2
            ],
            'picklist-2' => [
                'sku_1' => 3,
                'sku_3' => 0
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => false,
                'newInventory' => []
            ],
            'picklist-2' => [
                'isComplete' => false,
                'newInventory' => [
                    'sku_1' => ['tag_1', 'tag_2']
                ]
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);
    }

    /** @test */
    public function mapping_should_correctly_flag_complete_picklists()
    {
        $newInventory = [
            'sku_1' => [
                'tag_1',
                'tag_2',
                'tag_3'
            ],
            'sku_2' => [
                'tag_4',
                'tag_5'
            ],
            'sku_3' => [
                'tag_6'
            ]
        ];

        $picklistDeliquency = [
            'picklist-1' => [
                'sku_1' => 3,
                'sku_2' => 2
            ],
            'picklist-2' => [
                'sku_1' => 3,
                'sku_3' => 2
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => true,
                'newInventory' => [
                    'sku_1' => ['tag_1', 'tag_2', 'tag_3'],
                    'sku_2' => ['tag_4', 'tag_5']
                ]
            ],
            'picklist-2' => [
                'isComplete' => false,
                'newInventory' => [
                    'sku_3' => ['tag_6']
                ]
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);

        $newInventory = [
            'sku_1' => [
                'tag_1',
                'tag_2',
                'tag_3',
                'tag_99',
                'tag_98',
                'tag_97',
                'tag_96',
                'tag_95',
            ],
            'sku_2' => [
                'tag_4',
                'tag_5',
                'tag_7',
                'tag_8',
                'tag_9',
                'tag_10',
                'tag_11',
                'tag_12',
            ],
            'sku_3' => [
                'tag_a',
                'tag_b',
                'tag_c',
                'tag_d',
                'tag_e',
                'tag_f',
                'tag_g',
                'tag_h',
            ]
        ];

        $expectedMapping = [
            'picklist-1' => [
                'isComplete' => true,
                'newInventory' => [
                    'sku_1' => ['tag_1', 'tag_2', 'tag_3'],
                    'sku_2' => ['tag_4', 'tag_5']
                ]
            ],
            'picklist-2' => [
                'isComplete' => true,
                'newInventory' => [
                    'sku_1' => ['tag_99', 'tag_98', 'tag_97'],
                    'sku_3' => ['tag_a', 'tag_b']
                ]
            ],
        ];

        $mapping = $this->runMapping($newInventory, $picklistDeliquency);
        $this->assertEquals($expectedMapping, $mapping);
    }

    private function runMapping($newInventory, $picklistDeliquency)
    {
        $mockClass = $this->getMockFulfillIncompletePicklistsClass();
        $mapping =  $this->invokeMethod(
            $mockClass,
            'mapNewInventoryToIncompletePicklists', 
            [$newInventory, $picklistDeliquency]
        );

        return $mapping;
    }

    private function getMockFulfillIncompletePicklistsClass()
    {
        return new FulfillIncompletePicklists(
            $this->getMockMessageBus(),
            $this->getMockEntityManager()
        );
    }

    private function getMockMessageBus()
    {
        return $this
            ->getMockBuilder('\SimpleBus\Message\Bus\Middleware\MessageBusSupportingMiddleware')
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function getMockEntityManager()
    {
        return $this
            ->getMockBuilder('\Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
