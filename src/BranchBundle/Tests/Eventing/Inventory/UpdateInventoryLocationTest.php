<?php

namespace BranchBundle\Tests\Eventing\Inventory;

use BranchBundle\Service\InventoryMove\InventoryFinder;
use BranchBundle\Eventing\InventoryMove\InventoryMovedEvent;
use BranchBundle\Eventing\Inventory\UpdateInventoryLocation;

class UpdateInventoryLocationTest extends \PHPUnit_Framework_TestCase
{
    /** @test */
    public function should_source_same_mobile_location_if_moving_as_bulk()
    {
        $em = $this->getMockEntityManager();
        $finder = $this->getMockInventoryFinder();
        $updateInventoryLocation = new UpdateInventoryLocation($em, $finder);

        $movedEvent = new InventoryMovedEvent(
            'target',
            'storage_loc',
            'mobile_loc'
        );

        $inventoryTags = [
            'type' => InventoryFinder::TAG_MULTIPLE
        ];

        $mobileLocation = $this->invokeMethod(
            $updateInventoryLocation, 
            'getCorrectMobileLocation', 
            [$movedEvent, $inventoryTags]
        );

        $this->assertEquals('target', $mobileLocation);
    }

    /** @test */
    public function should_source_chosen_mobile_location_if_moving_individually()
    {
        $em = $this->getMockEntityManager();
        $finder = $this->getMockInventoryFinder();
        $updateInventoryLocation = new UpdateInventoryLocation($em, $finder);

        $movedEvent = new InventoryMovedEvent(
            'target',
            'storage_loc',
            'mobile_loc'
        );

        $inventoryTags = [
            'type' => InventoryFinder::TAG_SINGLE
        ];

        $mobileLocation = $this->invokeMethod(
            $updateInventoryLocation, 
            'getCorrectMobileLocation', 
            [$movedEvent, $inventoryTags]
        );

        $this->assertEquals('mobile_loc', $mobileLocation);
    }

    private function getMockEntityManager()
    {
        return $this->getMockBuilder('\Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();
    }

    private function getMockInventoryFinder()
    {
        return $this->getMockBuilder('\BranchBundle\Service\InventoryMove\InventoryFinder')
            ->disableOriginalConstructor()
            ->getMock();
    }


    public function invokeMethod(&$object, $methodName, array $parameters = array())
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
