<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClientType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options['validation_groups'][0] === 'add' || $options['validation_groups'][0] === 'update'){
            $builder
                ->add(
                    'name', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType',
                    [
                        'label' => 'Client Name'
                    ]
                )
                ->add(
                    'contactPerson', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType'
                )
                ->add(
                    'contactNumber', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType',
                    array(
                        'required' => false
                    )
                )
                ->add(
                    'emailAddress', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType'
                )
                ->add(
                    'hostAlias', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType'
                );
        }

        if($options['validation_groups'][0] === 'add' || $options['validation_groups'][0] === 'change_password'){
            $builder
                ->add(
                    'plainPassword',
                    'Symfony\Component\Form\Extension\Core\Type\RepeatedType',
                    array(
                        'type' => 'Symfony\Component\Form\Extension\Core\Type\PasswordType',
                        'first_options' => array('label' => 'Password'),
                        'second_options' => array('label' => 'Repeat Password'),
                        'invalid_message' => 'The password fields do not match'
                    )
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Client',
            'validation_groups' => ['add']
        ));
    }
}
