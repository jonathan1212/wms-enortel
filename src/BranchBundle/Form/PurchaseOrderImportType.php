<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 2/27/17
 * Time: 11:46 AM
 */

namespace BranchBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

class PurchaseOrderImportType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
            ->add(
                'client',
                'Symfony\Bridge\Doctrine\Form\Type\EntityType',
                array(
                    'class' => 'CoreBundle:Client',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'supplier',
                'Symfony\Bridge\Doctrine\Form\Type\EntityType',
                array(
                    'class' => 'CoreBundle:Supplier',
                    'choice_label' => 'name'
                )
            )

            ->add(
                'paymentType',
                'Symfony\Bridge\Doctrine\Form\Type\EntityType',
                array(
                    'class' => 'CoreBundle:PurchaseOrderPaymentType',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'remarks',
                'Symfony\Component\Form\Extension\Core\Type\TextareaType',
                array(
                    'required' => false
                )
            )
            ->add(
                'attachedFile', 
                'Symfony\Component\Form\Extension\Core\Type\FileType',
                array(
                    'constraints' => array(
                        new File(array(
                              'maxSize' => '10M',
                              'mimeTypes' => array(
                                  'image/jpg',
                                  'image/jpeg',
                                  'image/png',
                              ),
                             'mimeTypesMessage' => 'Please upload a valid jpg, jpeg or png image',
                          ))
                      )
                )
            )
            ->add(
                'referenceNo', 
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false
                )
            )
            ->add(
                'purchaseOrderImport',
                'Symfony\Component\Form\Extension\Core\Type\FileType',
                array(
                    'constraints' => array(
                        new File(array(
                            'maxSize' => '2M',
                            'mimeTypes' => [
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // xlsx
                                'application/vnd.ms-excel', // xls
                                'application/vnd.oasis.opendocument.formula', // odf
                                'application/vnd.oasis.opendocument.spreadsheet' // ods
                            ],
                            'mimeTypesMessage' => "The mime type of the file is invalid ({{ type }})."
                        ))
                    )
                )
            )
        ;
    }

    public function getName()
    {
        return 'purchase_order_import';
    }


}