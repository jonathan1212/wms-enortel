<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WarehouseMembershipType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'warehouseMemberships', 
                'Symfony\Component\Form\Extension\Core\Type\CollectionType', 
                array(
                    'entry_type' => 'BranchBundle\Form\WarehouseMemberType',
                    'allow_add'  => true,
                    'allow_delete'  => true,
                    'by_reference' => false,
                    'error_bubbling' => false
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\User',
        ));
    }
}
