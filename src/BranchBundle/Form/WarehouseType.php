<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WarehouseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name', 
                'Symfony\Component\Form\Extension\Core\Type\TextType'
            )
            ->add(
                'address', 
                'Symfony\Component\Form\Extension\Core\Type\TextType'
            )
            ->add(
                'contactNumber', 
                'Symfony\Component\Form\Extension\Core\Type\TextType', 
                array(
                    'required' => false
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Warehouse',
        ));
    }
}
