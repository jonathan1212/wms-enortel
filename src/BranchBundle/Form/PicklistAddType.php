<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use CoreBundle\Repository\ProductRepository;
use CoreBundle\Repository\InventoryRepository;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PicklistAddType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['entity_manager'];

        $builder
            ->add(
                'orderNumber',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => true
                )
            )

            ->add(
                'serialNo',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false
                )
            )

            ->add(
                'warehouse', 
                'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                array(
                    'class' => 'CoreBundle:Warehouse',
                    'choice_label' => 'name',
                    'placeholder' => 'Choose Warehouse',
                )
            )

            ->add('product', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                'class' => 'CoreBundle:Product',
                'query_builder' => function (ProductRepository $repo) {
                    return $repo->createQueryBuilder('p')
                        ->andWhere('p.deletedAt is null')
                        ;
                },
                'placeholder' => 'Choose Product',
                'choice_label' => 'clientSku'
            ])   
            ->add(
                'productDescription',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false,
                )
            )
            ->add(
                'costOfGoodsPurchased',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false
                )
            )
            ->add(
                'quantity',
                'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                array(
                    'choices' => array(
                        0       => 0
                    ),                    
                    'choices_as_values' => true,
                )
            )              
            ->add(
                'consigneeName',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => true
                )
            )
            ->add(
                'consigneeAddress',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => true
                )
            )
            ->add(
                'consigneeContactNumber',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => true
                )
            )                       
            ->add(
                'paymentType',
                'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                array(
                    'choices' => array(
                        'NON COD'   => 'NON COD',
                        'COD'       => 'COD',
                    ),                    
                    'choices_as_values' => true
                )
            )
        ;

        $builder
            ->addEventListener(
                FormEvents::PRE_SUBMIT,
                function (FormEvent $event) use ($em) {
                    $form = $event->getForm();

                    $data = $event->getData();

                    if (isset($data['warehouse'])) {

                        $modifier = $data['warehouse'];
                        $form->add('product', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', [
                            'class' => 'CoreBundle:Product',
                            'query_builder' => function (ProductRepository $repo) use ($modifier) {
                                return $repo->createQueryBuilder('p')
                                    ->andWhere('p.warehouse = :warehouse or p.warehouse is null')
                                    ->setParameter('warehouse', $modifier)
                                    ->andWhere('p.deletedAt is null')
                                    ;
                            },
                            'placeholder' => 'Choose Product',
                            'choice_label' => 'clientSku'
                        ]);    
                    }                    
                    
                    if (isset($data['product'])) {

                        $productModifier = $data['product'];
                        $inventory =  $em->getRepository('CoreBundle:Inventory')->getProductQuantity($productModifier);
                        $product = $em->getRepository('CoreBundle:Product')->find($data['product']);


                        $quantityData = [];                        
                        foreach (range(0, $inventory[0]['quantity']) as $number) {
                            $quantityData[$number] = $number;
                        }

                        $form->add(
                            'quantity',
                            'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                            array(
                                'choices' => $quantityData,                    
                                'choices_as_values' => true,
                            )
                        );
                        
                    }
                    

                }
            )
            ->addEventListener(
                FormEvents::SUBMIT,
                function (FormEvent $event) use ($em)  {
                    $form = $event->getForm();
                    $data = $event->getData();

                    if (isset($data['product'])) {
                        
                        $product = $em->getRepository('CoreBundle:Product')->find($data['product']);
                        $form->add(
                            'productDescription',
                            'Symfony\Component\Form\Extension\Core\Type\TextType',
                            array(
                                'required' => false,
                                'data' => $product->getProductDescription()
                            )
                        );

                        $form->add(
                            'costOfGoodsPurchased',
                            'Symfony\Component\Form\Extension\Core\Type\TextType',
                            array(
                                'required' => false,
                                'data' =>  $product->getCostOfGoodsPurchased()
                            )
                        );
                    }
                }
            )
            ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'is_from_client_portal' => false,
            'query_builder'         =>  null,
            'entity_manager'        => null
        ));
    }
}
