<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options['validation_groups'][0] === 'add' || $options['validation_groups'][0] === 'update'){
            $builder
                ->add(
                    'firstName', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType'
                )
                ->add(
                    'lastName', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType'
                )
                ->add(
                    'emailAddress', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType'
                )
                ->add(
                    'contactNumber', 
                    'Symfony\Component\Form\Extension\Core\Type\TextType',
                    array(
                        'required' => false
                    )
                );
        }

        if($options['validation_groups'][0] === 'change_password'){
            $builder
                ->add(
                    'oldPassword',
                    'Symfony\Component\Form\Extension\Core\Type\PasswordType',
                    array(
                        'label' => 'Old Password',
                        'mapped' => 'false'
                    )
                );
        }

        if($options['validation_groups'][0] === 'add' || $options['validation_groups'][0] === 'change_password'){
            $builder
                ->add(
                    'plainPassword',
                    'Symfony\Component\Form\Extension\Core\Type\RepeatedType',
                    array(
                        'type' => 'Symfony\Component\Form\Extension\Core\Type\PasswordType',
                        'first_options' => array('label' => 'Password'),
                        'second_options' => array('label' => 'Repeat Password'),
                        'invalid_message' => 'The password fields do not match'
                    )
                );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\User',
            'validation_groups' => ['add']
        ));
    }
}
