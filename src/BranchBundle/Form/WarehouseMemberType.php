<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WarehouseMemberType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'warehouse', 
                'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                array(
                    'class' => 'CoreBundle:Warehouse',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'roles',
                'Symfony\Component\Form\Extension\Core\Type\ChoiceType',
                array(
                    'choices' => array(
                        'ROLE_ADMIN' => 'ROLE_ADMIN',
                        'ROLE_USER' => 'ROLE_USER'
                    ),
                    'multiple' => true,
                    'expanded' => true,
                    'choices_as_values' => true
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\WarehouseMember',
        ));
    }
}
