<?php

namespace BranchBundle\Form;

use CoreBundle\Entity\Product;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use CoreBundle\Repository\ProductRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PurchaseOrderProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $queryBuilder = $options['query_builder'] ?: function(ProductRepository $repo){
            return $repo->createQueryBuilder('p')
                ->where('p.id = -1');
        };

        $builder
            ->add(
                'product', 
                'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                array(
                    'class' => 'CoreBundle:Product',
                    'required' => false,
                    'query_builder' => $queryBuilder,
                    'choice_label' => 'productDescription'
                )
            )
            ->add(
                'quantity',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false
                )
            )
            ->add(
                'costOfGoodsPurchased',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\PurchaseOrderProduct',
            'query_builder' => null
        ));
    }
}
