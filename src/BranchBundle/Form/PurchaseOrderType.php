<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use CoreBundle\Repository\ProductRepository;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class PurchaseOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options['is_from_client_portal']){
            $builder
                ->add(
                    'warehouse', 
                    'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                    array(
                        'class' => 'CoreBundle:Warehouse',
                        'choice_label' => 'name'
                    )
                );
        }
        else{
            $builder
                ->add(
                    'client', 
                    'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                    array(
                        'class' => 'CoreBundle:Client',
                        'choice_label' => 'name'
                    )
                );
        }

        $builder
            ->add(
                'supplier', 
                'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                array(
                    'class' => 'CoreBundle:Supplier',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'paymentType', 
                'Symfony\Bridge\Doctrine\Form\Type\EntityType', 
                array(
                    'class' => 'CoreBundle:PurchaseOrderPaymentType',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'file', 
                'Symfony\Component\Form\Extension\Core\Type\FileType',
                array(
                    'constraints' => array(
                        new File(array(
                              'maxSize' => '10M',
                              'mimeTypes' => array(
                                  'image/jpg',
                                  'image/jpeg',
                                  'image/png',
                                  'application/pdf',
                                  'application/x-pdf'
                              ),
                             'mimeTypesMessage' => 'Please upload a valid jpg, jpeg, pdf or png image',
                          ))
                    ),
                    'required' => false
                )
            )
            ->add(
                'referenceNo', 
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false
                )
            )
            ->add(
                'remarks', 
                'Symfony\Component\Form\Extension\Core\Type\TextareaType',
                array(
                    'required' => false
                )
            )
            ->add(
                'products', 
                'Symfony\Component\Form\Extension\Core\Type\CollectionType', 
                array(
                    'entry_type' => 'BranchBundle\Form\PurchaseOrderProductType',
                    'allow_add'  => true,
                    'allow_delete'  => true,
                    'by_reference' => false,
                    'error_bubbling' => false,
                    'entry_options' => [
                        'query_builder' => null
                    ]
                )
            )
        ;

        $builder->addEventListener(
            FormEvents::PRE_SUBMIT,
            function (FormEvent $event){
                $data = $event->getData();
                if(!array_key_exists('products', $data)){
                    return;
                }

                $purchaseOrderProducts = $data['products'];
                $form = $event->getForm();
                $form->add(
                    'products',
                    'Symfony\Component\Form\Extension\Core\Type\CollectionType',
                    array(
                        'entry_type' => 'BranchBundle\Form\PurchaseOrderProductType',
                        'allow_add'  => true,
                        'allow_delete'  => true,
                        'by_reference' => false,
                        'error_bubbling' => false,
                        'entry_options' => [
                            'query_builder' => function(ProductRepository $repo) use($purchaseOrderProducts) {
                                return $repo->createQueryBuilder('p')
                                    ->where('p.id IN (:ids)')
                                    ->setParameter('ids', array_map(function($purchaseOrderProduct){
                                        return $purchaseOrderProduct['product'];
                                    }, $purchaseOrderProducts));
                            },
                        ]
                    ) 
                );
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\PurchaseOrder',
            'is_from_client_portal' => false
        ));
    }
}
