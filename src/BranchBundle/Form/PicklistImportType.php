<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PicklistImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'picklistImport', 
                'Symfony\Component\Form\Extension\Core\Type\FileType',
                array(
                    'label' => 'Import File',
                    'constraints' => array(
                        new File(array(
                            'maxSize' => '2M',
                            'mimeTypes' => [
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // xlsx
                                'application/vnd.ms-office', // generic ms-office
                                'application/vnd.ms-excel', // xls
                                'application/vnd.oasis.opendocument.formula', // odf
                                'application/vnd.oasis.opendocument.spreadsheet' // ods                                
                            ],
                            'mimeTypesMessage' => "The mime type of the file is invalid ({{ type }})."
                        ))
                    )
                )
            );

        if(!$options['is_from_client_portal']){
            $builder
                ->add(
                    'client',
                    'Symfony\Bridge\Doctrine\Form\Type\EntityType',
                    array(
                        'class' => 'CoreBundle:Client',
                        'choice_label' => 'name'
                    )
                );
        }

            $builder->add(
                    'isQuarantine',
                    'Symfony\Component\Form\Extension\Core\Type\CheckboxType',
                    array(
                        'label'    => 'For Quarantine?',
                        'required' => false,
                    )
                );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'is_from_client_portal' => false
        ));
    }
}
