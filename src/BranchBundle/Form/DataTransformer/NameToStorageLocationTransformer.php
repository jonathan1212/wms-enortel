<?php

namespace BranchBundle\Form\DataTransformer;

use CoreBundle\Repository\StorageLocationRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class NameToStorageLocationTransformer implements DataTransformerInterface
{
    private $storageLocationRepository;

    public function __construct(StorageLocationRepository $storageLocationRepository)
    {
        $this->storageLocationRepository = $storageLocationRepository;
    }

    public function transform($storageLocation)
    {
        if (null === $storageLocation) {
            return '';
        }

        return $storageLocation->getName();
    }

    public function reverseTransform($storageLocationName)
    {
        if (!$storageLocationName) {
            throw new TransformationFailedException(sprintf('Storage Location cannot be null'));
        }

        $storageLocation = $this->storageLocationRepository
            ->findOneByName($storageLocationName);

        if (null === $storageLocation) {
            throw new TransformationFailedException(sprintf(
                'Storage Location "%s" does not exist!',
                $storageLocationName
            ));
        }

        return $storageLocation;
    }
}
