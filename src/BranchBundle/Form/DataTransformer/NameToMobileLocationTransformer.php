<?php

namespace BranchBundle\Form\DataTransformer;

use CoreBundle\Repository\MobileLocationRepository;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class NameToMobileLocationTransformer implements DataTransformerInterface
{
    private $mobileLocationRepository;

    public function __construct(MobileLocationRepository $mobileLocationRepository)
    {
        $this->mobileLocationRepository = $mobileLocationRepository;
    }

    public function transform($mobileLocation)
    {
        if (null === $mobileLocation) {
            return '';
        }

        return $mobileLocation->getName();
    }

    public function reverseTransform($mobileLocationName)
    {
        if (!$mobileLocationName) {
            return null;
        }

        $mobileLocation = $this->mobileLocationRepository
            ->findOneByName($mobileLocationName);

        if (null === $mobileLocation) {
            throw new TransformationFailedException(sprintf(
                'Mobile Location "%s" does not exist!',
                $mobileLocationName
            ));
        }

        return $mobileLocation;
    }
}
