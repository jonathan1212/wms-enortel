<?php

namespace BranchBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use CoreBundle\Repository\MobileLocationRepository;
use CoreBundle\Repository\StorageLocationRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use BranchBundle\Form\DataTransformer\NameToMobileLocationTransformer;
use BranchBundle\Form\DataTransformer\NameToStorageLocationTransformer;

class InventoryMoveHistoryType extends AbstractType
{
    private $storageLocationRepository;
    private $mobileLocationRepository;

    public function __construct(
        StorageLocationRepository $storageLocationRepository, 
        MobileLocationRepository $mobileLocationRepository
    )
    {
        $this->storageLocationRepository = $storageLocationRepository;
        $this->mobileLocationRepository = $mobileLocationRepository;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'target',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'label' => 'Scan Item or Mobile Location'
                )
            )
            ->add(
                'storageLocation',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'invalid_message' => 'That is not a valid Storage Location',
                    'label' => 'Target Storage Location'
                )
            )
            ->add(
                'mobileLocation',
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'invalid_message' => 'That is not a valid Mobile Location',
                    'required' => false,
                    'label' => 'Target Mobile Location'
                )
            )
        ;

        $builder->get('storageLocation')
            ->addModelTransformer(new NameToStorageLocationTransformer($this->storageLocationRepository));

        $builder->get('mobileLocation')
            ->addModelTransformer(new NameToMobileLocationTransformer($this->mobileLocationRepository));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\InventoryMoveHistory'
        ));
    }
}
