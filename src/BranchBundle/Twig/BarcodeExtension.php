<?php

namespace BranchBundle\Twig;

use BG\BarcodeBundle\Util\Base1DBarcode as barCode;

class BarcodeExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('render_barcode', array($this, 'renderBarcode')),
        );
    }

    public function renderBarcode($data, $barcodeType, $width, $height)
    {
        $barcode = new barCode();
        return $barcode->getBarcodeHTML($data, $barcodeType, $width, $height);
    }
}
