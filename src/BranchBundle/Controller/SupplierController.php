<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\Supplier;
use Symfony\Component\HttpFoundation\Request;
use BranchBundle\Commands\Supplier\CreateSupplierCommand;
use BranchBundle\Commands\Supplier\DeleteSupplierCommand;
use BranchBundle\Commands\Supplier\UpdateSupplierCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SupplierController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:Supplier')
            ->getIndexSuppliers();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:Supplier:index.html.twig',[
            'pagination' => $pagination,
            'moduleAction' => 'Manage Suppliers',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\SupplierType', 
            new Supplier()
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplier = $form->getData();
            $command = new CreateSupplierCommand(
                $supplier->getName(),
                $supplier->getAddress(),
                $supplier->getContactPerson(),
                $supplier->getContactNumber()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_supplier_index');
        }

        return $this->render('BranchBundle:Supplier:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Add Supplier',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function updateAction($supplierId, Request $request)
    {
        $supplier = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:Supplier', $supplierId);

        if(!$supplier){
            return $this->redirectToRoute('branch_supplier_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\SupplierType', 
            $supplier
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $supplier = $form->getData();
            $command = new UpdateSupplierCommand(
                $supplier->getId(),
                $supplier->getName(),
                $supplier->getAddress(),
                $supplier->getContactPerson(),
                $supplier->getContactNumber()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_supplier_index');
        }

        return $this->render('BranchBundle:Supplier:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Update Supplier',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function deleteAction($supplierId)
    {
        $supplier = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:Supplier', $supplierId);

        if(!$supplier){
            return $this->redirectToRoute('branch_supplier_index');
        }

        $command = new DeleteSupplierCommand($supplierId);
        $this->get('command_bus')->handle($command);
        return $this->redirectToRoute('branch_supplier_index');
    }
}
