<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CoreBundle\Entity\Inventory;
use CoreBundle\Entity\Billing;
use Symfony\Component\HttpFoundation\JsonResponse;
use Dompdf\Dompdf;
use Dompdf\Options;

class BillingController extends Controller
{

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine();
        $query = $em->getRepository('CoreBundle:Billing')->getIndexBilling();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );


        return $this->render("BranchBundle:Billing:list.html.twig", [
                'moduleAction'      => 'Manage Billing',
                'moduleDescription' => 'Billing List',
                'pagination'        => $pagination,                
            ]);   
    }

    public function exportAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billing = $em->getRepository('CoreBundle:Billing')->find($id);

        $billingTable = Billing::getBillingDetailObject($billing->getType());

        $billingDetail = $em->getRepository("CoreBundle:{$billingTable}")->criteria([
            'billing'   => $billing
        ])
        ->query()
        ->getResult();

        return $this->get('branch_billing_export_service')->exportBilling($billing,$billingDetail);
    }

    
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billing = $em->getRepository('CoreBundle:Billing')->find($id);
        $billingTable = Billing::getBillingDetailObject($billing->getType());

        $billingDetail = $em->getRepository("CoreBundle:{$billingTable}")->findByBilling($billing);

        foreach ($billingDetail as $detail) {
            $em->remove($detail);
        }

        $em->remove($billing);
        
        $em->flush();

        $this->addFlash('success', 'Successfully Removed!');

        return $this->redirectToRoute('branch_billing_list');
    }

    public function printAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();
        $billing = $em->getRepository('CoreBundle:Billing')->find($id);
        $dateFrom = $billing->getDateAdded()->format('Y-m-d');
        $dateTo = $billing->getDateTo()->format('Y-m-d');

        $billingTable = Billing::getBillingDetailObject($billing->getType());
        
        $results = $em->getRepository("CoreBundle:{$billingTable}")->criteria([
            'billing'   => $billing
        ])
        ->query()
        ->getResult();

        if ($billing->getType() == BILLING::BILLING_INBOUND) {
            $column = 'dateInbound';
        }
        else if ($billing->getType() == BILLING::BILLING_PICKED) {
            $column = 'datePick';
        }
        else if ($billing->getType() == BILLING::BILLING_PACKED) {
            $column = 'datePack';
        }
        else if ($billing->getType() == BILLING::BILLING_INSURANCE) {
            $column = 'insurance';
        }
        else if ($billing->getType() == BILLING::BILLING_STORAGE) {
            $column = 'storage';
        }

        $request->request->set('column', $column);
        $request->request->set('start', $dateFrom);
        $request->request->set('end', $dateTo);
        $template = 'print';

        $billingPrice = $this->getParameter('billing');
            
        $price = 0;
        $price = $billingPrice[strtoupper($column.'ed')]['value'];
        
        $datediff = $this->datediff($dateFrom,$dateTo);
        $dayleft = (int)$datediff + 1;

        if ($column == 'insurance') {
            $template = 'insurance';
            $column = 'dateInbound';
        } else if ($column == 'storage') {
            $template = 'storage';
            $column = 'dateInbound';
        } else if ($column == 'dateInbound') {
            $template = 'inbound';
            $column = 'dateInbound';
        } 

        if ($template == 'storage') {
            $storage = [];
            foreach ($results as $inventory) {
                if (method_exists($inventory, 'getInventory')) {
                    $inventory = $inventory->getInventory();
                }
                 
                $location = $inventory->getStorageLocation()->getName();

                $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
                $storage[$location] = array_unique($storage[$location]);
            }

            $results = $storage;    
        }

        $x = $this->render("BranchBundle:Billing:{$template}.html.twig", [
            'moduleAction' => 'Manage Billing',
            'moduleDescription' => 'Billing',
            'results'   => $results,
            'price'     => $price,
            'client'    => $billing->getClient(),
            'warehouse' => $this->getUser()->getWarehouse()->getName(),
            'dayleft'  => $dayleft
        ]);

        $docsPath = $this->get('kernel')->getRootDir() . '/../web/docs';

        $filename = $docsPath.'/'.$id.'.html';
        if (!file_exists($filename)) {
            file_put_contents($filename,$x->getContent() );    
        }

        $isPDF = $request->get('isPDF');
        if ($isPDF) {
            return $this->returnPDFResponseFromHTML($x->getContent());
        }

        return $x;
        
    }

    public function exportPdfAction(Request $request)
    {
        $id = $request->get('id');
        
        $docsPath = $this->get('kernel')->getRootDir() . '/../web/docs';

        $filename = $docsPath.'/'.$id.'.html';

        $options = new Options();
        $options->set('defaultFont', 'Courier');            
        $options->set( 'isRemoteEnabled', TRUE );

        $dompdf = new Dompdf($options);
        $dompdf->set_option('chroot', $this->get('kernel')->getRootDir() . '/../web/docs/');
        //$dompdf->loadHtml($x->getContent());
        $dompdf->load_html_file($filename);
        
        //$dompdf = new Dompdf();
        //$dompdf->set_option('defaultFont', 'Courier');

        // (Optional) Setup the paper size and orientation
        //$dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        //$xy = $dompdf->output();

        //$dompdf->render();

        return $dompdf->stream();      
    }

    public function returnPDFResponseFromHTML($html){
        //set_time_limit(30); uncomment this line according to your needs
        // If you are not in a controller, retrieve of some way the service container and then retrieve it
        //$pdf = $this->container->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        //if you are in a controlller use :
        $pdf = $this->get("white_october.tcpdf")->create('vertical', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Our Code World');
        $pdf->SetTitle(('Our Code World Title'));
        $pdf->SetSubject('Our Code World Subject');
        $pdf->setFontSubsetting(true);
        $pdf->SetFont('helvetica', '', 10, '', true);
        //$pdf->SetMargins(20,20,40, true);
        $pdf->AddPage();
        
        $filename = 'ourcodeworld_pdf_demo';
        
        $pdf->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 0, $ln = 1, $fill = 0, $reseth = true, $align = '', $autopadding = true);
        return $pdf->Output($filename.".pdf",'I'); // This will output the PDF as a response directly
   
    }

    public function paidAction(Request $request)
    {
        $id = $request->get('id');
        $remarks = $request->get('remarks');
        $em = $this->getDoctrine()->getManager();

        $billing = $em->getRepository('CoreBundle:Billing')->find($id);

        $billing->setIsPaid(1);
        $billing->setRemarks($remarks);
        
        $em->flush();

        return new JsonResponse([
            'isSuccessful' => true
        ]);

    }

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $clients = $em->getRepository('CoreBundle:Client')->getIndexClients()->getArrayResult();

        if($request->isMethod('POST')) {

            $authenticatedUser =  $this->getUser();
            $clientId = $request->get('client');
            $column = $request->get('column');
            $this->column = $column;
            $export = $request->get('export');
            $save = $request->get('save');
            $dateFrom = $request->get('start');
            $dateTo = $request->get('end');

            $template = 'print';

            $billing = $this->getParameter('billing');
            
            $price = 0;
            if ($column != 'all') {
                $price = $billing[strtoupper($column.'ed')]['value'];
            }

            if ($column == 'insurance') {
                $template = 'insurance';
                $column = 'dateInbound';
            } else if ($column == 'storage') {
                $template = 'storage';
                $column = 'dateInbound';
            } else if ($column == 'dateInbound') {
                $template = 'inbound';
                $column = 'dateInbound';
            } else if ($column == 'all') {
                $template = 'all';
                $column = 'dateInbound';
            }

            $results = $this->getResults($request,$column,$template,$clientId);            
            $resultsStorage = $results;
            $datediff = $this->datediff($request->get('start'),$request->get('end'));
            
            if ($template == 'storage') {
                $storage = [];
                foreach ($results as $inventory) {
                    $location = $inventory->getStorageLocation()->getName();

                    if (is_null($inventory->getProduct())) {
                        continue;
                    }
            
                    $storage[$location][] = $inventory->getDateInbounded()->format('Y-m-d');
                    $storage[$location] = array_unique($storage[$location]);
                }

                $results = $storage;    
            }

            $client = $em->getRepository('CoreBundle:Client')->find($clientId);
            $dayleft = (int)$datediff + 1;

            if ($export) {
                return $this->export(compact("results", "price", "client","template","column","dayleft" ,"resultsStorage"));
            }

            if ($save) {
                if ($template == 'storage') {
                    $results = $resultsStorage;    
                }

                $response = $this->get('branch_billing_export_service')->saveBilling(
                    compact("results", "price", "client","template","column","dayleft" ,"resultsStorage", "authenticatedUser", "dateFrom", "dateTo")
                );

                $this->addFlash('success', $response['message']);

                if ($response['isSuccessfull']) {
                    return $this->redirectToRoute('branch_billing_print',['id' => $response['id']]);
                } else {
                    return $this->redirectToRoute('branch_billing_list');    
                }
                
            }
            
            return $this->render("BranchBundle:Billing:{$template}.html.twig", [
                'moduleAction' => 'Manage Billing',
                'moduleDescription' => 'Billing',
                'results'   => $results,
                'price'     => $price,
                'client'    => $client,
                'warehouse' => $this->getUser()->getWarehouse()->getName(),
                'dayleft'  => $dayleft
            ]);
        }


        return $this->render('BranchBundle:Billing:index.html.twig', [
            'moduleAction' => 'Manage Billing',
            'moduleDescription' => 'Billing',
            'clients'   => $clients

        ]);
        
    }

    public function export($params)
    {        
        return $this->get('branch_billing_export_service')->export($params);    
    }

    protected function getResults($request,$column,$template,$clientId,$someQuery=null)
    {
        $em = $this->getDoctrine();
        $type = Billing::getTypeByField($this->column);
        $billingTable = Billing::getBillingDetailObject($type);

        if ($template == 'storage' || $template == 'insurance') {
            // from the beginning date
            $resultsV1 = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "{$column}Start" => $request->get('start'),
                    "{$column}End" => $request->get('end'),
                    'client'    => $clientId
                ))
                ->leftJoin(
                    "CoreBundle:{$billingTable}",
                    'billingDetail',
                    'WITH',
                    "this = billingDetail.inventory"
                )
                ->andWhere('billingDetail.inventory IS NULL')

                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();

            $resultsV2 = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "{$column}Start" => '2016-01-01',
                    "{$column}End" => $request->get('start'),
                    'client'    => $clientId
                ))
                ->andWhere('this.inventoryStatus != :dispatch')
                ->setParameter('dispatch', Inventory::DISPATCHED_STATUS)
                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();


            $resultDiff = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    /*"{$column}Start" => '2016-01-01',
                    "{$column}End" => $request->get('start'),*/
                    'client'    => $clientId
                ))
                //->andWhere('this.inventoryStatus != :dispatch')
                //->setParameter('dispatch', Inventory::DISPATCHED_STATUS)
                ->leftJoin(
                    "CoreBundle:{$billingTable}",
                    'billingDetail',
                    'WITH',
                    "this = billingDetail.inventory"
                )
                ->andWhere('billingDetail.dateProcess >= :start and billingDetail.dateProcess <= :end')
                ->setParameter('start', $request->get('start'))
                ->setParameter('end', $request->get('end'))


                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();


            $v2 = array_map(function ($inventory) {
                return $inventory->getId();
            },$resultsV2);

            $v3 = array_map(function ($inventory) {
                return $inventory->getId();
            },$resultDiff);

            
            $diff = array_diff($v2, $v3);


            $newResult = $em->getRepository('CoreBundle:Inventory')
                ->criteria()
                ->andWhere('this.id IN (:inventoryId)')
                ->setParameter('inventoryId', $diff)
                ->query()
                ->getResult();

            $results = array_merge($resultsV1,$newResult);


        } else {

            $results = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                    "{$column}Start" => $request->get('start'),
                    "{$column}End" => $request->get('end'),
                    'client'    => $clientId,
                ))
                ->leftJoin(
                    "CoreBundle:{$billingTable}",
                    'billingDetail',
                    'WITH',
                    "this = billingDetail.inventory"
                )
                ->andWhere('billingDetail.inventory IS NULL')

                ->orderBy('DESC',$column.'ed')
                ->query()
                ->getResult();
            
        }

        return $results;
    }

    private function datediff($start,$end)
    {

        $now = strtotime($end);
        $your_date = strtotime($start);
        $datediff = $now - $your_date;

        return floor($datediff / (60 * 60 * 24));
    }

}