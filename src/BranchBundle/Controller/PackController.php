<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use BranchBundle\Commands\Pack\PackInventoryCommand;
use BranchBundle\Commands\Pack\DispatchPicklistCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PackController extends Controller
{
    public function indexAction(Request $request)
    {
        if(!$request->isMethod('POST')){
            return $this->render('BranchBundle:Pack:index.html.twig', [
                'moduleAction' => 'Pack',
                'moduleDescription' => 'TBD'
            ]);
        }

        $picklistParticular = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->findOneByPicklistParticularNumber(
                $request->request->get('picklistNumber', -1)
            );

        if(!$picklistParticular){
            return $this->render('BranchBundle:Pack:index.html.twig', [
                'moduleAction' => 'Pack',
                'moduleDescription' => 'TBD'
            ]);
        }

        $picklistAggregate = $picklistParticular->getPicklistAggregate();
        $notYetForPacking = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getPicklistParticularsThatCannotBePackedByPicklistNumber(
                $picklistAggregate->getPicklistNumber()
            );

        if(count($notYetForPacking) !== 0){
            return $this->render('BranchBundle:Pack:index.html.twig', [
                'moduleAction' => 'Pack',
                'moduleDescription' => 'TBD'
            ]);
        }

        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:Picklist')
            ->findPicklistByPicklistNumberAndAcceptableStatusesState(
                $picklistAggregate->getPicklistNumber(),
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePacked()
            );

        if(!$picklist){
            $picklistForPackingStatus = $this->getDoctrine()
                ->getRepository('CoreBundle:PicklistStatus')
                ->findOneByValue(\CoreBundle\Entity\PicklistStatus::FOR_PACKING);

            $picklistAggregate->setPicklistStatus($picklistForPackingStatus);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('branch_pack_pack', [
            'picklistNumber' => $picklistAggregate->getPicklistNumber()
        ]);
    }

    public function packAction($picklistNumber, Request $request)
    {
        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:Picklist')
            ->getPicklistDetailsByPicklistNumberAndAcceptableStatusesState(
                $picklistNumber,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePacked()
            );

        if(!$picklist){
            return $this->render('BranchBundle:Pack:index.html.twig', [
                'moduleAction' => 'Pack',
                'moduleDescription' => 'TBD'
            ]);
        }

        return $this->render('BranchBundle:Pack:pack.html.twig', [
            'picklistCanBeReprinted' => \CoreBundle\Entity\PicklistStatus::getPicklistCanBeReprinted(),
            'inventoryHasBeenPacked' => \CoreBundle\Entity\InventoryStatus::getInventoryHasBeenPacked(),
            'picklistNumber' => $picklistNumber,
            'picklist' => $picklist,
            'moduleAction' => 'Pack',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function packInventoryAction(Request $request)
    {
        $picklistNumber = $request->request->get('picklistNumber', -1);
        $tag = $request->request->get('tag', -1);

        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:Picklist')
            ->getPicklistByPicklistNumberInventoryTagAndAcceptableStatusesState(
                $picklistNumber,
                $tag,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePacked(),
                \CoreBundle\Entity\InventoryStatus::getInventoryCanBePacked()
            );

        if(!$picklist){
            return new JsonResponse([
                'isSuccessful' => false,
                'errorMessage' => sprintf(
                    'Either %s is already packed, cannot be packed or is not a valid Inventory under Picklist %s', 
                    $tag, 
                    $picklistNumber
                )
            ]);
        }

        $command = new PackInventoryCommand(
            $picklistNumber,
            $tag
        );
        
        $this->get('command_bus')->handle($command);

        return new JsonResponse([
            'isSuccessful' => true,
            'tag' => $tag
        ]);
    }

    public function dispatchAction(Request $request)
    {
        $picklistNumber = $request->request->get('picklistNumber', -1);
        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:Picklist')
            ->findPicklistByPicklistNumberAndAcceptableStatusesState(
                $picklistNumber,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePacked()
            );

        if(!$picklist){
            return $this->redirectToRoute('branch_pack_index');
        }

        $tagsForDispatch = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistDetail')
            ->getInventoryTagsByPicklistNumberAndInventoryStatusAsArray(
                $picklistNumber,
                \CoreBundle\Entity\InventoryStatus::PACKED
            );

        $command = new DispatchPicklistCommand(
            $picklistNumber
        );

        $this->get('command_bus')->handle($command);

        if(count($tagsForDispatch) === 0){
            return $this->redirectToRoute('branch_pack_index');
        }

        $productDetails = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getInventoryDetailsForProofOfDeliveryByTagsAsArray($tagsForDispatch);

        return $this->render('BranchBundle:Pack:print.html.twig', compact(
            'productDetails',
            'picklist'
        ));
    }

    public function reprintAction(Request $request)
    {
        $picklistNumber = $request->get('picklistNumber', -1);
        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:Picklist')
            ->findPicklistByPicklistNumberAndAcceptableStatusesState(
                $picklistNumber,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBeReprinted()
            );

        if(!$picklist){
            return $this->redirectToRoute('branch_pack_pack', [
                'picklistNumber' => $picklistNumber
            ]);
        }

        //$tagsForReprint = $request->get('inventoryTags', null);

        $tagsForReprint = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistDetail')
            ->getInventoryTagsByPicklistNumberAndInventoryStatusAsArray(
                $picklistNumber,
                \CoreBundle\Entity\InventoryStatus::DISPATCHED
            );

        if(!$tagsForReprint || count($tagsForReprint) === 0){
            return $this->redirectToRoute('branch_pack_pack', [
                'picklistNumber' => $picklistNumber
            ]);
        }

        $productDetails = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getInventoryDetailsForProofOfDeliveryByTagsAsArray($tagsForReprint);

        $isReprint = true;
        return $this->render('BranchBundle:Pack:print.html.twig', compact(
            'productDetails',
            'picklist',
            'isReprint'
        ));
    }
}
