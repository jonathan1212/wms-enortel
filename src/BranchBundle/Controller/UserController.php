<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\User;
use CoreBundle\Entity\WarehouseMember;
use Symfony\Component\HttpFoundation\Request;
use BranchBundle\Commands\User\CreateUserCommand;
use BranchBundle\Commands\User\DeleteUserCommand;
use BranchBundle\Commands\User\UpdateUserCommand;
use BranchBundle\Commands\User\ChangeUserPasswordCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\User\UpdateUserWarehouseMembershipCommand;

class UserController extends Controller
{
    public function indexAction(Request $request)
    {
        $params = $request->query->all();
        $params['clientAccount'] = -1; // filter branch users only

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->getIndexUsers($params);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:User:index.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Manage Users',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\UserType', 
            new User(), 
            ['validation_groups' => ['add']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new CreateUserCommand(
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmailAddress(),
                $user->getPlainPassword(),
                $user->getContactNumber()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_user_index');
        }

        return $this->render('BranchBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Add Users',
            'moduleDescription' => 'TBD'
        ));
    }

    public function updateAction($userId, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->findOneBy([
                'id' => $userId, 
                'clientAccount' => null
            ]);

        if(!$user){
            return $this->redirectToRoute('branch_user_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\UserType', 
            $user, 
            ['validation_groups' => ['update']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new UpdateUserCommand(
                $user->getId(),
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmailAddress(),
                $user->getContactNumber()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_user_index');
        }

        return $this->render('BranchBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Update Users',
            'moduleDescription' => 'TBD'
        ));
    }

    public function changePasswordAction($userId, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->findOneBy([
                'id' => $userId, 
                'clientAccount' => null
            ]);

        if(!$user){
            return $this->redirectToRoute('branch_user_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\UserType', 
            $user, 
            ['validation_groups' => ['change_password']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new ChangeUserPasswordCommand(
                $user->getId(),
                $user->getPlainPassword()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_user_index');
        }

        return $this->render('BranchBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Change Password',
            'moduleDescription' => 'For security reasons, only the logged in user can change his/her password.'
        ));
    }

    public function deleteAction($userId)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->findOneBy([
                'id' => $userId, 
                'clientAccount' => null
            ]);

        if(!$user){
            return $this->redirectToRoute('branch_user_index');
        }

        $command = new DeleteUserCommand($userId);
        $this->get('command_bus')->handle($command);
        return $this->redirectToRoute('branch_user_index');
    }

    public function updateWarehouseMembershipAction($userId, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->findOneBy([
                'id' => $userId, 
                'clientAccount' => null
            ]);

        if(!$user){
            return $this->redirectToRoute('branch_user_index');
        }

        $originalMemberships = new \Doctrine\Common\Collections\ArrayCollection();

        foreach ($user->getWarehouseMemberships() as $membership) {
            $originalMemberships->add($membership);
        }

        $form = $this->createForm('BranchBundle\Form\WarehouseMembershipType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new UpdateUserWarehouseMembershipCommand(
                $user->getId(),
                $originalMemberships
            );

            try{
                $this->get('command_bus')->handle($command);
                return $this->redirectToRoute('branch_user_index');
            } catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e){
                $form->get('warehouseMemberships')->addError(new \Symfony\Component\Form\FormError(
                    'Duplicate membership found, please check your inputs.'
                ));
            }
        }

        return $this->render('BranchBundle:User:warehouse_membership_form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Update Warehouse Membership',
            'moduleDescription' => 'tbd'
        ));
    }
}
