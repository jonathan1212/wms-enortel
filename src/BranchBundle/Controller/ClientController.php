<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use BranchBundle\Commands\Client\CreateClientCommand;
use BranchBundle\Commands\Client\UpdateClientCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\Client\ChangeClientPasswordCommand;

class ClientController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:Client')
            ->getIndexClients();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:Client:index.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Manage Clients',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\ClientType', 
            new Client(), 
            ['validation_groups' => ['add']]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $command = new CreateClientCommand(
                $client->getName(),
                $client->getContactPerson(),
                $client->getContactNumber(),
                $client->getEmailAddress(),
                $client->getPlainPassword(),
                $client->getHostAlias()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_client_index');
        }

        return $this->render('BranchBundle:Client:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Add Client',
            'moduleDescription' => 'TBD'
        ));
    }

    public function updateAction($clientId, Request $request)
    {
        $client = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:Client', $clientId);

        if(!$client){
            return $this->redirectToRoute('branch_client_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\ClientType', 
            $client, 
            ['validation_groups' => ['update']]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $command = new UpdateClientCommand(
                $client->getId(),
                $client->getName(),
                $client->getContactPerson(),
                $client->getContactNumber(),
                $client->getEmailAddress(),
                $client->getHostAlias()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_client_index');
        }

        return $this->render('BranchBundle:Client:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Update Client',
            'moduleDescription' => 'TBD'
        ));
    }

    public function changePasswordAction($clientId, Request $request)
    {
        $client = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:Client', $clientId);

        if(!$client){
            return $this->redirectToRoute('branch_client_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\ClientType', 
            $client, 
            ['validation_groups' => ['change_password']]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $client = $form->getData();
            $command = new ChangeClientPasswordCommand(
                $client->getId(),
                $client->getPlainPassword()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_client_index');
        }

        return $this->render('BranchBundle:Client:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Change Client Password',
            'moduleDescription' => 'TBD'
        ));
    }
}
