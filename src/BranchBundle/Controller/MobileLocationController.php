<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\MobileLocation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\MobileLocation\CreateMobileLocationCommand;
use BranchBundle\Commands\MobileLocation\UpdateMobileLocationCommand;

class MobileLocationController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:MobileLocation')
            ->getMobileLocationsIndex();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:MobileLocation:index.html.twig',[
            'pagination' => $pagination,
            'moduleAction' => 'Mobile Location',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\MobileLocationType', 
            new MobileLocation()
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mobileLocation = $form->getData();
            $command = new CreateMobileLocationCommand(
                $mobileLocation->getName(),
                $this->getUser()->getWarehouse()->getId()
            );

            try{
                $this->get('command_bus')->handle($command);
                return $this->redirectToRoute('branch_mobile_location_index');
            } catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e){
                $form->get('name')->addError(new \Symfony\Component\Form\FormError(
                    'Duplicate Mobile Location name found.'
                ));
            }
        }

        return $this->render('BranchBundle:MobileLocation:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Add Mobile Location',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function updateAction($mobileLocationId, Request $request)
    {
        $mobileLocation = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:MobileLocation', $mobileLocationId);

        if(!$mobileLocation){
            return $this->redirectToRoute('branch_mobile_location_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\MobileLocationType', 
            $mobileLocation
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $mobileLocation = $form->getData();
            $command = new UpdateMobileLocationCommand(
                $mobileLocation->getId(),
                $mobileLocation->getName()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_mobile_location_index');
        }

        return $this->render('BranchBundle:MobileLocation:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Update Mobile Location',
            'moduleDescription' => 'TBD'
        ]);
    }
}
