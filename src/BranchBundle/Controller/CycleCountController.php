<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CoreBundle\Entity\Inventory;
use CoreBundle\Entity\CycleCount;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CycleCountController extends Controller
{

    public function listAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:CycleCount')
            ->getIndexCycleCount();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:CycleCount:list.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Manage CycleCount',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $storageLocations = $em
            ->getRepository('CoreBundle:StorageLocation')
            ->getStorageLocationsIndex()
            ->getResult();

        $clients = $em
            ->getRepository('CoreBundle:Client')
            ->getIndexClients()
            ->getResult();

        $mobileLocations = $this->getDoctrine()
            ->getRepository('CoreBundle:MobileLocation')
            ->getMobileLocationsIndex()
            ->getResult();

        return $this->render("BranchBundle:CycleCount:index.html.twig", [
            'moduleAction' => 'Cycle Count',
            'moduleDescription' => '',
            'storageLocations' => $storageLocations,
            'mobileLocations'   => $mobileLocations,
            'clients'           => $clients
            
        ]);

    }

    public function viewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        $cycleCount = $em->getRepository('CoreBundle:CycleCount')->find($id);

        $c = explode(',', $cycleCount->getAccuracy());
        
        $diff = $cycleCount->getStartDate()->diff($cycleCount->getEndDate());

        return new JsonResponse([
            'isSuccessful' => true,
            'data'          => $cycleCount->getDetails(),
            'list'          => $cycleCount->getList(),
            'cyclecountOk'  => $c[0],
            'cyclecountNot' => $c[1],
            'diffInMin'     => $diff->i,
            'diffInSec'     => $diff->s,
            'message' => 'Ok'
        ]);

    }

    public function exportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        $cycleCount = $em->getRepository('CoreBundle:CycleCount')->find($id);

        $c = explode(',', $cycleCount->getAccuracy());
        
        $diff = $cycleCount->getStartDate()->diff($cycleCount->getEndDate());

        ob_start();
        $fp = fopen('php://output', 'w');

        $warehouse = $cycleCount->getWarehouse() ? $cycleCount->getWarehouse()->getName() : '';
        fputcsv($fp,array('CYCLECOUNT REFERENCE','WAREHOUSE','USER', 'STORAGE LOCATION', 'DURATION'));

        fputcsv($fp, array( $cycleCount->getCyclecount(), $warehouse,
                            $cycleCount->getCreatedBy()->getFullname(),
                            $cycleCount->getStorageLocation()->getName(),
                            $cycleCount->getDiffDuration()
                ));
        fputcsv($fp, array());
        fputcsv($fp, array());

        fputcsv($fp,array('INVENTORY ACCURACY'));
        fputcsv($fp, array($cycleCount->checkAccuracy()));

        fputcsv($fp, array());
        fputcsv($fp, array());

        fputcsv($fp, array('TAG','MOBILE LOCATION', 'STORAGE LOCATION', 'SKU', 'ACCURACY', 'STATUS'));

        foreach (json_decode($cycleCount->getDetails(),true) as $detail) {
            fputcsv($fp, array($detail['tag'],$detail['mobileloc'],$detail['storageloc'], $detail['sku'], $detail['corrected'] ? 'miss': 'hit', $detail['status']));
        }


        fputcsv($fp, array());
        fputcsv($fp, array());
        fputcsv($fp, array('REMAINING'));
        fputcsv($fp, array('TAG','MOBILE LOCATION', 'STORAGE LOCATION', 'SKU', 'STATUS'));
        foreach (json_decode($cycleCount->getList(),true) as $detail) {
            fputcsv($fp, array($detail['tag'],$detail['mobile'],$detail['storage'], $detail['sku'], $detail['status']));
        }

        fclose($fp);
        $exportDate = date('mdYHis') . '.csv';

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', "attachment; filename={$exportDate}");
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
        
        return $response;
        ob_end_flush();
    }

    public function saveCycleAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $all = $request->request->all();
        $startDate = new \DateTime($request->get('startDate'));
        $endDate = new \DateTime($request->get('endDate'));
        $clientId = $request->get('clientId');
        $params = json_decode($all['params'],true);

        $storageLocation = $em->getRepository('CoreBundle:StorageLocation')->find($request->get('storage'));
        $mobileLocation = $em->getRepository('CoreBundle:MobileLocation')->find($request->get('mobile'));
        $client = $em->getRepository('CoreBundle:Client')->find($clientId);
        $results = [];
        $emptyData = [];
        $skuList = [];

        $cyclecountOk = 0;
        $cyclecountNot = 0;
        $cyclecountNotV2 = 0;


        $tags = array_map(function($p){
            return $p['tag'];
        }, $params);


        $resultInv = $this->getAllInboundedRecord(compact("tags","storageLocation", "mobileLocation"));
        
        foreach ($params as $param) {

            $product = $em->getRepository('CoreBundle:Inventory')
                ->criteria(['tag' => $param['tag']])
                ->query()->getOneOrNullResult();
            
            $sku = 'not found sku';
            $status = 'not recognized';

            if ($product) {
                $sku = $product->getProduct() ? $product->getProduct()->getClientSku() : 'not found sku';    
                $status = $product->getInventoryStatus() ? $product->getInventoryStatus()->getName() : 'not recognized';
            }
            
            $inventory = $em->getRepository('CoreBundle:Inventory')
                ->criteria([
                    'tag'           => $param['tag'],
                    'storageLocId'  => $param['storagelocId'],
                    'mobileLocId'   => $param['mobilelocId'],
                    'client'        => $client,
                    //'statusNotEqual'    => 7
                ])
                ->andWhere('this.inventoryStatus = :inventoryStatus')
                ->setParameter('inventoryStatus', 1)
                ->getCount();

            if ($inventory > 0) {
                $param['corrected'] = false;
                $param['sku'] = $sku;
                $param['status'] = $status;
                //$param['corrected_details'] = '';
                array_push($results, $param);
                array_push($skuList, $sku);
                $cyclecountOk++;
            } else {                
                $inventory = $em->getRepository('CoreBundle:Inventory')
                    ->criteria([
                        'tag'           => $param['tag'],
                        'client'        => $client,
                        'statusNotEqual'    => 7
                    ])->result();

                if (!$inventory) {
                    $cyclecountNotV2++;
                    $rs = [
                        'tag'           => $param['tag'],
                        'mobileloc'     => $param['mobileloc'],
                        'mobilelocId'   => $param['mobilelocId'],
                        'storageloc'    => $param['storageloc'],
                        'storagelocId'  => $param['storagelocId'],
                        'sku'           => $sku,
                        'status'        => $status,
                        'corrected'     => true

                    ];

                    //$emptyData[] = $rs;
                    array_push($results, $rs);
                    array_push($skuList, $sku);

                } else {
                    $cyclecountNot++;
                    
                    $data = [
                        'tag'           => $param['tag'],
                        'mobileloc'     => $param['mobileloc'],
                        'mobilelocId'   => $param['mobilelocId'],
                        'storageloc'    => $param['storageloc'],
                        'storagelocId'  => $param['storagelocId'],
                        'sku'           => $sku,
                        'status'        => $status,
                        'corrected'     => true

                    ];

                    array_push($results, $data);
                    array_push($skuList, $sku);
                }

                
            }
        }

        //if (count($results) > 0) {
            //$notOk = $cyclecountNot + $cyclecountNotV2;
            $notOk = $resultInv['countDiff'];

            $num_str = sprintf("%06d", mt_rand(1, 999999));
            $cycleCount = new CycleCount();
            $cycleCount->setCyclecount('CYCL'.$num_str);
            $cycleCount->setDetails(json_encode($results));
            $cycleCount->setList(json_encode($resultInv['listInv']));
            $cycleCount->setSkuList(json_encode( array_unique($skuList) ));

            $cycleCount->setCreatedBy($this->getUser());
            $cycleCount->setClient($client);
            $cycleCount->setDateCreated(new \DateTime("now"));
            $cycleCount->setAccuracy($cyclecountOk. ','.($notOk));
            $cycleCount->setStorageLocation($storageLocation);
            $cycleCount->setStartDate($startDate);
            $cycleCount->setEndDate($endDate);
            $cycleCount->setWarehouse($this->getUser()->getWarehouse());

            $em->persist($cycleCount);
            $em->flush();    
        //}
        
        $diff = $startDate->diff($endDate);

        return new JsonResponse([
            'isSuccessful' => true,
            'data'          => array_merge($results,$emptyData),
            'list'          => $resultInv['listInv'],
            'cyclecountOk'  => $cyclecountOk,
            'cyclecountNot' => $notOk,
            'startDate'     => $request->get('startDate'),
            'endDate'       => $request->get('endDate'),
            'diffInMin'     => $diff->i,
            'diffInSec'     => $diff->s,
            'message' => 'Ok'
        ]);


        
    }


    protected function getAllInboundedRecord($params)
    {

        $em = $this->getDoctrine()->getManager();

        $inventory = $em->getRepository('CoreBundle:Inventory')
                ->criteria([
                    'storageLocId'  => $params['storageLocation'],
                    'mobileLocId'   => $params['mobileLocation'],
                    //'statusNotEqual'    => 7
                ])
                ->andWhere('this.inventoryStatus = :inventoryStatus')
                ->setParameter('inventoryStatus', 1)
                ->query()
                ->getArrayResult()
                ;

        $invDiff1 = array_map(function($i){
            return $i['tag'];
        }, $inventory);
                

        $inventoryV2 = $em->getRepository('CoreBundle:Inventory')
                ->criteria([
                    'storageLocId'  => $params['storageLocation'],
                    'mobileLocId'   => $params['mobileLocation'],
                    'tag'           => $params['tags']
                ])
                ->andWhere('this.inventoryStatus = :inventoryStatus')
                ->setParameter('inventoryStatus', 1)
                ->query()
                ->getArrayResult()
                ;

        $invDiff2 = array_map(function($i){
            return $i['tag'];
        }, $inventoryV2);


        $diffRS = array_diff($invDiff1, $invDiff2);
        $countDiff = count($diffRS);
        
        $temp = [];
        $listInv = [];
    
        foreach ($inventory as $in) {
            $temp[$in['tag']] = [
                'tag'   => $in['tag'],
                'sku'   => $in['product']['clientSku'],
                'mobile'    => $in['mobileLocation']['name'],
                'storage'    => $in['storageLocation']['name'],
                'status'     => $in['inventoryStatus']['name'],
            ];  
        }        
        
        
        foreach ($diffRS as $d) {
            if (isset($temp[$d])) {
                $listInv[] = $temp[$d];
            }
        }

        return compact("inventory","countDiff","listInv");
        
    }

}