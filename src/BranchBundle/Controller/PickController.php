<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use BranchBundle\Commands\Pick\PickInventoryCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\Pick\HandoverPicklistForPackingCommand;

class PickController extends Controller
{
    public function indexAction(Request $request)
    {
        if(!$request->isMethod('POST')){
            return $this->render('BranchBundle:Pick:index.html.twig', [
                'moduleAction' => 'Pick',
                'moduleDescription' => 'TBD'
            ]);
        }

        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->findPicklistParticularByPicklistNumberAndAcceptableStatusesState(
                $request->request->get('picklistNumber', -1),
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePicked()
            );


        if(!$picklist){
            return $this->render('BranchBundle:Pick:index.html.twig', [
                'moduleAction' => 'Pick',
                'moduleDescription' => 'TBD'
            ]);
        }

        return $this->redirectToRoute('branch_pick_pick', [
            'picklistNumber' => $picklist->getPicklistParticularNumber()
        ]);
    }

    public function processBulkPrintAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $orderNumbers = trim($request->request->get('orderNumbers'));

        $orderNumbersArr = array_map('trim', explode(PHP_EOL, $orderNumbers));
        
        $data =  $em->getRepository('CoreBundle:Picklist')->getPicklistByOrderNumber($orderNumbersArr);

        return $this->render('BranchBundle:Picklist:print_v2.html.twig', ['data'=> $data]);
           
    }

    public function processBulkAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $orderNumbers = trim($request->request->get('orderNumbers'));

        $orderNumbersArr = array_map('trim', explode(PHP_EOL, $orderNumbers));

        $orderNumbersRs = $em->getRepository('CoreBundle:PicklistParticular')->getPicklistParticular($orderNumbersArr);
        
        $picklstpartics = $em->getRepository('CoreBundle:PicklistParticular')
            ->findPicklistParticularByPicklistNumberAndAcceptableStatusesStateMultiple(
                $orderNumbersRs,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePicked()
            );

        $retrievedParticulars = array_map(function($particular){
            return $particular['picklistParticularNumber'];
        }, $picklstpartics);

        $diff = array_diff($orderNumbersRs, $retrievedParticulars);

        if (count($diff) == 0) {

            $picklistArr = $em->getRepository('CoreBundle:PicklistParticular')
                ->getPicklistDetailByParticular($retrievedParticulars);

            $invoiceGenerator = $this->get('branch.service.picklist.invoice_number_generator');

            $picklistParticularIds = [];
            foreach ($picklistArr as $list) {

                $inventory = $em->getRepository('CoreBundle:Inventory')->findOneByTag($list['tag']);

                $inventoryPickedStatus = $em->getRepository('CoreBundle:InventoryStatus')
                    ->findOneByValue(\CoreBundle\Entity\InventoryStatus::DISPATCHED);

                $inventory->setInventoryStatus($inventoryPickedStatus);
                $inventory->setDatePicked(new \DateTime('now'));
                $inventory->setDatePacked(new \DateTime('now'));
                
                $picklistParticularIds[] = $list['particularId'];
                $em->flush();
            }

            $picklistStatus = $em->getRepository('CoreBundle:PicklistStatus')
                    ->findOneByValue(\CoreBundle\Entity\PicklistStatus::DISPATCHED);

            foreach ($picklistParticularIds as $id) {
                $picklistParticular = $em->getRepository('CoreBundle:PicklistParticular')->find($id);
                $picklistParticular->setPicklistStatus($picklistStatus);    

                $picklist = $picklistParticular->getPicklistAggregate();
                $picklist->setPicklistStatus($picklistStatus);
                $picklist->setInvoiceNumber($invoiceGenerator->generate());

            }

            $em->flush();

            $this->addFlash('success', 'successfully picklisted');

        } else {

            $errorList = implode(',', $diff);
             
            $this->addFlash('error', "error -  $errorList");

        }

        return $this->render('BranchBundle:Pick:index.html.twig', [
            'moduleAction' => 'Pick',
            'moduleDescription' => 'TBD'
        ]);

    }

    public function pickAction($picklistNumber, Request $request)
    {
        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getPicklistDetailsByPicklistParticularNumberAndAcceptableStatusesState(
                $picklistNumber,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePicked()
            );

        if(!$picklist){
            return $this->render('BranchBundle:Pick:index.html.twig', [
                'moduleAction' => 'Pick',
                'moduleDescription' => 'TBD'
            ]);
        }

        return $this->render('BranchBundle:Pick:pick.html.twig', [
            'picklistHasBeenPicked' => \CoreBundle\Entity\PicklistStatus::getPicklistHasAlreadyBeenPicked(),
            'inventoryHasBeenPicked' => \CoreBundle\Entity\InventoryStatus::getInventoryHasBeenPicked(),
            'picklistNumber' => $picklistNumber,
            'picklist' => $picklist,
            'moduleAction' => 'Pick',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function pickInventoryAction(Request $request)
    {
        $picklistNumber = $request->request->get('picklistNumber', -1);
        $tag = $request->request->get('tag', -1);

        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getPicklistByPicklistParticularNumberInventoryTagAndAcceptableStatusesState(
                $picklistNumber,
                $tag,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePicked(),
                \CoreBundle\Entity\InventoryStatus::getInventoryCanBePicked()
            );

        if(!$picklist){
            return new JsonResponse([
                'isSuccessful' => false,
                'errorMessage' => sprintf(
                    'Either %s is already picked, cannot be picked or is not a valid Inventory under Picklist %s', 
                    $tag, 
                    $picklistNumber
                )
            ]);
        }

        $command = new PickInventoryCommand(
            $picklistNumber,
            $tag
        );
        
        $this->get('command_bus')->handle($command);

        return new JsonResponse([
            'isSuccessful' => true,
            'tag' => $tag
        ]);
    }

    public function forPackingAction(Request $request)
    {
        $picklistNumber = $request->request->get('picklistNumber', -1);
        $picklist = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->findPicklistParticularByPicklistParticularNumberAndAcceptableStatusesState(
                $picklistNumber,
                \CoreBundle\Entity\PicklistStatus::getPicklistCanBePicked()
            );

        if(!$picklist){
            return new JsonResponse([
                'isSuccessful' => false,
                'errorMessage' => 'Invalid Picklist Number. Please refresh the page to proceed.'
            ]);
        }

        $command = new HandoverPicklistForPackingCommand(
            $picklistNumber
        );
        
        $this->get('command_bus')->handle($command);

        return new JsonResponse([
            'isSuccessful' => true
        ]);
    }
}
