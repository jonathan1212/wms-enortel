<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InventoryController extends Controller
{
    protected $em;
    protected $conn;


    public function indexAction(Request $request)
    {   
        $em = $this->getDoctrine()->getManager();
        $params = $this->getParams($request);

        if ($request->get('export')) {
            return $this->forward('BranchBundle:Inventory:export', array('request' => $request));
        }

        $mobileLocations = $em->getRepository('CoreBundle:MobileLocation')->findAll();
        $storageLocations = $em->getRepository('CoreBundle:StorageLocation')->findAll();
        $inventoryStatus = $em->getRepository('CoreBundle:InventoryStatus')->findAll();

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getIndexInventories($params);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:Inventory:index.html.twig',[
            'pagination' => $pagination,
            'mobileLocations'  => $mobileLocations,
            'storageLocations'   => $storageLocations,
            'inventoryStatus'   => $inventoryStatus,
            'moduleAction' => 'Inventory',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function inventoryMasterReportAction(Request $request)
    {
        $em = $this->getDoctrine();
        $conn = $em->getConnection();
        $inventoryService = $this->get('branch.service.inventory.inventory_service');

        $clients = $em->getRepository('CoreBundle:Client')->findAll();        
        $clientId = 12;

        if($request->isMethod('POST')) {
            $clientId = $request->get('client');
        }

        $warehouseId = $this->getUser()->getWarehouse()->getId();

        $rs = $conn->fetchAll(" 
            select p.id, p.client_sku, p.product_description, count(p.product_description) as productCount
            from inventory i
            left join product p on i.product_id = p.id
            where i.warehouse_id = :warehouseId and i.client_id = :clientId and p.deleted_at is null
            group by i.product_id order by p.client_sku ASC
            ", ["clientId" => $clientId, "warehouseId" => $warehouseId]
        );

        $list = [];

        foreach ($rs as $each) {

            $i = $inventoryService->count($each,$warehouseId,$clientId);

            if ($i['onHand'] > 0 || $i['allocated'] > 0 || $i['quaranteed'] > 0 ) {

                $product = [];

                $product['info']                 = $each;
                $product['countOnHand']          = $inventoryService->count($each,$warehouseId,$clientId);
                $product['countByOrderNumber']   = $inventoryService->countByOrderNumber($each);
                
                $list[] = $product;
            }

            

        }

        return $this->render('BranchBundle:Inventory:list.html.twig',[
            'moduleAction' => 'Inventory Report',
            'list'  => $list,
            'clients' => $clients,
            'moduleDescription' => 'TBD'
        ]);

    }

    public function exportFormAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $clients = $em->getRepository('CoreBundle:Client')->getIndexClients()->getArrayResult();

        return $this->render('BranchBundle:Inventory:export.html.twig',[
            'moduleAction' => 'Inventory',
            'moduleDescription' => 'TBD',
            'clients'       => $clients,
        ]);

    }

    public function exportAction(Request $request)
    {
        $this->em = $this->get('doctrine')->getEntityManager();        
        $this->conn = $this->em->getConnection();
        $inventoryService = $this->get('branch.service.inventory.inventory_service');
        
        ob_start();
        $fp = fopen('php://output', 'w');

        $params = $this->getParams($request);

        $result = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getIndexInventories($params)
            ->getArrayResult();

        fputcsv($fp, array(
            'CLIENT',
            'PICKLISTNO',
            'PICKLIST PARTICULAR',
            'CREATED BY',
            'REMARKS' ,
            'PICKLISTED BY',
            'DATE PICKLISTED',
            'CONSIGNEE',
            'ADDRESS',
            'TAG NO',
            'LMBM SKU',
            'SERIAL NO',
            'CLIENT SKU',
            'SUPPLIER SKU',
            'STORAGE LOCATION', 
            'MOBILE LOCATION',
            'ORDER NO',
            'PO NO',
            'COGP',
            'INVENTORY STATUS',
            'DELIVERY STATUS',
            'ATTACHMENT',
            'WAYBILL NUMBER',
            'DELIVERED REMARKS',
            'DATE DELIVERED',
            'DELIVERED TO', 
            'DATE INBOUNDED',
            'DATE PICKED',
            'DATE PACKED',
            'NAME',
            'BRANCH',

             ));

        foreach ($result as  $each) {
                
                $picklist = $inventoryService->getPicklist($each['picklistDetail']);
                
                $datePicked = $each['datePicked'] ? $each['datePicked']->format('Y-m-d H:i:s') : '';
                $datePacked = $each['datePacked'] ? $each['datePacked']->format('Y-m-d H:i:s') : '';

                fputcsv($fp, array( 
                    $each['client']['name'],
                    $picklist['picklistNumber'],
                    $picklist['picklistParticularNumber'],
                    $picklist['createdBy'],
                    $each['purchaseOrder']['remarks'],
                    $picklist['createdBy'],
                    $picklist['datePicklisted'],
                    $picklist['consignee'],
                    $picklist['address'],
                    $each['tag'],
                    $each['product']['sku'],
                    "{$each['serialNo']}",
                    $each['product']['clientSku'],
                    $each['product']['supplierSku'],
                    $each['storageLocation']['name'],
                    $each['mobileLocation']['name'],
                    $each['orderNumber'],
                    $each['purchaseOrder']['purchaseOrderNumber'],
                    $each['product']['costOfGoodsPurchased'],
                    $each['inventoryStatus']['name'],
                    $picklist['status'],
                    $picklist['attachment'],
                    $picklist['waybill_number'],
                    $picklist['remarks'],
                    $picklist['deliveredCreated'],
                    $picklist['deliveredto'],                       
                    $each['dateInbounded']->format('Y-m-d H:i:s'),
                    $datePicked,
                    $datePacked,
                    $each['product']['productDescription'],
                    $each['warehouse']['name']
                    )                    
                );
            }

            fclose($fp);
            $exportDate = "inventory". date('Y-m-d') . '.csv';

            $response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
            $response->headers->set('Content-Disposition', "attachment; filename={$exportDate}");
            $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
            
            return $response;
            ob_end_flush();

    }



    public function importSearialAction(Request $request)
    {
        $excelObject = null;
        $file = $request->files->get('file');

        try{
            $excelObject = $this->get('phpexcel')->createPHPExcelObject($file);
        } catch(\Exception $e) {
            return new JsonResponse(array(
                'isSuccessful' => false,
                'errorMessage' => 'Invalid File'
            ));
        }

        $data = $excelObject->getSheet(0)->toArray(null);
        unset($data[0]);

        $tagIdsNotFound = $this->get('branch.service.inventory.inventory_service')
            ->addSerial($data);

        return new JsonResponse(['isSuccessful' => true, 'data' => $tagIdsNotFound]);   
    }

    private function getParams($request)
    {
        return $params = [
            'status'    => $request->get('status'),
            'purchaseOrder' => $request->get('purchaseOrderId'),
            'orderNumber'   => $request->get('orderNumberId'),
            'tag'           => $request->get('tag'),
            'clientsku'   => $request->get('clientsku'),
            'mobileLocation'    => $request->get('mobileLocation'),
            'storageLocation'   => $request->get('storageLocation'),
            'client'            => $request->get('client'),
            'start'             => $request->get('start'),
            'end'               => $request->get('end')
        ];
         
    }
}
