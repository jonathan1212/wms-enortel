<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UnservedOrderController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:Picklist')
            ->getIndexPicklistsIsComplete(false);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:UnservedOrder:index.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Unserved Orders',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function viewAction($picklistNumber, Request $request)
    {
        $picklist = $this->getDoctrine()
            ->getManager()
            ->getRepository('CoreBundle:Picklist')
            ->findOneByPicklistNumber($picklistNumber);

        if(!$picklist){
            return $this->redirectToRoute('branch_unserved_order_index');
        }

        // compute diff between current status of PL versus that in orderHistory
        $diff = $this->get('branch.service.picklist.picklist_differ_service')
            ->getDiff($picklist);

        return $this->render('BranchBundle:UnservedOrder:view.html.twig', [
            'diff' => $diff,
            'moduleAction' => sprintf('%s Preview', $picklistNumber),
            'moduleDescription' => sprintf('Order Number: %s', $picklist->getOrderNumber()) 
        ]);
    }
}
