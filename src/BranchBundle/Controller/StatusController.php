<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use CoreBundle\Entity\PicklistDelivered;
use CoreBundle\Entity\PicklistStatus;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StatusController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getByStatus([
                'status' => PicklistStatus::DISPATCHED,
                'intransit'  => true
            ]);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:Status:index.html.twig', [
            'pagination' => $pagination,
            'activeNav' => 'Status',
            'moduleAction' => 'Status',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function deliveredAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getByStatus([
                'status' => PicklistStatus::DISPATCHED,
                'picklistDeliveredStatus'  => PicklistDelivered::STATUS_DELIVERED
            ]);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:Status:delivered.html.twig', [
            'pagination' => $pagination,
            'activeNav' => 'StatusDelivered',
            'moduleAction' => 'StatusDelivered',
            'moduleDescription' => 'TBD'
        ]);   
    }

    public function getStatusAction(Request $request)
    {
        //picklistId this is picklistparticularId
        $em = $this->getDoctrine()
            ->getManager();
            
        $picklistParticular = $em->getRepository('CoreBundle:PicklistParticular')->find($request->get('picklistId'));

        $picklistDelivered = $em->getRepository('CoreBundle:PicklistDelivered')->findOneByPicklistParticular($picklistParticular);
        
        if ($picklistDelivered) {
            return new JsonResponse([
                'isSuccessful' => true,
                'data'  => [
                    'waybill'       => $picklistDelivered->getWaybillNumber(),
                    'deliveredTo'   => $picklistDelivered->getDeliveredto(),
                    'remarks'       => $picklistDelivered->getRemarks()
                ]
            ]);
        }

        return new JsonResponse([
                'isSuccessful' => true,
                'data'  => [
                    'waybill'       => '',
                    'deliveredTo'   => '',
                    'remarks'       => ''
                ]
            ]);

    }

    public function updateStatusAction(Request $request)
    {
        //picklistId this is picklistparticularId
        $em = $this->getDoctrine()
            ->getManager();

        $status = $request->get('status');
        $waybill = $request->get('waybill');
        $remarks = $request->get('remarks');
        $pickupBy = $request->get('pickupBy');
        $attachment = $request->files->get('attachment');

        $picklistParticular = $em->getRepository('CoreBundle:PicklistParticular')->find($request->get('picklistId'));

        $picklistDelivered = $em->getRepository('CoreBundle:PicklistDelivered')->findOneByPicklistParticular($picklistParticular);
        
        if (is_null($picklistDelivered)) {
            $picklistDelivered = new PicklistDelivered();
        }
    
        $file = $this->moveUploadedFile($attachment);

        $picklistDelivered->setPicklistParticular($picklistParticular);
        $picklistDelivered->setStatus($status);
        $picklistDelivered->setRemarks($remarks);
        $picklistDelivered->setDeliveredto($pickupBy);
        $picklistDelivered->setWaybillNumber($waybill);
        $picklistDelivered->setCreatedBy($this->getUser());

        $picklistParticular->setFile($file);

        $em->persist($picklistDelivered);
        $em->flush();

        return new JsonResponse(array(
            'isSuccessful' => true,
            'message' => 'Successfull',
            'data'  => [
                'statusname'    => $picklistDelivered->getStatusName()
            ]
        ));

    }


    public function updateStatusMultipleAction(Request $request)
    {
        $em = $this->getDoctrine()
            ->getManager();

        $status = $request->get('statusId');
        $waybill = $request->get('waybill');
        $remarks = $request->get('remarks');
        $pickupBy = $request->get('pickupBy');
        $attachment = $request->files->get('attachment');
        $picklistParticularNumber = $request->get('picklistNumbers');

        foreach ($picklistParticularNumber as $picklistNumber) {
            
            $picklistParticular = $em->getRepository('CoreBundle:PicklistParticular')->findOneByPicklistParticularNumber($picklistNumber);

            $picklistDelivered = $em->getRepository('CoreBundle:PicklistDelivered')->findOneByPicklistParticular($picklistParticular);
            
            if (is_null($picklistDelivered)) {
                $picklistDelivered = new PicklistDelivered();
            }
        
            $file = $this->moveUploadedFile($attachment);

            $picklistDelivered->setPicklistParticular($picklistParticular);
            $picklistDelivered->setStatus($status);
            $picklistDelivered->setRemarks($remarks);
            $picklistDelivered->setDeliveredto($pickupBy);
            $picklistDelivered->setWaybillNumber($waybill);
            $picklistDelivered->setCreatedBy($this->getUser());

            $picklistParticular->setFile($file);

            $em->persist($picklistDelivered);
            $em->flush();    

            //dump($picklistDelivered->getId());exit;
        }
        

        return new JsonResponse(array(
            'isSuccessful' => true,
            'message' => 'Successfull',
            'data'  => [
                'statusname'    => ''
            ]
        ));

    }

    protected function moveUploadedFile(UploadedFile $file = null)
    {
        $filename = null;
        if (!is_null($file)) {

            $filename = date('His') . '.'. $file->getClientOriginalExtension(); 
            $uploadDirectory = $this->get('kernel')->getRootDir() . '/../web/documents/';
            
            $file->move ($uploadDirectory, $filename);
        }

        return $filename;
    }

}
