<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    public function loginAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        $warehouses = $this->getDoctrine()
            ->getRepository('CoreBundle:Warehouse')
            ->findAll();

        return $this->render('BranchBundle:Security:login.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'warehouses'    => $warehouses
        ));
    }
}
