<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CoreBundle\Entity\Inventory;
use CoreBundle\Entity\Billing;
use Symfony\Component\HttpFoundation\JsonResponse;
use Dompdf\Dompdf;
use Dompdf\Options;

class BillingV2Controller extends Controller
{
    protected $billingIds = [];

    public function listAction(Request $request)
    {
        $em = $this->getDoctrine();
        $query = $em->getRepository('CoreBundle:BillingSummary')->getIndexBilling();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );


        return $this->render("BranchBundle:BillingV2:list.html.twig", [
                'moduleAction'      => 'Manage Billing',
                'moduleDescription' => 'Billing List',
                'pagination'        => $pagination,                
            ]);   
    }

    public function billingStorageAction(Request $request)
    {
        $em = $this->getDoctrine();
        $query = $em->getRepository('CoreBundle:BillingStorage')->getIndexBilling();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );


        return $this->render("BranchBundle:BillingV2:storage_billing.html.twig", [
                'moduleAction'      => 'Manage Storage Billing',
                'moduleDescription' => 'Billing Storage List',
                'pagination'        => $pagination,                
            ]);   
               
    }

    public function deleteStorageAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billingStorage = $em->getRepository("CoreBundle:BillingStorage")->find($id);

        $em->remove($billingStorage);
        $em->flush();

        $this->addFlash('success', 'Successfully Removed!');

        return $this->redirectToRoute('branch_billing_storage');
    }


    public function exportStorageBillingAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billing = $em->getRepository('CoreBundle:BillingStorage')->find($id);

        return $this->get('branch_billing_export_service_excel')
            ->exportStorageToExcel($billing);   
    }

    public function billingStorageAddAction(Request $request)
    {

        $all = $request->request->all();

        $all['user'] = $this->getUser();

        $this->get('branch_billing_export_service_excel')->addStorageBilling($all);

        $this->addFlash('success', 'Successfully Saved!');
        return $this->redirectToRoute('branch_billing_storage');

    }

    public function kittingListAction(Request $request)
    {
        $em = $this->getDoctrine();
        $query = $em->getRepository('CoreBundle:BillingKitting')->getIndexBilling();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render("BranchBundle:BillingV2:kitting-list.html.twig", [
                'moduleAction'      => 'Manage Billing Kitting',
                'moduleDescription' => 'Billing Kitting',
                'pagination'        => $pagination,
            ]);

    }

    public function billingReportAction(Request $request)
    {
        if($request->isMethod('POST')) {
            
            $all = $request->request->all();
            return $this->get('branch_billing_export_service_excel')->exportBillingReport($all);

        }

        return $this->render("BranchBundle:BillingV2:report.html.twig", [
                'moduleAction'      => 'Billing Report',
                'moduleDescription' => 'Billing Report'
            ]);
    }


    public function kittingAddAction(Request $request)
    {
        $em = $this->getDoctrine();

        if($request->isMethod('POST')) {
            
            $all = $request->request->all();

            $all['user'] = $this->getUser();

            $this->get('branch_billing_export_service_excel')->saveKitting($all);

            $this->addFlash('success', 'Successfully Saved!');
            return $this->redirectToRoute('branch_billing_kitting_list');

        }

        $clients = $em->getRepository('CoreBundle:Client')->getIndexClients()->getArrayResult();

        return $this->render("BranchBundle:BillingV2:billing-kitting.html.twig", [
                'moduleAction'      => 'Manage Billing Kitting',
                'moduleDescription' => 'Billing Kitting',
                'clients'           =>  $clients
            ]);
    }

    public function deleteKittingAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billingKitting = $em->getRepository("CoreBundle:BillingKitting")->find($id);

        $billingKittingDetail = $em->getRepository("CoreBundle:BillingKittingDetail")->findByBillingKitting($billingKitting);

        foreach ($billingKittingDetail as $each) {

            $em->remove($each);
        }

        $em->remove($billingKitting);
        $em->flush();

        $this->addFlash('success', 'Successfully Removed!');

        return $this->redirectToRoute('branch_billing_kitting_list');
    }

    public function exportKittingAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $kitting = $em->getRepository('CoreBundle:BillingKitting')->find($id);

        return $this->get('branch_billing_export_service_excel')
            ->exportKittingToExcel($kitting);
           
    }


    public function exportAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billing = $em->getRepository('CoreBundle:BillingSummary')->find($id);

        return $this->get('branch_billing_export_service_excel')
            ->exportToExcel($billing);
    }

    public function exportV2Action(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billing = $em->getRepository('CoreBundle:BillingSummary')->find($id);

        $this->get('branch_billing_export_service_excel')
            ->exportToExcel($billing,true);

        return $this->redirect('/docs/'. $billing->getBillingNumber() . '.xls');
    }

    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $em = $this->getDoctrine()->getManager();

        $billingSummary = $em->getRepository("CoreBundle:BillingSummary")->find($id);

        $billingIds = json_decode($billingSummary->getRemarks());
        
        foreach ($billingIds as $id) {

            $billing = $em->getRepository('CoreBundle:Billing')->find($id);
            $billingTable = Billing::getBillingDetailObject($billing->getType());

            $billingDetail = $em->getRepository("CoreBundle:{$billingTable}")->findByBilling($billing);

            foreach ($billingDetail as $detail) {
                $em->remove($detail);
            }

            $em->remove($billing);
            
        }

        $em->remove($billingSummary);        
        $em->flush();

        $this->addFlash('success', 'Successfully Removed!');

        return $this->redirectToRoute('branch_billing_list_v2');
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine();
        $clients = $em->getRepository('CoreBundle:Client')->getIndexClients()->getArrayResult();

        if($request->isMethod('POST')) {

            $authenticatedUser =  $this->getUser();
            $clientId = $request->get('client');
            
            $dateFrom = $request->get('start');
            $dateTo = $request->get('end');
            $save = $request->get('save');
            
            $template = 'print';
            $billingParams = $this->getParameter('billing');

            $billing = $billingParams['default'];

            if (isset($billingParams['client_' . $clientId])) {
                 $billing = $billingParams['client_'.$clientId]; 
            }

            $price = 0;
           
            $collectionList = [
                ['column' => 'dateInbound', 'template' => 'insurance' ],
                ['column' => 'dateInbound', 'template' => 'storage' ],
                ['column' => 'dateInbound', 'template' => 'inbound' ],
                ['column' => 'datePick', 'template' => 'print' ],
                ['column' => 'datePack', 'template' => 'print' ],
            ];

            $user = $authenticatedUser->getFirstName() . ' ' . $authenticatedUser->getLastName();
            $warehouseId = $authenticatedUser->getWarehouseId();

            foreach ($collectionList as $v) {
                $isCbm = false;

                $column = $v['column'];
                $template = $v['template'];

                $price = $billing[strtoupper($column.'ed')]['value'];

                if ($template == 'storage') {
                    $price = $billing['STORAGEED']['value'];
                    if ($billing['IS_CBM']) {
                        $isCbm = true;
                    }
                }

                if ($template == 'insurance') {
                    $price = $billing['INSURANCEED']['value'];
                }

                $this->get('branch_billing_export_service_excel')
                ->process($request, $column, $template, $clientId, $price,$dateFrom,$dateTo,$user,$warehouseId,$isCbm );
            }

            $this->addFlash('success', 'Save');
            

            $id = $this->get('branch_billing_export_service_excel')->save(
                $clientId,$dateFrom, $dateTo, $user,$warehouseId
            );

            /*$billing = $em->getRepository('CoreBundle:BillingSummary')->find($id);

            $this->container->get('branch_billing_export_service_excel')
                ->exportToExcel($billing,true);*/
        }


        return $this->render('BranchBundle:BillingV2:add.html.twig', [
            'moduleAction' => 'Manage Billing',
            'moduleDescription' => 'BillingV2',
            'clients'   => $clients

        ]);
        
    }



}