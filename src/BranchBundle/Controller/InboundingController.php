<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\Inbounding\CreateInboundHistoryCommand;

class InboundingController extends Controller
{
    public function indexAction(Request $request)
    {
        $purchaseOrder = $this->getDoctrine()
            ->getRepository('CoreBundle:PurchaseOrder')
            ->findOneByPurchaseOrderNumber($request->request->get('purchaseOrderNumber', -1));

        if(!$purchaseOrder){

            return $this->render('BranchBundle:Inbounding:index.html.twig',[
                'moduleAction' => 'Inbounding',
                'moduleDescription' => 'TBD'
            ]);
        }

        return $this->redirectToRoute('branch_inbounding_inbound', [
            'purchaseOrderNumber' => $purchaseOrder->getPurchaseOrderNumber()
        ]);
    }

    public function printAction(Request $request)
    {
        $tagsGenerated = $this->get('session')->getFlashBag()->get('tagsGenerated');
        if(count($tagsGenerated) === 0){
            return $this->redirectToRoute('branch_inbounding_index');
        }

        $items = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getInventoryDetailsForStickerPrintByTags($tagsGenerated);

        return $this->render('BranchBundle:Inbounding:print.html.twig',[
            'items' => $items,
            'moduleAction' => 'Print Item Tags',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function printv2Action(Request $request)
    {

        $purchaseOrder = $request->get('purchaseOrder');
        $tags = array();

        $inventory = $this->getDoctrine()
                                ->getRepository('CoreBundle:Inventory')
                                ->getIndexInventories(['purchaseOrder' => $purchaseOrder])
                                ->getResult();
        
        foreach ($inventory as $each) {
            $tags[] = $each->getTag();
        }

        if(count($tags) === 0){
            return $this->redirectToRoute('branch_inbounding_index');
        }

        $items = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getInventoryDetailsForStickerPrintByTags($tags);

        /*$len = count($items);
        $firsthalf = array_slice($items, 0, $len / 2);
        $secondhalf = array_slice($items, $len / 2);*/

        return $this->render('BranchBundle:Inbounding:printv2.html.twig',[
            'items' => $items,
            'moduleAction' => 'Print Item Tags',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function inboundAction($purchaseOrderNumber, Request $request)
    {
        $purchaseOrder = $this->getDoctrine()
            ->getRepository('CoreBundle:PurchaseOrder')
            ->findPurchaseOrderByPurchaseOrderNumber($purchaseOrderNumber);

        if(!$purchaseOrder){
            return $this->redirectToRoute('branch_inbounding_index');
        }

        $currentInboundState = $this->get('branch.service.inbounding.inbound_history_processor')
            ->computeCurrentPurchaseOrderInboundDetails($purchaseOrder);

        return $this->render('BranchBundle:Inbounding:inbound.html.twig',[
            'moduleAction' => $purchaseOrderNumber,
            'moduleDescription' => 'TBD',
            'purchaseOrder' => $purchaseOrder,
            'currentInboundState' => $currentInboundState
        ]);
    }

    public function submitAction($purchaseOrderNumber, Request $request)
    {
        $purchaseOrder = $this->getDoctrine()
            ->getRepository('CoreBundle:PurchaseOrder')
            ->findPurchaseOrderByPurchaseOrderNumber($purchaseOrderNumber);

        if(!$purchaseOrder){
            return new JsonResponse([
                'isSuccessful' => false,
                'errorMessage' => 'Invalid Purchase Order'
            ]);
        }

        $inboundingDetails = $request->request->all();
        $validationResults = $this->get('branch.service.inbounding.location_validator')
            ->validate($inboundingDetails);

        if(!$validationResults['isSuccessful']){
            return new JsonResponse([
                'isSuccessful' => false,
                'errorMessage' => $validationResults['errorMessage']
            ]);
        }

        $command = new CreateInboundHistoryCommand(
            $purchaseOrderNumber,
            $inboundingDetails,
            $this->getUser()->getWarehouse()->getId()
        );

        try{
            $this->get('command_bus')->handle($command);
        } catch(\UnexpectedValueException $e){
            return new JsonResponse([
                'isSuccessful' => false,
                'errorMessage' => $e->getMessage()
            ]);
        }

        return new JsonResponse([
            'isSuccessful' => true
        ]);
    }
}
