<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\Warehouse;
use BranchBundle\Form\WarehouseType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand;
use BranchBundle\Commands\Warehouse\DeleteWarehouseCommand;
use BranchBundle\Commands\Warehouse\UpdateWarehouseCommand;

class WarehouseController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:Warehouse')
            ->getIndexWarehouses();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:Warehouse:index.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Manage Warehouse',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm('BranchBundle\Form\WarehouseType', new Warehouse());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $warehouse = $form->getData();
            $command = new CreateWarehouseCommand(
                $warehouse->getName(),
                $warehouse->getAddress(),
                $warehouse->getContactNumber()
            ); 

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_warehouse_index');
        }

        return $this->render('BranchBundle:Warehouse:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Add Warehouse',
            'moduleDescription' => 'TBD'
        ));
    }

    public function updateAction($warehouseId, Request $request)
    {
        $warehouse = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:Warehouse', $warehouseId);

        if(!$warehouse){
            return $this->redirectToRoute('branch_warehouse_index');
        }

        $form = $this->createForm('BranchBundle\Form\WarehouseType', $warehouse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $warehouse = $form->getData();
            $command = new UpdateWarehouseCommand(
                $warehouse->getId(),
                $warehouse->getName(),
                $warehouse->getAddress(),
                $warehouse->getContactNumber()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_warehouse_index');
        }

        return $this->render('BranchBundle:Warehouse:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Edit Warehouse',
            'moduleDescription' => 'TBD'
        ));
    }

    public function deleteAction($warehouseId)
    {
        $warehouse = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:Warehouse', $warehouseId);

        if(!$warehouse){
            return $this->redirectToRoute('branch_warehouse_index');
        }

        $command = new DeleteWarehouseCommand($warehouseId);
        $this->get('command_bus')->handle($command);
        return $this->redirectToRoute('branch_warehouse_index');
    }
}
