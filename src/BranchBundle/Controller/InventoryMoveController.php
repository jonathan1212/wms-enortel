<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\InventoryMoveHistory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\InventoryMove\CreateInventoryMoveHistoryCommand;

class InventoryMoveController extends Controller
{
    public function indexAction(Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\InventoryMoveHistoryType',
            new InventoryMoveHistory()
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $history = $form->getData();
            $command = new CreateInventoryMoveHistoryCommand(
                $history->getTarget(),
                $history->getStorageLocation()->getName(),
                $history->getMobileLocation() ? $history->getMobileLocation()->getName() : ''
            );

            try{
                $this->get('command_bus')->handle($command);
            } catch(\Exception $e){
                // silently ignore exception for now
            }

            return $this->redirectToRoute('branch_inventory_move_index');
        }

        return $this->render('BranchBundle:InventoryMove:index.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Inventory Move',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function searchAction(Request $request)
    {
        $query = trim($request->request->get('q', ''));
        $searchResults = $this->get('branch.service.inventory_move.inventory_finder')
            ->getInventoryDetailsAsArrayByQueryTag($query);

        return new JsonResponse($searchResults);
    }
}
