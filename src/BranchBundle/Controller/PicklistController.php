<?php

namespace BranchBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use BranchBundle\Commands\Picklist\PrintPicklistsCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\Picklist\CreatePicklistImportHistoryCommand;
use CoreBundle\Entity\PicklistStatus;


class PicklistController extends Controller
{
    public function indexAction(Request $request)
    {
        $params = [
            'clients'   => $request->get('client'),
            'status'    => $request->get('status'),
            'picklistno'     => $request->get('picklistno'),
            'ordernumber'   => $request->get('ordernumber'),
            'client'        => $request->get('client'),
            'datefrom'      => $request->get('datefrom'),
            'dateto'      => $request->get('dateto'),
        ];

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getIndexPicklistParticulars($params);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1),
                50
            );

        $clients = $this->getDoctrine()->getRepository('CoreBundle:Client')->getIndexClients()->getArrayResult();

        return $this->render('BranchBundle:Picklist:index.html.twig', [
            'pagination' => $pagination,
            'statuses'   => PicklistStatus::getAlStatus(),
            'clients'     => $clients,
            'moduleAction' => 'Picklist',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function importAction(Request $request)
    {
        $form = $this->createForm('BranchBundle\Form\PicklistImportType');
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $picklistImport = $form->getData();
            try{
                $command = new CreatePicklistImportHistoryCommand(
                    $picklistImport['client']->getId(),
                    $picklistImport['picklistImport'],
                    $picklistImport['isQuarantine']
                );

                $this->get('command_bus')->handle($command);
                return $this->redirectToRoute('branch_picklist_index');
            } catch(\UnexpectedValueException $e){
                $form->get('picklistImport')->addError(new \Symfony\Component\Form\FormError(
                    $e->getMessage()
                ));
            }
        }

        return $this->render('BranchBundle:Picklist:import.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Import Picklist',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function viewAction($picklistParticularNumber, Request $request)
    {
        $picklistParticular = $this->getDoctrine()
            ->getManager()
            ->getRepository('CoreBundle:PicklistParticular')
            ->findOneByPicklistParticularNumber($picklistParticularNumber);

        if(!$picklistParticular){
            return $this->redirectToRoute('branch_picklist_index');
        }

        $printAggregate = $this->get('branch.service.picklist.picklist_print_aggregator')
            ->aggregatePicklists([$picklistParticularNumber]);

        return $this->render('BranchBundle:Picklist:view.html.twig', [
            'picklistParticularNumber' => $picklistParticularNumber,
            'printAggregate' => $printAggregate,
            'moduleAction' => $picklistParticularNumber.' Preview',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function printAction(Request $request)
    {
        if(!$request->isMethod('POST')){
            return $this->redirectToRoute('branch_picklist_index');
        }

        $picklistNumbers = $request->request->get('picklistNumbers');
        if(!$picklistNumbers){
            return $this->redirectToRoute('branch_picklist_index');
        }

        $printAggregate = $this->get('branch.service.picklist.picklist_print_aggregator')
            ->aggregatePicklists($picklistNumbers);

        $command = new PrintPicklistsCommand(
            $picklistNumbers
        );

        $this->get('command_bus')->handle($command);
        return $this->render('BranchBundle:Picklist:print.html.twig', compact('printAggregate'));
    }
}
