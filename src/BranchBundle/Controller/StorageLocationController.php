<?php

namespace BranchBundle\Controller;

use CoreBundle\Entity\StorageLocation;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use BranchBundle\Commands\StorageLocation\CreateStorageLocationCommand;
use BranchBundle\Commands\StorageLocation\UpdateStorageLocationCommand;

class StorageLocationController extends Controller
{
	public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:StorageLocation')
            ->getStorageLocationsIndex();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('BranchBundle:StorageLocation:index.html.twig',[
            'pagination' => $pagination,
            'moduleAction' => 'Storage Location',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function addAction(Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\StorageLocationType', 
            new StorageLocation()
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $storageLocation = $form->getData();
            $command = new CreateStorageLocationCommand(
                $storageLocation->getName(),
                $this->getUser()->getWarehouse()->getId()
            );

            try{
                $this->get('command_bus')->handle($command);
                return $this->redirectToRoute('branch_storage_location_index');
            } catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e){
                $form->get('name')->addError(new \Symfony\Component\Form\FormError(
                    'Duplicate Storage Location name found.'
                ));
            }
        }

        return $this->render('BranchBundle:StorageLocation:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Add Storage Location',
            'moduleDescription' => 'TBD'
        ]);
    }

    public function updateAction($storageLocationId, Request $request)
    {
        $storageLocation = $this->getDoctrine()
            ->getManager()
            ->find('CoreBundle:StorageLocation', $storageLocationId);

        if(!$storageLocation){
            return $this->redirectToRoute('branch_storage_location_index');
        }

        $form = $this->createForm(
            'BranchBundle\Form\StorageLocationType', 
            $storageLocation
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $storageLocation = $form->getData();
            $command = new UpdateStorageLocationCommand(
                $storageLocation->getId(),
                $storageLocation->getName()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('branch_storage_location_index');
        }

        return $this->render('BranchBundle:StorageLocation:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Update Storage Location',
            'moduleDescription' => 'TBD'
        ]);
    }
}
