<?php

namespace BranchBundle\Commands\MobileLocation;

class UpdateMobileLocationCommand
{
    private $mobileLocationId;
    private $name;

    public function __construct($mobileLocationId, $name)
    {
        $this->mobileLocationId = $mobileLocationId;
        $this->name = $name;
    }

    /**
     * Gets the value of mobileLocationId.
     *
     * @return mixed
     */
    public function getMobileLocationId()
    {
        return $this->mobileLocationId;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
