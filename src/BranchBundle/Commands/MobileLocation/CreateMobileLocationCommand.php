<?php

namespace BranchBundle\Commands\MobileLocation;

class CreateMobileLocationCommand
{
    private $name;
    private $warehouseId;

    public function __construct($name, $warehouseId)
    {
        $this->name = $name;
        $this->warehouseId = $warehouseId;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of warehouseId.
     *
     * @return mixed
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }
}
