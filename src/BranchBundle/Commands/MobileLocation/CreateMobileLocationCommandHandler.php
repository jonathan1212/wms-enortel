<?php

namespace BranchBundle\Commands\MobileLocation;

use CoreBundle\Entity\MobileLocation;
use CoreBundle\Repository\WarehouseRepository;
use CoreBundle\Repository\MobileLocationRepository;
use BranchBundle\Commands\MobileLocation\CreateMobileLocationCommand;

class CreateMobileLocationCommandHandler
{
    private $mobileLocationRepository;
    private $warehouseRepository;

    public function __construct(MobileLocationRepository $mobileLocationRepository, WarehouseRepository $warehouseRepository)
    {
        $this->mobileLocationRepository = $mobileLocationRepository;
        $this->warehouseRepository = $warehouseRepository;
    }

    public function handle(CreateMobileLocationCommand $command)
    {
        $warehouse = $this->warehouseRepository->find($command->getWarehouseId());
        $location = new MobileLocation();
        $location->setName($command->getName())
            ->setWarehouse($warehouse);

        $this->mobileLocationRepository->add($location);
    }
}
