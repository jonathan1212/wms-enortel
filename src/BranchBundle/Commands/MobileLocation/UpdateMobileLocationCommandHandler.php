<?php

namespace BranchBundle\Commands\MobileLocation;

use CoreBundle\Repository\MobileLocationRepository;
use BranchBundle\Commands\MobileLocation\UpdateMobileLocationCommand;

class UpdateMobileLocationCommandHandler
{
    private $mobileLocationRepository;

    public function __construct(MobileLocationRepository $mobileLocationRepository)
    {
        $this->mobileLocationRepository = $mobileLocationRepository;
    }

    public function handle(UpdateMobileLocationCommand $command)
    {
        $location = $this->mobileLocationRepository->find($command->getMobileLocationId());
        $location->setName($command->getName());
    }
}
