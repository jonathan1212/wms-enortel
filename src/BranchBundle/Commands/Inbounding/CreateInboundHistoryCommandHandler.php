<?php

namespace BranchBundle\Commands\Inbounding;

use SimpleBus\Message\Recorder\RecordsMessages;
use CoreBundle\Repository\PurchaseOrderRepository;
use CoreBundle\Repository\InboundHistoryRepository;
use BranchBundle\Service\Inbounding\InboundHistoryProcessor;

class CreateInboundHistoryCommandHandler
{
    private $inboundHistoryRepository;
    private $purchaseOrderRepository;
    private $inboundHistoryProcessor;
    private $eventRecorder;

    public function __construct(
        RecordsMessages $eventRecorder,
        InboundHistoryRepository $inboundHistoryRepository, 
        PurchaseOrderRepository $purchaseOrderRepository,
        InboundHistoryProcessor $inboundHistoryProcessor
    )
    {
        $this->eventRecorder = $eventRecorder;
        $this->inboundHistoryRepository = $inboundHistoryRepository;
        $this->purchaseOrderRepository = $purchaseOrderRepository;
        $this->inboundHistoryProcessor = $inboundHistoryProcessor;
    }

    public function handle(
        \BranchBundle\Commands\Inbounding\CreateInboundHistoryCommand $command
    )
    {
        $purchaseOrder = $this->purchaseOrderRepository
            ->findOneByPurchaseOrderNumber($command->getPurchaseOrderNumber());

        if(!$this->isInboundDetailValid($purchaseOrder, $command->getInboundDetails())){
            throw new \UnexpectedValueException("Invalid Received Quantity");
        }

        $history = new \CoreBundle\Entity\InboundHistory();
        $history->setPurchaseOrder($purchaseOrder)
            ->setDetails($this->applyCorrectCostOfGoodsPurchased(
                $purchaseOrder, 
                $command->getInboundDetails()
            ));

        $this->inboundHistoryRepository->add($history);
        $this->eventRecorder->record(
            new \BranchBundle\Eventing\Inbounding\PurchaseOrderInboundedEvent(
                $history->getPurchaseOrder()->getPurchaseOrderNumber(),
                $history->getDetails(),
                $command->getWarehouseId()
            )
        );
    }

    private function applyCorrectCostOfGoodsPurchased(
        \CoreBundle\Entity\PurchaseOrder $purchaseOrder, 
        $inboundDetail
    )
    {
        $costOfGoodsPurchasedList = $this->inboundHistoryProcessor
            ->retrieveListOfCorrectCostOfGoodsPurchased($purchaseOrder);

        foreach($costOfGoodsPurchasedList as $sku => $cost){
            $inboundDetail[$sku]['costOfGoodsPurchased'] = $cost;
        }

        return $inboundDetail;
    }

    private function isInboundDetailValid($purchaseOrder, $inboundDetail)
    {
        $currentInboundState = $this->inboundHistoryProcessor
            ->computeCurrentPurchaseOrderInboundDetails($purchaseOrder);
        
        foreach($currentInboundState as $sku => $detail){
            $maxQuantityAllowable = $detail['expectedQuantity'] - $detail['partialQuantity'];
            if($maxQuantityAllowable < intval($inboundDetail[$sku]['receivedQuantity'])){
                return false;
            }
        }

        return true;
    }
}
