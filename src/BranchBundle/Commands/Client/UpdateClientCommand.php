<?php

namespace BranchBundle\Commands\Client;

class UpdateClientCommand
{
    private $clientId;
    private $name;
    private $contactPerson;
    private $contactNumber;
    private $emailAddress;
    private $hostAlias;

    public function __construct($clientId, $name, $contactPerson, $contactNumber, $emailAddress, $hostAlias)
    {
        $this->clientId = $clientId;
        $this->name = $name;
        $this->contactPerson = $contactPerson;
        $this->contactNumber = $contactNumber;
        $this->emailAddress = $emailAddress;
        $this->hostAlias = $hostAlias;
    }

    /**
     * Gets the value of clientId.
     *
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of contactPerson.
     *
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Gets the value of contactNumber.
     *
     * @return mixed
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Gets the value of emailAddress.
     *
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Gets the value of hostAlias.
     *
     * @return mixed
     */
    public function getHostAlias()
    {
        return $this->hostAlias;
    }
}
