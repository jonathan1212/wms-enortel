<?php

namespace BranchBundle\Commands\Client;

class ChangeClientPasswordCommand
{
    private $clientId;
    private $plainPassword;

    public function __construct($clientId, $plainPassword)
    {
        $this->clientId = $clientId;
        $this->plainPassword = $plainPassword;
    }

    /**
     * Gets the value of clientId.
     *
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Gets the value of plainPassword.
     *
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
}
