<?php

namespace BranchBundle\Commands\Client;

use CoreBundle\Repository\ClientRepository;
use BranchBundle\Commands\Client\ChangeClientPasswordCommand;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class ChangeClientPasswordCommandHandler
{
    private $clientRepository;
    private $encoder;

    public function __construct(ClientRepository $clientRepository, UserPasswordEncoder $encoder)
    {
        $this->clientRepository = $clientRepository;
        $this->encoder = $encoder;
    }

    public function handle(ChangeClientPasswordCommand $command)
    {
        $client = $this->clientRepository->find($command->getClientId());
        $client->setPassword(
            $this->encoder->encodePassword($client, $command->getPlainPassword())
        );
    }
}
