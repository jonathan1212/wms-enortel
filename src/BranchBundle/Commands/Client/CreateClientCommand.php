<?php

namespace BranchBundle\Commands\Client;

class CreateClientCommand
{
    private $name;
    private $contactPerson;
    private $contactNumber;
    private $emailAddress;
    private $plainPassword;
    private $hostAlias;

    public function __construct($name, $contactPerson, $contactNumber, $emailAddress, $plainPassword, $hostAlias)
    {
        $this->name = $name;
        $this->contactPerson = $contactPerson;
        $this->contactNumber = $contactNumber;
        $this->emailAddress = $emailAddress;
        $this->plainPassword = $plainPassword;
        $this->hostAlias = $hostAlias;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of contactPerson.
     *
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Gets the value of contactNumber.
     *
     * @return mixed
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Gets the value of email.
     *
     * @return mixed
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Gets the value of plainPassword.
     *
     * @return mixed
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Gets the value of hostAlias.
     *
     * @return mixed
     */
    public function getHostAlias()
    {
        return $this->hostAlias;
    }
}
