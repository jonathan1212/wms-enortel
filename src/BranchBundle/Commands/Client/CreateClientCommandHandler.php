<?php

namespace BranchBundle\Commands\Client;

use CoreBundle\Entity\Client;
use CoreBundle\Repository\ClientRepository;
use BranchBundle\Commands\Client\CreateClientCommand;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class CreateClientCommandHandler
{
    private $clientRepository;
    private $encoder;

    public function __construct(ClientRepository $clientRepository, UserPasswordEncoder $encoder)
    {
        $this->clientRepository = $clientRepository;
        $this->encoder = $encoder;
    }

    public function handle(CreateClientCommand $command)
    {
        $client = new Client();
        $client->setName($command->getName())
            ->setContactPerson($command->getContactPerson())
            ->setContactNumber($command->getContactNumber())
            ->setEmailAddress($command->getEmailAddress())
            ->setHostAlias($command->getHostAlias())
            ->setPassword(
                $this->encoder->encodePassword($client, $command->getPlainPassword())
            );

        $this->clientRepository->add($client);
    }
}
