<?php

namespace BranchBundle\Commands\Client;

use CoreBundle\Repository\ClientRepository;
use BranchBundle\Commands\Client\UpdateClientCommand;

class UpdateClientCommandHandler
{
    private $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function handle(UpdateClientCommand $command)
    {
        $client = $this->clientRepository->find($command->getClientId());
        $client->setName($command->getName())
            ->setContactPerson($command->getContactPerson())
            ->setContactNumber($command->getContactNumber())
            ->setEmailAddress($command->getEmailAddress())
            ->setHostAlias($command->getHostAlias());
    }
}
