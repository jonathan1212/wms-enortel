<?php

namespace BranchBundle\Commands\Pick;

use SimpleBus\Message\Recorder\RecordsMessages;
use CoreBundle\Repository\PicklistStatusRepository;
use CoreBundle\Repository\PicklistParticularRepository;
use BranchBundle\Commands\Pick\HandoverPicklistForPackingCommand;

class HandoverPicklistForPackingCommandHandler
{
    private $eventRecorder;
    private $picklistParticularRepository;
    private $picklistStatusRepository;

    public function __construct(
        RecordsMessages $eventRecorder,
        PicklistParticularRepository $picklistParticularRepository, 
        PicklistStatusRepository $picklistStatusRepository
    )
    {
        $this->eventRecorder = $eventRecorder;
        $this->picklistParticularRepository = $picklistParticularRepository;
        $this->picklistStatusRepository = $picklistStatusRepository;
    }

    public function handle(HandoverPicklistForPackingCommand $command)
    {
        $picklist = $this->picklistParticularRepository
            ->findOneByPicklistParticularNumber(
                $command->getPicklistNumber()
            );

        $picklistForPackingStatus = $this->picklistStatusRepository
            ->findOneByValue(
                \CoreBundle\Entity\PicklistStatus::FOR_PACKING
            );

        $picklist->setPicklistStatus($picklistForPackingStatus);

        $this->eventRecorder->record(
            new \BranchBundle\Eventing\Pick\PicklistTaggedForPackingEvent(
                $picklist->getPicklistParticularNumber()
            )
        );
    }
}
