<?php

namespace BranchBundle\Commands\Pick;

use BranchBundle\Commands\Pick\PickInventoryCommand;
use CoreBundle\Repository\InventoryStatusRepository;
use CoreBundle\Repository\PicklistParticularRepository;

class PickInventoryCommandHandler
{
    private $picklistParticularRepository;
    private $inventoryStatusRepository;
    private $em;

    public function __construct(
        PicklistParticularRepository $picklistParticularRepository, 
        InventoryStatusRepository $inventoryStatusRepository,
        $em)
    {
        $this->picklistParticularRepository = $picklistParticularRepository;
        $this->inventoryStatusRepository = $inventoryStatusRepository;
        $this->em = $em;
    }

    public function handle(PickInventoryCommand $command)
    {
        $picklist = $this->picklistParticularRepository
            ->getPicklistWithSpecificInventoryTagByPicklistNumber(
                $command->getPicklistNumber(),
                $command->getInventoryTag()
            );

        $picklistDetail = $picklist->getItems()->get(0);
        $inventory = $picklistDetail->getInventory();

        $inventoryPickedStatus = $this->inventoryStatusRepository
            ->findOneByValue(\CoreBundle\Entity\InventoryStatus::PICKED);

        $inventory->setInventoryStatus($inventoryPickedStatus);
        $inventory->setDatePicked(new \DateTime('now'));
        $this->em->flush();
    }
}
