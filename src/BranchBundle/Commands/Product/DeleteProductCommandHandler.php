<?php

namespace BranchBundle\Commands\Product;

use CoreBundle\Repository\ProductRepository;
use BranchBundle\Commands\Product\DeleteProductCommand;

class DeleteProductCommandHandler
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function handle(DeleteProductCommand $command)
    {
        $product = $this->productRepository->find($command->getProductId());
        $this->productRepository->delete($product);
    }
}
