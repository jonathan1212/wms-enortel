<?php

namespace BranchBundle\Commands\Product;

class DeleteProductCommand
{
    private $productId;

    public function __construct($productId)
    {
        $this->productId = $productId;
    }

    /**
     * Gets the value of productId.
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
