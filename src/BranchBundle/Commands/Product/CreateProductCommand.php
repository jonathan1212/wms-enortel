<?php

namespace BranchBundle\Commands\Product;

class CreateProductCommand
{
    private $productDescription;
    private $clientSku;
    private $supplierSku;
    private $costOfGoodsPurchased;
    private $supplierId;
    private $clientId;
    private $length;
    private $width;
    private $height;
    private $weigth;

    public function __construct($productDescription, $clientSku, $supplierSku, $costOfGoodsPurchased, $supplierId, $clientId, $length, $width, $height, $weigth)
    {
        $this->productDescription = $productDescription;
        $this->clientSku = $clientSku;
        $this->supplierSku = $supplierSku;
        $this->costOfGoodsPurchased = $costOfGoodsPurchased;
        $this->supplierId = $supplierId;
        $this->clientId = $clientId;
        $this->length = $length;
        $this->width = $width;
        $this->height = $height;
        $this->weigth = $weigth;
    }

    /**
     * Gets the value of productDescription.
     *
     * @return mixed
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * Gets the value of clientSku.
     *
     * @return mixed
     */
    public function getClientSku()
    {
        return $this->clientSku;
    }

    /**
     * Gets the value of supplierSku.
     *
     * @return mixed
     */
    public function getSupplierSku()
    {
        return $this->supplierSku;
    }

    /**
     * Gets the value of costOfGoodsPurchased.
     *
     * @return mixed
     */
    public function getCostOfGoodsPurchased()
    {
        return $this->costOfGoodsPurchased;
    }

    public function getLength()
    {
        return $this->length;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function getWeigth()
    {
        return $this->weigth;
    }

    /**
     * Gets the value of supplierId.
     *
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * Gets the value of clientId.
     *
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }
}
