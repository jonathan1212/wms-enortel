<?php

namespace BranchBundle\Commands\Product;

class UpdateProductCommand
{
    private $productId;
    private $productDescription;
    private $clientSku;
    private $supplierSku;
    private $costOfGoodsPurchased;

    public function __construct($productId, $productDescription, $clientSku, $supplierSku, $costOfGoodsPurchased)
    {
        $this->productId = $productId;
        $this->productDescription = $productDescription;
        $this->clientSku = $clientSku;
        $this->supplierSku = $supplierSku;
        $this->costOfGoodsPurchased = $costOfGoodsPurchased;
    }

    /**
     * Gets the value of productDescription.
     *
     * @return mixed
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * Gets the value of clientSku.
     *
     * @return mixed
     */
    public function getClientSku()
    {
        return $this->clientSku;
    }

    /**
     * Gets the value of supplierSku.
     *
     * @return mixed
     */
    public function getSupplierSku()
    {
        return $this->supplierSku;
    }

    /**
     * Gets the value of productId.
     *
     * @return mixed
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Gets the value of costOfGoodsPurchased.
     *
     * @return mixed
     */
    public function getCostOfGoodsPurchased()
    {
        return $this->costOfGoodsPurchased;
    }
}
