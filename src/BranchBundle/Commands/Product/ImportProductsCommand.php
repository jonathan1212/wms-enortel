<?php

namespace BranchBundle\Commands\Product;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportProductsCommand
{
    private $file;
    private $clientId;
    private $supplierId;

    public function __construct(UploadedFile $file, $clientId, $supplierId)
    {
        $this->file = $file;
        $this->clientId = $clientId;
        $this->supplierId = $supplierId;
    }

    /**
     * Gets the value of file.
     *
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Gets the value of clientId.
     *
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Gets the value of supplierId.
     *
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }
}
