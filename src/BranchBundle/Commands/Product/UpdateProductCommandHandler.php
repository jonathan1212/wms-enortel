<?php

namespace BranchBundle\Commands\Product;

use CoreBundle\Repository\ProductRepository;
use BranchBundle\Commands\Product\UpdateProductCommand;

class UpdateProductCommandHandler
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function handle(UpdateProductCommand $command)
    {
        $product = $this->productRepository->find($command->getProductId());
        $product->setProductDescription($command->getProductDescription())
            ->setClientSku($command->getClientSku())
            ->setSupplierSku($command->getSupplierSku())
            ->setCostOfGoodsPurchased($command->getCostOfGoodsPurchased());
    }
}
