<?php

namespace BranchBundle\Commands\Product;

use CoreBundle\Entity\Product;
use CoreBundle\Repository\ClientRepository;
use CoreBundle\Repository\ProductRepository;
use CoreBundle\Repository\SupplierRepository;
use BranchBundle\Service\Product\SkuGenerator;
use BranchBundle\Commands\Product\CreateProductCommand;

class CreateProductCommandHandler
{
    private $productRepository;
    private $skuGenerator;
    private $supplierRepository;
    private $clientRepository;

    public function __construct(
        ProductRepository $productRepository, 
        SupplierRepository $supplierRepository,
        ClientRepository $clientRepository,
        SkuGenerator $skuGenerator
    )
    {
        $this->productRepository = $productRepository;
        $this->supplierRepository = $supplierRepository;
        $this->skuGenerator = $skuGenerator;
        $this->clientRepository = $clientRepository;
    }

    public function handle(CreateProductCommand $command)
    {
        $supplier = $this->supplierRepository->find($command->getSupplierId());
        $client = $this->clientRepository->find($command->getClientId());
        $product = new Product();

        $product->setProductDescription($command->getProductDescription())
            ->setClientSku($command->getClientSku())
            ->setSupplierSku($command->getSupplierSku())
            ->setCostOfGoodsPurchased($command->getCostOfGoodsPurchased())
            ->setSku($this->skuGenerator->generate())
            ->setSupplier($supplier)
            ->setLength($command->getLength())
            ->setWidth($command->getWidth())
            ->setHeight($command->getHeight())
            ->setWeigth($command->getWeigth())
            ->setClient($client);

        $this->productRepository->add($product);
    }
}
