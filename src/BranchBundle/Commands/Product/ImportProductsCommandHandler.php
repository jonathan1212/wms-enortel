<?php

namespace BranchBundle\Commands\Product;

use CoreBundle\Entity\Product;
use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;
use BranchBundle\Service\Product\SkuGenerator;
use BranchBundle\Commands\Product\ImportProductsCommand;

class ImportProductsCommandHandler
{
    private $batchSize = 30;
    private $em;
    private $phpExcelFactory;
    private $skuGenerator;
    private $clientRepository;
    private $supplierRepository;

    private $errorMapping = [
        0 => 'Empty Product Description found at row %d',
        1 => 'Empty Client SKU found at row %d',
        3 => 'Empty Cost of Goods Purchased found at row %d'
    ];

    public function __construct(EntityManager $em, SkuGenerator $skuGenerator, Factory $phpExcelFactory)
    {
        $this->em = $em;
        $this->phpExcelFactory = $phpExcelFactory;
        $this->skuGenerator = $skuGenerator;
        $this->clientRepository = $em->getRepository('CoreBundle:Client');
        $this->supplierRepository = $em->getRepository('CoreBundle:Supplier');
    }

    public function handle(ImportProductsCommand $command)
    {
        $reader = $this->phpExcelFactory->createPHPExcelObject($command->getFile());
        $rowIterator = $reader->getSheet(0)->getRowIterator();
        $deps = $this->getEntityDependencies(
            $command->getClientId(),
            $command->getSupplierId()
        );

        $i = 0;
        foreach($rowIterator as $row){
            $rowIndex = $row->getRowIndex();
            if($rowIndex === 1){
                continue;
            }

            $productArray = $reader->getActiveSheet()
                ->rangeToArray(
                    "A$rowIndex".":"."H$rowIndex"
                )[0];

            foreach (array_keys($this->errorMapping) as $index) {
                if(!$productArray[$index]){
                    throw new \UnexpectedValueException(
                        sprintf($this->errorMapping[$index], $rowIndex)
                    );
                }
            }

            $product = new Product();
            $product->setProductDescription($productArray[0])
                ->setSku($this->skuGenerator->generate())
                ->setClientSku($productArray[1])
                ->setSupplierSku($productArray[2])
                ->setCostOfGoodsPurchased($productArray[3])
                ->setLength($productArray[4])
                ->setWidth($productArray[5])
                ->setHeight($productArray[6])
                ->setWeigth($productArray[7])
                ->setClient($deps['client'])
                ->setSupplier($deps['supplier']);

            $this->em->persist($product);
            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $deps = $this->getEntityDependencies(
                    $command->getClientId(),
                    $command->getSupplierId()
                );
            }
            ++$i;
        }

        $this->em->flush();
        $this->em->clear();
    }

    private function getEntityDependencies($clientId, $supplierId)
    {
        return [
            'client' => $this->clientRepository->find($clientId),
            'supplier' => $this->supplierRepository->find($supplierId)
        ];
    }
}
