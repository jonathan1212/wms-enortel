<?php

namespace BranchBundle\Commands\Warehouse;

use CoreBundle\Entity\Warehouse;
use CoreBundle\Repository\WarehouseRepository;
use BranchBundle\Commands\Warehouse\UpdateWarehouseCommand;

class UpdateWarehouseCommandHandler 
{
    private $warehouseRepository;

    public function __construct(WarehouseRepository $warehouseRepository)
    {
        $this->warehouseRepository = $warehouseRepository;
    }

    public function handle(UpdateWarehouseCommand $command)
    {
        $warehouse = $this->warehouseRepository->find($command->getWarehouseId());
        $warehouse->setName($command->getName())
            ->setAddress($command->getAddress())
            ->setContactNumber($command->getContactNumber());
    }
}
