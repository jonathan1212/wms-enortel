<?php

namespace BranchBundle\Commands\Warehouse;

use CoreBundle\Entity\Warehouse;
use CoreBundle\Repository\WarehouseRepository;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand;

class CreateWarehouseCommandHandler 
{
    private $warehouseRepository;

    public function __construct(WarehouseRepository $warehouseRepository)
    {
        $this->warehouseRepository = $warehouseRepository;
    }

    public function handle(CreateWarehouseCommand $command)
    {
        $warehouse = new Warehouse();
        $warehouse->setName($command->getName())
            ->setAddress($command->getAddress())
            ->setContactNumber($command->getContactNumber());

        $this->warehouseRepository->add($warehouse);
    }
}
