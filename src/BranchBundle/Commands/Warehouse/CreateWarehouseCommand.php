<?php

namespace BranchBundle\Commands\Warehouse;

class CreateWarehouseCommand 
{
    private $name;
    private $address;
    private $contactNumber;

    public function __construct($name, $address, $contactNumber)
    {
        $this->name = $name;
        $this->address = $address;
        $this->contactNumber = $contactNumber;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getContactNumber()
    {
        return $this->contactNumber;
    }
}
