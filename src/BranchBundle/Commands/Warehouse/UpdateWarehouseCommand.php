<?php

namespace BranchBundle\Commands\Warehouse;

class UpdateWarehouseCommand 
{
    private $warehouseId;
    private $name;
    private $address;
    private $contactNumber;

    public function __construct($warehouseId, $name, $address, $contactNumber)
    {
        $this->warehouseId = $warehouseId;
        $this->name = $name;
        $this->address = $address;
        $this->contactNumber = $contactNumber;
    }

    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getContactNumber()
    {
        return $this->contactNumber;
    }

}
