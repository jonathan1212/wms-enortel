<?php

namespace BranchBundle\Commands\Warehouse;

use CoreBundle\Entity\Warehouse;
use CoreBundle\Repository\WarehouseRepository;
use BranchBundle\Commands\Warehouse\DeleteWarehouseCommand;

class DeleteWarehouseCommandHandler 
{
    private $warehouseRepository;

    public function __construct(WarehouseRepository $warehouseRepository)
    {
        $this->warehouseRepository = $warehouseRepository;
    }

    public function handle(DeleteWarehouseCommand $command)
    {
        $warehouse = $this->warehouseRepository->find($command->getWarehouseId());
        $this->warehouseRepository->delete($warehouse);
    }
}
