<?php

namespace BranchBundle\Commands\Warehouse;

class DeleteWarehouseCommand 
{
    private $warehouseId;

    public function __construct($warehouseId)
    {
        $this->warehouseId = $warehouseId;
    }

    public function getWarehouseId()
    {
        return $this->warehouseId;
    }
}
