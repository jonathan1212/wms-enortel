<?php

namespace BranchBundle\Commands\PurchaseOrder;

use CoreBundle\Entity\Client;
use CoreBundle\Entity\Supplier;
use CoreBundle\Entity\Warehouse;
use Doctrine\Common\Collections\Collection;
use CoreBundle\Entity\PurchaseOrderPaymentType;

class CreatePurchaseOrderCommand 
{
    private $supplier;
    private $remarks;
    private $products;
    private $warehouse;
    private $paymentType;
    private $client;
    private $file;
    private $referenceno;

    public function __construct(
        Supplier $supplier, 
        Collection $products, 
        Warehouse $warehouse, 
        PurchaseOrderPaymentType $paymentType,
        Client $client,
        $remarks,
        $file,
        $referenceno
    )
    {
        $this->supplier = $supplier;
        $this->remarks = $remarks;
        $this->products = $products;
        $this->warehouse = $warehouse;
        $this->paymentType = $paymentType;
        $this->client = $client;
        $this->file = $file;
        $this->referenceno = $referenceno;

    }

    /**
     * Gets the value of supplier.
     *
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Gets the value of remarks.
     *
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Gets the value of products.
     *
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Gets the value of warehouse.
     *
     * @return mixed
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Gets the value of paymentType.
     *
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Gets the value of client.
     *
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    public function getReferenceNo()
    {
        return $this->referenceno;
    }

    public function getFile()
    {
        return $this->file;
    }
}
