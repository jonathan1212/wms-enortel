<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 2/27/17
 * Time: 2:25 PM
 */

namespace BranchBundle\Commands\PurchaseOrder;

use BranchBundle\Service\PurchaseOrder\PurchaseOrderNumberGenerator;
use CoreBundle\Entity\Product;
use CoreBundle\Entity\PurchaseOrder;
use CoreBundle\Entity\PurchaseOrderProduct;
use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportPurchaseOrderCommandHandler
{

    private $batchSize = 30;
    private $em;
    private $phpExcelFactory;
    private $clientRepository;
    private $supplierRepository;
    private $purchaseOrderNumberGenerator;
    private $authenticatedUser;
    private $serviceContainer;

    private $errorMapping = [
        0 => 'Empty Client SKU found at row %d',
        2 => 'Empty Quantity found at row %d',
    ];

    public function __construct(EntityManager $em,
                                PurchaseOrderNumberGenerator $purchaseOrderNumberGenerator,
                                Factory $phpExcelFactory,
                                TokenStorage $tokenStorage,
                                $serviceContainer
                                )
    {
        $this->em = $em;
        $this->phpExcelFactory = $phpExcelFactory;

        $this->clientRepository = $em->getRepository('CoreBundle:Client');
        $this->supplierRepository = $em->getRepository('CoreBundle:Supplier');
        $this->product = $em->getRepository('CoreBundle:Product');

        $this->purchaseOrderNumberGenerator = $purchaseOrderNumberGenerator;
        $this->authenticatedUser = $tokenStorage->getToken()->getUser();
        $this->serviceContainer = $serviceContainer;
    }

    public function handle(ImportPurchaseOrderCommand $command)
    {
        $reader = $this->phpExcelFactory->createPHPExcelObject($command->getFile());
        $rowIterator = $reader->getSheet(0)->getRowIterator();

        $i = 0;

        $productIds = array();
        $fileName = $this->moveUploadedFile($command->getAttachedFile());
        // insert purchase order first
        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->setSupplier($command->getSupplier())
            ->setRemarks($command->getRemarks())
            ->setWarehouse($command->getWarehouse())
            ->setPaymentType($command->getPaymentType())
            ->setCreatedBy($this->authenticatedUser)
            ->setClient($command->getClient())
            ->setPurchaseOrderNumber(
                $this->purchaseOrderNumberGenerator->generate()
            )
            ->setFile($fileName)
            ->setReferenceNo($command->getReferenceNo());

        foreach($rowIterator as $row){
            $rowIndex = $row->getRowIndex();
            if($rowIndex === 1){
                continue;
            }

            $productArray = $reader->getActiveSheet()
                ->rangeToArray(
                    "A$rowIndex".":"."C$rowIndex"
                )[0];

            foreach (array_keys($this->errorMapping) as $index) {
                if(!$productArray[$index]){
                    throw new \UnexpectedValueException(
                        sprintf($this->errorMapping[$index], $rowIndex)
                    );
                }
            }

            if (!is_numeric($productArray[2])) {
                throw new \UnexpectedValueException( sprintf("Not Valid Quantity at Line %d", $rowIndex ));
            }

            $purchaseOrderProduct = new PurchaseOrderProduct();

            $product = $this->product->findOneBy(
                array(
                    'clientSku' => $productArray[0],
                    'client'    => $command->getClient(),
                    'supplier'    => $command->getSupplier(),
                )
            );

            if ($product) {

                $purchaseOrderProduct
                    ->setProduct($product)
                    ->setQuantity($productArray[2])
                    ->setCostOfGoodsPurchased($product->getCostOfGoodsPurchased())
                    ->setPurchaseOrder($purchaseOrder)
                ;

                if (in_array($product->getId(), $productIds)) {
                    throw new \UnexpectedValueException( sprintf("Duplicate Client SKU %s at Line %d", $productArray[0], $rowIndex ));
                } 

                array_push($productIds, $product->getId());

                $purchaseOrder->addProduct($purchaseOrderProduct);

            } else {
                throw new \UnexpectedValueException( sprintf("Client SKU %s Not Found Under Client %s and Supplier %s at Line %d", $productArray[0], $command->getClient()->getName(), $command->getSupplier()->getName(), $rowIndex ));
            }
            
        }

        $this->em->persist($purchaseOrder);
        $this->em->flush();
        $this->em->clear();
    }

    public function moveUploadedFile(UploadedFile $file = null)
    {
        $filename = null;
        if (!is_null($file)) {

            $filename = date('His') . $file->getClientOriginalName();
            $uploadDirectory = $this->serviceContainer->get('kernel')->getRootDir() . '/../web/documents/';
            
            $file->move ($uploadDirectory, $filename);
        }

        return $filename;
        
    }

}