<?php
/**
 * Created by PhpStorm.
 * User: jonathan
 * Date: 2/27/17
 * Time: 2:21 PM
 */

namespace BranchBundle\Commands\PurchaseOrder;


use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportPurchaseOrderCommand
{

    private $attachedFile;
    private $client;
    private $supplier;
    private $paymentType;
    private $remarks;
    private $warehouse;

    public function __construct(UploadedFile $file, $client, $supplier,$paymentType, $remarks, $warehouse,
        $attachedFile,
        $referenceno)
    {
        $this->file = $file;
        $this->client = $client;
        $this->supplier = $supplier;
        $this->paymentType = $paymentType;
        $this->remarks = $remarks;
        $this->warehouse = $warehouse;
        $this->attachedFile = $attachedFile;
        $this->referenceno = $referenceno;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }


    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * @param mixed $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return mixed
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param mixed $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return mixed
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * @param mixed $supplier
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;
    }

    /**
     * @return mixed
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * @param mixed $remarks
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;
    }

    /**
     * @return mixed
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param mixed $warehouse
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
    }

    public function getReferenceNo()
    {
        return $this->referenceno;
    }

    public function getAttachedFile()
    {
        return $this->attachedFile;
    }





}