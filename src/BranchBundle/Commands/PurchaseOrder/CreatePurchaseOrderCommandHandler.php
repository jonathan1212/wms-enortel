<?php

namespace BranchBundle\Commands\PurchaseOrder;

use CoreBundle\Entity\PurchaseOrder;
use CoreBundle\Repository\PurchaseOrderRepository;
use BranchBundle\Commands\PurchaseOrder\CreatePurchaseOrderCommand;
use BranchBundle\Service\PurchaseOrder\PurchaseOrderNumberGenerator;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreatePurchaseOrderCommandHandler
{
    private $purchaseOrderRepository;
    private $purchaseOrderNumberGenerator;
    private $blameableUser;
    private $serviceContainer;

    public function __construct(
        PurchaseOrderRepository $purchaseOrderRepository, 
        PurchaseOrderNumberGenerator $purchaseOrderNumberGenerator,
        $blameableUser,
        $serviceContainer
    )
    {
        $this->purchaseOrderRepository = $purchaseOrderRepository;
        $this->purchaseOrderNumberGenerator = $purchaseOrderNumberGenerator;
        $this->blameableUser = $blameableUser;
        $this->serviceContainer = $serviceContainer;
    }

    public function handle(CreatePurchaseOrderCommand $command)
    {
        $fileName = $this->moveUploadedFile($command->getFile());

        $purchaseOrder = new PurchaseOrder();
        $purchaseOrder->setSupplier($command->getSupplier())
            ->setRemarks($command->getRemarks())
            ->setWarehouse($command->getWarehouse())
            ->setPaymentType($command->getPaymentType())
            ->setCreatedBy($this->blameableUser->getBlameable())
            ->setClient($command->getClient())
            ->setPurchaseOrderNumber(
                $this->purchaseOrderNumberGenerator->generate()
            )
            ->setFile($fileName)
            ->setReferenceNo($command->getReferenceNo());

        foreach($command->getProducts() as $product){
            $purchaseOrder->addProduct($product);
        }

        $this->purchaseOrderRepository->add($purchaseOrder);

        
    }

    public function moveUploadedFile(UploadedFile $file = null)
    {
        $filename = null;
        if (!is_null($file)) {

            $filename = date('His') . $file->getClientOriginalName();
            $uploadDirectory = $this->serviceContainer->get('kernel')->getRootDir() . '/../web/documents/';
            
            $file->move ($uploadDirectory, $filename);
        }

        return $filename;
    }
}
