<?php

namespace BranchBundle\Commands\Pack;

class PackInventoryCommand
{
    private $picklistNumber;
    private $inventoryTag;

    public function __construct($picklistNumber, $inventoryTag)
    {
        $this->picklistNumber = $picklistNumber;
        $this->inventoryTag = $inventoryTag;
    }

    /**
     * Gets the value of picklistNumber.
     *
     * @return mixed
     */
    public function getPicklistNumber()
    {
        return $this->picklistNumber;
    }

    /**
     * Gets the value of inventoryTag.
     *
     * @return mixed
     */
    public function getInventoryTag()
    {
        return $this->inventoryTag;
    }
}
