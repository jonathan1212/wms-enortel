<?php

namespace BranchBundle\Commands\Pack;

use CoreBundle\Repository\PicklistRepository;
use CoreBundle\Repository\InventoryStatusRepository;

class PackInventoryCommandHandler
{
    private $picklistRepository;
    private $inventoryStatusRepository;

    public function __construct(
        PicklistRepository $picklistRepository, 
        InventoryStatusRepository $inventoryStatusRepository
    )
    {
        $this->picklistRepository = $picklistRepository;
        $this->inventoryStatusRepository = $inventoryStatusRepository;
    }

    public function handle(PackInventoryCommand $command)
    {
        $picklist = $this->picklistRepository
            ->getPicklistWithSpecificInventoryTagByPicklistNumber(
                $command->getPicklistNumber(),
                $command->getInventoryTag()
            );

        $picklistDetail = $picklist->getInventoryListing()->get(0);
        $inventory = $picklistDetail->getInventory();

        $inventoryPickedStatus = $this->inventoryStatusRepository
            ->findOneByValue(\CoreBundle\Entity\InventoryStatus::PACKED);

        $inventory->setInventoryStatus($inventoryPickedStatus);        
        $inventory->setDatePacked($inventory->getDatePicked());
    }
}
