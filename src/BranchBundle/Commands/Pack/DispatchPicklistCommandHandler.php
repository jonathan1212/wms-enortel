<?php

namespace BranchBundle\Commands\Pack;

use CoreBundle\Repository\PicklistRepository;
use SimpleBus\Message\Recorder\RecordsMessages;
use CoreBundle\Repository\PicklistStatusRepository;
use BranchBundle\Commands\Pack\DispatchPicklistCommand;
use BranchBundle\Service\Picklist\InvoiceNumberGenerator;

class DispatchPicklistCommandHandler
{
    private $eventRecorder;
    private $picklistRepository;
    private $picklistStatusRepository;
    private $generator;

    public function __construct(
        RecordsMessages $eventRecorder, 
        PicklistRepository $picklistRepository, 
        PicklistStatusRepository $picklistStatusRepository,
        InvoiceNumberGenerator $generator
    )
    {
        $this->eventRecorder = $eventRecorder;
        $this->picklistRepository = $picklistRepository;
        $this->picklistStatusRepository = $picklistStatusRepository;
        $this->generator = $generator;
    }

    public function handle(DispatchPicklistCommand $command)
    {
        $picklistNumber = $command->getPicklistNumber();
        $dispatchStatusValue = $this->getDispatchStatus($picklistNumber);
        $dispatchStatus = $this->picklistStatusRepository
            ->findOneByValue(
                $dispatchStatusValue
            );

        $picklist = $this->picklistRepository
            ->findOneByPicklistNumber(
                $picklistNumber
            );

        $picklist->setPicklistStatus($dispatchStatus);
        foreach($picklist->getParticulars() as $particular){
            $particular->setPicklistStatus($dispatchStatus);
        }

        if(!$picklist->getInvoiceNumber()){
            $picklist->setInvoiceNumber(
                $this->generator->generate()
            );
        }

        $this->eventRecorder->record(
            new \BranchBundle\Eventing\Pack\PicklistTaggedForDispatchingEvent(
                $picklistNumber
            )
        );
    }

    private function getDispatchStatus($picklistNumber)
    {
        $hasLostInventory = $this->picklistRepository
            ->findPicklistByInventoryStatusesState(
                $picklistNumber,
                [\CoreBundle\Entity\InventoryStatus::LOST]
            );

        return $hasLostInventory === null ? 
            \CoreBundle\Entity\PicklistStatus::DISPATCHED 
            : 
            \CoreBundle\Entity\PicklistStatus::PARTIALLY_DISPATCHED;
    }
}
