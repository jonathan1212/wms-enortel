<?php

namespace BranchBundle\Commands\Pack;

class DispatchPicklistCommand
{
    private $picklistNumber;

    public function __construct($picklistNumber)
    {
        $this->picklistNumber = $picklistNumber;
    }

    /**
     * Gets the value of picklistNumber.
     *
     * @return mixed
     */
    public function getPicklistNumber()
    {
        return $this->picklistNumber;
    }
}
