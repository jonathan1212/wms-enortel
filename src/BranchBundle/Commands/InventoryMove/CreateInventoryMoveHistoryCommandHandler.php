<?php

namespace BranchBundle\Commands\InventoryMove;

use CoreBundle\Entity\InventoryMoveHistory;
use SimpleBus\Message\Recorder\RecordsMessages;
use CoreBundle\Repository\MobileLocationRepository;
use CoreBundle\Repository\StorageLocationRepository;
use CoreBundle\Repository\InventoryMoveHistoryRepository;
use BranchBundle\Eventing\InventoryMove\InventoryMovedEvent;
use BranchBundle\Commands\InventoryMove\CreateInventoryMoveHistoryCommand;

class CreateInventoryMoveHistoryCommandHandler
{
    private $eventRecorder;
    private $inventoryMoveHistoryRepository;
    private $storageLocationRepository;
    private $mobileLocationRepository;

    public function __construct(
        RecordsMessages $eventRecorder,
        InventoryMoveHistoryRepository $inventoryMoveHistoryRepository, 
        StorageLocationRepository $storageLocationRepository, 
        MobileLocationRepository $mobileLocationRepository
    )
    {
        $this->eventRecorder = $eventRecorder;
        $this->inventoryMoveHistoryRepository = $inventoryMoveHistoryRepository;
        $this->storageLocationRepository = $storageLocationRepository;
        $this->mobileLocationRepository = $mobileLocationRepository;
    }

    public function handle(CreateInventoryMoveHistoryCommand $command)
    {
        $history = new InventoryMoveHistory();
        $storageLocation = $this->storageLocationRepository
            ->findOneByName($command->getStorageLocation());

        $mobileLocation = $this->mobileLocationRepository
            ->findOneByName($command->getMobileLocation());
        
        $history->setTarget($command->getTarget())
            ->setStorageLocation($storageLocation)
            ->setMobileLocation($mobileLocation);

        $this->inventoryMoveHistoryRepository
            ->add($history);

        $this->eventRecorder->record(
            new \BranchBundle\Eventing\InventoryMove\InventoryMovedEvent(
                $command->getTarget(),
                $command->getStorageLocation(),
                $command->getMobileLocation()
            )
        );
    }
}
