<?php

namespace BranchBundle\Commands\InventoryMove;

class CreateInventoryMoveHistoryCommand
{
    private $target;
    private $storageLocation;
    private $mobileLocation;

    public function __construct($target, $storageLocation, $mobileLocation)
    {
        $this->target = $target;
        $this->storageLocation = $storageLocation;
        $this->mobileLocation = $mobileLocation;
    }

    /**
     * Gets the value of target.
     *
     * @return mixed
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Gets the value of storageLocation.
     *
     * @return mixed
     */
    public function getStorageLocation()
    {
        return $this->storageLocation;
    }

    /**
     * Gets the value of mobileLocation.
     *
     * @return mixed
     */
    public function getMobileLocation()
    {
        return $this->mobileLocation;
    }
}
