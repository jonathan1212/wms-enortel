<?php

namespace BranchBundle\Commands\Supplier;

class UpdateSupplierCommand
{
    private $supplierId;
    private $name;
    private $address;
    private $contactPerson;
    private $contactNumber;

    public function __construct($supplierId, $name, $address, $contactPerson, $contactNumber)
    {
        $this->supplierId = $supplierId;
        $this->name = $name;
        $this->address = $address;
        $this->contactPerson = $contactPerson;
        $this->contactNumber = $contactNumber;
    }

    /**
     * Gets the value of supplierId.
     *
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Gets the value of address.
     *
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Gets the value of contactPerson.
     *
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Gets the value of contactNumber.
     *
     * @return mixed
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }
}
