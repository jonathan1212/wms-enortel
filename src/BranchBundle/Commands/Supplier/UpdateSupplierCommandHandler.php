<?php

namespace BranchBundle\Commands\Supplier;

use CoreBundle\Repository\SupplierRepository;
use BranchBundle\Commands\Supplier\UpdateSupplierCommand;

class UpdateSupplierCommandHandler
{
    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function handle(UpdateSupplierCommand $command)
    {
        $supplier = $this->supplierRepository->find($command->getSupplierId());
        $supplier->setName($command->getName())
            ->setAddress($command->getAddress())
            ->setContactPerson($command->getContactPerson())
            ->setContactNumber($command->getContactNumber());
    }
}
