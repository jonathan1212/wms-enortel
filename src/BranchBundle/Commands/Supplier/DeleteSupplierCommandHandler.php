<?php

namespace BranchBundle\Commands\Supplier;

use CoreBundle\Repository\SupplierRepository;
use BranchBundle\Commands\Supplier\DeleteSupplierCommand;

class DeleteSupplierCommandHandler
{
    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function handle(DeleteSupplierCommand $command)
    {
        $supplier = $this->supplierRepository->find($command->getSupplierId());
        $this->supplierRepository->delete($supplier);
    }
}
