<?php

namespace BranchBundle\Commands\Supplier;

use CoreBundle\Entity\Supplier;
use CoreBundle\Repository\SupplierRepository;
use BranchBundle\Commands\Supplier\CreateSupplierCommand;

class CreateSupplierCommandHandler
{
    private $supplierRepository;

    public function __construct(SupplierRepository $supplierRepository)
    {
        $this->supplierRepository = $supplierRepository;
    }

    public function handle(CreateSupplierCommand $command)
    {
        $supplier = new Supplier();
        $supplier->setName($command->getName())
            ->setAddress($command->getAddress())
            ->setContactPerson($command->getContactPerson())
            ->setContactNumber($command->getContactNumber());

        $this->supplierRepository->add($supplier);
    }
}
