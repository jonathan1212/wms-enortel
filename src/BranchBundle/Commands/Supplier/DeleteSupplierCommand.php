<?php

namespace BranchBundle\Commands\Supplier;

class DeleteSupplierCommand
{
    private $supplierId;

    public function __construct($supplierId)
    {
        $this->supplierId = $supplierId;
    }

    /**
     * Gets the value of supplierId.
     *
     * @return mixed
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }
}
