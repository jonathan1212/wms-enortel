<?php

namespace BranchBundle\Commands\StorageLocation;

class UpdateStorageLocationCommand
{
    private $storageLocationId;
    private $name;

    public function __construct($storageLocationId, $name)
    {
        $this->storageLocationId = $storageLocationId;
        $this->name = $name;
    }

    /**
     * Gets the value of storageLocationId.
     *
     * @return mixed
     */
    public function getStorageLocationId()
    {
        return $this->storageLocationId;
    }

    /**
     * Gets the value of name.
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}
