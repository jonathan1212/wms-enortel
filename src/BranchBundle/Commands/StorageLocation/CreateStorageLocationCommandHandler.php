<?php

namespace BranchBundle\Commands\StorageLocation;

use CoreBundle\Entity\StorageLocation;
use CoreBundle\Repository\WarehouseRepository;
use CoreBundle\Repository\StorageLocationRepository;
use BranchBundle\Commands\StorageLocation\CreateStorageLocationCommand;

class CreateStorageLocationCommandHandler
{
    private $storageLocationRepository;
    private $warehouseRepository;

    public function __construct(StorageLocationRepository $storageLocationRepository, WarehouseRepository $warehouseRepository)
    {
        $this->storageLocationRepository = $storageLocationRepository;
        $this->warehouseRepository = $warehouseRepository;
    }

    public function handle(CreateStorageLocationCommand $command)
    {
        $warehouse = $this->warehouseRepository->find($command->getWarehouseId());
        $storageLocation = new StorageLocation();
        $storageLocation->setName($command->getName())
            ->setWarehouse($warehouse);
        $this->storageLocationRepository->add($storageLocation);
    }
}
