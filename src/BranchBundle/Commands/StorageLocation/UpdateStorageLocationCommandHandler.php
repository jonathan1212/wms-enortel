<?php

namespace BranchBundle\Commands\StorageLocation;

use CoreBundle\Repository\StorageLocationRepository;
use BranchBundle\Commands\StorageLocation\UpdateStorageLocationCommand;

class UpdateStorageLocationCommandHandler
{
    private $storageLocationRepository;

    public function __construct(StorageLocationRepository $storageLocationRepository)
    {
        $this->storageLocationRepository = $storageLocationRepository;
    }

    public function handle(UpdateStorageLocationCommand $command)
    {
        $location = $this->storageLocationRepository->find($command->getStorageLocationId());
        $location->setName($command->getName());
    }
}
