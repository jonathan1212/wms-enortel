<?php

namespace BranchBundle\Commands\Picklist;

use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;
use CoreBundle\Entity\PicklistImportHistory;
use SimpleBus\Message\Recorder\RecordsMessages;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use BranchBundle\Commands\Picklist\CreatePicklistImportHistoryCommand;

class CreatePicklistImportHistoryCommandHandler
{
    private $picklistImportHistoryRepository;
    private $warehouseRepository;
    private $picklistRepository;
    private $productRepository;
    private $phpExcelFactory;
    private $eventRecorder;
    private $em;
    private $blameableUser;

    public function __construct(
        RecordsMessages $eventRecorder,
        EntityManager $em,
        Factory $phpExcelFactory,
        $blameableUser
    )
    {
        $this->em = $em;
        $this->eventRecorder = $eventRecorder;
        $this->phpExcelFactory = $phpExcelFactory;
        $this->picklistImportHistoryRepository = $em->getRepository('CoreBundle:PicklistImportHistory');
        $this->productRepository = $em->getRepository('CoreBundle:Product');
        $this->warehouseRepository = $em->getRepository('CoreBundle:Warehouse');
        $this->picklistRepository = $em->getRepository('CoreBundle:Picklist');
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->blameableUser = $blameableUser;
    }

    public function handle(CreatePicklistImportHistoryCommand $command)
    {
        $clientId = $command->getClientId();
        $picklistContents = $this->retrievePicklistImportDetailsFromWorksheet(
            $command->getFile(),
            $command->getIsQuarantine()
        );

        $this->validateImportPicklistData($picklistContents, $clientId, $command->getIsQuarantine());
        $picklistImportHistory = new PicklistImportHistory();
        $picklistImportHistory->setData([
            'client_id' => $clientId,
            'orders' => $picklistContents['importDetails'],
            'uploadedBy' => $this->blameableUser->getBlameable()
        ]);

        $this->picklistImportHistoryRepository->add($picklistImportHistory);
        $this->eventRecorder->record(
            new \BranchBundle\Eventing\Picklist\RequestForPicklistGenerationEvent(
                $picklistImportHistory
            )
        );
    }

    /**
     * Validates Import Data for Invalid SKUs and Warehouses
     * @param  array $picklistContents
     * @return null
     * @throws UnexpectedValueException
     */
    private function validateImportPicklistData($picklistContents, $clientId, $isQuarantine)
    {
        $errorMessage = '';

        $invalidSkus = $this->productRepository
            ->filterValidClientSkusAsArray($picklistContents['distinctProducts']);

        $invalidWarehouses = $this->warehouseRepository
            ->filterValidWarehousesAsArray($picklistContents['distinctWarehouses']);

        $invalidOrderNumbers = $this->picklistRepository
            ->filterValidOrderNumbersAsArray($picklistContents['distinctOrderNumbers'], $clientId);

        $inventorySerial = $this->inventoryRepository
            ->filterValidSerialNoAsArray($picklistContents['serialNos'], $isQuarantine);

        $inventoryQuarantine = $this->inventoryRepository
            ->filterValidClientSkusQuarantineAsArray($picklistContents['distinctProducts'],$isQuarantine);    
        

        if(count($invalidSkus) === 0 && count($invalidWarehouses) === 0 && count($invalidOrderNumbers) === 0 && count($inventorySerial) === 0 && count($inventoryQuarantine) === 0){
            return;
        }

        if(count($invalidSkus) > 0){
            $errorMessage .= sprintf('Invalid SKU/s: %s', implode(',',  $invalidSkus));
        }

        if(count($inventoryQuarantine) > 0){
            $errorMessage .= sprintf('Invalid Status/Quarantine: %s', implode(',',  $inventoryQuarantine));
        }

        if(count($inventorySerial) > 0){
            $errorMessage .= sprintf('SerialNo/s (Invalid or Not Inbounded): %s', implode(',',  $inventorySerial));
        }

        if(count($invalidWarehouses) > 0){
            if($errorMessage){
                $errorMessage .= ', ';
            }

            $errorMessage .= sprintf('Invalid Warehouse/s: %s', implode(',',  $invalidWarehouses));
        }

        if(count($invalidOrderNumbers) > 0){
            if($errorMessage){
                $errorMessage .= ', ';
            }

            $errorMessage .= sprintf('Duplicate Order Number/s: %s', implode(',',  $invalidOrderNumbers));
        }

        throw new \UnexpectedValueException($errorMessage);
    }

    private function retrievePicklistImportDetailsFromWorksheet(UploadedFile $file, $isQuarantine=false)
    {
        $reader = $this->phpExcelFactory->createPHPExcelObject($file);
        $rowIterator = $reader->getSheet(0)->getRowIterator();

        $picklistData = [];
        $products = [];
        $warehouses = [];
        $orderNumbers = [];
        $serialNos = [];

        foreach($rowIterator as $row){
            $rowIndex = $row->getRowIndex();
            if($rowIndex === 1){
                continue;
            }

            $rowData = $reader->getActiveSheet()
                ->rangeToArray(
                    "A$rowIndex".":"."K$rowIndex"
                )[0];

            if(!$rowData[0]){
                continue;
            }

            if(!array_key_exists($rowData[0], $picklistData)){
                $picklistData[$rowData[0]] = [
                    'consignee' => $rowData[1],
                    'consignee_address' => $rowData[2],
                    'contact_number' => $rowData[3],
                    'warehouse' => $rowData[4],
                    'payment_method' => $rowData[9],
                    'products' => [],
                    'srps' => [],
                    'serial_no' => $rowData[10],
                    'isQuarantine'  => $isQuarantine
                ];
            }

            if(!array_key_exists($rowData[5], $picklistData[$rowData[0]]['products'])){
                $picklistData[$rowData[0]]['products'][$rowData[5]] = intval($rowData[8]);
                $picklistData[$rowData[0]]['srps'][$rowData[5]] = floatval(str_replace(',', '', $rowData[7]));
            }
            else{
                $picklistData[$rowData[0]]['products'][$rowData[5]] += intval($rowData[8]);
            }

            if(!in_array($rowData[0], $orderNumbers)){
                $orderNumbers[] = $rowData[0];
            }

            if(!in_array($rowData[4], $warehouses)){
                $warehouses[] = $rowData[4];
            }

            if(!in_array($rowData[5], $products)){
                $products[] = $rowData[5];
            }

            if(!in_array($rowData[10], $serialNos)){
                if (!empty($rowData[10])) {
                    $serialNos[] = $rowData[10];    
                }
            }
        }

        return [
            'importDetails' => $picklistData,
            'distinctWarehouses' => $warehouses,
            'distinctProducts' => $products,
            'distinctOrderNumbers' => $orderNumbers,
            'serialNos' => $serialNos
        ];
    }
}
