<?php

namespace BranchBundle\Commands\Picklist;

class CreatePicklistAddHistoryCommand
{
    private $clientId;
    private $params;

    public function __construct($clientId, $params)
    {
        $this->clientId = $clientId;
        $this->params = $params;
    }

    /**
     * Gets the value of clientId.
     *
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Gets the value of file.
     *
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }
}
