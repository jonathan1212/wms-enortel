<?php

namespace BranchBundle\Commands\Picklist;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class CreatePicklistImportHistoryCommand
{
    private $clientId;
    private $file;
    private $isQuarantine;

    public function __construct($clientId, UploadedFile $file, $isQuarantine = false)
    {
        $this->clientId = $clientId;
        $this->file = $file;
        $this->isQuarantine = $isQuarantine;
    }

    /**
     * Gets the value of clientId.
     *
     * @return mixed
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Gets the value of file.
     *
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getIsQuarantine()
    {
        return $this->isQuarantine;
    }
}