<?php

namespace BranchBundle\Commands\Picklist;

class PrintPicklistsCommand
{
    private $picklistNumbers;

    public function __construct($picklistNumbers)
    {
        $this->picklistNumbers = $picklistNumbers;
    }

    /**
     * Gets the value of picklistNumbers.
     *
     * @return mixed
     */
    public function getPicklistNumbers()
    {
        return $this->picklistNumbers;
    }
}
