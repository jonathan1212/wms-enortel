<?php

namespace BranchBundle\Commands\Picklist;

use Doctrine\ORM\EntityManager;
use SimpleBus\Message\Recorder\RecordsMessages;
use BranchBundle\Commands\Picklist\PrintPicklistsCommand;

class PrintPicklistsCommandHandler
{
    private $batchSize = 20;
    private $em;
    private $eventRecorder;
    private $picklistStatusRepository;
    private $picklistParticularRepository;

    public function __construct(RecordsMessages $eventRecorder, EntityManager $em)
    {
        $this->eventRecorder = $eventRecorder;
        $this->em = $em;
        $this->picklistStatusRepository = $em->getRepository('CoreBundle:PicklistStatus');
        $this->picklistParticularRepository = $em->getRepository('CoreBundle:PicklistParticular');
    }

    public function handle(PrintPicklistsCommand $command)
    {
        $picklistParticularsIterator = $this->picklistParticularRepository
            ->getPicklistParticularsAsIterable($command->getPicklistNumbers());

        $forPickingStatus = $this->picklistStatusRepository
            ->findOneByValue(\CoreBundle\Entity\PicklistStatus::FOR_PICKING);

        $i = 0;
        $picklistNumbers = [];
        foreach($picklistParticularsIterator as $row){
            $picklistParticular = $row[0];
            $particularStatusValue = $picklistParticular->getPicklistStatus()->getValue();

            // filter out particulars that has already been picked to avoid re-updating
            // TODO: Move this out of this so it can be unit testable
            if(in_array($particularStatusValue, \CoreBundle\Entity\PicklistStatus::getPicklistForReprint())){
                continue;
            }

            $picklistParticular->setPicklistStatus($forPickingStatus);
            $picklistNumbers[] = $picklistParticular->getPicklistParticularNumber();
            if(($i % $this->batchSize) === 0){
                $this->em->flush();
                $this->em->clear();
                $forPickingStatus = $this->picklistStatusRepository
                    ->findOneByValue(\CoreBundle\Entity\PicklistStatus::FOR_PICKING);
            }
            ++$i;
        }

        $this->em->flush();
        $this->em->clear();

        $this->eventRecorder->record(
            new \BranchBundle\Eventing\Picklist\PicklistsPrintedEvent(
                $picklistNumbers
            )
        );
    }
}
