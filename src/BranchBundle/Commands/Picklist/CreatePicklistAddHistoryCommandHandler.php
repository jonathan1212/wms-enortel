<?php

namespace BranchBundle\Commands\Picklist;

use Doctrine\ORM\EntityManager;
use Liuggio\ExcelBundle\Factory;
use CoreBundle\Entity\PicklistImportHistory;
use SimpleBus\Message\Recorder\RecordsMessages;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use BranchBundle\Commands\Picklist\CreatePicklistAddHistoryCommand;

class CreatePicklistAddHistoryCommandHandler
{
    private $picklistImportHistoryRepository;
    private $warehouseRepository;
    private $picklistRepository;
    private $productRepository;
    private $phpExcelFactory;
    private $eventRecorder;
    private $em;
    private $blameableUser;

    public function __construct(
        RecordsMessages $eventRecorder,
        EntityManager $em,
        Factory $phpExcelFactory,
        $blameableUser
    )
    {
        $this->em = $em;
        $this->eventRecorder = $eventRecorder;
        $this->phpExcelFactory = $phpExcelFactory;
        $this->picklistImportHistoryRepository = $em->getRepository('CoreBundle:PicklistImportHistory');
        $this->productRepository = $em->getRepository('CoreBundle:Product');
        $this->warehouseRepository = $em->getRepository('CoreBundle:Warehouse');
        $this->picklistRepository = $em->getRepository('CoreBundle:Picklist');
        $this->inventoryRepository = $em->getRepository('CoreBundle:Inventory');
        $this->blameableUser = $blameableUser;
    }

    public function handle(CreatePicklistAddHistoryCommand $command)
    {
        $clientId = $command->getClientId();
        $picklistContents = $this->retrievePicklistImportDetailsFromWorksheet(
            $command->getParams()
        );

        $this->validateImportPicklistData($picklistContents, $clientId);

        $picklistImportHistory = new PicklistImportHistory();
        $picklistImportHistory->setData([
            'client_id' => $clientId,
            'orders' => $picklistContents,
            'uploadedBy' => $this->blameableUser->getBlameable()
        ]);

        $this->picklistImportHistoryRepository->add($picklistImportHistory);
        $this->eventRecorder->record(
            new \BranchBundle\Eventing\Picklist\RequestForPicklistGenerationEvent(
                $picklistImportHistory
            )
        );
    }

    /**
     * Validates Import Data for Invalid SKUs and Warehouses
     * @param  array $picklistContents
     * @return null
     * @throws UnexpectedValueException
     */
    private function validateImportPicklistData($picklistContents, $clientId)
    {
        $content = array_values($picklistContents);
        $picklistContents = $content[0];
        
        $invalidSkus = $this->productRepository
            ->filterValidClientSkusAsArray(array($picklistContents['clientSku']));

        $invalidWarehouses = $this->warehouseRepository
            ->filterValidWarehousesAsArray(array($picklistContents['warehouse']));

        $invalidOrderNumbers = $this->picklistRepository
            ->filterValidOrderNumbersAsArray(array($picklistContents['orderNumber']), $clientId);

        $inventorySerial = [];
        
        if (trim($picklistContents['serial_no']) != '') {
            $inventorySerial = $this->inventoryRepository
                ->filterValidSerialNoAsArray(array($picklistContents['serial_no']));
        }
        
        
        if(count($invalidSkus) === 0 && count($invalidWarehouses) === 0 && count($invalidOrderNumbers) === 0 && count($inventorySerial) === 0){
            return;
        }

        $errorMessage = '';
        if(count($invalidSkus) > 0){
            $errorMessage .= sprintf('Invalid SKU/s: %s', implode(',',  $invalidSkus));
        }

        if(count($inventorySerial) > 0){
            $errorMessage .= sprintf('SerialNo/s (Invalid or Not Inbounded): %s', implode(',',  $inventorySerial));
        }

        if(count($invalidWarehouses) > 0){
            if($errorMessage){
                $errorMessage .= ', ';
            }

            $errorMessage .= sprintf('Invalid Warehouse/s: %s', implode(',',  $invalidWarehouses));
        }

        if(count($invalidOrderNumbers) > 0){
            if($errorMessage){
                $errorMessage .= ', ';
            }

            $errorMessage .= sprintf('Duplicate Order Number/s: %s', implode(',',  $invalidOrderNumbers));
        }

        throw new \UnexpectedValueException($errorMessage);
    }

    private function retrievePicklistImportDetailsFromWorksheet($data)
    {
      
      
        $picklistData[$data['orderNumber']] = [
            'consignee'         => $data['consigneeName'],
            'consignee_address' => $data['consigneeAddress'],
            'contact_number'    => $data['consigneeContactNumber'],
            'warehouse'         => $data['warehouse']->getName(),
            'payment_method'    => $data['paymentType'],
            'products'          => [],
            'srps'              => [],
            'orderNumber'       => $data['orderNumber'],
            'serial_no'          => $data['serialNo'],
            'clientSku'         => $data['product']->getClientSku(),
            'isQuarantine'      => false
        ];

        $picklistData[$data['orderNumber']]['products'][$data['product']->getClientSku()] = intval($data['quantity']);
        $picklistData[$data['orderNumber']]['srps'][$data['product']->getClientSku()] = floatval(str_replace(',', '', $data['costOfGoodsPurchased'] ));
            
        return $picklistData;
            
    }
}
