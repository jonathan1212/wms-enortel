<?php

namespace BranchBundle\Commands\User;

class UpdateUserCommand
{
    private $userId;
    private $firstName;
    private $lastName;
    private $emailAddress;
    private $contactNumber;

    public function __construct($userId, $firstName, $lastName, $emailAddress, $contactNumber)
    {
        $this->userId = $userId;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->emailAddress = $emailAddress;
        $this->contactNumber = $contactNumber;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function getContactNumber()
    {
        return $this->contactNumber;
    }
}
