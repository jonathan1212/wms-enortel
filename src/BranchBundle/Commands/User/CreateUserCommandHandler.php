<?php

namespace BranchBundle\Commands\User;

use CoreBundle\Entity\User;
use CoreBundle\Repository\UserRepository;
use BranchBundle\Commands\User\CreateUserCommand;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class CreateUserCommandHandler
{
    private $userRepository;
    private $encoder;

    public function __construct(UserRepository $userRepository, UserPasswordEncoder $encoder)
    {
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    public function handle(CreateUserCommand $command)
    {
        $user = new User();
        $user->setFirstName($command->getFirstName())
            ->setLastName($command->getLastName())
            ->setEmailAddress($command->getEmailAddress())
            ->setClientAccount($command->getClientAccount())
            ->setPassword(
                $this->encoder->encodePassword($user, $command->getPlainPassword())
            )
            ->setContactNumber($command->getContactNumber());

        $this->userRepository->add($user);
    }
}
