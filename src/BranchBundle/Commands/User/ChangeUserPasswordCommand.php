<?php

namespace BranchBundle\Commands\User;

class ChangeUserPasswordCommand
{
    private $userId;
    private $newPassword;

    public function __construct($userId, $newPassword)
    {
        $this->userId = $userId;
        $this->newPassword = $newPassword;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getNewPassword()
    {
        return $this->newPassword;        
    }
}
