<?php

namespace BranchBundle\Commands\User;

use CoreBundle\Repository\UserRepository;
use BranchBundle\Commands\User\UpdateUserCommand;

class UpdateUserCommandHandler
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(UpdateUserCommand $command)
    {
        $user = $this->userRepository->find($command->getUserId());
        $user->setFirstName($command->getFirstName())
            ->setLastName($command->getLastName())
            ->setEmailAddress($command->getEmailAddress())
            ->setContactNumber($command->getContactNumber());
    }
}
