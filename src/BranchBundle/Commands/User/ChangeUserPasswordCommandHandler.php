<?php

namespace BranchBundle\Commands\User;

use CoreBundle\Repository\UserRepository;
use BranchBundle\Commands\User\ChangeUserPasswordCommand;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

class ChangeUserPasswordCommandHandler
{
    private $userRepository;
    private $encoder;

    public function __construct(UserRepository $userRepository, UserPasswordEncoder $encoder)
    {
        $this->userRepository = $userRepository;
        $this->encoder = $encoder;
    }

    public function handle(ChangeUserPasswordCommand $command)
    {
        $user = $this->userRepository->find($command->getUserId());
        $user->setPassword(
            $this->encoder->encodePassword($user, $command->getNewPassword())
        );
    }
}
