<?php

namespace BranchBundle\Commands\User;

use CoreBundle\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use CoreBundle\Repository\WarehouseMemberRepository;
use BranchBundle\Commands\User\UpdateUserWarehouseMembershipCommand;

class UpdateUserWarehouseMembershipCommandHandler
{
    private $userRepository;
    private $memberRepository;

    public function __construct(UserRepository $userRepository, WarehouseMemberRepository $memberRepository)
    {
        $this->userRepository = $userRepository;
        $this->memberRepository = $memberRepository;
    }

    public function handle(UpdateUserWarehouseMembershipCommand $command)
    {
        $user = $this->userRepository->find($command->getUserId());
        $originalMemberships = $command->getOriginalMemberships();
        $modifiedMemberships = $user->getWarehouseMemberships();

        foreach ($originalMemberships as $membership) {
            if (false === $modifiedMemberships->contains($membership)) {
                $this->memberRepository->delete($membership);
            }
        }
    }
}
