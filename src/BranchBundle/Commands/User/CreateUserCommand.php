<?php

namespace BranchBundle\Commands\User;

class CreateUserCommand 
{
    private $firstName;
    private $lastName;
    private $emailAddress;
    private $plainPassword;
    private $contactNumber;
    private $clientAccount;

    public function __construct(
        $firstName, 
        $lastName, 
        $emailAddress, 
        $plainPassword, 
        $contactNumber, 
        $clientAccount = null
    )
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->emailAddress = $emailAddress;
        $this->plainPassword = $plainPassword;
        $this->contactNumber = $contactNumber;
        $this->clientAccount = $clientAccount;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Gets the value of clientAccount.
     *
     * @return mixed
     */
    public function getClientAccount()
    {
        return $this->clientAccount;
    }
}
