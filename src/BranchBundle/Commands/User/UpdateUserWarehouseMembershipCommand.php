<?php

namespace BranchBundle\Commands\User;

use Doctrine\ORM\PersistentCollection;
use Doctrine\Common\Collections\ArrayCollection;

class UpdateUserWarehouseMembershipCommand
{
    private $userId;
    private $originalMemberships;
    private $modifiedMemberships;

    public function __construct($userId, ArrayCollection $originalMemberships)
    {
        $this->userId = $userId;
        $this->originalMemberships = $originalMemberships;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function getOriginalMemberships()
    {
        return $this->originalMemberships;
    }
}
