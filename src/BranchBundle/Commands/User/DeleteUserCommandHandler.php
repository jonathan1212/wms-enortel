<?php

namespace BranchBundle\Commands\User;

use CoreBundle\Repository\UserRepository;
use BranchBundle\Commands\User\DeleteUserCommand;

class DeleteUserCommandHandler
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function handle(DeleteUserCommand $command)
    {
        $user = $this->userRepository->find($command->getUserId());
        $this->userRepository->delete($user);
    }
}
