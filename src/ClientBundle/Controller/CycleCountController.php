<?php

namespace ClientBundle\Controller;

use CoreBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use CoreBundle\Entity\Inventory;
use CoreBundle\Entity\CycleCount;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class CycleCountController extends Controller
{
    public function listAction(Request $request)
    {
        $warehouseId = $request->get('warehouse');

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:CycleCount')
            ->criteria([
                'warehouse' => $warehouseId > 0 ? $warehouseId : null
                ])
            ->query();

        $warehouses = $this->getDoctrine()
            ->getRepository('CoreBundle:Warehouse')
            ->getIndexWarehouses()
            ->getResult();

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('ClientBundle:CycleCount:list.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Manage Cycle Count',
            'moduleDescription' => '',
            'warehouses'    => $warehouses
        ]);
    }

    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();

        return $this->render("ClientBundle:CycleCount:index.html.twig", [
            'moduleAction' => 'Cycle Count',
            'moduleDescription' => '',
            'clients'           => $clients
            
        ]);

    }

    public function viewAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        $cycleCount = $em->getRepository('CoreBundle:CycleCount')->find($id);

        $c = explode(',', $cycleCount->getAccuracy());
        
        $diff = $cycleCount->getStartDate()->diff($cycleCount->getEndDate());

        return new JsonResponse([
            'isSuccessful' => true,
            'data'          => $cycleCount->getDetails(),
            'list'          => $cycleCount->getList(),
            'cyclecountOk'  => $c[0],
            'cyclecountNot' => $c[1],
            'diffInMin'     => $diff->i,
            'diffInSec'     => $diff->s,
            'message' => 'Ok'
        ]);

    }

    public function exportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');

        $cycleCount = $em->getRepository('CoreBundle:CycleCount')->find($id);

        $c = explode(',', $cycleCount->getAccuracy());
        
        $diff = $cycleCount->getStartDate()->diff($cycleCount->getEndDate());

        ob_start();
        $fp = fopen('php://output', 'w');

        $warehouse = $cycleCount->getWarehouse() ? $cycleCount->getWarehouse()->getName() : '';
        fputcsv($fp,array('CYCLECOUNT REFERENCE','WAREHOUSE','STORAGE LOCATION', 'DURATION'));

        fputcsv($fp, array( $cycleCount->getCyclecount(), $warehouse,
                            $cycleCount->getStorageLocation()->getName(),
                            $cycleCount->getDiffDuration()
                ));
        fputcsv($fp, array());
        fputcsv($fp, array());

        fputcsv($fp,array('INVENTORY ACCURACY'));
        fputcsv($fp, array($cycleCount->checkAccuracy()));

        fputcsv($fp, array());
        fputcsv($fp, array());

        fputcsv($fp, array('TAG','MOBILE LOCATION', 'STORAGE LOCATION', 'SKU', 'ACCURACY', 'STATUS'));

        foreach (json_decode($cycleCount->getDetails(),true) as $detail) {
            fputcsv($fp, array($detail['tag'],$detail['mobileloc'],$detail['storageloc'], $detail['sku'], $detail['corrected'] ? 'miss': 'hit', $detail['status']));
        }


        fputcsv($fp, array());
        fputcsv($fp, array());
        fputcsv($fp, array('REMAINING'));
        fputcsv($fp, array('TAG','MOBILE LOCATION', 'STORAGE LOCATION', 'SKU', 'STATUS'));
        foreach (json_decode($cycleCount->getList(),true) as $detail) {
            fputcsv($fp, array($detail['tag'],$detail['mobile'],$detail['storage'], $detail['sku'], $detail['status']));
        }

        fclose($fp);
        $exportDate = date('mdYHis') . '.csv';

        $response = new Response();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', "attachment; filename={$exportDate}");
        $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
        
        return $response;
        ob_end_flush();
    }



}