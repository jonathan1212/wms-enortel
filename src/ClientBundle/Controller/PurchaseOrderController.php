<?php

namespace ClientBundle\Controller;

use CoreBundle\Entity\PurchaseOrder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BranchBundle\Commands\PurchaseOrder\CreatePurchaseOrderCommand;

/**
 * @Security("has_role('ROLE_HAS_CLIENT_PURCHASE_ORDER_MODULE_ACCESS')")
 */
class PurchaseOrderController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $params = $this->getParams($request);

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:PurchaseOrder')
            ->getIndexPurchaseOrders($params);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('ClientBundle:PurchaseOrder:index.html.twig',[
            'pagination' => $pagination,
            'moduleAction' => 'Manage Purchase Orders',
            'moduleDescription' => ''
        ]);
    }

    public function viewAction($clientDomain, $purchaseOrderId, Request $request)
    {
        $purchaseOrder = $this->getDoctrine()
            ->getRepository('CoreBundle:PurchaseOrder')
            ->findPurchaseOrderById($purchaseOrderId);

        $list = $this->showDetails($purchaseOrderId);

        if(!$purchaseOrder){
            return $this->redirectToRoute('client_purchase_order_index', ['clientDomain' => $clientDomain]);
        }

        return $this->render('ClientBundle:PurchaseOrder:view.html.twig',[
            'moduleAction' => $purchaseOrder->getPurchaseOrderNumber(),
            'moduleDescription' => '',
            'purchaseOrder' => $purchaseOrder,
            'list'      => $list
        ]);
    }

    public function printAction($clientDomain, $purchaseOrderNumber, Request $request)
    {
        $purchaseOrder = $this->getDoctrine()
            ->getRepository('CoreBundle:PurchaseOrder')
            ->findPurchaseOrderByPurchaseOrderNumber($purchaseOrderNumber);

        if(!$purchaseOrder){
            return $this->redirectToRoute('client_purchase_order_index', ['clientDomain' => $clientDomain]);
        }

        return $this->render('ClientBundle:PurchaseOrder:print.html.twig', compact('purchaseOrder'));
    }

    public function addAction($clientDomain, Request $request)
    {
        $clientPurchaseOrder = new PurchaseOrder();
        $clientPurchaseOrder->setClient(
            $this->getUser()->getClient()
        );

        $form = $this->createForm(
            'BranchBundle\Form\PurchaseOrderType',
            $clientPurchaseOrder,
            ['is_from_client_portal' => true]
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $purchaseOrder = $form->getData();
            $command = new CreatePurchaseOrderCommand(
                $purchaseOrder->getSupplier(),
                $purchaseOrder->getProducts(),
                $purchaseOrder->getWarehouse(),
                $purchaseOrder->getPaymentType(),
                $purchaseOrder->getClient(),
                $purchaseOrder->getRemarks(),
                $purchaseOrder->getFile(),
                $purchaseOrder->getReferenceNo()
            );

            try{
                $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('client_purchase_order_index', ['clientDomain' => $clientDomain]);

            } catch(\UnexpectedValueException $e){
                $form->get('file')->addError(new \Symfony\Component\Form\FormError(
                    $e->getMessage()
                ));
            }
            
        }

        return $this->render('ClientBundle:PurchaseOrder:add.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'ADD PURCHASE ORDER',
            'moduleDescription' => ''
        ]);
    }

    public function importAction(Request $request)
    {
        $form = $this->createForm('purchase_order_import',['is_from_client_portal' => true]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();


            $command = new ImportPurchaseOrderCommand(
                $data['purchaseOrderImport'],
                $this->getUser()->getClient(),
                $data['supplier'],
                $data['paymentType'],
                $data['remarks'],
                $data['warehouse']
            );

            try{
                $this->get('command_bus')->handle($command);
                return $this->redirectToRoute('branch_purchase_order_index');
            } catch(\UnexpectedValueException $e){
                $form->get('purchaseOrderImport')->addError(new \Symfony\Component\Form\FormError(
                    $e->getMessage()
                ));
            } catch(\Doctrine\DBAL\Exception\UniqueConstraintViolationException $e){
                $form->get('purchaseOrderImport')->addError(new \Symfony\Component\Form\FormError(
                    'Duplicate Client and Client SKUs found, please review your file.'
                ));
            }

        }

        return $this->render('BranchBundle:PurchaseOrder:form.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'IMPORT',
            'moduleDescription' => ''
        ]);
    }

    public function productSearchAction($clientDomain, Request $request)
    {
        $searchQuery = [];
        $searchQuery['query'] = $request->request->get('query', '');
        $searchQuery['supplierId'] = $request->request->get('supplierId', '');
        
        $products = $this->getDoctrine()
            ->getRepository('CoreBundle:Product')
            ->findProductsByGeneralQuery($searchQuery);

        return new JsonResponse(['items' => $products]);
    }

    private function getParams($request)
    {
        return $params = [
            'clients'   => $request->get('client'),
            'purchaseOrder'     => $request->get('purchaseOrder'),
            'referenceNo'   => $request->get('referenceNo'),
            'client'        => $request->get('client'),
            'datefrom'      => $request->get('datefrom'),
            'dateto'      => $request->get('dateto'),
        ];
    }

    private function showDetails($purchaseOrderId)
    {
        $em = $this->get('doctrine')->getEntityManager();
        $conn = $em->getConnection();
        $inventoryService = $this->get('branch.service.inventory.inventory_service');

        $rs = $conn->fetchAll(" 
            select p.id, p.client_sku, p.product_description, count(p.product_description) as productCount
            from inventory i
            left join product p on i.product_id = p.id
            where i.purchase_order_id = :purchase_order_id and p.deleted_at is null
            group by i.product_id
            ", ["purchase_order_id" => $purchaseOrderId]
        );

        $list = [];

        $totalOnHand = 0;
        $totalAllocated = 0;
        $totalQuaranteen = 0;
        $totalDispatch = 0;
        $totalPicked = 0;
        $totalPacked = 0;

        foreach ($rs as $each) {
            $product = [];

            $product['info']                 = $each;
            $product['countOnHand']          = $inventoryService->count($each);
            $product['countByOrderNumber']   = $inventoryService->countByOrderNumber($each);
            
            $totalOnHand = $totalOnHand + (int)$product['countOnHand']['onHand'];
            $totalAllocated = $totalAllocated + (int)$product['countOnHand']['allocated'];
            $totalQuaranteen = $totalQuaranteen + (int)$product['countOnHand']['quaranteed'];
            $totalDispatch = $totalDispatch + (int)$product['countOnHand']['dispatch'];
            $totalPicked = $totalPicked + (int)$product['countOnHand']['picked'];
            $totalPacked = $totalPacked + (int)$product['countOnHand']['packed'];

            $list[] = $product;
        }


        return  [
            'list'              => $list,
            'totalOnHand'       => $totalOnHand,
            'totalAllocated'    => $totalAllocated,
            'totalQuaranteen'   => $totalQuaranteen,
            'totalDispatch'     => $totalDispatch,
            'totalPicked'       => $totalPicked,
            'totalPacked'       => $totalPacked
        ];
    }
}
