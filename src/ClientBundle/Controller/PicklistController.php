<?php

namespace ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use BranchBundle\Commands\Picklist\PrintPicklistsCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use BranchBundle\Commands\Picklist\CreatePicklistImportHistoryCommand;
use BranchBundle\Commands\Picklist\CreatePicklistAddHistoryCommand;
use CoreBundle\Entity\PicklistStatus;

/**
 * @Security("has_role('ROLE_HAS_CLIENT_PICKLIST_MODULE_ACCESS')")
 */
class PicklistController extends Controller
{
    public function indexAction(Request $request)
    {
        $params = [
            'clients'   => $request->get('client'),
            'status'    => $request->get('status'),
            'picklistno'     => $request->get('picklistno'),
            'ordernumber'   => $request->get('ordernumber'),
            'client'        => $request->get('client'),
            'datefrom'      => $request->get('datefrom'),
            'dateto'      => $request->get('dateto'),
        ];

        $clients = $this->getDoctrine()->getRepository('CoreBundle:Client')->getIndexClients()->getArrayResult();

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:PicklistParticular')
            ->getIndexPicklistParticulars($params);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('ClientBundle:Picklist:index.html.twig', [
            'pagination' => $pagination,
            'statuses'   => PicklistStatus::getAlStatus(),
            'clients'     => $clients,
            'moduleAction' => 'Picklist',
            'moduleDescription' => ''
        ]);
    }

    public function addAction($clientDomain, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(
            'BranchBundle\Form\PicklistAddType', 
            null,
            ['is_from_client_portal' => true, 'entity_manager' => $em]
        );

        $form->handleRequest($request);

        if($form->isSubmitted() && !is_null($request->get('submit')) ){
            $picklist = $form->getData();

            try{                
                $command = new CreatePicklistAddHistoryCommand(
                    $this->getUser()->getClient()->getId(),
                    $picklist
                );

                $this->get('command_bus')->handle($command);

                $this->addFlash('success', 'Successfully Created Picklist');

                return $this->redirectToRoute('client_picklist_add', ['clientDomain' => $clientDomain]);


            } catch(\UnexpectedValueException $e){
                $form->get('serialNo')->addError(new \Symfony\Component\Form\FormError(
                    $e->getMessage()
                ));
            }

        }


        return $this->render('ClientBundle:Picklist:add.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Add Picklist',
            'moduleDescription' => ''
        ]);   
    }


    public function importAction($clientDomain, Request $request)
    {
        $form = $this->createForm(
            'BranchBundle\Form\PicklistImportType', 
            null,
            ['is_from_client_portal' => true]
        );

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $picklistImport = $form->getData();
            try{
                $command = new CreatePicklistImportHistoryCommand(
                    $this->getUser()->getClient()->getId(),
                    $picklistImport['picklistImport'],
                    $picklistImport['isQuarantine']
                );

                $this->get('command_bus')->handle($command);
                return $this->redirectToRoute('client_picklist_index', ['clientDomain' => $clientDomain]);
            } catch(\UnexpectedValueException $e){
                $form->get('picklistImport')->addError(new \Symfony\Component\Form\FormError(
                    $e->getMessage()
                ));
            }
        }

        return $this->render('ClientBundle:Picklist:import.html.twig',[
            'form' => $form->createView(),
            'moduleAction' => 'Import Picklist',
            'moduleDescription' => ''
        ]);
    }

    public function viewAction($clientDomain, $picklistParticularNumber, Request $request)
    {
        $picklistParticular = $this->getDoctrine()
            ->getManager()
            ->getRepository('CoreBundle:PicklistParticular')
            ->findOneByPicklistParticularNumber($picklistParticularNumber);

        if(!$picklistParticular){
            return $this->redirectToRoute('client_picklist_index', ['clientDomain' => $clientDomain]);
        }

        $printAggregate = $this->get('branch.service.picklist.picklist_print_aggregator')
            ->aggregatePicklists([$picklistParticularNumber]);

        return $this->render('ClientBundle:Picklist:view.html.twig', [
            'picklistParticularNumber' => $picklistParticularNumber,
            'printAggregate' => $printAggregate,
            'moduleAction' => $picklistParticularNumber.' Preview',
            'moduleDescription' => ''
        ]);
    }
}
