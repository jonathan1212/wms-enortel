<?php

namespace ClientBundle\Controller;

use CoreBundle\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use BranchBundle\Commands\User\CreateUserCommand;
use BranchBundle\Commands\User\DeleteUserCommand;
use BranchBundle\Commands\User\UpdateUserCommand;
use BranchBundle\Commands\User\ChangeUserPasswordCommand;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class UserController extends Controller
{
    public function indexAction(Request $request)
    {
        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->getIndexUsers($request->query->all());

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('ClientBundle:User:index.html.twig', [
            'pagination' => $pagination,
            'moduleAction' => 'Manage Users',
            'moduleDescription' => ''
        ]);
    }

    /**
     * @Security("has_role('ROLE_CAN_CREATE_CLIENT_USER')")
     */
    public function addAction($clientDomain, Request $request)
    {
        $clientUser = new User();
        $clientUser->setClientAccount($this->getUser()->getClient());
        $form = $this->createForm(
            'BranchBundle\Form\UserType', 
            $clientUser, 
            ['validation_groups' => ['add']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new CreateUserCommand(
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmailAddress(),
                $user->getPlainPassword(),
                $user->getContactNumber(),
                $this->getUser()->getClient()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
        }

        return $this->render('ClientBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Add Users',
            'moduleDescription' => ''
        ));
    }

    /**
     * @Security("has_role('ROLE_CAN_UPDATE_CLIENT_USER')")
     */
    public function updateAction($clientDomain, $userId, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->find($userId);

        if(!$user){
            return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
        }

        $form = $this->createForm(
            'BranchBundle\Form\UserType', 
            $user, 
            ['validation_groups' => ['update']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new UpdateUserCommand(
                $user->getId(),
                $user->getFirstName(),
                $user->getLastName(),
                $user->getEmailAddress(),
                $user->getContactNumber()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
        }

        return $this->render('ClientBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Update Users',
            'moduleDescription' => ''
        ));
    }

    public function changePasswordAction($clientDomain, $userId, Request $request)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->find($userId);

        if(!$user){
            return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
        }

        $form = $this->createForm(
            'BranchBundle\Form\UserType', 
            $user, 
            ['validation_groups' => ['change_password']]
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $command = new ChangeUserPasswordCommand(
                $user->getId(),
                $user->getPlainPassword()
            );

            $this->get('command_bus')->handle($command);
            return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
        }

        return $this->render('ClientBundle:User:form.html.twig', array(
            'form' => $form->createView(),
            'moduleAction' => 'Change Password',
            'moduleDescription' => 'For security reasons, only the logged in user can change his/her password.'
        ));
    }

    /**
     * @Security("has_role('ROLE_CAN_DELETE_CLIENT_USER')")
     */
    public function deleteAction($clientDomain, $userId)
    {
        $user = $this->getDoctrine()
            ->getRepository('CoreBundle:User')
            ->find($userId);

        if(!$user){
            return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
        }

        $command = new DeleteUserCommand($userId);
        $this->get('command_bus')->handle($command);
        return $this->redirectToRoute('client_user_index', ['clientDomain' => $clientDomain]);
    }
}
