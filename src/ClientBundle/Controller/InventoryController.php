<?php

namespace ClientBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class InventoryController extends Controller
{
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine();
        $params = $this->getParams($request);

        if ($request->get('export')) {
            return $this->forward('ClientBundle:Inventory:export', array('request' => $request));
        }

        $mobileLocations = $em->getRepository('CoreBundle:MobileLocation')->findAll();
        $storageLocations = $em->getRepository('CoreBundle:StorageLocation')->findAll();
        $inventoryStatus = $em->getRepository('CoreBundle:InventoryStatus')->findAll();

        $query = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getIndexInventories($params);

        $pagination = $this->get('knp_paginator')
            ->paginate(
                $query,
                $request->query->getInt('page', 1)
            );

        return $this->render('ClientBundle:Inventory:index.html.twig',[
            'pagination' => $pagination,
            'mobileLocations'  => $mobileLocations,
            'storageLocations'   => $storageLocations,
            'inventoryStatus'   => $inventoryStatus,
            'moduleAction' => 'Inventory',
            'moduleDescription' => ''
        ]);
    }


    public function exportFormAction(Request $request)
    {
        $em = $this->getDoctrine();
        $warehouses = $em->getRepository('CoreBundle:Warehouse')->getIndexWarehouses()->getArrayResult();

        return $this->render('ClientBundle:Inventory:export.html.twig',[
            'moduleAction' => 'Inventory',
            'moduleDescription' => '',
            'warehouses'       => $warehouses,
        ]);

    }


    public function exportAction(Request $request)
    {
        $this->em = $this->getDoctrine();
        $this->conn = $this->em->getConnection();
        $inventoryService = $this->get('branch.service.inventory.inventory_service');
        
        ob_start();
        $fp = fopen('php://output', 'w');
        $params = $this->getParams($request);

        $result = $this->getDoctrine()
            ->getRepository('CoreBundle:Inventory')
            ->getIndexInventories($params)
            ->getArrayResult();

        fputcsv($fp, array(
            'CLIENT',
            'PICKLISTNO',
            'PICKLIST PARTICULAR',
            'CREATED BY',
            'REMARKS' ,
            'PICKLISTED BY',
            'DATE PICKLISTED',
            'CONSIGNEE',
            'ADDRESS',
            'TAG NO',
            'LMBM SKU',
            'SERIAL NO',
            'CLIENT SKU',
            'SUPPLIER SKU',
            'STORAGE LOCATION', 
            'MOBILE LOCATION',
            'ORDER NO',
            'PO NO',
            'COGP',
            'INVENTORY STATUS',
            'DELIVERY STATUS',
            'ATTACHMENT',
            'WAYBILL NUMBER',
            'DELIVERED REMARKS',
            'DATE DELIVERED',
            'DELIVERED TO', 
            'DATE INBOUNDED',
            'DATE PICKED',
            'DATE PACKED',
            'NAME',
            'BRANCH' ));

        foreach ($result as  $each) {
                
                $picklist = $inventoryService->getPicklist($each['picklistDetail']);
                
                $datePicked = $each['datePicked'] ? $each['datePicked']->format('Y-m-d H:i:s') : '';
                $datePacked = $each['datePacked'] ? $each['datePacked']->format('Y-m-d H:i:s') : '';
                
                fputcsv($fp, array( 
                    $each['client']['name'],
                    $picklist['picklistNumber'],
                    $picklist['picklistParticularNumber'],
                    $picklist['createdBy'],
                    $each['purchaseOrder']['remarks'],
                    $picklist['createdBy'],
                    $picklist['datePicklisted'],
                    $picklist['consignee'],
                    $picklist['address'],
                    $each['tag'],
                    $each['product']['sku'],
                    "{$each['serialNo']}",
                    $each['product']['clientSku'],
                    $each['product']['supplierSku'],
                    $each['storageLocation']['name'],
                    $each['mobileLocation']['name'],
                    $each['orderNumber'],
                    $each['purchaseOrder']['purchaseOrderNumber'],
                    $each['product']['costOfGoodsPurchased'],
                    $each['inventoryStatus']['name'],
                    $picklist['status'],
                    $picklist['attachment'],
                    $picklist['waybill_number'],
                    $picklist['remarks'],
                    $picklist['deliveredCreated'],
                    $picklist['deliveredto'],                       
                    $each['dateInbounded']->format('Y-m-d H:i:s'),
                    $datePicked,
                    $datePacked,
                    $each['product']['productDescription'],
                    $each['warehouse']['name']
                    )                    
                );
            }

            fclose($fp);
            $exportDate = "inventory". date('Y-m-d') . '.csv';

            $response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
            $response->headers->set('Content-Disposition', "attachment; filename={$exportDate}");
            $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');
            
            return $response;
            ob_end_flush();

    }

    

    public function summaryAction(Request $request)
    {
        $em = $this->getDoctrine();
        $conn = $em->getConnection();

        $warehouses = $em->getRepository('CoreBundle:Warehouse')->findAll();
        
        $clientId = $this->getUser()->getClient()->getId();
        $warehouseId = 1;

        if($request->isMethod('POST')) {
            $warehouseId = $request->get('warehouse');
        }

        $rs = $conn->fetchAll(" 
            select p.id, p.client_sku, p.product_description, count(p.product_description) as productCount
            from inventory i
            left join product p on i.product_id = p.id
            where i.warehouse_id = :warehouseId and i.client_id = :clientId and p.deleted_at is null
            group by i.product_id order by p.client_sku ASC
            ", ["clientId" => $clientId, "warehouseId" => $warehouseId]
        );

        $list = [];
        foreach ($rs as $each) {
            $product = [];

            $product['info']                 = $each;
            $product['countOnHand']          =  $this->count($each, $warehouseId);
            $product['countByOrderNumber']   = $this->countByOrderNumber($each);
            
            if ($product['countOnHand']['onHand'] > 0) {
                $list[] = $product;    
            }
            
        }

        return $this->render('ClientBundle:Inventory:list.html.twig',[
            'moduleAction' => 'Inventory Report',
            'list'  => $list,
            'warehouses' => $warehouses,
            'moduleDescription' => ''
        ]);

    }

    protected function count($each,$warehouseId)
    {
        $clientId = $this->getUser()->getClient()->getId();

        $em = $this->getDoctrine();
        $conn = $em->getConnection();

        $rs = $conn->fetchAll(" 
             select count(i.product_id) as productCount,
                 count(i.date_picked) as countDatePick, 
                 count(date_packed) as countDatePack,
                 i.inventory_status_id as status 
             from inventory i 
             where i.product_id = :productId
             and i.client_id = :clientId
             and i.warehouse_id = :warehouseId
             group by i.product_id ,i.inventory_status_id", ['productId' => $each['id'], 'clientId' => $clientId, 'warehouseId' => $warehouseId]
            );

        $count = [
            'onHand'        => 0,
            'allocated'     => 0,
            'quaranteed'    => 0,
            'dispatch'      => 0,
            'picked'        => 0,
            'packed'        => 0,
        ];

        foreach ($rs as $each) {
                
            // inbounded
            if ($each['status'] == 1) {
                $count['onHand'] = $each['productCount']; 
            }
            // allocated
            else if ($each['status'] == 2) {
                $count['allocated'] = $each['productCount']; 
            }
            // quaranteen
            else if ($each['status'] == 4) {
                $count['quaranteed'] = $each['productCount']; 
            }

            // dispatch
            else if ($each['status'] == 7) {
                $count['dispatch'] = $each['productCount']; 
                $count['picked'] = $each['countDatePick'];
                $count['packed'] = $each['countDatePack'];
            }
            // lost 8 
            else if ($each['status'] == 8) {
                $count['lost'] = $each['productCount']; 
            }

        }

        return $count;
    }

    protected function countByOrderNumber($each)
    {
        $em = $this->getDoctrine();
        $conn = $em->getConnection();
        $rs = $conn->fetchAll(" 
             select order_number ,
              count(order_number) as orderNumberCount,
              count(date_picked) as picked,
              count(date_packed) as packed
            from inventory 
            where 
            product_id = :productId 
            group by order_number", ['productId' => $each['id']]
            
            );


        return $rs;
    }

    private function getParams($request)
    {
        return $params = [
            'status'    => $request->get('status'),
            'purchaseOrder' => $request->get('purchaseOrderId'),
            'orderNumber'   => $request->get('orderNumberId'),
            'tag'           => $request->get('tag'),
            'clientsku'   => $request->get('clientsku'),
            'mobileLocation'    => $request->get('mobileLocation'),
            'storageLocation'   => $request->get('storageLocation'),
            'warehouse'           => $request->get('warehouse'),
            'start'             => $request->get('start'),
            'end'               => $request->get('end')
        ];
         
    }
}
