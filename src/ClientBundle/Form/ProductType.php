<?php

namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'productDescription', 
                'Symfony\Component\Form\Extension\Core\Type\TextType'
            )
            ->add(
                'clientSku', 
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'label' => 'Client SKU'
                )
            )
            ->add(
                'supplier',
                'Symfony\Bridge\Doctrine\Form\Type\EntityType',
                array(
                    'class' => 'CoreBundle:Supplier',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'supplierSku', 
                'Symfony\Component\Form\Extension\Core\Type\TextType',
                array(
                    'required' => false,
                    'label' => 'Supplier SKU'
                )
            )
            ->add(
                'costOfGoodsPurchased', 
                'Symfony\Component\Form\Extension\Core\Type\TextType'
            )

            ->add(
                'length', 
                'Symfony\Component\Form\Extension\Core\Type\NumberType',
                array(
                    'required' => true,
                    'label' => 'Length in (cm)',
                    'constraints' => array(
                        new GreaterThanOrEqual(1)
                    )
                )
            )
            ->add(
                'width', 
                'Symfony\Component\Form\Extension\Core\Type\NumberType',
                array(
                    'required' => true,
                    'label' => 'Width in (cm)',
                    'constraints' => array(
                        new GreaterThanOrEqual(1)
                    )
                )
            )
            ->add(
                'height', 
                'Symfony\Component\Form\Extension\Core\Type\NumberType',
                array(
                    'required' => true,
                    'label' => 'Height in (cm)',
                    'constraints' => array(
                        new GreaterThanOrEqual(1)
                    )
                )
            )
            ->add(
                'weigth', 
                'Symfony\Component\Form\Extension\Core\Type\NumberType',
                array(
                    'required' => true,
                    'label' => 'Weight in (KG)',
                    'constraints' => array(
                        new GreaterThanOrEqual(1)
                    )
                )
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'CoreBundle\Entity\Product'
        ));
    }
}
