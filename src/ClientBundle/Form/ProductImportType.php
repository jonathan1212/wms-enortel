<?php

namespace ClientBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductImportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'supplier',
                'Symfony\Bridge\Doctrine\Form\Type\EntityType',
                array(
                    'class' => 'CoreBundle:Supplier',
                    'choice_label' => 'name'
                )
            )
            ->add(
                'productImport', 
                'Symfony\Component\Form\Extension\Core\Type\FileType',
                array(
                    'constraints' => array(
                        new File(array(
                            'maxSize' => '2M',
                            'mimeTypes' => [
                                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', // xlsx
                                'application/vnd.ms-excel', // xls
                                'application/vnd.oasis.opendocument.formula', // odf
                                'application/vnd.oasis.opendocument.spreadsheet' // ods
                            ],
                            'mimeTypesMessage' => "The mime type of the file is invalid ({{ type }})."
                        ))
                    )
                )
            )
        ;
    }
}
