<?php

namespace ClientBundle\Security\User;

use CoreBundle\Repository\UserRepository;
use CoreBundle\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class ClientUserProvider implements UserProviderInterface
{
    private $userRepository;
    private $clientRepository;
    private $requestStack;

    public function __construct(
        UserRepository $userRepository, 
        ClientRepository $clientRepository,
        RequestStack $requestStack 
    )
    {
        $this->userRepository = $userRepository;
        $this->clientRepository = $clientRepository;
        $this->requestStack = $requestStack;
    }

    /**
     * Refer to app/security.yml to know the capabilities of each role
     */
    public function loadUserByUsername($emailAddress)
    {
        $request = $this->requestStack->getCurrentRequest();
        $clientAccount = $request->getSession()->get('_client');
        $user = $this->clientRepository->findOneBy([
            'emailAddress' => $emailAddress,
            'hostAlias' => $clientAccount->getHostAlias()
        ]);

        if($user){
            $user->setRoles(['ROLE_CLIENT_ROOT']);
            return $user;
        }

        $user = $this->userRepository->findOneBy([
            'emailAddress' => $emailAddress,
            'clientAccount' => $clientAccount
        ]);

        if($user){
            $user->setRoles(['ROLE_CLIENT_USER']);
            return $user;
        }

        throw new UsernameNotFoundException(
            sprintf('User with email "%s" does not exist.', $emailAddress)
        );
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof \CoreBundle\Entity\User || 
            !$user instanceof \CoreBundle\Entity\Client
            ) {
            throw new UnsupportedUserException(
                sprintf('Instances of "%s" are not supported.', get_class($user))
            );
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'CoreBundle\Entity\User' || $class === 'CoreBundle\Entity\Client';
    }
}
