<?php

namespace ClientBundle\Security\RequestMatcher;

use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @link http://shout.setfive.com/2014/09/14/symfony2-using-request_matcher-for-custom-firewall-rules/
 */
class ClientFirewallMatcher implements RequestMatcherInterface
{
    private $clientHost;

    public function __construct($clientHost)
    {
        $this->clientHost = $clientHost;
    }

    public function matches(Request $request)
    {
        $currentHost = $request->getHttpHost();
        return strpos($currentHost, $this->clientHost) !== false;
    }
}
