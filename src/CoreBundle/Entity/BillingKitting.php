<?php

namespace CoreBundle\Entity;

/**
 * BillingKitting
 */
use CoreBundle\Entity\Interfaces\WarehouseAware;

class BillingKitting implements WarehouseAware
{
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateAdded;

    /**
     * @var \DateTime
     */
    private $dateTo;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isPaid;

    /**
     * @var string
     */
    private $billingNumber;

    /**
     * @var string
     */
    private $remarks;

    /**
     * @var string
     */
    private $amount = 0;

    /**
     * @var integer
     */
    private $noOfPerson;

    /**
     * @var string
     */
    private $rate = 0;

    /**
     * @var string
     */
    private $overcharge = 0;

    /**
     * @var string
     */
    private $othercharge = 0;

    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return BillingKitting
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return BillingKitting
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return BillingKitting
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isPaid
     *
     * @param boolean $isPaid
     *
     * @return BillingKitting
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid
     *
     * @return boolean
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set billingNumber
     *
     * @param string $billingNumber
     *
     * @return BillingKitting
     */
    public function setBillingNumber($billingNumber)
    {
        $this->billingNumber = $billingNumber;

        return $this;
    }

    /**
     * Get billingNumber
     *
     * @return string
     */
    public function getBillingNumber()
    {
        return $this->billingNumber;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return BillingKitting
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return BillingKitting
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set noOfPerson
     *
     * @param integer $noOfPerson
     *
     * @return BillingKitting
     */
    public function setNoOfPerson($noOfPerson)
    {
        $this->noOfPerson = $noOfPerson;

        return $this;
    }

    /**
     * Get noOfPerson
     *
     * @return integer
     */
    public function getNoOfPerson()
    {
        return $this->noOfPerson;
    }

    /**
     * Set rate
     *
     * @param string $rate
     *
     * @return BillingKitting
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return string
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Set overcharge
     *
     * @param string $overcharge
     *
     * @return BillingKitting
     */
    public function setOvercharge($overcharge)
    {
        $this->overcharge = $overcharge;

        return $this;
    }

    /**
     * Get overcharge
     *
     * @return string
     */
    public function getOvercharge()
    {
        return $this->overcharge;
    }

    /**
     * Set othercharge
     *
     * @param string $othercharge
     *
     * @return BillingKitting
     */
    public function setOthercharge($othercharge)
    {
        $this->othercharge = $othercharge;

        return $this;
    }

    /**
     * Get othercharge
     *
     * @return string
     */
    public function getOthercharge()
    {
        return $this->othercharge;
    }

    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return BillingKitting
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return BillingKitting
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
