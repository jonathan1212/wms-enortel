<?php

namespace CoreBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use BranchBundle\Validator\Constraints as CustomAssert;

/**
 * InventoryMoveHistory
 * @CustomAssert\LocationConsistentWithMobileLocationGroup()
 */
class InventoryMoveHistory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $target;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \CoreBundle\Entity\StorageLocation
     */
    private $storageLocation;

    /**
     * @var \CoreBundle\Entity\MobileLocation
     */
    private $mobileLocation;

    /**
     * @var \CoreBundle\Entity\User
     */
    private $movedBy;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set target
     *
     * @param string $target
     *
     * @return InventoryMoveHistory
     */
    public function setTarget($target)
    {
        $this->target = trim($target);

        return $this;
    }

    /**
     * Get target
     *
     * @return string
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return InventoryMoveHistory
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set storageLocation
     *
     * @param \CoreBundle\Entity\StorageLocation $storageLocation
     *
     * @return InventoryMoveHistory
     */
    public function setStorageLocation(\CoreBundle\Entity\StorageLocation $storageLocation = null)
    {
        $this->storageLocation = $storageLocation;

        return $this;
    }

    /**
     * Get storageLocation
     *
     * @return \CoreBundle\Entity\StorageLocation
     */
    public function getStorageLocation()
    {
        return $this->storageLocation;
    }

    /**
     * Set mobileLocation
     *
     * @param \CoreBundle\Entity\MobileLocation $mobileLocation
     *
     * @return InventoryMoveHistory
     */
    public function setMobileLocation(\CoreBundle\Entity\MobileLocation $mobileLocation = null)
    {
        $this->mobileLocation = $mobileLocation;

        return $this;
    }

    /**
     * Get mobileLocation
     *
     * @return \CoreBundle\Entity\MobileLocation
     */
    public function getMobileLocation()
    {
        return $this->mobileLocation;
    }

    /**
     * Set movedBy
     *
     * @param \CoreBundle\Entity\User $movedBy
     *
     * @return InventoryMoveHistory
     */
    public function setMovedBy(\CoreBundle\Entity\User $movedBy = null)
    {
        $this->movedBy = $movedBy;

        return $this;
    }

    /**
     * Get movedBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getMovedBy()
    {
        return $this->movedBy;
    }
}
