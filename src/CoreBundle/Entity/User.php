<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use CoreBundle\Entity\Interfaces\ClientAware;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use BranchBundle\Validator\Constraints as CustomAssert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

/**
 * User
 * @CustomAssert\UniqueClientUserEmailAddress(
 *     groups={"add", "update"}
 * )
 */
class User implements UserInterface, ClientAware
{
    /**
     * @var int
     * 
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"add", "update"})
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "First Name cannot be longer than {{ limit }} characters",
     *      groups={"add", "update"}
     * )
     */
    private $firstName;

    /**
     * @var string
     * @Assert\NotBlank(groups={"add", "update"})
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Last Name cannot be longer than {{ limit }} characters",
     *      groups={"add", "update"}
     * )
     */
    private $lastName;

    /**
     * @var string
     * @Assert\Email(groups={"add", "update"})
     */
    private $emailAddress;

    /**
     * @Assert\NotBlank(groups={"add"})
     * @Assert\Length(
     *     min=8,
     *     max=4096,
     *     minMessage = "For security purposes, password should be atleast {{ limit }} characters long",
     *     groups={"add", "change_password"}
     * )
     */
    private $plainPassword;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     * @SecurityAssert\UserPassword(
     *     message = "Incorrect Password",
     *     groups={"change_password"}
     * )
     */
    private $oldPassword;

    /**
     * @var string
     * @Assert\Length(
     *      max = 50,
     *      maxMessage = "Contact Number cannot be longer than {{ limit }} characters",
     *      groups={"add", "update"}
     * )
     */
    private $contactNumber;

    /**
     * @var ArrayCollection
     * @Assert\Valid
     */
    private $warehouseMemberships;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var CoreBundle:Warehouse
     */
    private $warehouse = null;

    /**
     * @var int
     */
    private $warehouseId = 0;

    /**
     * @var \DateTime
     */
    private $deletedAt;

    /**
     * @var \CoreBundle\Entity\Client
     */
    private $clientAccount = null;

    public function __construct()
    {
        $this->warehouseMemberships = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Get Fullname
     *
     * @return string 
     */
    public function getFullName()
    {
        return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     * @return User
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string 
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set old password
     *
     * @param string $oldPassword
     * @return User
     */
    public function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;

        return $this;
    }

    /**
     * Get old/current password
     *
     * @return string 
     */
    public function getOldPassword()
    {
        return $this->oldPassword;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     * @return User
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string 
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Add warehouseMembership
     *
     * @param \CoreBundle\Entity\WarehouseMember $warehouseMembership
     *
     * @return User
     */
    public function addWarehouseMembership(\CoreBundle\Entity\WarehouseMember $warehouseMembership)
    {
        $warehouseMembership->setUser($this);
        $this->warehouseMemberships[] = $warehouseMembership;
        return $this;
    }

    /**
     * Remove warehouseMembership
     *
     * @param \CoreBundle\Entity\WarehouseMember $warehouseMembership
     */
    public function removeWarehouseMembership(\CoreBundle\Entity\WarehouseMember $warehouseMembership)
    {
        $this->warehouseMemberships->removeElement($warehouseMembership);
    }

    /**
     * Get warehouseMemberships
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWarehouseMemberships()
    {
        return $this->warehouseMemberships;
    }

    /**
     * Get roles for user
     * 
     * @param array
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Sets session warehouse ID
     * 
     * @param int
     */
    public function setWarehouseId($warehouseId)
    {
        $this->warehouseId = $warehouseId;

        return $this;
    }

    /**
     * Gets session warehouse ID
     * 
     * @return int
     */
    public function getWarehouseId()
    {
        return $this->warehouseId;
    }

    /**
     * Sets the warehouse entity for later use
     * 
     * @param \CoreBundle\Entity\Warehouse
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Gets the warehouse entity
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return User
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }
    
    /**
     * {@inheritDoc}
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * {@inheritDoc}
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->emailAddress;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * Set clientAccount
     *
     * @param \CoreBundle\Entity\Client $clientAccount
     *
     * @return User
     */
    public function setClientAccount(\CoreBundle\Entity\Client $clientAccount = null)
    {
        $this->clientAccount = $clientAccount;

        return $this;
    }

    /**
     * Get clientAccount
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClientAccount()
    {
        return $this->clientAccount;
    }

    public function getClient()
    {
        return $this->getClientAccount();
    }
}
