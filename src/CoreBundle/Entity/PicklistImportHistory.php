<?php

namespace CoreBundle\Entity;

/**
 * PicklistImportHistory
 */
class PicklistImportHistory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateUploaded;

    /**
     * @var array
     */
    private $data;

    /**
     * @var \CoreBundle\Entity\User
     */
    private $uploadedBy;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateUploaded
     *
     * @param \DateTime $dateUploaded
     *
     * @return PicklistImportHistory
     */
    public function setDateUploaded($dateUploaded)
    {
        $this->dateUploaded = $dateUploaded;

        return $this;
    }

    /**
     * Get dateUploaded
     *
     * @return \DateTime
     */
    public function getDateUploaded()
    {
        return $this->dateUploaded;
    }

    /**
     * Set data
     *
     * @param array $data
     *
     * @return PicklistImportHistory
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set uploadedBy
     *
     * @param \CoreBundle\Entity\User $uploadedBy
     *
     * @return PicklistImportHistory
     */
    public function setUploadedBy(\CoreBundle\Entity\User $uploadedBy = null)
    {
        $this->uploadedBy = $uploadedBy;

        return $this;
    }

    /**
     * Get uploadedBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getUploadedBy()
    {
        return $this->uploadedBy;
    }
}
