<?php

namespace CoreBundle\Entity;

use CoreBundle\Entity\Interfaces\ClientAware;
use CoreBundle\Entity\Interfaces\WarehouseAware;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PurchaseOrder
 * @UniqueEntity(
 *     fields={"purchaseOrderNumber"}, 
 *     message="You've managed to win the lottery in probability. No worries, just resubmit the details!"
 * )
 */
class PurchaseOrder implements WarehouseAware, ClientAware
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var guid
     */
    private $purchaseOrderNumber;

    /**
     * @var string
     */
    private $remarks;

    /**
     * @var \CoreBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @Assert\Valid
     * @Assert\Count(
     *     min = "1",
     *     minMessage = "Purchase Order should have atleast 1 Product"
     * )
     */
    private $products;

    /**
     * @var \DateTime
     */
    private $dateCreated;
    
    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \CoreBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var \CoreBundle\Entity\PurchaseOrderPaymentType
     */
    private $paymentType;
    
    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set purchaseOrderNumber
     *
     * @param guid $purchaseOrderNumber
     *
     * @return PurchaseOrder
     */
    public function setPurchaseOrderNumber($purchaseOrderNumber)
    {
        $this->purchaseOrderNumber = $purchaseOrderNumber;

        return $this;
    }

    /**
     * Get purchaseOrderNumber
     *
     * @return guid
     */
    public function getPurchaseOrderNumber()
    {
        return $this->purchaseOrderNumber;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return PurchaseOrder
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set supplier
     *
     * @param \CoreBundle\Entity\Supplier $supplier
     *
     * @return PurchaseOrder
     */
    public function setSupplier(\CoreBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \CoreBundle\Entity\Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Add product
     *
     * @param \CoreBundle\Entity\PurchaseOrderProduct $product
     *
     * @return PurchaseOrder
     */
    public function addProduct(\CoreBundle\Entity\PurchaseOrderProduct $product)
    {
        $product->setPurchaseOrder($this);
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \CoreBundle\Entity\PurchaseOrderProduct $product
     */
    public function removeProduct(\CoreBundle\Entity\PurchaseOrderProduct $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return PurchaseOrder
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return PurchaseOrder
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set createdBy
     *
     * @param \CoreBundle\Entity\User $createdBy
     *
     * @return PurchaseOrder
     */
    public function setCreatedBy(\CoreBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
    
    public function created()
    {
        if ($c = $this->createdBy) {
            return $c->getFullName();
        }
        return null;
    }


    /**
     * Set paymentType
     *
     * @param \CoreBundle\Entity\PurchaseOrderPaymentType $paymentType
     *
     * @return PurchaseOrder
     */
    public function setPaymentType(\CoreBundle\Entity\PurchaseOrderPaymentType $paymentType = null)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return \CoreBundle\Entity\PurchaseOrderPaymentType
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return PurchaseOrder
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var string
     */
    private $file;

    /**
     * @var string
     */
    private $referenceNo;


    /**
     * Set file
     *
     * @param string $file
     *
     * @return PurchaseOrder
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set referenceNo
     *
     * @param string $referenceNo
     *
     * @return PurchaseOrder
     */
    public function setReferenceNo($referenceNo)
    {
        $this->referenceNo = $referenceNo;

        return $this;
    }

    /**
     * Get referenceNo
     *
     * @return string
     */
    public function getReferenceNo()
    {
        return $this->referenceNo;
    }
}
