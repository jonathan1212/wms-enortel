<?php

namespace CoreBundle\Entity;

use CoreBundle\Entity\Interfaces\ClientAware;
use CoreBundle\Entity\Interfaces\WarehouseAware;

/**
 * PicklistParticular
 */
class PicklistParticular implements WarehouseAware, ClientAware
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var guid
     */
    private $picklistParticularNumber;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $items;

    /**
     * @var \CoreBundle\Entity\PicklistStatus
     */
    private $picklistStatus;

    /**
     * @var \CoreBundle\Entity\Picklist
     */
    private $picklistAggregate;

    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;
    
    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set picklistParticularNumber
     *
     * @param guid $picklistParticularNumber
     *
     * @return PicklistParticular
     */
    public function setPicklistParticularNumber($picklistParticularNumber)
    {
        $this->picklistParticularNumber = $picklistParticularNumber;

        return $this;
    }

    /**
     * Get picklistParticularNumber
     *
     * @return guid
     */
    public function getPicklistParticularNumber()
    {
        return $this->picklistParticularNumber;
    }

    /**
     * Add item
     *
     * @param \CoreBundle\Entity\PicklistDetail $item
     *
     * @return PicklistParticular
     */
    public function addItem(\CoreBundle\Entity\PicklistDetail $item)
    {
        $item->setPicklistParticular($this);

        $this->items[] = $item;

        return $this;
    }

    /**
     * Remove item
     *
     * @param \CoreBundle\Entity\PicklistDetail $item
     */
    public function removeItem(\CoreBundle\Entity\PicklistDetail $item)
    {
        $this->items->removeElement($item);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Set picklistStatus
     *
     * @param \CoreBundle\Entity\PicklistStatus $picklistStatus
     *
     * @return PicklistParticular
     */
    public function setPicklistStatus(\CoreBundle\Entity\PicklistStatus $picklistStatus = null)
    {
        $this->picklistStatus = $picklistStatus;

        return $this;
    }

    /**
     * Get picklistStatus
     *
     * @return \CoreBundle\Entity\PicklistStatus
     */
    public function getPicklistStatus()
    {
        return $this->picklistStatus;
    }

    /**
     * Set picklistAggregate
     *
     * @param \CoreBundle\Entity\Picklist $picklistAggregate
     *
     * @return PicklistParticular
     */
    public function setPicklistAggregate(\CoreBundle\Entity\Picklist $picklistAggregate = null)
    {
        $this->picklistAggregate = $picklistAggregate;

        return $this;
    }

    /**
     * Get picklistAggregate
     *
     * @return \CoreBundle\Entity\Picklist
     */
    public function getPicklistAggregate()
    {
        return $this->picklistAggregate;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return PicklistParticular
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return PicklistParticular
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var string
     */
    private $file;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $picklistDelivered;


    /**
     * Set file
     *
     * @param string $file
     *
     * @return PicklistParticular
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Add picklistDelivered
     *
     * @param \CoreBundle\Entity\PicklistDelivered $picklistDelivered
     *
     * @return PicklistParticular
     */
    public function addPicklistDelivered(\CoreBundle\Entity\PicklistDelivered $picklistDelivered)
    {
        $this->picklistDelivered[] = $picklistDelivered;

        return $this;
    }

    /**
     * Remove picklistDelivered
     *
     * @param \CoreBundle\Entity\PicklistDelivered $picklistDelivered
     */
    public function removePicklistDelivered(\CoreBundle\Entity\PicklistDelivered $picklistDelivered)
    {
        $this->picklistDelivered->removeElement($picklistDelivered);
    }

    /**
     * Get picklistDelivered
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPicklistDelivered()
    {
        return $this->picklistDelivered;
    }
}
