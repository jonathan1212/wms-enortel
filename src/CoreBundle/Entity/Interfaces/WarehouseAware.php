<?php

namespace CoreBundle\Entity\Interfaces;

/**
 * Interface for filtering entities that belongs to a certain warehouse
 */
interface WarehouseAware
{
    /**
     * Returns to which warehouse this entity belongs
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse();
}
