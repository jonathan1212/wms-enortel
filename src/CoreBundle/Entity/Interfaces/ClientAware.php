<?php

namespace CoreBundle\Entity\Interfaces;

/**
 * Interface for filtering entities that belongs to a certain client
 */
interface ClientAware
{
    /**
     * Returns to which client this entity belongs
     * @return \CoreBundle\Entity\Client
     */
    public function getClient();
}
