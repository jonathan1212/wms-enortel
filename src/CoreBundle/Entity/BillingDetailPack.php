<?php

namespace CoreBundle\Entity;

/**
 * BillingDetailPack
 */
class BillingDetailPack
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateProcess;

    /**
     * @var \CoreBundle\Entity\Inventory
     */
    private $inventory;

    /**
     * @var \CoreBundle\Entity\Billing
     */
    private $billing;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateProcess
     *
     * @param \DateTime $dateProcess
     *
     * @return BillingDetailPack
     */
    public function setDateProcess($dateProcess)
    {
        $this->dateProcess = $dateProcess;

        return $this;
    }

    /**
     * Get dateProcess
     *
     * @return \DateTime
     */
    public function getDateProcess()
    {
        return $this->dateProcess;
    }

    /**
     * Set inventory
     *
     * @param \CoreBundle\Entity\Inventory $inventory
     *
     * @return BillingDetailPack
     */
    public function setInventory(\CoreBundle\Entity\Inventory $inventory = null)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory
     *
     * @return \CoreBundle\Entity\Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set billing
     *
     * @param \CoreBundle\Entity\Billing $billing
     *
     * @return BillingDetailPack
     */
    public function setBilling(\CoreBundle\Entity\Billing $billing = null)
    {
        $this->billing = $billing;

        return $this;
    }

    /**
     * Get billing
     *
     * @return \CoreBundle\Entity\Billing
     */
    public function getBilling()
    {
        return $this->billing;
    }
}
