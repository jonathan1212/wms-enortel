<?php

namespace CoreBundle\Entity;

/**
 * InventoryStatus
 */
class InventoryStatus
{
    /**
     * constant values for statuses
     */
    const INBOUNDED     = 10;
    const ALLOCATED     = 20;
    const PICKLISTED    = 30;
    const QUARANTINED   = 40;
    const PICKED        = 50;
    const PACKED        = 60;
    const DISPATCHED    = 70; // for releasing new status
    const LOST          = 80;
    const RELEASED      = 110; // meaning dispatch


    private static $inventoryHasBeenPicked = [
        self::PICKED,
        self::PACKED,
        self::DISPATCHED
    ];

    private static $inventoryHasBeenPacked = [
        self::PACKED,
        self::DISPATCHED
    ];

    private static $inventoryCanBePacked = [
        self::PICKED,
        self::PACKED
    ];

    private static $inventoryCanBePicked = [
        self::PICKED,
        self::PICKLISTED,
        self::LOST
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $value;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InventoryStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return InventoryStatus
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Returns statuses that will consider the inventory has been picked already
     * @return array
     */
    public static function getInventoryHasBeenPicked()
    {
        return self::$inventoryHasBeenPicked;
    }

    /**
     * Returns statuses that will consider the inventory has been packed already
     * @return array
     */
    public static function getInventoryHasBeenPacked()
    {
        return self::$inventoryHasBeenPacked;
    }

    /**
     * Returns statuses that will consider if the inventory can be picked
     * @return array
     */
    public static function getInventoryCanBePicked()
    {
        return self::$inventoryCanBePicked;
    }

    /**
     * Returns statuses that will consider if the inventory can be packed
     * @return array
     */
    public static function getInventoryCanBePacked()
    {
        return self::$inventoryCanBePacked;
    }
}
