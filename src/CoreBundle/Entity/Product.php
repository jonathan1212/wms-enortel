<?php

namespace CoreBundle\Entity;

use CoreBundle\Entity\Interfaces\ClientAware;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use CoreBundle\Entity\Interfaces\WarehouseAware;

/**
 * Product
 * @UniqueEntity(
 *     fields={"sku"}, 
 *     message="The 1 in 2176782336 probability just happened. No worries, just resubmit the details!"
 * )
 * @UniqueEntity(
 *     fields={"client", "clientSku" , "supplier", "warehouse"}, 
 *     message="Duplicate Client, Client SKU and Supplier combination found, please check your inputs."
 * )
 */
class Product implements ClientAware, WarehouseAware
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $productDescription;

    /**
     * @var guid
     */
    private $sku;

    /**
     * @var string
     * @Assert\Length(
     *      max = 50,
     *      maxMessage = "Supplier SKU cannot be longer than {{ limit }} characters"
     * )
     */
    private $supplierSku = null;

    /**
     * @var string
     * @Assert\Length(
     *      max = 50,
     *      maxMessage = "Client SKU cannot be longer than {{ limit }} characters"
     * )
     */
    private $clientSku = null;

    /**
     * @var \DateTime
     */
    private $deletedAt;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="{{ value }} is not a valid number."
     * )
     */
    private $costOfGoodsPurchased;

    /**
     * @var \CoreBundle\Entity\Supplier
     */
    private $supplier;

    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productDescription
     *
     * @param string $productDescription
     *
     * @return Product
     */
    public function setProductDescription($productDescription)
    {
        $this->productDescription = $productDescription;

        return $this;
    }

    /**
     * Get productDescription
     *
     * @return string
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * Set sku
     *
     * @param guid $sku
     *
     * @return Product
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return guid
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set supplierSku
     *
     * @param string $supplierSku
     *
     * @return Product
     */
    public function setSupplierSku($supplierSku)
    {
        $this->supplierSku = $supplierSku;

        return $this;
    }

    /**
     * Get supplierSku
     *
     * @return string
     */
    public function getSupplierSku()
    {
        return $this->supplierSku;
    }

    /**
     * Set clientSku
     *
     * @param guid $clientSku
     *
     * @return Product
     */
    public function setClientSku($clientSku)
    {
        $this->clientSku = $clientSku;

        return $this;
    }

    /**
     * Get clientSku
     *
     * @return guid
     */
    public function getClientSku()
    {
        return $this->clientSku;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return Product
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set costOfGoodsPurchased
     *
     * @param string $costOfGoodsPurchased
     *
     * @return Product
     */
    public function setCostOfGoodsPurchased($costOfGoodsPurchased)
    {
        $this->costOfGoodsPurchased = $costOfGoodsPurchased;

        return $this;
    }

    /**
     * Get costOfGoodsPurchased
     *
     * @return string
     */
    public function getCostOfGoodsPurchased()
    {
        return $this->costOfGoodsPurchased;
    }

    /**
     * Set supplier
     *
     * @param \CoreBundle\Entity\Supplier $supplier
     *
     * @return Product
     */
    public function setSupplier(\CoreBundle\Entity\Supplier $supplier = null)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return \CoreBundle\Entity\Supplier
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return Product
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;


    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return Product
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    /**
     * @var string
     */
    private $length = 0;

    /**
     * @var string
     */
    private $width = 0;

    /**
     * @var string
     */
    private $height = 0;

    /**
     * @var string
     */
    private $weigth = 0;


    /**
     * Set length
     *
     * @param string $length
     *
     * @return Product
     */
    public function setLength($length)
    {
        $this->length = $length;

        return $this;
    }

    /**
     * Get length
     *
     * @return string
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Set width
     *
     * @param string $width
     *
     * @return Product
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param string $height
     *
     * @return Product
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return string
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set weigth
     *
     * @param string $weigth
     *
     * @return Product
     */
    public function setWeigth($weigth)
    {
        $this->weigth = $weigth;

        return $this;
    }

    /**
     * Get weigth
     *
     * @return string
     */
    public function getWeigth()
    {
        return $this->weigth;
    }
}
