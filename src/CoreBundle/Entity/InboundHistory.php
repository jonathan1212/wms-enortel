<?php

namespace CoreBundle\Entity;

/**
 * InboundHistory
 */
class InboundHistory
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var array
     */
    private $details;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \CoreBundle\Entity\PurchaseOrder
     */
    private $purchaseOrder;

    /**
     * @var \CoreBundle\Entity\User
     */
    private $createdBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set details
     *
     * @param array $details
     *
     * @return InboundHistory
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return array
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set purchaseOrder
     *
     * @param \CoreBundle\Entity\PurchaseOrder $purchaseOrder
     *
     * @return InboundHistory
     */
    public function setPurchaseOrder(\CoreBundle\Entity\PurchaseOrder $purchaseOrder = null)
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return \CoreBundle\Entity\PurchaseOrder
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * Set createdBy
     *
     * @param \CoreBundle\Entity\User $createdBy
     *
     * @return InboundHistory
     */
    public function setCreatedBy(\CoreBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
