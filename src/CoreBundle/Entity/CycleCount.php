<?php

namespace CoreBundle\Entity;

use CoreBundle\Entity\Interfaces\WarehouseAware;
use CoreBundle\Entity\Interfaces\ClientAware;
/**
 * CycleCount
 */
class CycleCount implements WarehouseAware, ClientAware
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $cyclecount;

    /**
     * @var array
     */
    private $details;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var string
     */
    private $accuracy;

    /**
     * @var \CoreBundle\Entity\StorageLocation
     */
    private $client;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cyclecount
     *
     * @param string $cyclecount
     *
     * @return CycleCount
     */
    public function setCyclecount($cyclecount)
    {
        $this->cyclecount = $cyclecount;

        return $this;
    }

    /**
     * Get cyclecount
     *
     * @return string
     */
    public function getCyclecount()
    {
        return $this->cyclecount;
    }

    /**
     * Set details
     *
     * @param array $details
     *
     * @return CycleCount
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return array
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return CycleCount
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set accuracy
     *
     * @param string $accuracy
     *
     * @return CycleCount
     */
    public function setAccuracy($accuracy)
    {
        $this->accuracy = $accuracy;

        return $this;
    }

    /**
     * Get accuracy
     *
     * @return string
     */
    public function getAccuracy()
    {
        return $this->accuracy;
    }

    /**
     * @var \CoreBundle\Entity\StorageLocation
     */
    private $storageLocation;


    /**
     * Set storageLocation
     *
     * @param \CoreBundle\Entity\StorageLocation $storageLocation
     *
     * @return CycleCount
     */
    public function setStorageLocation(\CoreBundle\Entity\StorageLocation $storageLocation = null)
    {
        $this->storageLocation = $storageLocation;

        return $this;
    }

    /**
     * Get storageLocation
     *
     * @return \CoreBundle\Entity\StorageLocation
     */
    public function getStorageLocation()
    {
        return $this->storageLocation;
    }
    /**
     * @var \CoreBundle\Entity\User
     */
    private $createdBy;


    /**
     * Set createdBy
     *
     * @param \CoreBundle\Entity\User $createdBy
     *
     * @return CycleCount
     */
    public function setCreatedBy(\CoreBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;


    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return CycleCount
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return CycleCount
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    public function getDiffDuration()
    {
        $diff = $this->startDate->diff($this->endDate);
        
        return " {$diff->i} min, {$diff->s} sec ";
    }

    public function checkAccuracy()
    {
        $a = explode(',', $this->accuracy);

        if ($a[1] == 0) {
            return '100% / '. $a[0];
        }

        return "{$a[0]} / $a[1] ";

    }
    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;


    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return CycleCount
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    /**
     * @var array
     */
    private $list;


    /**
     * Set list
     *
     * @param array $list
     *
     * @return CycleCount
     */
    public function setList($list)
    {
        $this->list = $list;

        return $this;
    }

    /**
     * Get list
     *
     * @return array
     */
    public function getList()
    {
        return $this->list;
    }
    /**
     * @var array
     */
    private $sku_list;


    /**
     * Set skuList
     *
     * @param array $skuList
     *
     * @return CycleCount
     */
    public function setSkuList($skuList)
    {
        $this->sku_list = $skuList;

        return $this;
    }

    /**
     * Get skuList
     *
     * @return array
     */
    public function getSkuList()
    {
        return $this->sku_list;
    }

    public function getSkuListCollection()
    {
        if ($this->getSkuList()) {
            return implode(',', json_decode($this->getSkuList()));    
        }
        
        return '';
         
    }

    public function getUpdater()
    {

    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return CycleCount
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
