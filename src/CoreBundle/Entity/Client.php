<?php

namespace CoreBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Client
 * @UniqueEntity(
 *     fields="emailAddress", 
 *     message="Email already taken", 
 *     groups={"add", "update"}
 * )
 * @UniqueEntity(
 *     fields="name", 
 *     message="Client name already taken", 
 *     groups={"add", "update"}
 * )
 * @UniqueEntity(
 *     fields="hostAlias", 
 *     message="Host alias name already taken", 
 *     groups={"add", "update"}
 * )
 */
class Client implements UserInterface
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank(groups={"add", "update"})
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Name cannot be longer than {{ limit }} characters",
     *      groups={"add", "update"}
     * )
     */
    private $name;

    /**
     * @var string
     * @Assert\NotBlank(groups={"add", "update"})
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Contact Person cannot be longer than {{ limit }} characters",
     *      groups={"add", "update"}
     * )
     */
    private $contactPerson;

    /**
     * @var string
     * @Assert\NotBlank(groups={"add", "update"})
     * @Assert\Length(
     *      max = 50,
     *      maxMessage = "Contact Number cannot be longer than {{ limit }} characters",
     *      groups={"add", "update"}
     * )
     */
    private $contactNumber;

    /**
     * @var string
     * @Assert\Email(groups={"add", "update"})
     */
    private $emailAddress;

    /**
     * @var string
     * @Assert\NotBlank(groups={"add"})
     * @Assert\Length(
     *     min=8,
     *     max=4096,
     *     minMessage = "For security purposes, password should be atleast {{ limit }} characters long",
     *     groups={"add", "change_password"}
     * )
     */
    private $plainPassword;

    /**
     * @var string
     */
    private $password;

    /**
     * @var array
     */
    private $roles;

    /**
     * @var string
     * @Assert\Regex(
     *     pattern     = "/^[a-z]+$/i",
     *     htmlPattern = "^[a-zA-Z]+$",
     *     message = "Host alias should only be composed of letters",
     *     groups={"add", "update"}
     * )
     * @Assert\Length(
     *     max=20,
     *     maxMessage = "Host alias cannot be more than {{ limit }} characters long",
     *     groups={"add", "update"}
     * )
     */
    private $hostAlias;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contactPerson
     *
     * @param string $contactPerson
     *
     * @return Client
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * Set contactNumber
     *
     * @param string $contactNumber
     *
     * @return Client
     */
    public function setContactNumber($contactNumber)
    {
        $this->contactNumber = $contactNumber;

        return $this;
    }

    /**
     * Get contactNumber
     *
     * @return string
     */
    public function getContactNumber()
    {
        return $this->contactNumber;
    }

    /**
     * Set emailAddress
     *
     * @param string $emailAddress
     *
     * @return Client
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;

        return $this;
    }

    /**
     * Get emailAddress
     *
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * {@inheritDoc}
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * {@inheritDoc}
     */
    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * {@inheritDoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getUsername()
    {
        return $this->emailAddress;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }

    /**
     * Set hostAlias
     *
     * @param string $hostAlias
     *
     * @return Client
     */
    public function setHostAlias($hostAlias)
    {
        $this->hostAlias = $hostAlias;

        return $this;
    }

    /**
     * Get hostAlias
     *
     * @return string
     */
    public function getHostAlias()
    {
        return $this->hostAlias;
    }

    public function getClient()
    {
        return $this;
    }
}
