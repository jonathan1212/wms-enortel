<?php

namespace CoreBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * PurchaseOrderProduct
 */
class PurchaseOrderProduct
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     * @Assert\GreaterThan(0)
     */
    private $quantity;

    /**
     * @var \CoreBundle\Entity\PurchaseOrder
     */
    private $purchaseOrder;

    /**
     * @var \CoreBundle\Entity\Product
     */
    private $product;

    /**
     * @var string
     */
    private $costOfGoodsPurchased;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return PurchaseOrderProduct
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set purchaseOrder
     *
     * @param \CoreBundle\Entity\PurchaseOrder $purchaseOrder
     *
     * @return PurchaseOrderProduct
     */
    public function setPurchaseOrder(\CoreBundle\Entity\PurchaseOrder $purchaseOrder = null)
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return \CoreBundle\Entity\PurchaseOrder
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * Set product
     *
     * @param \CoreBundle\Entity\Product $product
     *
     * @return PurchaseOrderProduct
     */
    public function setProduct(\CoreBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \CoreBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set costOfGoodsPurchased
     *
     * @param string $costOfGoodsPurchased
     *
     * @return PurchaseOrderProduct
     */
    public function setCostOfGoodsPurchased($costOfGoodsPurchased)
    {
        $this->costOfGoodsPurchased = $costOfGoodsPurchased;

        return $this;
    }

    /**
     * Get costOfGoodsPurchased
     *
     * @return string
     */
    public function getCostOfGoodsPurchased()
    {
        return $this->costOfGoodsPurchased;
    }
}
