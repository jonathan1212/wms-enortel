<?php

namespace CoreBundle\Entity;

use Doctrine\ORM\Event\LifecycleEventArgs;
use CoreBundle\Entity\Interfaces\ClientAware;
use CoreBundle\Entity\Interfaces\WarehouseAware;

/**
 * Inventory
 */
class Inventory implements WarehouseAware, ClientAware
{
    const INBOUND_STATUS = 1;
    const DISPATCHED_STATUS = 7;
    const QUARANTINE_STATUS = 4;
    /**
     * @var int
     */
    private $id;

    /**
     * @var guid
     */
    private $tag;

    /**
     * @var string
     */
    private $costOfGoodsPurchased;

    /**
     * @var \CoreBundle\Entity\Product
     */
    private $product;

    /**
     * @var \CoreBundle\Entity\PurchaseOrder
     */
    private $purchaseOrder;

    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \CoreBundle\Entity\StorageLocation
     */
    private $storageLocation;

    /**
     * @var \CoreBundle\Entity\MobileLocation
     */
    private $mobileLocation = null;

    /**
     * @var \CoreBundle\Entity\InventoryStatus
     */
    private $inventoryStatus;

    /**
     * @var \DateTime
     */
    private $dateInbounded;

    /**
     * @var string
     */
    private $orderNumber = null;

    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;

    private $picklistDetail;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param guid $tag
     *
     * @return Inventory
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return guid
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set costOfGoodsPurchased
     *
     * @param string $costOfGoodsPurchased
     *
     * @return Inventory
     */
    public function setCostOfGoodsPurchased($costOfGoodsPurchased)
    {
        $this->costOfGoodsPurchased = $costOfGoodsPurchased;

        return $this;
    }

    /**
     * Get costOfGoodsPurchased
     *
     * @return string
     */
    public function getCostOfGoodsPurchased()
    {
        return $this->costOfGoodsPurchased;
    }

    /**
     * Set product
     *
     * @param \CoreBundle\Entity\Product $product
     *
     * @return Inventory
     */
    public function setProduct(\CoreBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \CoreBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set purchaseOrder
     *
     * @param \CoreBundle\Entity\PurchaseOrder $purchaseOrder
     *
     * @return Inventory
     */
    public function setPurchaseOrder(\CoreBundle\Entity\PurchaseOrder $purchaseOrder = null)
    {
        $this->purchaseOrder = $purchaseOrder;

        return $this;
    }

    /**
     * Get purchaseOrder
     *
     * @return \CoreBundle\Entity\PurchaseOrder
     */
    public function getPurchaseOrder()
    {
        return $this->purchaseOrder;
    }

    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return Inventory
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set storageLocation
     *
     * @param \CoreBundle\Entity\StorageLocation $storageLocation
     *
     * @return Inventory
     */
    public function setStorageLocation(\CoreBundle\Entity\StorageLocation $storageLocation = null)
    {
        $this->storageLocation = $storageLocation;

        return $this;
    }

    /**
     * Get storageLocation
     *
     * @return \CoreBundle\Entity\StorageLocation
     */
    public function getStorageLocation()
    {
        return $this->storageLocation;
    }

    /**
     * Set mobileLocation
     *
     * @param \CoreBundle\Entity\MobileLocation $mobileLocation
     *
     * @return Inventory
     */
    public function setMobileLocation(\CoreBundle\Entity\MobileLocation $mobileLocation = null)
    {
        $this->mobileLocation = $mobileLocation;

        return $this;
    }

    /**
     * Get mobileLocation
     *
     * @return \CoreBundle\Entity\MobileLocation
     */
    public function getMobileLocation()
    {
        return $this->mobileLocation;
    }

    /**
     * Set inventoryStatus
     *
     * @param \CoreBundle\Entity\InventoryStatus $inventoryStatus
     *
     * @return Inventory
     */
    public function setInventoryStatus(\CoreBundle\Entity\InventoryStatus $inventoryStatus = null)
    {
        $this->inventoryStatus = $inventoryStatus;

        return $this;
    }

    /**
     * Get inventoryStatus
     *
     * @return \CoreBundle\Entity\InventoryStatus
     */
    public function getInventoryStatus()
    {
        return $this->inventoryStatus;
    }

    /**
     * Set dateInbounded
     *
     * @param \DateTime $dateInbounded
     *
     * @return Inventory
     */
    public function setDateInbounded($dateInbounded)
    {
        $this->dateInbounded = $dateInbounded;

        return $this;
    }

    /**
     * Get dateInbounded
     *
     * @return \DateTime
     */
    public function getDateInbounded()
    {
        return $this->dateInbounded;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     *
     * @return Inventory
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Sets initial inventory status on persist
     * @param LifecycleEventArgs $event
     */
    public function setInventoryStatusAsInbounded(LifecycleEventArgs $event)
    {
        $em = $event->getEntityManager();
        $inventoryStatusRepository = $em->getRepository('CoreBundle:InventoryStatus');
        $this->inventoryStatus = $inventoryStatusRepository
            ->findOneByValue(\CoreBundle\Entity\InventoryStatus::INBOUNDED);
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return Inventory
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
    /**
     * @var string
     */
    private $serialNo;


    /**
     * Set serialNo
     *
     * @param string $serialNo
     *
     * @return Inventory
     */
    public function setSerialNo($serialNo)
    {
        $this->serialNo = $serialNo;

        return $this;
    }

    /**
     * Get serialNo
     *
     * @return string
     */
    public function getSerialNo()
    {
        return $this->serialNo;
    }
    /**
     * @var \DateTime
     */
    private $datePicked;

    /**
     * @var \DateTime
     */
    private $datePacked;


    /**
     * Set datePicked
     *
     * @param \DateTime $datePicked
     *
     * @return Inventory
     */
    public function setDatePicked($datePicked)
    {
        $this->datePicked = $datePicked;

        return $this;
    }

    /**
     * Get datePicked
     *
     * @return \DateTime
     */
    public function getDatePicked()
    {
        return $this->datePicked;
    }

    /**
     * Set datePacked
     *
     * @param \DateTime $datePacked
     *
     * @return Inventory
     */
    public function setDatePacked($datePacked)
    {
        $this->datePacked = $datePacked;

        return $this;
    }

    /**
     * Get datePacked
     *
     * @return \DateTime
     */
    public function getDatePacked()
    {
        return $this->datePacked;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->picklistDetail = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add picklistDetail
     *
     * @param \CoreBundle\Entity\PicklistDetail $picklistDetail
     *
     * @return Inventory
     */
    public function addPicklistDetail(\CoreBundle\Entity\PicklistDetail $picklistDetail)
    {
        $this->picklistDetail[] = $picklistDetail;

        return $this;
    }

    /**
     * Remove picklistDetail
     *
     * @param \CoreBundle\Entity\PicklistDetail $picklistDetail
     */
    public function removePicklistDetail(\CoreBundle\Entity\PicklistDetail $picklistDetail)
    {
        $this->picklistDetail->removeElement($picklistDetail);
    }

    /**
     * Get picklistDetail
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPicklistDetail()
    {
        return $this->picklistDetail;
    }
}
