<?php

namespace CoreBundle\Entity;

/**
 * PicklistStatus
 */
class PicklistStatus
{
    /**
     * constant values for statuses
     */
    const CREATED               = 10;
    const FOR_PICKING           = 20;
    const FOR_PACKING           = 30;
    const DISPATCHED            = 40; //new name is for release
    const PARTIALLY_DISPATCHED  = 50;
    const INTRANSIT             = 60;
    const DELIVERED             = 70;

    private static $allStatus = [
        self::CREATED               => 'Created',
        self::FOR_PICKING           => 'For Picking',
        self::FOR_PACKING           => 'For Packing',
        self::DISPATCHED            => 'Dispatched',
        self::PARTIALLY_DISPATCHED  => 'Partially Dispatched', 
        self::INTRANSIT             => 'Intransit',
        self::DELIVERED             => 'Delivered'
    ];

    private static $picklistCanBePicked = [
        self::FOR_PICKING,
        self::FOR_PACKING,
        self::PARTIALLY_DISPATCHED
    ];

    private static $picklistHasAlreadyBeenPicked = [
        self::FOR_PACKING,
        self::PARTIALLY_DISPATCHED,
        self::DISPATCHED
    ];

    private static $picklistCanBePacked = [
        self::FOR_PACKING,
        self::PARTIALLY_DISPATCHED
    ];

    private static $picklistCannotBePacked = [
        self::CREATED,
        self::FOR_PICKING
    ];

    private static $picklistCanBeReprinted = [
        self::PARTIALLY_DISPATCHED,
        self::DISPATCHED,
        self::FOR_PACKING,
    ];

    private static $picklistForReprint = [
        self::FOR_PICKING,
        self::FOR_PACKING,
        self::PARTIALLY_DISPATCHED,
        self::DISPATCHED
    ];

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $value;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PicklistStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param integer $value
     *
     * @return PicklistStatus
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Get statuses that will consider picklist can be picked
     * @return array
     */
    public static function getPicklistCanBePicked()
    {
        return self::$picklistCanBePicked;
    }

    /**
     * Get statuses that will consider picklist can be packed
     * @return array
     */
    public static function getPicklistCanBePacked()
    {
        return self::$picklistCanBePacked;
    }

    /**
     * Get statuses that will determine if picklist has already been picked
     * @return array
     */
    public static function getPicklistHasAlreadyBeenPicked()
    {
        return self::$picklistHasAlreadyBeenPicked;
    }

    /**
     * Get statuses that will determine if picklist can be reprinted
     * @return array
     */
    public static function getPicklistCanBeReprinted()
    {
        return self::$picklistCanBeReprinted;
    }

    /**
     * Get statuses that will determine if picklist cannot be packed
     * @return array
     */
    public static function getPicklistCannotBePacked()
    {
        return self::$picklistCannotBePacked;
    }

    /**
     * Get statuses that will determine if picklist is for reprinting
     * @return array
     */
    public static function getPicklistForReprint()
    {
        return self::$picklistForReprint;
    }


    /**
     * Get all statuses
     * @return array
     */
    public static function getAlStatus()
    {
        return self::$allStatus;
    }
}
