<?php

namespace CoreBundle\Entity;

/**
 * BillingKittingDetail
 */
class BillingKittingDetail
{
    
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \CoreBundle\Entity\Picklist
     */
    private $picklist;

    /**
     * @var \CoreBundle\Entity\BillingKitting
     */
    private $billingKitting;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set picklist
     *
     * @param \CoreBundle\Entity\Picklist $picklist
     *
     * @return BillingKittingDetail
     */
    public function setPicklist(\CoreBundle\Entity\Picklist $picklist = null)
    {
        $this->picklist = $picklist;

        return $this;
    }

    /**
     * Get picklist
     *
     * @return \CoreBundle\Entity\Picklist
     */
    public function getPicklist()
    {
        return $this->picklist;
    }

    /**
     * Set billingKitting
     *
     * @param \CoreBundle\Entity\BillingKitting $billingKitting
     *
     * @return BillingKittingDetail
     */
    public function setBillingKitting(\CoreBundle\Entity\BillingKitting $billingKitting = null)
    {
        $this->billingKitting = $billingKitting;

        return $this;
    }

    /**
     * Get billingKitting
     *
     * @return \CoreBundle\Entity\BillingKitting
     */
    public function getBillingKitting()
    {
        return $this->billingKitting;
    }
}
