<?php

namespace CoreBundle\Entity;

use CoreBundle\Entity\Interfaces\ClientAware;
use CoreBundle\Entity\Interfaces\WarehouseAware;

/**
 * Picklist
 */
class Picklist implements WarehouseAware, ClientAware
{
    /**
     * Specifies how much product to squeeze in an a5 paper
     * @var  int
     */
    const PRINT_AGGREGATE_COUNT = 5;

    /**
     * @var int
     */
    private $id;

    /**
     * @var guid
     */
    private $picklistNumber;

    /**
     * @var string
     */
    private $orderNumber;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $inventoryListing;

    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;

    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \CoreBundle\Entity\User
     */
    private $createdBy;

    /**
     * @var boolean
     */
    private $isComplete = false;

    /**
     * @var array
     */
    private $orderDetails;
    
    /**
     * @var boolean
     */
    private $isPrinted = false;
        
    /**
     * @var \CoreBundle\Entity\PicklistStatus
     */
    private $picklistStatus;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $particulars;

    /**
     * @var guid
     */
    private $invoiceNumber = null;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->inventoryListing = new \Doctrine\Common\Collections\ArrayCollection();
        $this->particulars = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set picklistNumber
     *
     * @param guid $picklistNumber
     *
     * @return Picklist
     */
    public function setPicklistNumber($picklistNumber)
    {
        $this->picklistNumber = $picklistNumber;

        return $this;
    }

    /**
     * Get picklistNumber
     *
     * @return guid
     */
    public function getPicklistNumber()
    {
        return $this->picklistNumber;
    }

    /**
     * Set orderNumber
     *
     * @param string $orderNumber
     *
     * @return Picklist
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;

        return $this;
    }

    /**
     * Get orderNumber
     *
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return Picklist
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Add inventoryListing
     *
     * @param \CoreBundle\Entity\PicklistDetail $inventoryListing
     *
     * @return Picklist
     */
    public function addInventoryListing(\CoreBundle\Entity\PicklistDetail $inventoryListing)
    {
        // Set Reference
        $inventoryListing->setPicklist($this);

        // Set Picklist Order Number to Inventory
        $inventoryListing->getInventory()->setOrderNumber($this->orderNumber);

        $this->inventoryListing[] = $inventoryListing;

        return $this;
    }

    /**
     * Remove inventoryListing
     *
     * @param \CoreBundle\Entity\PicklistDetail $inventoryListing
     */
    public function removeInventoryListing(\CoreBundle\Entity\PicklistDetail $inventoryListing)
    {
        $this->inventoryListing->removeElement($inventoryListing);
    }

    /**
     * Get inventoryListing
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInventoryListing()
    {
        return $this->inventoryListing;
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return Picklist
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return Picklist
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set createdBy
     *
     * @param \CoreBundle\Entity\User $createdBy
     *
     * @return Picklist
     */
    public function setCreatedBy(\CoreBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set isComplete
     *
     * @param boolean $isComplete
     *
     * @return Picklist
     */
    public function setIsComplete($isComplete)
    {
        $this->isComplete = $isComplete;

        return $this;
    }

    /**
     * Get isComplete
     *
     * @return boolean
     */
    public function getIsComplete()
    {
        return $this->isComplete;
    }

    /**
     * Set orderDetails
     *
     * @param array $orderDetails
     *
     * @return Picklist
     */
    public function setOrderDetails($orderDetails)
    {
        $this->orderDetails = $orderDetails;

        return $this;
    }

    /**
     * Get orderDetails
     *
     * @return array
     */
    public function getOrderDetails()
    {
        return $this->orderDetails;
    }

    /**
     * Set isPrinted
     *
     * @param boolean $isPrinted
     *
     * @return Picklist
     */
    public function setIsPrinted($isPrinted)
    {
        $this->isPrinted = $isPrinted;

        return $this;
    }

    /**
     * Get isPrinted
     *
     * @return boolean
     */
    public function getIsPrinted()
    {
        return $this->isPrinted;
    }

    /**
     * Set picklistStatus
     *
     * @param \CoreBundle\Entity\PicklistStatus $picklistStatus
     *
     * @return Picklist
     */
    public function setPicklistStatus(\CoreBundle\Entity\PicklistStatus $picklistStatus = null)
    {
        $this->picklistStatus = $picklistStatus;

        return $this;
    }

    /**
     * Get picklistStatus
     *
     * @return \CoreBundle\Entity\PicklistStatus
     */
    public function getPicklistStatus()
    {
        return $this->picklistStatus;
    }

    /**
     * Add particular
     *
     * @param \CoreBundle\Entity\PicklistParticular $particular
     *
     * @return Picklist
     */
    public function addParticular(\CoreBundle\Entity\PicklistParticular $particular)
    {
        $particular->setPicklistAggregate($this);

        $this->particulars[] = $particular;

        return $this;
    }

    /**
     * Remove particular
     *
     * @param \CoreBundle\Entity\PicklistParticular $particular
     */
    public function removeParticular(\CoreBundle\Entity\PicklistParticular $particular)
    {
        $this->particulars->removeElement($particular);
    }

    /**
     * Get particulars
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParticulars()
    {
        return $this->particulars;
    }

    /**
     * Set invoiceNumber
     *
     * @param guid $invoiceNumber
     *
     * @return Picklist
     */
    public function setInvoiceNumber($invoiceNumber)
    {
        $this->invoiceNumber = $invoiceNumber;

        return $this;
    }

    /**
     * Get invoiceNumber
     *
     * @return guid
     */
    public function getInvoiceNumber()
    {
        return $this->invoiceNumber;
    }
    
}
