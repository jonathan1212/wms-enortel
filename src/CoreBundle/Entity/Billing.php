<?php

namespace CoreBundle\Entity;

use CoreBundle\Entity\Interfaces\WarehouseAware;

/**
 * Billing
 */
class Billing implements WarehouseAware
{
    const BILLING_INBOUND = 1;
    const BILLING_PICKED = 2;
    const BILLING_PACKED = 3;
    const BILLING_INSURANCE = 4;
    const BILLING_STORAGE = 5;

    protected $billingTypeNames = [
        1   => 'INBOUNDED',
        2   => 'PICKED',
        3   => 'PACKED',
        4   => 'INSURANCE',
        5   => 'STORAGE'
    ];

    protected static $billingTypeField = [
        'dateInbound'   => self::BILLING_INBOUND,
        'datePick'      => self::BILLING_PICKED,
        'datePack'      => self::BILLING_PACKED,
        'insurance'     => self::BILLING_INSURANCE,
        'storage'       => self::BILLING_STORAGE

    ];

    protected static $billingTables = [
        self::BILLING_INBOUND   => 'billing_detail_inbound',
        self::BILLING_PICKED    => 'billing_detail_pick',
        self::BILLING_PACKED    => 'billing_detail_pack',
        self::BILLING_INSURANCE => 'billing_detail_insurance',
        self::BILLING_STORAGE   => 'billing_detail_storage'
    ];

    protected static $billingTableObject = [
        self::BILLING_INBOUND   => 'BillingDetailInbound',
        self::BILLING_PICKED    => 'BillingDetailPick',
        self::BILLING_PACKED    => 'BillingDetailPack',
        self::BILLING_INSURANCE => 'BillingDetailInsurance',
        self::BILLING_STORAGE   => 'BillingDetailStorage'
    ];

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateAdded;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isPaid;

    /**
     * @var string
     */
    private $billingNumber;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Billing
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return Billing
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isPaid
     *
     * @param boolean $isPaid
     *
     * @return Billing
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid
     *
     * @return boolean
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set billingNumber
     *
     * @param string $billingNumber
     *
     * @return Billing
     */
    public function setBillingNumber($billingNumber)
    {
        $this->billingNumber = $billingNumber;

        return $this;
    }

    /**
     * Get billingNumber
     *
     * @return string
     */
    public function getBillingNumber()
    {
        return $this->billingNumber;
    }
    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;


    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return Billing
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }
    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;


    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return Billing
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    //public function getBillingType()
    /**
     * @var integer
     */
    private $type;


    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Billing
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType($isString = false)
    {
        if ($isString) {
            return isset($this->billingTypeNames[$this->type]) ? $this->billingTypeNames[$this->type] : 'NONE';
        }

        return $this->type;
    }

    public static function getTypeByField($column)
    {
        return isset(self::$billingTypeField[$column]) ? self::$billingTypeField[$column] : 'NONE';
    }

    public static function getBillingDetailTables($type)
    {
        return self::$billingTables[$type];
    }

    public static function getBillingDetailObject($type)
    {
        return self::$billingTableObject[$type];
    }

    /**
     * @var \DateTime
     */
    private $dateTo;


    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return Billing
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }
    /**
     * @var string
     */
    private $remarks;


    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return Billing
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }
    /**
     * @var string
     */
    private $amount;


    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return Billing
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }
}
