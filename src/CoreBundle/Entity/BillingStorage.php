<?php

namespace CoreBundle\Entity;

/**
 * BillingStorage
 */
class BillingStorage
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $dateAdded;

    /**
     * @var \DateTime
     */
    private $dateTo;

    /**
     * @var string
     */
    private $modifiedBy;

    /**
     * @var boolean
     */
    private $isPaid;

    /**
     * @var string
     */
    private $billingNumber;

    /**
     * @var string
     */
    private $remarks;

    /**
     * @var string
     */
    private $storageAmount = 0;

    /**
     * @var string
     */
    private $storageInsurance = 0;

    /**
     * @var \CoreBundle\Entity\Warehouse
     */
    private $warehouse;

    /**
     * @var \CoreBundle\Entity\Client
     */
    private $client;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return BillingStorage
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateTo
     *
     * @param \DateTime $dateTo
     *
     * @return BillingStorage
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Get dateTo
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Set modifiedBy
     *
     * @param string $modifiedBy
     *
     * @return BillingStorage
     */
    public function setModifiedBy($modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return string
     */
    public function getModifiedBy()
    {
        return $this->modifiedBy;
    }

    /**
     * Set isPaid
     *
     * @param boolean $isPaid
     *
     * @return BillingStorage
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid
     *
     * @return boolean
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set billingNumber
     *
     * @param string $billingNumber
     *
     * @return BillingStorage
     */
    public function setBillingNumber($billingNumber)
    {
        $this->billingNumber = $billingNumber;

        return $this;
    }

    /**
     * Get billingNumber
     *
     * @return string
     */
    public function getBillingNumber()
    {
        return $this->billingNumber;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return BillingStorage
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set storageAmount
     *
     * @param string $storageAmount
     *
     * @return BillingStorage
     */
    public function setStorageAmount($storageAmount)
    {
        $this->storageAmount = $storageAmount;

        return $this;
    }

    /**
     * Get storageAmount
     *
     * @return string
     */
    public function getStorageAmount()
    {
        return $this->storageAmount;
    }

    /**
     * Set storageInsurance
     *
     * @param string $storageInsurance
     *
     * @return BillingStorage
     */
    public function setStorageInsurance($storageInsurance)
    {
        $this->storageInsurance = $storageInsurance;

        return $this;
    }

    /**
     * Get storageInsurance
     *
     * @return string
     */
    public function getStorageInsurance()
    {
        return $this->storageInsurance;
    }

    /**
     * Set warehouse
     *
     * @param \CoreBundle\Entity\Warehouse $warehouse
     *
     * @return BillingStorage
     */
    public function setWarehouse(\CoreBundle\Entity\Warehouse $warehouse = null)
    {
        $this->warehouse = $warehouse;

        return $this;
    }

    /**
     * Get warehouse
     *
     * @return \CoreBundle\Entity\Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * Set client
     *
     * @param \CoreBundle\Entity\Client $client
     *
     * @return BillingStorage
     */
    public function setClient(\CoreBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \CoreBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}

