<?php

namespace CoreBundle\Entity;

/**
 * PicklistDetail
 */
class PicklistDetail
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var \CoreBundle\Entity\Picklist
     */
    private $picklist;

    /**
     * @var \CoreBundle\Entity\Inventory
     */
    private $inventory;

    /**
     * @var \CoreBundle\Entity\PicklistParticular
     */
    private $picklistParticular;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set picklist
     *
     * @param \CoreBundle\Entity\Picklist $picklist
     *
     * @return PicklistDetail
     */
    public function setPicklist(\CoreBundle\Entity\Picklist $picklist = null)
    {
        $this->picklist = $picklist;

        return $this;
    }

    /**
     * Get picklist
     *
     * @return \CoreBundle\Entity\Picklist
     */
    public function getPicklist()
    {
        return $this->picklist;
    }

    /**
     * Set inventory
     *
     * @param \CoreBundle\Entity\Inventory $inventory
     *
     * @return PicklistDetail
     */
    public function setInventory(\CoreBundle\Entity\Inventory $inventory = null)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory
     *
     * @return \CoreBundle\Entity\Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * Set picklistParticular
     *
     * @param \CoreBundle\Entity\PicklistParticular $picklistParticular
     *
     * @return PicklistDetail
     */
    public function setPicklistParticular(\CoreBundle\Entity\PicklistParticular $picklistParticular = null)
    {
        $this->picklistParticular = $picklistParticular;

        return $this;
    }

    /**
     * Get picklistParticular
     *
     * @return \CoreBundle\Entity\PicklistParticular
     */
    public function getPicklistParticular()
    {
        return $this->picklistParticular;
    }
}
