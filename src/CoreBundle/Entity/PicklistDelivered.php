<?php

namespace CoreBundle\Entity;

/**
 * PicklistDelivered
 */
class PicklistDelivered
{
    const STATUS_INTRANSIT = 1;
    const STATUS_DELIVERED = 2;
    const STATUS_RELEASED = 3;
    
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $waybillNumber;

    /**
     * @var string
     */
    private $remarks;

    /**
     * @var \DateTime
     */
    private $dateCreated;

    /**
     * @var string
     */
    private $deliveredto;

    /**
     * @var integer
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set waybillNumber
     *
     * @param string $waybillNumber
     *
     * @return PicklistDelivered
     */
    public function setWaybillNumber($waybillNumber)
    {
        $this->waybillNumber = $waybillNumber;

        return $this;
    }

    /**
     * Get waybillNumber
     *
     * @return string
     */
    public function getWaybillNumber()
    {
        return $this->waybillNumber;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     *
     * @return PicklistDelivered
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return PicklistDelivered
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set deliveredto
     *
     * @param string $deliveredto
     *
     * @return PicklistDelivered
     */
    public function setDeliveredto($deliveredto)
    {
        $this->deliveredto = $deliveredto;

        return $this;
    }

    /**
     * Get deliveredto
     *
     * @return string
     */
    public function getDeliveredto()
    {
        return $this->deliveredto;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PicklistDelivered
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

   
    private $createdBy;


    /**
     * Set createdBy
     *
     * @param \CoreBundle\Entity\User $createdBy
     *
     * @return PicklistDelivered
     */
    public function setCreatedBy(\CoreBundle\Entity\User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \CoreBundle\Entity\User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    public function getStatusName()
    {
        if ($this->status == 1) {
            return 'INTRANSIT';
        } else if ($this->status == 2) {
            return 'DELIVERED';
        } else {
            return 'RELEASED';
        }
    }
    /**
     * @var \CoreBundle\Entity\PicklistParticular
     */
    private $picklistParticular;


    /**
     * Set picklistParticular
     *
     * @param \CoreBundle\Entity\PicklistParticular $picklistParticular
     *
     * @return PicklistDelivered
     */
    public function setPicklistParticular(\CoreBundle\Entity\PicklistParticular $picklistParticular = null)
    {
        $this->picklistParticular = $picklistParticular;

        return $this;
    }

    /**
     * Get picklistParticular
     *
     * @return \CoreBundle\Entity\PicklistParticular
     */
    public function getPicklistParticular()
    {
        return $this->picklistParticular;
    }
}
