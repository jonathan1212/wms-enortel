<?php

namespace CoreBundle\EventListener;

use CoreBundle\Repository\ClientRepository;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @link https://knpuniversity.com/screencast/question-answer-day/symfony2-dynamic-subdomains
 * @link http://stackoverflow.com/questions/26464624/redirect-with-event-listener-for-all-no-route-found-404-not-found-notfoundhtt
 */
class ClientHostListener
{
    private $clientRepository;
    private $clientHost;

    public function __construct(ClientRepository $clientRepository, $clientHost)
    {
        $this->clientRepository = $clientRepository;
        $this->clientHost = $clientHost;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if(!$event->isMasterRequest()){
            return;
        }

        $request = $event->getRequest();
        $currentHost = $request->getHttpHost();
        if(strpos($currentHost, $this->clientHost) === false){
            return;
        }

        // Workaround for the localhost environment
        $port = $request->getPort();
        if($port !== 80 && $port !== 443){
            $this->clientHost .= ':'.$port;
        }

        $hostAlias = str_replace('.'.$this->clientHost, '', $currentHost);
        $client = $this->clientRepository->findOneByHostAlias($hostAlias);
        if($client){
            // Set client in session for later use in authentication
            $request->getSession()->set('_client', $client);
            return;
        }

        throw new NotFoundHttpException('Client Domain does not exist. Please contact LMBM immediately.');
    }
}
