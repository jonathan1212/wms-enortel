<?php

namespace CoreBundle\EventListener;

use Symfony\Component\HttpKernel\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\FlattenException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * @link http://stackoverflow.com/questions/9819023/log-errors-even-more-verbosely-with-symfony2
 */
class ExceptionListener
{
    private $logger = null;

    public function __construct(LoggerInterface $logger = null)
    {
        $this->logger = $logger;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if($this->logger === null){
            return;
        }

        $exception = $event->getException();
        $flattenException = FlattenException::create($exception);
        $this->logger->error('Stack trace');
        foreach ($flattenException->getTrace() as $trace) {
            $traceMessage = sprintf('  at %s line %s', $trace['file'], $trace['line']);
            $this->logger->error($traceMessage);
        }
    }
}
