<?php

namespace CoreBundle\Filters;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @link http://www.michaelperrin.fr/2014/12/05/doctrine-filters/
 */
class WarehouseFilterConfigurator
{
    private $em;
    private $tokenStorage;

    public function __construct(EntityManager $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest()
    {
        if($user = $this->getUser()){
            $filter = $this->em->getFilters()->enable('warehouseFilter');
            $filter->setParameter('warehouseId', $user->getWarehouse()->getId());
        }
    }

    private function getUser()
    {
        $token = $this->tokenStorage->getToken();

        if(!$token){
            return null;
        }

        $user = $token->getUser();

        if(!($user instanceof \CoreBundle\Entity\User)){
            return null;
        }

        if($user->getWarehouseId() === 0){
            return null;
        }

        return $user;
    }
}
