<?php

namespace CoreBundle\Filters;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

class ClientFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if entity implements ClientAware interface
        if(!$targetEntity->reflClass->implementsInterface('CoreBundle\Entity\Interfaces\ClientAware')){
            return "";
        }

        // Hack around the behavior of Doctrine of applying the filter to left joined entities as well
        // Basically apply filter on the first mentioned entity only
        $position = intval(substr($targetTableAlias, 1, 1));
        if($position > 0){
            return "";
        }

        try{
            $clientId = $this->getParameter('clientId');
        } catch (\InvalidArgumentException $e) {
            // No clientId id has been defined
            return "";
        }

        if(empty($clientId)){
            return "";
        }

        return sprintf('%s.%s = %s', $targetTableAlias, 'client_id', $clientId);
    }
}
