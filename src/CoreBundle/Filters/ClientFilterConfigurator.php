<?php

namespace CoreBundle\Filters;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @link http://www.michaelperrin.fr/2014/12/05/doctrine-filters/
 */
class ClientFilterConfigurator
{
    private $em;
    private $tokenStorage;

    public function __construct(EntityManager $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelRequest()
    {
        if($clientId = $this->getClientId()){
            $filter = $this->em->getFilters()->enable('clientFilter');
            $filter->setParameter('clientId', $clientId);
        }
    }

    private function getClientId()
    {
        $token = $this->tokenStorage->getToken();

        if(!$token){
            return null;
        }

        $user = $token->getUser();
        if(!($user instanceof \CoreBundle\Entity\User) && 
           !($user instanceof \CoreBundle\Entity\Client)){
            return null;
        }

        if($user instanceof \CoreBundle\Entity\Client){
            return $user->getId();
        }

        if($user->getClientAccount() === null){
            return null;
        }

        return $user->getClientAccount()->getId();
    }
}
