<?php

namespace CoreBundle\Filters;

use Doctrine\ORM\Mapping\ClassMetaData;
use Doctrine\ORM\Query\Filter\SQLFilter;

/**
 * @link http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/filters.html
 */
class WarehouseFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, $targetTableAlias)
    {
        // Check if entity implements WarehouseAware interface
        if(!$targetEntity->reflClass->implementsInterface('CoreBundle\Entity\Interfaces\WarehouseAware')){
            return "";
        }

        try{
            $warehouseId = $this->getParameter('warehouseId');
        } catch (\InvalidArgumentException $e) {
            // No warehouse id has been defined
            return "";
        }

        if(empty($warehouseId)){
            return "";
        }

        return sprintf('%s.%s = %s OR %s.%s is null', $targetTableAlias, 'warehouse_id', $warehouseId, $targetTableAlias, 'warehouse_id');
    }
}
