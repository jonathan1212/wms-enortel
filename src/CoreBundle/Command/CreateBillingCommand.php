<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Console\Input\InputOption;

// wms:core:create-billing --clientId=13 --from=2018-04-21 --to=2018-04-30 --warehouseId=1 --env=prod
class CreateBillingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:create-billing')
            ->setDescription('test')
            ->addOption('clientId',null,InputOption::VALUE_OPTIONAL,'clientId',null)
            ->addOption('from',null,InputOption::VALUE_OPTIONAL,'from',null)
            ->addOption('to',null,InputOption::VALUE_OPTIONAL,'to',null)
            ->addOption('user',null,InputOption::VALUE_OPTIONAL,'to', 'Jonathan Antivo')
            ->addOption('warehouseId',null,InputOption::VALUE_OPTIONAL,'warehouseId',1)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
            $container = $this->getContainer();
            $this->container = $container;

            $request = new Request();

            $clientId = $input->getOption("clientId");
            $dateFrom = $input->getOption("from");
            $dateTo = $input->getOption("to");
            $warehouseId = $input->getOption('warehouseId');
            $user = $input->getOption('user');
            
            $request->query->set('client', $clientId);
            $request->query->set('start', $dateFrom);
            $request->query->set('end', $dateTo);
            
                
            $template = 'print';
            $billingParams = $this->container->getParameter('billing');

            $billing = $billingParams['default'];

            if (isset($billingParams['client_' . $clientId])) {
                 $billing = $billingParams['client_'.$clientId]; 
            }

            $price = 0;
           
            $collectionList = [
                ['column' => 'dateInbound', 'template' => 'insurance' ],
                ['column' => 'dateInbound', 'template' => 'storage' ],
                ['column' => 'dateInbound', 'template' => 'inbound' ],
                ['column' => 'datePick', 'template' => 'print' ],
                ['column' => 'datePack', 'template' => 'print' ],
            ];

            foreach ($collectionList as $v) {
                $column = $v['column'];
                $template = $v['template'];

                $price = $billing[strtoupper($column.'ed')]['value'];

                if ($template == 'storage') {
                    $price = $billing['STORAGEED']['value'];
                }

                if ($template == 'insurance') {
                    $price = $billing['INSURANCEED']['value'];
                }

                $this->container->get('branch_billing_export_service_excel')
                    ->process($request, $column, $template, $clientId, $price,$dateFrom,$dateTo,$user, $warehouseId );
            }

            
            $this->container->get('branch_billing_export_service_excel')->save(
                $clientId,$dateFrom, $dateTo, $user,$warehouseId
            );

                
    }
}
