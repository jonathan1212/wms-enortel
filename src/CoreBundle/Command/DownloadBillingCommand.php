<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Console\Input\InputOption;

// wms:core:download-billing --id=
class DownloadBillingCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:download-billing')
            ->setDescription('test')
            ->addOption('id',null,InputOption::VALUE_OPTIONAL,'id',null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
            $container = $this->getContainer();
            $this->container = $container;
            $em = $container->get('doctrine')->getEntityManager();

            $id = $input->getOption("id");
                        
            $billing = $em->getRepository('CoreBundle:BillingSummary')->find($id);

            $this->container->get('branch_billing_export_service_excel')
                ->exportToExcel($billing,true);                
    }
}


