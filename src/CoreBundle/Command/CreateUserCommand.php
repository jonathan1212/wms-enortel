<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use BranchBundle\Commands\User\CreateUserCommand as CreateUserMessage;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateUserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:create-user')
            ->setDescription('Creates new user')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $helper = $this->getHelper('question');

        $question = new Question('Email Address: ', '');
        $emailAddress = $helper->ask($input, $output, $question);

        $question = new Question('Password: ', '');
        $password = $helper->ask($input, $output, $question);

        $question = new Question('First Name: ', '');
        $firstName = $helper->ask($input, $output, $question);

        $question = new Question('Last Name: ', '');
        $lastName = $helper->ask($input, $output, $question);

        $question = new Question('Contact Number: ', '');
        $contactNumber = $helper->ask($input, $output, $question);

        $command = new CreateUserMessage(
            $firstName,
            $lastName,
            $emailAddress,
            $password,
            $contactNumber
        );

        $container->get('command_bus')->handle($command);
        $name = $firstName . " " . $lastName;
        $output->writeln("{$name} was successfully created");
    }
}
