<?php

namespace CoreBundle\Command;

use CoreBundle\Entity\WarehouseMember;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use BranchBundle\Commands\User\UpdateUserWarehouseMembershipCommand;

class AssignUserToWarehouseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:assign-new-user-to-warehouse')
            ->setDescription('Assigns a new user to a warehouse')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $em = $container->get('doctrine.orm.entity_manager');
        $helper = $this->getHelper('question');

        // query for user, throw exception if not found
        $question = new Question('User email address: ', '');
        $emailAddress = $helper->ask($input, $output, $question);
        $user = $em->getRepository('CoreBundle:User')->findOneByEmailAddress($emailAddress);
        if(!$user){
            throw new \Exception(
                sprintf('User with email "%s" does not exist.', $emailAddress)
            );
        }

        // query for warehouse, throw exception if not found
        $question = new Question('Warehouse name: ', '');
        $warehouseName = $helper->ask($input, $output, $question);
        $warehouse = $em->getRepository('CoreBundle:Warehouse')->findOneByName($warehouseName);
        if(!$warehouse){
            throw new \Exception(
                sprintf('Warehouse with name "%s" does not exist.', $warehouseName)
            );
        }

        // create new membership object
        $membership = new WarehouseMember();
        $membership->setWarehouse($warehouse)
            ->setUser($user)
            ->setRoles(['ROLE_USER']);

        // attach membership object to user
        $user->addWarehouseMembership($membership);

        // execute command
        $command = new UpdateUserWarehouseMembershipCommand(
            $user->getId(),
            new ArrayCollection()
        );

        try{
            $container->get('command_bus')->handle($command);
            $output->writeln("{$user->getFirstName()} was successfully added to {$warehouseName}");
        } catch(UniqueConstraintViolationException $e) {
            $output->writeln("{$user->getFirstName()} is already a member of {$warehouseName}");
        }
    }
}
