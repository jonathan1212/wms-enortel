<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand as CreateWarehouseMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class CreateWarehouseCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:create-warehouse')
            ->setDescription('Creates new warehouse')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $helper = $this->getHelper('question');

        $question = new Question('Warehouse Name: ', '');
        $warehouseName = $helper->ask($input, $output, $question);

        $question = new Question('Address: ', '');
        $address = $helper->ask($input, $output, $question);

        $question = new Question('Contact Number: ', '');
        $contactNumber = $helper->ask($input, $output, $question);

        $command = new CreateWarehouseMessage(
            $warehouseName,
            $address,
            $contactNumber
        );

        $container->get('command_bus')->handle($command);
        $output->writeln("{$warehouseName} was successfully created");
    }
}
