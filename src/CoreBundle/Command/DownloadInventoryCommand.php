<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;


class DownloadInventoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:download-invenotry')
            ->setDescription('Update status for  pick/pack date')
            ->addOption('clientId',null,InputOption::VALUE_OPTIONAL,'clientId',12)
            ->addOption('dateFrom',null,InputOption::VALUE_OPTIONAL,'dateFrom','2018-01-01')
            
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $client = $input->getOption("clientId");
        $start = $input->getOption("dateFrom");

        $inventoryService = $container->get('branch.service.inventory.inventory_service');
        
        ob_start();
        $docsPath = $container->get('kernel')->getRootDir() . '/../web/docs';
        $exportDate = "inventory". date('Y-m-dHis') . '.csv';

        $filename = $docsPath . '/'. $exportDate;
        $fp = fopen($filename, 'w');
        

        $result = $em
            ->getRepository('CoreBundle:Inventory')
            ->getIndexInventories(array('client' => $client, 'start' => $start))
            ->getArrayResult();

        fputcsv($fp, array(
            'CLIENT',
            'PICKLISTNO',
            'PICKLIST PARTICULAR',
            'CREATED BY',
            'REMARKS' ,
            'PICKLISTED BY',
            'DATE PICKLISTED',
            'CONSIGNEE',
            'ADDRESS',
            'TAG NO',
            'LMBM SKU',
            'SERIAL NO',
            'CLIENT SKU',
            'SUPPLIER SKU',
            'STORAGE LOCATION', 
            'MOBILE LOCATION',
            'ORDER NO',
            'PO NO',
            'COGP',
            'INVENTORY STATUS',
            'DELIVERY STATUS',
            'ATTACHMENT',
            'WAYBILL NUMBER',
            'DELIVERED REMARKS',
            'DATE DELIVERED',
            'DELIVERED TO', 
            'DATE INBOUNDED',
            'DATE PICKED',
            'DATE PACKED',
            'NAME',
            'BRANCH' ));

        foreach ($result as  $each) {
                
                $picklist = $inventoryService->getPicklist($each['picklistDetail']);
                
                $datePicked = $each['datePicked'] ? $each['datePicked']->format('Y-m-d H:i:s') : '';
                $datePacked = $each['datePacked'] ? $each['datePacked']->format('Y-m-d H:i:s') : '';
                
                fputcsv($fp, array( 
                    $each['client']['name'],
                    $picklist['picklistNumber'],
                    $picklist['picklistParticularNumber'],
                    $picklist['createdBy'],
                    $each['purchaseOrder']['remarks'],
                    $picklist['createdBy'],
                    $picklist['datePicklisted'],
                    $picklist['consignee'],
                    $picklist['address'],
                    $each['tag'],
                    $each['product']['sku'],
                    "{$each['serialNo']}",
                    $each['product']['clientSku'],
                    $each['product']['supplierSku'],
                    $each['storageLocation']['name'],
                    $each['mobileLocation']['name'],
                    $each['orderNumber'],
                    $each['purchaseOrder']['purchaseOrderNumber'],
                    $each['product']['costOfGoodsPurchased'],
                    $each['inventoryStatus']['name'],
                    $picklist['status'],
                    $picklist['attachment'],
                    $picklist['waybill_number'],
                    $picklist['remarks'],
                    $picklist['deliveredCreated'],
                    $picklist['deliveredto'],                       
                    $each['dateInbounded']->format('Y-m-d H:i:s'),
                    $datePicked,
                    $datePacked,
                    $each['product']['productDescription'],
                    $each['warehouse']['name']
                    )                    
                );
            }

            fclose($fp);
            

            echo "ok";
            /*$response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
            $response->headers->set('Content-Disposition', "attachment; filename='{$exportDate}'");
            $response->headers->set('Set-Cookie', 'fileDownload=true; path=/');*/
            
            /*return $response;
            ob_end_flush();    */

    }



}
