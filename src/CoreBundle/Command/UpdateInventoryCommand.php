<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand as CreateWarehouseMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;


class UpdateInventoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:update-inventory')
            ->setDescription('')
            ->addOption('tagId',null,InputOption::VALUE_OPTIONAL,'tagId',null)
            ->addOption('description',null,InputOption::VALUE_OPTIONAL,'description',null)
            ->addOption('lmbmsku',null,InputOption::VALUE_OPTIONAL,'lmbmsku',null)
            ->addOption('clientsku',null,InputOption::VALUE_OPTIONAL,'clientsku',null)
            ->addOption('serialno',null,InputOption::VALUE_OPTIONAL,'serialno',null)
            ->addOption('pono',null,InputOption::VALUE_OPTIONAL,'pono',null)

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $rs = array();
        $container = $this->getContainer();

        $tagId = $input->getOption('tagId');
        $description = $input->getOption('description');
        $lmbmsku = $input->getOption('lmbmsku');
        $clientsku = $input->getOption('clientsku');
        $serialno = $input->getOption('serialno');
        $pono = $input->getOption('pono');

        $em = $container->get('doctrine')->getManager();

        $result = $em
            ->getRepository('CoreBundle:Inventory')
            ->getIndexInventories()
            ->getArrayResult();

        foreach ($result as  $each) {
                
            $data = array( 
                    $each['tag'],
                    $each['product']['sku'],
                    $each['serialNo'],
                    $each['product']['clientSku'],
                    $each['product']['supplierSku'],
                    $each['storageLocation']['name'],
                    $each['mobileLocation']['name'],
                    $each['orderNumber'],
                    $each['purchaseOrder']['purchaseOrderNumber'],
                    $each['product']['costOfGoodsPurchased'],
                    $each['inventoryStatus']['name'],
                    $each['product']['productDescription'],
                    $each['warehouse']['name']
                                
                );
            
            $rs[$each['tag']] = $data;
        }


        if (array_key_exists($tagId, $rs) ) {

                $a = $rs[$tagId];


                if ( $a[1] != $lmbmsku) {
                    $newdata[] =  array($a[1] => $lmbmsku);

                }

                if ( $a[3] != $clientsku) {
                    $newdata[] =  array($a[3] => $clientsku);

                }

                if ( $a[11] != $description) {
                    $newdata[] =  array($a[11] => $description);

                }

                if ( $a[8] != $pono) {
                    $newdata[] =  array($a[8] => $pono);

                }

        }

        dump($newdata);


        /*$newdata = array();

        foreach ($this->listData() as $key => $each ) {

            if (array_key_exists($key, $rs) ) {

                $a = $rs[$key];


                if ( $a[1] != $each[1]) {
                    $newdata[] =  array($a[1] => $each[1]);

                }

            }
        }


        dump($newdata);*/

        
    }

}
