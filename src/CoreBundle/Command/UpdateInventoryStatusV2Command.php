<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand as CreateWarehouseMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;


class UpdateInventoryStatusV2Command extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:update-inventory-status-v2')
            ->setDescription('Update status for  pick/pack date')

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $conn = $em->getConnection();

        /*$sql = "
        select p.order_number , pd.inventory_id, p.picklist_status_id, p.picklist_number
        from picklist p
        left join picklist_detail pd on pd.picklist_id = p.id 
        WHERE  `picklist_status_id` = 4
        ";

        $picklist = $conn->fetchAll($sql);

        foreach ($picklist as $p) {
            $ordernumber = $p['order_number'];
            $inventory = $p['inventory_id'];
            
            if ($inventory) {
                $conn->exec("UPDATE inventory set order_number = '{$ordernumber}' , inventory_status_id = 7 where id = {$inventory} and inventory_status_id = 1 ");
                var_dump($p);    
            }
        }*/

        /*$sql = "
        select distinct i.id , pd.picklist_id , p.order_number
        from inventory i 
        left join picklist_detail pd on pd.inventory_id = i.id
        left join picklist p on pd.picklist_id = p.id 
        WHERE i.date_picked IS NULL and  i.date_packed is NULL 
        AND i.inventory_status_id = 1 AND p.picklist_status_id = 1 
        ";

        $inventory = $conn->fetchAll($sql);

        foreach ($inventory as $i) {

                $inventoryId = $i['id'];
                $ordernumber = $i['order_number'];

                $conn->exec("UPDATE inventory set order_number = '{$ordernumber}' , inventory_status_id = 2 where id = {$inventoryId} and inventory_status_id = 1 and order_number is null ");
                var_dump($i);    
        }*/

        $sql = "
        select distinct ih.purchase_order_id, i.inventory_status_id, ih.details, i.id as inventoryId
        from inventory i 
        left join inbound_history ih on i.purchase_order_id = ih.purchase_order_id
        WHERE i.date_picked IS NULL and  i.date_packed is NULL 
        AND i.inventory_status_id = 1
        ";

        $purchaseorder = $conn->fetchAll($sql);

        foreach ($purchaseorder as $i) {

                $x = array_values(json_decode($i['details'], true));
                
                $quarantine = $x[0]['forQuarantine'];
                $inventoryId = $i['inventoryId'];
                
                if ($quarantine == 'true') {

                    var_dump($inventoryId);
                    $conn->exec("UPDATE inventory set inventory_status_id = 4 where id = {$inventoryId} and inventory_status_id = 1 and order_number is null and date_picked is null and date_packed is null");
                }
                
        }

        exit;

    }
}
