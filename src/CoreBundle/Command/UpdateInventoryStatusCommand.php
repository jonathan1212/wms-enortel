<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand as CreateWarehouseMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;


class UpdateInventoryStatusCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:update-inventory-status')
            ->setDescription('Update status for  pick/pack date')
            ->addOption('idStart',null,InputOption::VALUE_OPTIONAL,'idStart',1)
            ->addOption('idEnd',null,InputOption::VALUE_OPTIONAL,'idEnd',1000)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $start = $input->getOption("idStart");
        $end = $input->getOption("idEnd");

        $result = $em->getRepository('CoreBundle:Inventory')->criteria(array(
                'idStart' => $start,
                'idEnd' => $end
            ))
            ->orderBy('DESC','id')
            ->query()
            ->getResult();

        foreach ($result as $inventoryOne) {
            
            $inventoryhistory = $em->getRepository('CoreBundle:InventoryHistory');

            $ih = $inventoryhistory->findBy(['objectId' => $inventoryOne]);

            foreach ($ih as $inventory) {
                
                $status =  isset($inventory->getData()['inventoryStatus']) ? $inventory->getData()['inventoryStatus']['id'] : -1;
                
                $dateLoggedAt = $inventory->getLoggedAt();

                dump($inventoryOne->getId());
                dump($status);

                // picked
                if ($status == 5) {
                    dump('hi');
                    $inventoryOne->setDatePicked($dateLoggedAt);
                }

                // packed
                if ($status == 6) {
                    dump('hello');
                    $inventoryOne->setDatePacked($dateLoggedAt);
                }
                
                $inventory = null;
            }

            $inventoryOne = null;
        }

        $em->flush();

        
        
        exit;
        
    }

}
