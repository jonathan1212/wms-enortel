<?php

namespace CoreBundle\Command;

use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use BranchBundle\Commands\Warehouse\CreateWarehouseCommand as CreateWarehouseMessage;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputOption;


class DeletePOPicklistCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('wms:core:delete-po-picklist')
            ->setDescription('delete PO PICKLIST')
            ->addOption('order_number',null,InputOption::VALUE_OPTIONAL,'order_number',null)
            ->addOption('pono',null,InputOption::VALUE_OPTIONAL,'pono',null)

        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        $container = $this->getContainer();
        $em = $container->get('doctrine')->getEntityManager();

        $this->conn = $em->getConnection();

        $ordernumber = $input->getOption("order_number");

        $sql = "
        select order_number, id
        from picklist
        where order_number = :order_number
        ";

        $picklist = $this->conn->fetchAll($sql, ['order_number' => $ordernumber]);
        
        
        foreach ($picklist as $p) {

            $this->deleteBilling($p);
            
            $picklistId = $p['id'];
            $or = $p['order_number'];
            
            $this->conn->exec("DELETE from picklist_detail where picklist_id = {$picklistId} ");
            $this->conn->exec("DELETE from picklist_particular where picklist_aggregate_id = {$picklistId} ");

            $this->conn->exec("UPDATE inventory set order_number = null , inventory_status_id = 1 where order_number = '{$or}'");
            $this->conn->exec("DELETE from picklist where id = $picklistId ");
        }

    }

    protected function deleteBilling($p)
    {
        $picklistId = $p['id'];

        $sql = " 
        select i.id 
        from picklist_detail pd 
        left join inventory i on pd.inventory_id = i.id
        where pd.picklist_id = {$picklistId}
        ";

        $inventory = $this->conn->fetchAll($sql);
        
        foreach ($inventory as $i) {
            $inventoryId = $p['id'];

            $this->conn->exec("DELETE from billing_detail_inbound where inventory_id = {$inventoryId} ");
            $this->conn->exec("DELETE from billing_detail_insurance where inventory_id = {$inventoryId} ");
            $this->conn->exec("DELETE from billing_detail_pack where inventory_id = {$inventoryId} ");
            $this->conn->exec("DELETE from billing_detail_pick where inventory_id = {$inventoryId} ");
            $this->conn->exec("DELETE from billing_detail_storage where inventory_id = {$inventoryId} ");

        }

    }


}
