<?php

namespace CoreBundle\Repository;

use CoreBundle\Entity\PicklistImportHistory;

/**
 * PicklistImportHistoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PicklistImportHistoryRepository extends \Doctrine\ORM\EntityRepository
{
    public function add(PicklistImportHistory $history)
    {
        $this->getEntityManager()->persist($history);
        return $history;
    }
}
