<?php

namespace CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * BaseCoreEntityRepository
 *
 * Implements various helper methods that can be used
 * on all Repositories inside CoreBundle
 */
class BaseCoreEntityRepository extends EntityRepository
{
    public function isFilterPresent($key, $filterArray)
    {
        return array_key_exists($key, $filterArray) && trim($filterArray[$key]);
    }
}
