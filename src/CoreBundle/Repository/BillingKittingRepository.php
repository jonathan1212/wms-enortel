<?php

namespace CoreBundle\Repository;

/**
 * BillingKittingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BillingKittingRepository extends \Doctrine\ORM\EntityRepository
{

	public function getIndexBilling()
    {
        return $this->_em
            ->createQueryBuilder()
            ->select('b')
            ->from('CoreBundle:BillingKitting', 'b')
            ->orderBy("b.id", 'DESC')
            ->getQuery();
    }

    public function findByDateFromAndDateTo($dateFrom, $dateTo)
    {
        return $this->_em
            ->createQueryBuilder()
            ->select('b')
            ->from('CoreBundle:BillingKitting', 'b')

            ->andWhere('b.dateAdded >= :dateAdded')->setParameter('dateAdded', $dateFrom)
            ->andWhere('b.dateTo <= :dateTo')->setParameter('dateTo', $dateTo . ' 23:23:23')
            ->getQuery()
            ->getOneOrNullResult();
    }

}
