<?php

namespace CoreBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;

/**
 * PicklistRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PicklistRepository extends QueryRepository
{

    public function criteria($params = array())
    {
        $this->create();

        $this
            ->leftJoin('this.client', 'c')
            ->addSelect('c')
            ->leftJoin('this.createdBy', 'u')
            ->addSelect('u')
            ->leftJoin('this.picklistStatus', 'pls')
            ->addSelect('pls');


        if (array_key_exists('picklistDeliveredStatus', $params)) {
            $this->leftJoin("CoreBundle:PicklistDelivered", "pd", Join::WITH, "this = pd.picklist")
                ->andWhere('pd.status = :pdStatus')
                ->setParameter('pdStatus', $params['picklistDeliveredStatus']);
        }
        
        if (array_key_exists('intransit', $params)) {
            $this->leftJoin("CoreBundle:PicklistDelivered", "pd", Join::WITH, " pd.picklist = this AND pd.status <> 2");
        }

        if (array_key_exists('status', $params)) {
            $this->andWhere('pls.value = :status')
                ->setParameter('status', $params['status']);

            //$this->andWhere('pd.status = 1');
        
        }

        /*dump($this->query()->getSQL());exit;*/

        return $this;

    }

    public function getPicklistByOrderNumber($orderNumbers)
    {

        $conn = $this->getEntityManager()->getConnection();        

        $sql = "SELECT p.*, sl.name as storage_name, ml.name as mobile_name, c.name as client
            FROM picklist p
            LEFT JOIN client c on p.client_id = c.id
            LEFT JOIN inventory i on p.order_number = i.order_number
            LEFT JOIN storage_location sl on i.storage_location_id = sl.id
            LEFT JOIN mobile_location ml on i.mobile_location_id = ml.id
            WHERE p.order_number IN  ('" . implode("', '", $orderNumbers) ."')
        ";

        $r = $conn->fetchAll($sql);
        
        $data = [];
        foreach ($r as $i => $v) {

            if (!isset($data[$v['order_number']])) {

                $data[$v['order_number']] = array(
                    'order_details' => json_decode($v['order_details'],true),
                    'client'        => $v['client'],
                    'order_number'  => $v['order_number'],
                    'storage'       => [],
                    'mobile'        => []
                );    
            }

            if ( !in_array($v['storage_name'], $data[$v['order_number']]['storage']) ) {
                    array_push($data[$v['order_number']]['storage'] , $v['storage_name']);
            }

            if ( !in_array($v['mobile_name'], $data[$v['order_number']]['mobile']) ) {
                    array_push($data[$v['order_number']]['mobile'] , $v['mobile_name']);
            }
            

        }

        return $data;

    }


    public function getIndexPicklistsIsComplete($isComplete = true)
    {
        return $this->_em
            ->createQueryBuilder()
            ->select('p')
            ->from('CoreBundle:Picklist', 'p')
            ->leftJoin('p.client', 'c')
            ->addSelect('c')
            ->leftJoin('p.createdBy', 'u')
            ->addSelect('u')
            ->leftJoin('p.picklistStatus', 'pls')
            ->addSelect('pls')
            ->where('p.isComplete = :isComplete')
            ->setParameter('isComplete', $isComplete)
            ->getQuery();
    }

    public function getPicklistsForPrinting($picklistNumbers)
    {
        return $this->_em
            ->createQueryBuilder()
            ->select('
                partial pl.{id, orderNumber, picklistNumber, orderDetails},
                partial client.{id, name},
                partial pld.{id},
                partial i.{id, tag},
                partial p.{id, sku, productDescription},
                partial sl.{id, name},
                partial ml.{id, name}
            ')
            ->from('CoreBundle:Picklist', 'pl')
            ->leftJoin('pl.client', 'client')
            ->leftJoin('pl.inventoryListing', 'pld')
            ->leftJoin('pld.inventory', 'i')
            ->leftJoin('i.product', 'p')
            ->leftJoin('i.storageLocation', 'sl')
            ->leftJoin('i.mobileLocation', 'ml')
            ->where('pl.picklistNumber in (:picklistNumbers)')
            ->setParameter('picklistNumbers', $picklistNumbers)
            ->getQuery()
            ->getArrayResult();
    }

    public function getPicklistsAsIterable($picklistNumbers)
    {
        return $this->_em
            ->createQueryBuilder()
            ->select('pl')
            ->from('CoreBundle:Picklist', 'pl')
            ->where('pl.picklistNumber IN (:picklistNumbers)')
            ->setParameter('picklistNumbers', $picklistNumbers)
            ->getQuery()
            ->iterate();
    }

    public function getIncompletePicklistsWithRequiredInventoryLeftAsArray()
    {
        $picklists = $this->_em
            ->createQueryBuilder()
            ->select('
                partial pl.{id, picklistNumber, orderDetails},
                partial pld.{id}, 
                partial i.{id}, 
                partial p.{id, clientSku}
            ')
            ->from('CoreBundle:Picklist', 'pl')
            ->leftJoin('pl.inventoryListing', 'pld')
            ->leftJoin('pld.inventory', 'i')
            ->leftJoin('i.product', 'p')
            ->where('pl.isComplete = :false')
            ->setParameter('false', false)
            ->getQuery()
            ->getArrayResult();

        $summarizedCount = [];
        foreach($picklists as $picklist){
            $picklistDetails = $picklist['inventoryListing'];
            if(!array_key_exists($picklist['picklistNumber'], $summarizedCount)){
                $summarizedCount[$picklist['picklistNumber']] = $picklist['orderDetails']['products'];
            }

            foreach($picklistDetails as $listing){
                $clientSku = $listing['inventory']['product']['clientSku'];
                $summarizedCount[$picklist['picklistNumber']][$clientSku]--;
            }
        }

        return $summarizedCount;
    }

    public function getInventoryTagsOfPicklistsAsArray($picklistNumbers)
    {
       $picklists =  $this->_em
            ->createQueryBuilder()
            ->select('
                partial pl.{id, picklistNumber},
                partial pld.{id}, 
                partial i.{id, tag}
            ')
            ->from('CoreBundle:Picklist', 'pl')
            ->leftJoin('pl.inventoryListing', 'pld')
            ->leftJoin('pld.inventory', 'i')
            ->where('pl.picklistNumber IN (:picklistNumbers)')
            ->setParameter('picklistNumbers', $picklistNumbers)
            ->getQuery()
            ->getArrayResult();

        $tags = [];
        foreach($picklists as $picklist){
            foreach($picklist['inventoryListing'] as $listing){
                $tags[] = $listing['inventory']['tag'];
            }
        }

        return $tags;
    }

    public function getPicklistWithSpecificInventoryTagByPicklistNumber($picklistNumber, $inventoryTag)
    {
        return $this->createQueryBuilder('pl')
            ->leftJoin('pl.inventoryListing', 'pld')
            ->addSelect('pld')
            ->leftJoin('pld.inventory', 'i')
            ->addSelect('i')
            ->where('pl.picklistNumber = :picklistNumber')
            ->setParameter('picklistNumber', $picklistNumber)
            ->andWhere('pl.isComplete = :true')
            ->setParameter('true', true)
            ->andWhere('i.tag = :inventoryTag')
            ->setParameter('inventoryTag', $inventoryTag)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getPicklistByPicklistNumberInventoryTagAndAcceptableStatusesState
    (
        $picklistNumber,
        $inventoryTag,
        $acceptablePicklistStatusState,
        $acceptableInventoryStatusState
    )
    {
        return $this->createQueryBuilder('pl')
            ->where('pl.picklistNumber = :picklistNumber')
            ->setParameter('picklistNumber', $picklistNumber)
            ->andWhere('pl.isComplete = :true')
            ->setParameter('true', true)
            ->leftJoin('pl.picklistStatus', 'pls')
            ->andWhere('pls.value IN (:acceptablePicklistStatusState)')
            ->setParameter('acceptablePicklistStatusState', $acceptablePicklistStatusState)
            ->leftJoin('pl.inventoryListing', 'pld')
            ->addSelect('pld')
            ->leftJoin('pld.inventory', 'i')
            ->addSelect('i')
            ->andWhere('i.tag = :inventoryTag')
            ->setParameter('inventoryTag', $inventoryTag)
            ->leftJoin('i.inventoryStatus', 'inventoryStatus')
            ->andWhere('inventoryStatus.value IN (:acceptableInventoryStatusState)')
            ->setParameter('acceptableInventoryStatusState', $acceptableInventoryStatusState)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPicklistByPicklistNumberAndAcceptableStatusesState
    (
        $picklistNumber,
        $acceptablePicklistStatusState
    )
    {
        return $this->createQueryBuilder('pl')
            ->where('pl.picklistNumber = :picklistNumber')
            ->setParameter('picklistNumber', $picklistNumber)
            ->leftJoin('pl.picklistStatus', 'pls')
            ->andWhere('pls.value IN (:acceptablePicklistStatusState)')
            ->setParameter('acceptablePicklistStatusState', $acceptablePicklistStatusState)
            ->andWhere('pl.isComplete = :true')
            ->setParameter('true', true)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getPicklistDetailsByPicklistNumberAndAcceptableStatusesState
    (
        $picklistNumber,
        $acceptablePicklistStatusState
    )
    {
        return $this->createQueryBuilder('pl')
            ->where('pl.picklistNumber = :picklistNumber')
            ->setParameter('picklistNumber', $picklistNumber)
            ->andWhere('pl.isComplete = :true')
            ->setParameter('true', true)
            ->leftJoin('pl.picklistStatus', 'pls')
            ->addSelect('pls')
            ->andWhere('pls.value IN (:acceptablePicklistStatusState)')
            ->setParameter('acceptablePicklistStatusState', $acceptablePicklistStatusState)
            ->leftJoin('pl.inventoryListing', 'pld')
            ->addSelect('pld')
            ->leftJoin('pld.inventory', 'i')
            ->addSelect('i')
            ->leftJoin('i.inventoryStatus', 'status')
            ->addSelect('status')
            ->leftJoin('i.product', 'p')
            ->addSelect('p')
            ->leftJoin('i.storageLocation', 'sl')
            ->addSelect('sl')
            ->leftJoin('i.mobileLocation', 'ml')
            ->addSelect('ml')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findPicklistByInventoryStatusesState
    (
        $picklistNumber,
        $acceptableInventoryStatusState
    )
    {
        return $this->createQueryBuilder('pl')
            ->where('pl.picklistNumber = :picklistNumber')
            ->setParameter('picklistNumber', $picklistNumber)
            ->leftJoin('pl.inventoryListing', 'pld')
            ->leftJoin('pld.inventory', 'i')
            ->leftJoin('i.inventoryStatus', 'status')
            ->andWhere('status.value IN (:acceptableInventoryStatusState)')
            ->setParameter('acceptableInventoryStatusState', $acceptableInventoryStatusState)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function filterValidOrderNumbersAsArray($orderNumbers, $clientId)
    {
        $picklists = $this->_em
            ->createQueryBuilder()
            ->select('
                partial pl.{id, orderNumber}
            ')
            ->from('CoreBundle:Picklist', 'pl')
            ->where('pl.orderNumber IN (:orderNumbers)')
            ->setParameter('orderNumbers', $orderNumbers)
            ->leftJoin('pl.client', 'client')
            ->andWhere('client.id = :clientId')
            ->setParameter('clientId', $clientId)
            ->getQuery()
            ->getArrayResult();

        $retrievedOrderNumbers = array_map(function($picklist){
            return $picklist['orderNumber'];
        }, $picklists);

        return array_intersect($orderNumbers, $retrievedOrderNumbers);
    }
}
