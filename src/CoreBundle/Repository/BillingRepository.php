<?php

namespace CoreBundle\Repository;

/**
 * BillingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class BillingRepository extends \Doctrine\ORM\EntityRepository
{

    public function getIndexBilling()
    {
        return $this->_em
            ->createQueryBuilder()
            ->select('b')
            ->from('CoreBundle:Billing', 'b')
            ->orderBy("b.id", 'DESC')
            ->getQuery();
    }
}
