<?php

namespace CoreBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Tools\Pagination\Paginator;

class QueryRepository extends EntityRepository
{
    protected $qb;

    protected $alias;

    public function create($alias='this')
    {
        $this->alias = $alias;
        $this->qb = $this->createQueryBuilder($alias);
        return $this;
    }

    public function distinct()
    {
        $this->qb->distinct();

        return $this;
    }

    public function query()
    {
        return $this->qb->getQuery();
    }

    public function result()
    {
        return $this->query()->getResult(Query::HYDRATE_OBJECT);
    }

    public function setLimit($limit)
    {
        $this->qb->setMaxResults($limit);

        return $this;
    }

    public function paginate($page, $hydrationMode = Query::HYDRATE_OBJECT ,$fetchJoinCollection = true)
    {

        $limit = $this->qb->getMaxResults();
        $offset = ($page - 1 > 0) ? $page - 1 : 0;

        $this->qb->setFirstResult($offset * $limit);

        return new Paginator($this->qb->getQuery()->setHydrationMode($hydrationMode), $fetchJoinCollection);
    }

    public function orderBy($order='ASC', $field='id')
    {
        $this->qb->orderBy("{$this->alias}.$field", $order);

        return $this;
    }

    public function groupBy($field)
    {
        $this->qb->groupBy("{$this->alias}.$field");

        return $this;
    }

    public function selectField($selectField)
    {
        $this->qb->select("IDENTITY({$this->alias}.$selectField) as $selectField");

        return $this;
    }

    public function addSelectField($selectField)
    {
        $this->qb->addSelect("IDENTITY({$this->alias}.$selectField) as $selectField");

        return $this;
    }

    public function dumpSQL()
    {
        $sql = $this->query()->getSQL();
        dump($sql);

        $parameters = $this->query()->getParameters()->toArray();
        foreach ($parameters as $parameter) {
            dump($parameter->getName());
            dump($parameter->getValue());
        }
        
        exit;
    }

    public function getCount()
    {
        
        $count = $this->qb->select("count(distinct {$this->alias})")
              ->setMaxResults(null)
              ->setFirstResult(null)
              ->getQuery()
              ->getSingleScalarResult();
        

        return $count;
    }

    public function __call($name, $arguments)
    {
        if (method_exists($this->qb, $name)) {
            call_user_func_array(array($this->qb, $name), $arguments);
        }
        else {
            return parent::__call($name, $arguments);
        }

        return $this;
    }
}