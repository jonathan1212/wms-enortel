<?php

namespace CoreBundle\DoctrineListener;

use CoreBundle\Entity\Product;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;


class ProductListener
{
    protected $service;
    protected $authenticatedUser;

    public function __construct($service)
    {
        $this->service = $service;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $entityManager = $args->getEntityManager();

        
        if ($entity instanceof Product) {  

            $user = $this->service->get('security.token_storage')->getToken()->getUser();
            
            if (method_exists($user,'getWarehouse')) {
                $entity->setWarehouse($user->getWarehouse());
                $entityManager->flush();
            }
            
        }

    }
}
