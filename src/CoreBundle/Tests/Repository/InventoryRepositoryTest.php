<?php

namespace CoreBundle\Tests\Repository;

use Liip\FunctionalTestBundle\Test\WebTestCase;

class InventoryRepositoryTest extends WebTestCase
{
    private $container;

    public function setUp()
    {
        $this->container = $this->getContainer();
        $this->loadFixtureFiles(array(
            '@CoreBundle/DataFixtures/TestDrivers/inventory.yml',
        ));
    }

    /** @test */
    public function should_retrieve_correct_occupied_mobile_locations()
    {
        $inventoryRepository = $this->container->get('doctrine.orm.entity_manager')
            ->getRepository('CoreBundle:Inventory');

        $occupiedMobileLocations = $inventoryRepository->getOccupiedMobileLocations();
        $this->assertEquals([
                'mobile_location_1'
            ], 
            $occupiedMobileLocations
        );
    }
}
